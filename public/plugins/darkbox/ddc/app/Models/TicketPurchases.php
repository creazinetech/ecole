<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class TicketPurchases extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ticket_purchases';
    protected $guarded = ['id'];
    protected $hidden = ['created_at','updated_at'];

    function ticket()
    {
        return $this->BelongsTo('App\Models\Tickets','id','ticket_type');
    }

    function agent()
    {
        return $this->BelongsTo('App\User','referral_code','agent_code');
    }

    function customer()
    {
        return $this->BelongsTo('App\User','id','customer_id');
    }

    function promoCode()
    {
        return $this->BelongsTo('App\Models\PromoCode','id','promo_code');
    }
}
