<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $table = 'user_profile';

    public function User()
    {
        return $this->BelongsTo('App\User','id','user_id');
    }
}
