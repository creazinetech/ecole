<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CreditTransactions extends Model
{
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $table = 'credit_transactions';
    protected $hidden = ['created_at','updated_at'];

    public function agent()
    {
        return $this->BelongsTo('App\User','id','agent_id');
    }

    public function customer()
    {
        return $this->BelongsTo('App\User','id','customer_id');
    }
}
