<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Tickets extends Model
{
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $table = 'tickets';
    protected $hidden = ['created_at','updated_at'];
    public function User()
    {
        return $this->BelongsTo('App\User','id','user_id');
    }

    public function credits()
    {
        return $this->belongsTo('App\Models\CreditTypes','credit_id','id');
    }
    public function CreditName()
    {
        return ($this->credits) ? $this->credits->name : '';
    }

}
