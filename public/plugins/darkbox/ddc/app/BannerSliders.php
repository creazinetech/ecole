<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerSliders extends Model
{
    protected $table="banner_sliders";
    protected $guarded=['id'];
}
