<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tickets;
use App\Http\Repositories\TicketRepository;

class TicketController extends Controller
{
    public $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function getListings(Request $request)
    {
        $tickets = $this->ticketRepository->getListings($request->all());
        return response()->json([
                            'tickets' => $tickets
                        ]);
    }

    public function purchaseTicket(Request $request)
    {
        $tickets = $this->ticketRepository->purchaseTicket($request);

        if(isset($tickets['error'])){
            return response()->json([
                            'error' => $tickets['error']
                        ],401);
        } elseif(isset($tickets['success'])){
            return response()->json([
                'data'      => $tickets['success'],
                'message'   => 'ticket purchased successfully'
            ],200);
        }
    }
}
