<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\BannerSliders;

class BannerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function getDetails()
    {
        $banners = BannerSliders::get();
        return response()->json([
                            'banners' => $banners
                        ]);
    }
}
