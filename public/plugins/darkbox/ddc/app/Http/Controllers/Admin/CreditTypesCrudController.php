<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Http\Request;


class CreditTypesCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\CreditTypes');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/credit-types');
        $this->crud->setEntityNameStrings('Credit Type', 'Credit Types');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addFields([
                [ // select_from_array
                    'name' => 'status',
                    'label' => "Status",
                    'type' => 'select_from_array',
                    'options' => ["A" => "Active", "I" => "InActive"],
                    'allows_null' => false
                ]
            ], 'update/create/both');
        $this->crud->removeColumns(['type','status']); // remove an array of columns from the stack

        $this->crud->addColumns([
            [
               'label' => "Status", // Table column heading
               'type' => "radio",
               'name' => 'status',
               'options' => ["A" => "Active", "I" => "InActive"]
            ]
        ]);
    }

    public function store(Request $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(Request $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
