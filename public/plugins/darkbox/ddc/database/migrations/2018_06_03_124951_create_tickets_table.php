<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('winning_price');
            $table->date('start_date');
            $table->date('end_date');
            $table->date('result_date');
            $table->integer('credit_id')->unsigned();
            $table->text('description')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });


       Schema::table('tickets', function($table) {
           $table->foreign('credit_id')->references('id')->on('credit_types');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
