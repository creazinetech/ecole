<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->text('profile_pic')->nullable();
            $table->date('dob')->nullable();
            $table->string('gender')->nullable();
            $table->text('city')->nullable();
            $table->text('state')->nullable();
            $table->text('area')->nullable();
            $table->text('address')->nullable();
            $table->timestamps();
        });


       Schema::table('user_profile', function($table) {
           $table->foreign('user_id')->references('id')->on('users');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
    }
}
