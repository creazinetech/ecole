<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => "Admin"], function()
{
  // Backpack\CRUD: Define the resources for the entities you want to CRUD.
    CRUD::resource('users', 'UserCrudController');
    CRUD::resource('promocode', 'PromoCodeCrudController');
    CRUD::resource('credit-types', 'CreditTypesCrudController');
    CRUD::resource('tickets', 'TicketsCrudController');

    Route::get('/agent-credits/{agentID}','UserCrudController@listAgentCredits')->name('list_agent_credits');
    Route::get('/agent-credits/{agentID}/add','UserCrudController@addAgentCredits')->name('add_agent_credits');
});