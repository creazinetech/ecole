<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\PassportController@login');
Route::post('register', 'Api\PassportController@register');
Route::post('otp-varification', 'Api\PassportController@otpVarification');
Route::get('banners','Api\BannerController@getDetails');
Route::post('tickets','Api\TicketController@getListings');
Route::post('purchase-ticket','Api\TicketController@purchaseTicket');

Route::group(['middleware' => 'auth:api'], function(){
	Route::get('get-details', 'Api\UserController@getDetails');
	Route::post('update-profile', 'Api\UserController@updateProfile');
	Route::get('get-enrolled-tickets', 'Api\UserController@getEnrolledTickets');
	// Route::post('get-details', 'API\PassportController@getDetails');
});
