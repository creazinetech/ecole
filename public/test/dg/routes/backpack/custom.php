<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

  // Backpack\CRUD: Define the resources for the entities you want to CRUD.
    CRUD::resource('users', 'UserCrudController');
    CRUD::resource('promocode', 'PromoCodeCrudController');
    CRUD::resource('credit-types', 'CreditTypesCrudController');
    CRUD::resource('tickets', 'TicketsCrudController');
    CRUD::resource('banner-sliders', 'BannerSlidersCrudController');

    // CRUD::resource('/agent-credits/{id}', 'AgentCreditCrudController');
    Route::post('agent-credits/{id}/search', ['as' => 'crud.agent-credits.search', 'uses' => 'AgentCreditCrudController@search']);
    Route::get('/agent-credits/{id}','AgentCreditCrudController@index')->name('list_agent_credits');
    // Route::get('/agent-credits/{id}','AgentCreditCrudController@index')->name('add_agent_credits');
    Route::get('/agent-credits/{id}/create','AgentCreditCrudController@addAgentCredits')->name('add_agent_credits');
    Route::post('/agent-credits/{id}','AgentCreditCrudController@store')->name('update_agent_credits');

    Route::post('ticket/{id}/purchases/search', ['as' => 'crud.ticket.search', 'uses' => 'TicketPurchasesCrudController@search']);
    Route::get('/ticket/{id}/purchases','TicketPurchasesCrudController@index')->name('list_ticket_purchases');
    Route::get('/ticket/{id}/result','TicketPurchasesCrudController@declareResult')->name('declare_result');
}); // this should be the absolute last line of this file
