<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\PassportController@login');
Route::post('register', 'Api\PassportController@register');
Route::post('otp-varification', 'Api\PassportController@otpVarification');
Route::get('banners','Api\BannerController@getDetails');
Route::get('credit-types','Api\CreditTypesController@getDetails');
Route::post('forgot-password','Api\PassportController@sendPasswordRequest');
Route::post('reset-password','Api\PassportController@updatePassword');

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('tickets','Api\TicketController@getListings');
	Route::get('get-details', 'Api\UserController@getDetails');
	Route::post('update-profile', 'Api\UserController@updateProfile');
	Route::get('get-enrolled-tickets', 'Api\UserController@getEnrolledTickets');
	Route::get('get-user/{userID}', 'Api\UserController@getUserByID');
	Route::post('purchase-ticket','Api\TicketController@purchaseTicket');
	Route::post('get-ticket-by-status','Api\TicketController@getTickets');
	Route::post('enroll-ticket-by-agent','Api\TicketController@enrollTicket');
	Route::get('get-past-winning','Api\TicketController@getPastWinners');
	Route::get('get-current-credits','Api\UserController@getCredits');
	Route::post('change-password','Api\PassportController@changePassword');
	// Route::post('get-details', 'API\PassportController@getDetails');
});
