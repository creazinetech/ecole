<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // protected $appends = ['']
    /*public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'email'
            ]
        ];
    }*/

    public function UserProfile()
    {
        return $this->hasOne('App\Models\UserProfile','user_id','id');
    }

    public function ticketsPurchased()
    {
        return $this->hasMany('App\Models\TicketPurchases','customer_id','id');
    }

    public function agentCreditTransactions()
    {
        return $this->hasMany('App\Models\CreditTransactions','agent_id','id');
    }

    public function agentCreditPurchased()
    {
        return $this->hasMany('App\Models\CreditTransactions','agent_id','id')->where('transaction_type','welcome')->orWhere('transaction_type','credit_purchased');
    }

    public function openGoogle($crud = false)
    {
        return ($this->user_type == 'a') ? '<a class="btn btn-xs btn-default" target="_blank" href="'.url('admin/agent-credits/'.$this->id).'" data-toggle="tooltip" title="Manage credits of '.ucwords($this->name).'."><i class="fa fa-search"></i>View Credits</a>' : '';
    }
}
