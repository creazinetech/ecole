<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class TicketPurchases extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ticket_purchases';
    protected $guarded = ['id'];
    protected $hidden = ['created_at','updated_at'];
    protected $appends = ['ticket_status'];


    function ticket()
    {
        return $this->BelongsTo('App\Models\Tickets','ticket_type','id')->with('credits');
    }

    function agent()
    {
        return $this->BelongsTo('App\User','agent_code','referral_code');
    }

    function customer()
    {
        return $this->BelongsTo('App\User','customer_id','id');
    }

    function promoCode()
    {
        return $this->BelongsTo('App\Models\PromoCode','id','promo_code');
    }

    public function customerName()
    {
        return ($this->customer) ? $this->customer->name : "-";
    }

    public function agentName()
    {
        return ($this->agent) ? $this->agent->name : "-";
    }

    public function openGoogle($crud = false)
    {

        $k = ($this->ticket->end_date >= date('Y-m-d H:i:s') && ($this->ticket->winner_declare ==0)) ? '<a class="btn btn-xs btn-default"  onClick="return confirm(\'Are you sure want to declare '.ucwords($this->customerName()).' as winner\')" href="'.url('admin/ticket/'.$this->id.'/result/').'" data-toggle="tooltip" title="Declare '.ucwords($this->customerName()).' as winner"><i class="fa fa-trophy"></i> Declare as Winner</a>' : '';
        if($this->is_winner == 1)
            $k .= '<strong>Winner<strong>';
        return $k;
    }

    public function getTicketStatusAttribute()
    {
        if(date('Y-m-d') > $this->ticket->result_date){
            return 'expired';
        } else if($this->status == 'r'){
            return 'rejected';
        } else if($this->status == 'i'){
            return 'pending';
        } else {
            return 'active';
        }
    }
}
