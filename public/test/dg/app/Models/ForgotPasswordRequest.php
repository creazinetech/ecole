<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForgotPasswordRequest extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $table = 'forgot_password_request';

}
