<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CreditTransactions extends Model
{
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $table = 'credit_transactions';
    protected $hidden = ['created_at','updated_at'];

    public function agent()
    {
        return $this->BelongsTo('App\User','id','agent_id');
    }

    public function customer()
    {
        return $this->BelongsTo('App\User','customer_id','id')->where('user_type','c');
    }

    public function customerName()
    {
        return ($this->customer) ? $this->customer->name : "-";
    }/*

    public function openGoogle($crud = false)
    {
        return ($this->user_type == 'a') ? '<a class="btn btn-xs btn-default" href="javascript:void()"  data-toggle="tooltip" title="Add Credit to agent"><i class="fa fa-search"></i>View Credits   dfdf</a>' : '';
    }*/
}
