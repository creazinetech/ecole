<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CreditTypes extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'credit_types';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $hidden = ['created_at','updated_at'];

    // protected $fillable = ['promo_code', 'type', 'amount', 'status'];
    // protected $hidden = [];
    // protected $dates = [];
}
