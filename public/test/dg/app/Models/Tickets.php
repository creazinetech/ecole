<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Tickets extends Model
{
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $table = 'tickets';
    protected $hidden = ['created_at','updated_at'];

    public function User()
    {
        return $this->BelongsTo('App\User','user_id','id');
    }

    public function credits()
    {
        return $this->belongsTo('App\Models\CreditTypes','credit_id','id');
    }
    public function CreditName()
    {
        return ($this->credits) ? $this->credits->name : '';
    }

    public function TicketPurchases()
    {
        return $this->hasMany('App\Models\TicketPurchases','ticket_type','id');//->with('credits');
    }

    public function winner()
    {
        return $this->TicketPurchases()->where('is_winner',1)->with(['agent','customer', 'ticket']);
    }

    public function openGoogle($crud = false)
    {
        return '<a class="btn btn-xs btn-default" target="_blank" href="'.url('admin/ticket/'.$this->id.'/purchases').'" data-toggle="tooltip" title="View tickets purchases"><i class="fa fa-users"></i>View Purchased</a>';
    }
}
