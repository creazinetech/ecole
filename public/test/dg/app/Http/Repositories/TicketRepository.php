<?php

namespace App\Http\Repositories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Models\Tickets;
use App\Models\TicketPurchases;
use App\User;

class TicketRepository extends Controller
{
    public function __construct()
    {
    }

    public function getListings($request, $user)
    {
        $startDate = $request['start_date'];
        $endDate   = $request['end_date'];
        $credit    = (int)$request['credit_type'];
        $tickets   = Tickets::where('status','A')->where('winner_declare' ,'!=',1);

        if($startDate == null) $startDate = date('Y-m-d');

        /*if($startDate != null){
            $tickets->where('start_date', '>=', $startDate);
        }*/
        if($endDate != null){
            $tickets->where('end_date', '>=', $endDate);
        }


        if($credit != 0){
            $tickets->where('credit_id', $credit);
        }

        $ticketPurchased = TicketPurchases::where('customer_id', $user->id)->pluck('ticket_type')->toArray();
        $tickets->whereNotIn('id',$ticketPurchased);
        return $tickets->with('credits')->get();
    }

    public function purchaseTicket($request, $userID)
    {
        $validator = Validator::make($request->all(), [
            'ticket_id'     => 'required',
            'agent_code'    => 'required',
        ]);

        if ($validator->fails()) {
            return ['error'=>$validator->errors()];
        }

        $agent = User::where(['referral_code' => $request->agent_code, 'status' => 'A'])->first();
        if(!$agent){
            $message = array('message' => ['invalid agent code']);
            return ['error'=> $message];
        }

        $tickets = Tickets::where('id',$request->ticket_id)->where('status','A')->first();
        if(!$tickets){
            $message = array('message' => ['invalid ticket']);
            return ['error'=> $message];
        }

        $checkExist = TicketPurchases::where([
            'ticket_type' => $request->ticket_id,
            'customer_id' => $userID,
            'agent_code'  => $request->agent_code,
        ])->count();

        if($checkExist !=0){
            $message = array('message' => ['You have already purchased this ticket']);
            return ['error'=> $message];
        }

        $purchaseData = array(
            'ticket_type' => $request->ticket_id,
            'customer_id' => $userID,
            'agent_code'  => $request->agent_code,
            'status'      => 'i'
        );
        return ['success' => TicketPurchases::create($purchaseData)];
    }

    public function getTickets($request, $user)
    {
        $status = ($request->status == 'enrolled') ? 'a' : 'i';
        $date   = ($request->start_date == "") ? date('Y-m-d H:i:s') : $request->start_date;

        if($user->user_type == 'a'){
            $data = TicketPurchases::with(['customer','ticket'])->where([
                'status' => $status,
                'agent_code' => $user->referral_code
            ])->where('created_at', '>=', $date)->get();
            return ['success' => $data];
        } else{

            $data = TicketPurchases::with(['agent','ticket'])->where([
                'status' => $status,
                'customer_id' => $user->id
            ])->where('created_at', '>=', $date)->get();

            return ['success' => $data];
        }
    }

    public function enrollTicket($request, $user)
    {

        $data = TicketPurchases::with('ticket.credits')->where([
            'id' => $request->transaction_id,
            'status' => 'i'
        ])->get();
        if($data->count() ==0){
            return ['success' => 'Transaction not found for this ID'];
        }
        if(!$request->status){
            return ['success' => 'Transaction not found for this ID'];
        }
        $data = $data->first();

        if($request->status != 'accept'){
            $data->status = 'r';
            $data->save();
            return ['success' => 'Transaction has been rejected', 'data' => $data];
        }
        $credit = $data->ticket->credit_amount;
        /*if($credit > $user->credits)
            return ['error' => 'insufficient Amount'];*/

        $data->status = 'a';
        $data->enrolled_number = date('Ymd').rand(1,15625).$user->id;
        $data->save();

        $array = array(
                    'transaction_id'     => date('Ymd').rand(1,15625).$user->id,
                    'ticket_purchase_id' => $data->id,
                    'agent_id'           => $user->id,
                    'customer_id'        => $data->customer_id,
                    'transaction_type'   => 'ticket_purchase',
                    'credits'            => $credit
                );
                /*$user->credits =  $user->credits - $credit;
                $user->save();*/
                $user->agentCreditTransactions()->create($array);


        return ['success' => $data, 'status' => 'ticket enrolled successfully'];
    }
}
