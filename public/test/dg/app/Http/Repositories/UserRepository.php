<?php

namespace App\Http\Repositories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Models\Tickets;
use App\Models\TicketPurchases;
use App\User;

class UserRepository extends Controller
{
    public function __construct()
    {
    }

    public function addWelcomeCreditToAgent($agent)
    {
        if($agent->user_type == 'a' && $agent->status == 'A'){
            $transCount = $agent->agentCreditTransactions->count();
            if($transCount ==0){
                $array = array(
                    'transaction_id' => date('Ymd').rand(1,15625).$agent->id,
                    'agent_id'      =>  $agent->id,
                    'customer_id'   => 0,
                    'transaction_type' => 'welcome',
                    'credits' => '150'
                );
                $agent->referral_code = $agent->id.rand(100,999);
                $agent->credits = '150';
                $agent->save();
                $agent->agentCreditTransactions()->create($array);
            }
        }
    }

    function getAgentCredits($user){
        if($user->user_type == 'a'){
            $data['current'] = $user->credits;
            $data['total_purchased'] = $user->agentCreditPurchased->sum('credits');
            $data['used_credits'] = $data['total_purchased'] - $data['current'];
            return $data;
        }
        return [ 'status' => 'unauthorised'];
    }

    /*public function getListings($request)
    {
        $startDate = $request['start_date'];
        $endDate   = $request['end_date'];
        $credit    = (int)$request['credit_type'];
        $tickets   = Tickets::where('status','a');

        if($startDate == null) $startDate = date('Y-m-d');

        if($startDate != null){
            $tickets->where('start_date', '>=', $startDate);
        }

        if($endDate != null){
            $tickets->where('end_date', '>=', $endDate);
        }

        if($credit != 0){
            $tickets->where('credit_id', $credit);
        }

        return $tickets->with('credits')->get();
    }

    public function purchaseTicket($request)
    {

        $validator = Validator::make($request->all(), [
            'ticket_id'     => 'required',
            'agent_code'    => 'required',
        ]);

        if ($validator->fails()) {
            return ['error'=>$validator->errors()];
        }

        $agent = User::where(['referral_code' => $request->agent_code, 'status' => 'A'])->first();
        if(!$agent){
            $message = array('message' => ['invalid agent code']);
            return ['error'=> $message];
        }

        $tickets = Tickets::where('id',$request->ticket_id)->where('status','A')->first();
        if(!$tickets){
            $message = array('message' => ['invalid ticket']);
            return ['error'=> $message];
        }

        if($request->promo_code){
            $promoCode = PromoCode::where('id',$request->promo_code)->first();
            if(!$promoCode){
                $message = array('message' => ['invalid promo code']);
                return ['error'=> $message];
            }
        }

        $purchaseData = array(
            'ticket_type' => $request->ticket_id,
            'customer_id' => '1',
            'agent_code'  => $request->agent_code,
            'promo_code'  => isset($request->promo_code) ?$request->promo_code: '',
            'status'      => 'i'
        );
        return ['success' => TicketPurchases::create($purchaseData)];
    }*/
}
