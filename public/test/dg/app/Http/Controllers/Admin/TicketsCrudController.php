<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Http\Request;
use App\Http\Requests\TicketsCrudRequest;


class TicketsCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Tickets');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/tickets');
        $this->crud->setEntityNameStrings('Tickets', 'Tickets');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();

        $this->crud->addColumns([
            [
                'name' => 'winning_price', 'label' => "Winning Price"
            ],
            [
                'name' => 'start_date', 'label' => "Start Date", 'type' => 'date'
            ],
            [
                'name' => 'end_date', 'label' => "End Date", 'type' => 'date'
            ],
            [
                'name' => 'result_date', 'label' => "Result Date", 'type' => 'date'
            ],
            [
                'name' => 'credit_id', 'label' => "Credit Type", 'type' => 'model_function',
                'function_name' => 'CreditName',
                'attribute' => 'name'
            ],
            [
                'name' => 'description', 'label' => "Description"
            ],
        ]);

        $this->crud->addFields([
                [
                    'name' => 'winning_price',
                    'label'=> 'Winning price',
                    'type' => 'text'
                ],
                [
                    'name' => 'description',
                    'label'=> 'Ticket Description',
                    'type' => 'textarea'
                ],
                [
                    'name' => 'start_date',
                    'label'=> 'Start Date',
                    'type' => 'date'
                ],
                [
                    'name' => 'end_date',
                    'type' => 'date',
                    'label' => 'End Date'
                ],
                [
                    'name' => 'result_date',
                    'type' => 'date',
                    'label' => 'Result Date'
                ],
                [   // Select
                    'label' => "Credit Type",
                    'type' => 'select',
                    'name' => 'credit_id', // the db column for the foreign key
                    'entity' => 'credits', // the method that defines the relationship in your Model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'model' => "App\Models\CreditTypes" // foreign key model
                ],
                [
                    'name' => 'credit_amount',
                    'type' => 'number',
                    'label' => 'Credit Amount',
                    'attributes' => ["step" => "1"]
                ],
                [ // select_from_array
                    'name' => 'status',
                    'label' => "Status",
                    'type' => 'select_from_array',
                    'options' => ["A" => "Active", "I" => "InActive"],
                    'allows_null' => false
                ]
            ], 'update/create/both');

        $this->crud->removeColumns(['type','status']); // remove an array of columns from the stack

        $this->crud->addColumns([
            [
               'label' => "Status", // Table column heading
               'type' => "radio",
               'name' => 'status',
               'options' => ["A" => "Active", "I" => "InActive"]
            ]
        ]);
        $this->crud->addButtonFromModelFunction('line', 'open_google', 'openGoogle', 'end');
    }

    public function store(TicketsCrudRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(Request $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
