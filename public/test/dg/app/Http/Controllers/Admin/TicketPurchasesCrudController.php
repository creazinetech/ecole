<?php
namespace App\Http\Controllers\Admin;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Http\Request as StoreRequest;
use Illuminate\Http\Request as UpdateRequest;
use App\User;
use App\Http\Repositories\UserRepository;
use App\Models\TicketPurchases;


class TicketPurchasesCrudController extends CrudController {
    public $userRepository;

    public function setup() {
        $this->userRepository = new UserRepository();

        $ticketID = \Route::current()->parameter('id');

        $this->crud->setModel("App\Models\TicketPurchases");
        $this->crud->addClause('where', 'ticket_type', '=', $ticketID);

        $this->crud->setRoute("admin/ticket/".$ticketID.'/purchases');
        $this->crud->setEntityNameStrings('ticket purchase', 'ticket purchases');


        $this->crud->removeAllButtons();
        // $this->crud->setColumns(['credits','transaction_type','transaction_id']);
        $this->crud->addColumn([
            'name'          => 'customer_id', // The db column name
            'label'         => "Customer Name", // Table column heading
            'type'          => 'model_function',
            'function_name' => 'customerName'
        ]);

        $this->crud->addColumn([
            'name'   => 'created_at', // The db column name
            'label'  => "Purchased at", // Table column heading
            'type'   => 'date',
            'format' => 'd-m-Y'
        ]);

        $this->crud->addColumn([
            'name'          => 'agent_code', // The db column name
            'label'         => "Agent Name", // Table column heading
            'type'          => 'model_function',
            'function_name' => 'agentName'
        ]);
        $this->crud->addButtonFromModelFunction('line', 'open_google', 'openGoogle', 'end');
        // $this->crud->addFields($fields, 'update/create/both');
    }
    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        // $this->crud->removeButton('edit');

        return view('crud::list', $this->data);
    }
    public function declareResult(StoreRequest $request, $id)
    {
        $id = (int)$id;
        if($id ==0 ) return redirect()->back();
        $purchased = TicketPurchases::with(['agent','ticket'])->where('id',$id)->first();
        if($purchased->ticket->winner_declare == 0){
            $purchased->is_winner = 1;
            \Alert::success('Winner has been declared successfully')->flash();
            $purchased->save();
            $purchased->ticket->winner_declare = 1;
            $purchased->ticket->save();
            return redirect('admin/ticket/'.$purchased->ticket->id.'/purchases');
        }
         return redirect()->back();

    }

    public function update(UpdateRequest $request)
    {}

    public function listAgentCredits($agentID)
    {}
    public function addAgentCredits($agentID)
    {
        # code...
    }
}