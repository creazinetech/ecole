<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Http\Request;

class BannerSlidersCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\BannerSliders');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/banner-sliders');
        $this->crud->setEntityNameStrings('Banner Slider', 'Banner Sliders');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addFields([
                [
                    'label' => "Banner Image",
                    'name' => "banner_image",
                    'type' => 'image',
                    'upload' => true,
                    'crop' => true, // set to true to allow cropping, false to disable
                    'aspect_ratio' => 2, // ommit or set to 0 to allow any aspect ratio
                    // 'disk' => 's3_bucket', // in case you need to show images from a different disk
                    // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value

                ]
            ], 'update/create/both');
        /*$this->crud->removeColumns(['type','status']); // remove an array of columns from the stack

        $this->crud->addColumns([
            [
               'name' => 'type', // The db column name
               'label' => "Promo Type", // Table column heading
               'type'        => 'radio',
               'options' => ["1" => "Value", "2" => "Percentage"]
            ],
            [
               'label' => "Status", // Table column heading
               'type' => "radio",
               'name' => 'status',
               'options' => ["A" => "Active", "I" => "InActive"]
            ]
        ]);*/
    }

    public function store(Request $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(Request $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
