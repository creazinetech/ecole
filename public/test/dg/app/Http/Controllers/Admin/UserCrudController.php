<?php
namespace App\Http\Controllers\Admin;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Http\Request as StoreRequest;
use Illuminate\Http\Request as UpdateRequest;
use App\User;
use App\Http\Repositories\UserRepository;


class UserCrudController extends CrudController {
    public $userRepository;
    public function setup() {
        $this->userRepository = new UserRepository();
        $this->crud->setModel("App\User");
        $this->crud->setRoute("admin/users");
        $this->crud->setEntityNameStrings('user', 'users');
        $this->crud->setColumns([
            'name',
            [
                'name'        => 'user_type',
                'label'       => 'User Type',
                'type'        => 'radio',
                'options'     => [
                        'c' => "Customer",
                        'a' => "Agent"
                    ]
            ],
            'email',
            'mobile',
            'referral_code'
        ]);
        $fields = array(
            [ // Text
                'name'  => 'name',
                'label' => "Name",
                'type'  => 'text',
            ],
            [ // Text
                'name'  => 'email',
                'label' => "Email address",
                'type'  => 'email',
            ],
            [ // Text
                'name'  => 'mobile',
                'label' => "Mobile Number",
                'type'  => 'text',
            ],
            [ // select_from_array
                'name' => 'user_type',
                'label' => "User Type",
                'type' => 'select_from_array',
                'options' => ['c' => 'Customer', 'a' => 'Agent'],
                'allows_null' => false,
                'default' => 'a'
            ],
            [ // select_from_array
                'name' => 'otp_varified',
                'label' => "OTP Varified",
                'type' => 'select_from_array',
                'options' => ['0' => 'Not Varified', '1' => 'Varified'],
                'allows_null' => false,
                'default' => '0'
            ],
            [ // select_from_array
                'name' => 'status',
                'label' => "Status",
                'type' => 'select_from_array',
                'options' => ['p' => 'select status','A' => 'Active', 'I' => 'InActive'],
                'allows_null' => false
            ]
        );
        $this->crud->addFields($fields, 'update/create/both');
        $this->crud->addButtonFromModelFunction('line', 'open_google', 'openGoogle', 'end');
    }
    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        // $this->crud->removeButton('edit');

        return view('crud::list', $this->data);
    }
    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        $this->crud->hasAccessOrFail('update');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }

        // update the row in the db
        $item = $this->crud->update($request->get($this->crud->model->getKeyName()),
                            $request->except('redirect_after_save', '_token','save_action'));
        $this->data['entry'] = $this->crud->entry = $item;
        $this->userRepository->addWelcomeCreditToAgent($item);
        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        return \Redirect::to($this->crud->route);
    }

    public function listAgentCredits($agentID)
    {

        $agentID = (int)$agentID;
        if($agentID == 0) return redirect()->back();
        $agentDetails = User::where(['id' => $agentID,'user_type' => 'a'])->with('agentCreditTransactions')->first();
        if($agentDetails == null) return redirect()->back();
        $title = "Credit History ::".ucfirst($agentDetails->name);
        $this->crud->removeAllButtons();
        // $this->crud->addButtonFromModelFunction('line', 'open_google', 'openGoogle', 'end');

        $this->crud->setColumns(['credits','transaction_type','transaction_id','customer_id']);
        $data = array(
            'agentDetails' => $agentDetails,
            'crud'         => $this->crud,
            'entries'      => $agentDetails->agentCreditTransactions,
            'title'        => $title
        );
        return view('admin.agent_list', $data);
    }
    public function addAgentCredits($agentID)
    {
        # code...
    }
}