<?php
namespace App\Http\Controllers\Admin;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Http\Request as StoreRequest;
use Illuminate\Http\Request as UpdateRequest;
use App\User;
use App\Http\Repositories\UserRepository;


class AgentCreditCrudController extends CrudController {
    public $userRepository;

    public function setup() {
        $this->userRepository = new UserRepository();

        $agentID = \Route::current()->parameter('id');

        $this->crud->setModel("App\Models\CreditTransactions");
        $this->crud->addClause('where', 'agent_id', '=', $agentID);

        $this->crud->setRoute("admin/agent-credits/".$agentID);
        $this->crud->setEntityNameStrings('agent credit', 'agent credits');


        // $this->crud->removeAllButtons();
        $this->crud->setColumns(['credits','transaction_type','transaction_id']);
        $this->crud->addColumn([
                'name'   => 'created_at', // The db column name
                'label'  => "Created at", // Table column heading
                'type'   => 'date',
                'format' => 'd-m-Y'
         ]);
        $this->crud->addColumn([
                    'name'      => 'customer_id', // The db column name
                    'label'     => "Related Customer", // Table column heading
                    'type'      => 'model_function',
                    'function_name' => 'customerName'
         ]);


        $fields = array(
            [ // Text
                'name'  => 'credits',
                'label' => "Credits",
                'type'  => 'text',
            ]
        );
        $this->crud->addFields($fields, 'update/create/both');

        $this->crud->removeAllButtonsFromStack('line');
    }
    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        // $this->crud->removeButton('edit');

        return view('crud::list', $this->data);
    }
    public function store(StoreRequest $request)
    {
        echo "<pre>";
        $agentID = \Route::current()->parameter('id');
        $request->request->add([
                            'transaction_id' =>  date('YmdHis'),
                            'agent_id' => $agentID,
                            'customer_id' => '0',
                            'transaction_type' => 'added'
                        ]);
        print_r($request->all());
        User::where('id',$agentID)->first()->increment('credits',$request->credits);
        echo "</pre>";
// exit;
        // dd($request);
        return parent::storeCrud();
    }

    public function addAgentCredits($agentID)
    {
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }
}