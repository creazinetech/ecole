<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tickets;
use App\Http\Repositories\TicketRepository;
use Auth;

class TicketController extends Controller
{
    public $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function getListings(Request $request)
    {
        $user    = \Auth::user();
        $tickets = $this->ticketRepository->getListings($request->all(), $user);
        return response()->json([
                            'tickets' => $tickets
                        ]);
    }

    public function purchaseTicket(Request $request)
    {
        $userid = \Auth::id();
        $tickets = $this->ticketRepository->purchaseTicket($request, $userid);

        if(isset($tickets['error'])){
            return response()->json([
                            'error' => $tickets['error']
                        ],401);
        } elseif(isset($tickets['success'])){
            return response()->json([
                'data'      => $tickets['success'],
                'message'   => 'ticket purchased successfully'
            ],200);
        }
    }

    public function getTickets(Request $request)
    {
        $user = \Auth::user();

        $tickets = $this->ticketRepository->getTickets($request, $user);

        if(isset($tickets['error'])){
            return response()->json([
                            'error' => $tickets['error']
                        ],401);
        } elseif(isset($tickets['success'])){
            return response()->json([
                'data'      => $tickets['success'],
                'message'   => 'details'
            ],200);
        }
    }

    public function enrollTicket(Request $request)
    {
        $user = \Auth::user();
        if($user->user_type != 'a'){

            return response()->json([
                            'error' => 'Unauthorized Access'
                        ],401);
        }

        $data = $this->ticketRepository->enrollTicket($request, $user);

        if(isset($data['error'])){
            return response()->json([
                            'error' => $data['error']
                        ],401);
        } elseif(isset($data['success'])){
            return response()->json([
                'data'      => $data['success'],
                'message'   => 'details',
                'status'    => 'success'
            ],200);
        }
    }

    public function getPastWinners()
    {
        return Tickets::with(['winner'])->where('winner_declare',1)->get();
    }
}
