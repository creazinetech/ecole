<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Auth;
use Validator;
use File;
use Intervention\Image\Facades\Image;
use App\Http\Repositories\UserRepository;

class UserController extends Controller
{
    public $successStatus = 200;
    public $userRepository;
    public function getDetails()
    {
        $user = Auth::user()->load('UserProfile');
        return response()->json(['success' => $user], $this->successStatus);
    }
    public function updateProfile(Request $request)
    {
        $request = $request->only(['profile_pic','dob','gender','city','state','area','address']);
        $user = Auth::user();
        $request['user_id'] = $user->id;
        $request['dob'] =  date('Y-m-d', strtotime($request['dob']));
        $path = public_path() .'/images/';
        File::exists($path) or File::makeDirectory($path);
        $file = @$request['profile_pic'];
        // $thumbImageName = date('ymdhis');
        if($file){

            if($file->getClientOriginalExtension()){
                $thumb = Image::make($file->getRealPath());
                // $thumb = Image::make($file->getRealPath());
                $thumbImageName = time().'.'.$file->getClientOriginalExtension();
                $request['profile_pic'] = 'thumb_' . $thumbImageName;
                $thumb->fit(500, 500)->save($path . 'thumb_' . $thumbImageName, 100);
                $thumb->destroy();
            }
        }

        try
        {
            $user->UserProfile()->updateOrCreate($request);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],401);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['error' => $e->getMessage()],401);
        }
        return response()->json(['data' => $user->load('UserProfile')]);
    }
    public function getEnrolledTickets()
    {
        $user = Auth::user()->load('ticketsPurchased');
        return response()->json(['data' => $user]);
    }

    public function getUserByID($userID)
    {
        return User::where('id', $userID)->get();
    }

    public function getCredits()
    {
        $user = Auth::user()->load('agentCreditPurchased');
        $this->userRepository = new UserRepository();
        $response = $this->userRepository->getAgentCredits($user);
        return response()->json(['data' => $response]);
    }
}