<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\CreditTypes;

class CreditTypesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function getDetails()
    {
        $credits = CreditTypes::where('status','A')->get();
        return response()->json([
                            'credits' => $credits
                        ]);
    }
}
