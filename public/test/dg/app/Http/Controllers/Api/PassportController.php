<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\ForgotPasswordRequest;
use Validator;

class PassportController extends Controller
{

    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password'), 'user_type' => request('user_type')/*, 'status' => 'A'*/])){
            $user = Auth::user();
            if($user->otp_varified == 0)
                return response()->json(['error' => 'Please verifiy otp'], 401);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required',
            'email'    => 'sometimes|nullable|email|max:255|unique:users',
            'password' => 'required',
            'mobile'   => 'required|string|max:12|unique:users'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();

       /* if($request->user_type == 'a')
            $input['referral_code'] = 'DC'.date('YmdHis');*/
        $input['password'] = bcrypt($input['password']);
        $input['otp'] =  'test';
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */

    public function otpVarification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required',
            'otp' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $user = User::where(['mobile' => $request->mobile])->first();
        if ($user->otp_varified == 0) {
            if ($user->otp == $request->otp) {
                $user->otp_varified = 1;
                $user->status = 'A';
                $user->update();
                return response()->json(['success' => 'otp varified successfully'], 200);
            } else {
                return response()->json(['error' => 'Invalid Otp'], 400);
            }
        } else {
            return response()->json(['success' => 'otp already varified'], 200);
        }
    }


    public function sendPasswordRequest(Request $request)
    {
        $user = User::where(['mobile' => $request->mobile])->first();
        if($user != null){
            ForgotPasswordRequest::where(['mobile' => $request->mobile])->delete();
            ForgotPasswordRequest::create(['mobile' => $request->mobile]);
            return response()->json(['success' => 'otp send successfully'], 200);
        }
        return response()->json(['error' => 'User Not Found'], 400);
    }


    public function updatePassword(Request $request)
    {
        $user = ForgotPasswordRequest::where(['mobile' => $request->mobile])->first();
        if($user != null){
            if($user->otp == 'test' && $request->password !=''){
                $updated = User::where('mobile',$request->mobile)->first();
                $updated->password = \Hash::make($request->password);
                $updated->save();
                return response()->json(['success' => 'password updated successfully'], 200);
            }
            return response()->json(['success' => 'OTP not matched.'], 200);
        }
        return response()->json(['error' => 'Request Not Found'], 400);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        if(\Hash::check($request->current_password, $user->password)){
                $user->password = \Hash::make($request->new_password);
                $user->save();
                return response()->json(['success' => 'password updated successfully'], 200);

        }
        return response()->json(['error' => 'Password not matched'], 400);
    }
}