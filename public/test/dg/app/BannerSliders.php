<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class BannerSliders extends Model
{
    use CrudTrait;

    protected $table="banner_sliders";
    protected $guarded=['id'];
    protected $appends = ['full_banner_path'];

    public function getFullBannerPathAttribute()
    {
    	return url($this->banner_image);//\Storage::disk('uploads')->url(trim($this->banner_image,'/'));
    	// return "123";
    }

    public function setBannerImageAttribute($value)
    {
        $attribute_name = "banner_image";
        $disk = "uploads";
        $destination_path = "";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            // echo $disk;exit;
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }
}
