<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class PaperResearch extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'paper_research';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pid','jaid','prtitle','publish_in','publish_date','presented_at','presentation_date'];

}
