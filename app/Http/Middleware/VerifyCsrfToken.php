<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
       'success',
        'apply-for-admission1',
        'apply-for-admission',
        'checkout',
        'checkout1',
        'proceed-to-payment',
        'proceed-to-payment1',
        'failure', 
        'apply-for-job',
        'applyjob',
        'apply-for-job123',
        'applyjob123',
    ];
}
