<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\FooterSliders; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class FslidersController extends Controller
{
	
    public function footersliders(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','2')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$FooterSliders = DB::table('footer_sliders')->get();
				return view('footer-sliders',compact('FooterSliders'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function addfssliders(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','2')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$FooterSliders = new FooterSliders;
				if($request->hasFile('fs_img')) {
					$file = Input::file('fs_img');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$fs_img = $rand.$timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/footerslider-images/',$fs_img);
					$FooterSliders->fs_img =$fs_img;
				}
				$FooterSliders->fs_status ='Active';
				$FooterSliders->fs_title1 =stripslashes($request->input('fs_title1'));
				$FooterSliders->save();
				$request->session()->flash('alert-success', 'Images uploaded successfully!');
				return redirect('footer-sliders');
			}else{
				return view('unauthorised');
			}
		}
	}
	
	public function fsstatus(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$FooterSliders = new FooterSliders;
				$Data = array(
				'fs_status' => $request->input('status'),
				);
				$FooterSliders->where('fs_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	public function fsorder(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$FooterSliders = new FooterSliders;
				$Data = array(
				'fs_order' => $request->input('order'),
				);
				$FooterSliders->where('fs_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function fsdelete($id){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','2')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$fs_id = Crypt::decrypt($id);
				DB::table('footer_sliders')->where('fs_id', '=', $fs_id)->delete();
				session()->flash('alert-success', 'Image has been deleted!');
				return redirect('footer-sliders');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	
}
