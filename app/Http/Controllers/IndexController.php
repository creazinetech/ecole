<?php

namespace App\Http\Controllers;
use Auth;
use App\IndexSliders; //iNCLUDE MODAL
use App\QuickEnquiry; //iNCLUDE MODAL
use App\SchoolCalendar; //iNCLUDE MODAL
use App\Alumni; //iNCLUDE MODAL
use App\News; //iNCLUDE MODAL
use App\Newsletter; //iNCLUDE MODAL
use App\ContactEnquiry; //iNCLUDE MODAL
use App\JobApplications; //iNCLUDE MODAL
use App\EducationDetails; //iNCLUDE MODAL
use App\ProfessionalCourses; //iNCLUDE MODAL
use App\ProfessionalCourses1; //iNCLUDE MODAL
use App\PaperResearch; //iNCLUDE MODAL
use App\EmploymentHistory; //iNCLUDE MODAL
use App\EmploymentReference; //iNCLUDE MODAL
use App\Pages; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller
{

    public function index(){
			$Sliders = DB::table('index_sliders')->where('is_status','=','Active')->get();
			return view('index',compact('Sliders'));
    }

    public function messagefromprincipal(){
			return view('message-from-the-principal');
    }

    public function schoolcalendar(){
			return view('school-calendar');
    }

	public function sendquickenquiry(Request $request){
		if ($request->ajax()) {

    	    $ip = $_SERVER['REMOTE_ADDR'];
            $secretkey = '6LfniTsUAAAAABekDJEWHvzFX6mnSwc-0L1zksZQ';
            $captcha = $request->input('g-recaptcha-response');
            $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;
            /**
            $response = file_get_contents($api_url);
            /**/
            $ch = curl_init($api_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $obj = json_decode($response);
            if($obj->success == true) {

			$QuickEnquiry = new QuickEnquiry;
			$QuickEnquiry->qe_name = $request->input('qname1');
			$QuickEnquiry->qe_email = strtolower($request->input('qemail'));
			$QuickEnquiry->qe_mobile = $request->input('qphone');
			//$QuickEnquiry->qe_message = $request->input('qmsg');
			$ID= $QuickEnquiry->save();

			if($ID ==1){
				//////////////MAIL START///////////////
    				$to = strtolower($request->input('qemail'));
                    $subject = "Ecole Mondiale Enquiry";
                    $message = "<b>Dear ".$request->input('qname1').", </b><br>";
                    $message.= "<p>we have got your enquiry details, we will contact you soon..!</p>";
                    $message.= "<br><br><small>Thank you for contacting us, Have a good day.!</small>";
                    $header = "From:noreply@ecolmondiale.org \r\n";
                    $header.= "MIME-Version: 1.0\r\n";
                    $header.= "Content-type: text/html\r\n";
                    mail ($to,$subject,$message,$header);
				//////////////MAIL END////////////////
				//////////////MAIL START///////////////
    				$to1 = "quickenquiry@ecolemondiale.org";
                    $subject1 = "Ecole Mondiale Enquiry";
                    $message1 = "Hi,<br>";
                    $message1.= "<p>New enquiry details are as follows:</p>";
                    $message1.= "Name: ".$request->input('qname1')."<br>Email: ".strtolower($request->input('qemail'))."<br>";
                    $message1.= "Phone: ".$request->input('qphone')."<br><br>";
                   // $message1.= "Phone: ".$request->input('qphone')."<br> Message: ".$request->input('qmsg')."<br><br>";
                    $header1 = "From:noreply@ecolmondiale.org \r\n";
                    $header1.= "MIME-Version: 1.0\r\n";
                    $header1.= "Content-type: text/html\r\n";
                    mail ($to1,$subject1,$message1,$header1);
					//////////////MAIL END////////////////


    			echo 'success'; die;
    			}else{

    				echo 'failed'; die;
    			}
    		}
    		else{
    			echo 'failed';
    			die;
    		}
		}else{
			echo 'failed';
			die;
		}
	}

	public function addalumni(Request $request){


	    //Validation Start//
        $errors = $this->validate($request, [
        	'firstname' => 'required',
        	'lastname' => 'required',
        	'graduate_yr' => 'required',
        	'home_address' => 'required',
        	'city' => 'required',
        	'state' => 'required',
        	'zip' => 'required|numeric',
        	'contact1' => 'required|numeric',
        	'contact2' => 'required|numeric',
        	'emailid' => 'required|email',
        	'website' => 'required',
        	'study_type' => 'required',
        	'university' => 'required',
        	'programme' => 'required',
        	'last_university' => 'required',
        	'last_programme' => 'required',
        	'last_address' => 'required',
        	'alumni_ambassador' => 'required',
        	'occupation' => 'required',
        	'organisation' => 'required',
        	'designation' => 'required',
        	'off_address' => 'required',
        	'off_contact' => 'required|numeric',
        	'off_fax' => 'required',
        	'off_email' => 'required|email',
        	'off_website' => 'required',
        	'work_type' => 'required',
        	'gender' => 'required',
        	'dob' => 'required',
        //	'personal' => 'required',
        //	'professional' => 'required',
        //	'hobbies' => 'required',
        //	'vacation' => 'required',
        	'g-recaptcha-response' => 'required',
        ]);
        //Validation End//


        $ip = $_SERVER['REMOTE_ADDR'];
        $secretkey = '6LfniTsUAAAAABekDJEWHvzFX6mnSwc-0L1zksZQ';
        $captcha = $request->input('g-recaptcha-response');
        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;

        $ch = curl_init($api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $obj = json_decode($response);
        if($obj->success == true) {



		$Alumni = new Alumni;
		$Alumni->firstname = $request->input('firstname');
		$Alumni->lastname = $request->input('lastname');
		$Alumni->graduate_yr = $request->input('graduate_yr');
		$Alumni->city = $request->input('city');
		$Alumni->state = $request->input('state');
		$Alumni->zip = $request->input('zip');
		$Alumni->contact1 = $request->input('contact1');
		$Alumni->contact2 = $request->input('contact2');
		$Alumni->home_address = $request->input('home_address');
		$Alumni->emailid = strtolower($request->input('emailid'));
		$Alumni->website = strtolower($request->input('website'));
		$Alumni->university = $request->input('university');
		$Alumni->programme = $request->input('programme');
		$Alumni->last_university = $request->input('last_university');
		$Alumni->last_programme = $request->input('last_programme');
		$Alumni->last_address = $request->input('last_address');
		$Alumni->occupation = $request->input('occupation');
		$Alumni->organisation = $request->input('organisation');
		$Alumni->designation = $request->input('designation');
		$Alumni->off_address = $request->input('off_address');
		$Alumni->off_contact = $request->input('off_contact');
		$Alumni->off_fax = $request->input('off_fax');
		$Alumni->off_email = strtolower($request->input('off_email'));
		$Alumni->off_website = strtolower($request->input('off_website'));
		$Alumni->work_type = $request->input('work_type');
		$Alumni->study_type = $request->input('study_type');
		$Alumni->gender = $request->input('gender');
		$Alumni->dob = $request->input('dob');
		$Alumni->alumni_ambassador = $request->input('alumni_ambassador');
		$Alumni->personal = $request->input('personal');
		$Alumni->professional = $request->input('professional');
		$Alumni->hobbies = $request->input('hobbies');
		$Alumni->vacation = $request->input('vacation');
		$ID= $Alumni->save();

	    //Email
     	$subject = "Alumni Enquiry - Ecole Mondiale";

        $to1 =  "alumni@ecolemondiale.org";
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        // More headers
        $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
    	$message = "<html>
        <head>
        <title>Alumni Details</title>
        </head>
        <body>
        <table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' >
        <tr><td>Name</td><td>:</td><td>".$request->input('firstname').' '.$request->input('lastname')."</td></tr>
        <tr><td>Graduate Year</td><td>:</td><td>".stripslashes($request->input('graduate_yr'))."</td></tr>
        <tr><td>City </td><td>:</td><td>".stripslashes($request->input('city'))."</td></tr>
        <tr><td>State </td><td>:</td><td>".stripslashes($request->input('state'))."</td></tr>
        <tr><td>Pincode </td><td>:</td><td>".stripslashes($request->input('zip'))."</td></tr>
        <tr><td>Contact No.</td><td>:</td><td>".stripslashes($request->input('contact1')).' '.stripslashes($request->input('contact2'))."</td></tr>
        <tr><td>Email Id </td><td>:</td><td>".stripslashes($request->input('emailid'))."</td></tr>
        <tr><td>website </td><td>:</td><td>".stripslashes($request->input('website'))."</td></tr>
        <tr><td>Home Address </td><td>:</td><td>".stripslashes($request->input('home_address'))."</td></tr>
        <tr><td>Study Type </td><td>:</td><td>".stripslashes($request->input('study_type'))."</td></tr>
        <tr><td>University </td><td>:</td><td>".stripslashes($request->input('university'))."</td></tr>
        <tr><td>Programme </td><td>:</td><td>".stripslashes($request->input('programme'))."</td></tr>
        <tr><td>Last University </td><td>:</td><td>".stripslashes($request->input('last_university'))."</td></tr>
        <tr><td>Last Programme </td><td>:</td><td>".stripslashes($request->input('last_programme'))."</td></tr>
        <tr><td>Last Address </td><td>:</td><td>".stripslashes($request->input('last_address'))."</td></tr>
        <tr><td>Occupation </td><td>:</td><td>".stripslashes($request->input('occupation'))."</td></tr>
        <tr><td>Organisation </td><td>:</td><td>".stripslashes($request->input('organisation'))."</td></tr>
        <tr><td>Designation </td><td>:</td><td>".stripslashes($request->input('designation'))."</td></tr>
        <tr><td>Office Address </td><td>:</td><td>".stripslashes($request->input('off_address'))."</td></tr>
        <tr><td>Office Contact </td><td>:</td><td>".stripslashes($request->input('off_contact'))."</td></tr>
        <tr><td>Office Fax </td><td>:</td><td>".stripslashes($request->input('off_fax'))."</td></tr>
        <tr><td>Office Email </td><td>:</td><td>".stripslashes($request->input('off_email'))."</td></tr>
        <tr><td>Office Website </td><td>:</td><td>".stripslashes($request->input('off_website'))."</td></tr>
        <tr><td>Work Type </td><td>:</td><td>".stripslashes($request->input('work_type'))."</td></tr>
        <tr><td>Gender </td><td>:</td><td>".stripslashes($request->input('gender'))."</td></tr>
        <tr><td>DOB </td><td>:</td><td>".stripslashes($request->input('dob'))."</td></tr>
        <tr><td>Alumni Ambassador? </td><td>:</td><td>".stripslashes($request->input('alumni_ambassador'))."</td></tr>
        <tr><td>Personal </td><td>:</td><td>".stripslashes($request->input('personal'))."</td></tr>
        <tr><td>Proffessional </td><td>:</td><td>".stripslashes($request->input('professional'))."</td></tr>
        <tr><td>Hobbies </td><td>:</td><td>".stripslashes($request->input('hobbies'))."</td></tr>
        <tr><td>Vacation </td><td>:</td><td>".stripslashes($request->input('vacation'))."</td></tr>
        </table></body>
        </html>";
        mail ($to1,$subject,$message,$headers);

	    //Email


		$request->session()->flash('alert-success', 'Alumni enquiry sent successfully!');
		return redirect('alumni');

		}else{
            $VeriFailed = 'Captcha verification failed';
            return redirect('alumni')->with('CaptchaVerificaion', 'Captcha Verification Failed.') ->withInput();
	    }
	}


	public function viewnews($id){
		$Data = DB::table('news')->where('id', '=', $id)->get();
		return view('view-news',compact('Data'));

	}

	public function viewnewsletter($id){
		$Data = DB::table('newsletter')->where('nlid', '=', $id)->get();
		return view('view-newsletter',compact('Data'));

	}

	public function viewevent($id){
		$Data = DB::table('events')->where('id', '=', $id)->get();
		return view('view-event',compact('Data'));

	}

	public function viewyoutube($id){
		$Data = DB::table('youtube_video')->where('ycatid', '=', $id)->where('ystatus', '=', 'Active')->get();
		return view('view-youtube-video',compact('Data'));

	}

	public function downloadcount(Request $request){
		if ($request->ajax()) {
			$request->input('Nlid');
			$Data = DB::table('newsletter')->where('nlid', '=',$request->input('Nlid'))->get();
			foreach($Data as $row){}
			$row->downloads;

			$Newsletter = new Newsletter;
			$Data = array(
			'downloads' => $row->downloads+1
			);
			$Newsletter->where('nlid',$request->input('Nlid'))->update($Data);

			$Data = DB::table('newsletter')->where('nlid', '=',$request->input('Nlid'))->get();
			foreach($Data as $row){}
			echo $row->downloads; die;

		}
	}

	public function addcontactusenquiry(Request $request){
	   //var_dump($_POST);die;
	     //Validation Start//
        $errors = $this->validate($request, [
        	'name' => 'required',
        	'dob' => 'required',
        	'address' => 'required',
        	'nopskul' => 'required',
        	'grade' => 'required',
        	'gradetjoin' => 'required',
        	'year' => 'required',
        	'nationality' => 'required',
        	'whattoknow' => 'required',
        	'aboutshcool' => 'required',
        	'city' => 'required',
        	'state' => 'required',
        	'country' => 'required',
        	'pincode' => 'required|numeric',
			'fname' => 'required',
			'foccupation' => 'required',
			'fdesignation' => 'required',
			'forganization' => 'required',
			'foffadd' => 'required',
        	'fofftel' => 'required|',
        	'femail' => 'required|email',
        	'fmobile' => 'required|numeric',
        	'frestel' => 'required|numeric'
        ]);
        //Validation End//


        $ip = $_SERVER['REMOTE_ADDR'];
        // $secretkey = '6LfniTsUAAAAABekDJEWHvzFX6mnSwc-0L1zksZQ';
        $secretkey = '6Ldc_18UAAAAAAd5IlnToo6LQV6B1UUk86QWNbrm';
        $captcha = $request->input('g-recaptcha-response');
        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;
        /**
        $response = file_get_contents($api_url);
        /**/
        $ch = curl_init($api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $obj = json_decode($response);
        if($obj->success == true) {

		$CEObj = new ContactEnquiry;
		$CEObj->name = ucwords($request->input('name'));
		$CEObj->dob = date('Y-m-d',strtotime($request->input('dob')));
		$CEObj->address = $request->input('address');
		$CEObj->nopskul = ucwords($request->input('nopskul'));
		$CEObj->grade = ucwords($request->input('grade'));
		$CEObj->gradetjoin = ucwords($request->input('gradetjoin'));
		$CEObj->year = $request->input('year');
		$CEObj->city = ucwords($request->input('city'));
		$CEObj->state = ucwords($request->input('state'));
		$CEObj->country = ucwords($request->input('country'));
		$CEObj->pincode = $request->input('pincode');
		$CEObj->whattoknow = $request->input('whattoknow');
		$CEObj->aboutschool = $request->input('aboutshcool');
		$CEObj->nationality = ucwords($request->input('nationality'));

		$CEObj->fname = ucwords($request->input('fname'));
		$CEObj->foccupation = ucwords($request->input('foccupation'));
		$CEObj->fdesignation = ucwords($request->input('fdesignation'));
		$CEObj->forganization = ucwords($request->input('forganization'));
		$CEObj->foffadd = ucwords($request->input('foffadd'));
		$CEObj->fofftel = $request->input('fofftel');
		$CEObj->fmobile = $request->input('fmobile');
		$CEObj->frestel = $request->input('frestel');
		$CEObj->femail = strtolower($request->input('femail'));

		$CEObj->mname = ucwords($request->input('mname'));
		$CEObj->moccupation = ucwords($request->input('moccupation'));
		$CEObj->mdesignation = ucwords($request->input('mdesignation'));
		$CEObj->morganization = ucwords($request->input('morganization'));
		$CEObj->moffadd = ucwords($request->input('moffadd'));
		$CEObj->mofftel = $request->input('mofftel');
		$CEObj->mmobile = $request->input('mmobile');
		$CEObj->mrestel = $request->input('mrestel');
		$CEObj->memail = strtolower($request->input('memail'));

		$CEObj->save();

		$request->session()->flash('alert-success', 'Your enquiry sent successfully, We will contact you soon!');
		return redirect('contact-us');
	    }else{
            $VeriFailed = 'Captcha verification failed';
            return redirect('contact-us')->with('CaptchaVerificaion', 'Captcha Verification Failed.') ->withInput();
	    }
	}


	public function applyjob(Request $request){


	    //var_dump($_POST);die;
	    //Validation Start//

	    /*
		if(!empty($request->input('name')) && preg_match('/www\.|http:|https:/',$request->input('name'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedname', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('children')) && preg_match('/www\.|http:|https:/',$request->input('children'))){
            return redirect('apply-for-job')->with('UrlsNotAllowed', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('cadd')) && preg_match('/www\.|http:|https:/',$request->input('cadd'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedcadd', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('padd')) && preg_match('/www\.|http:|https:/',$request->input('padd'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedpadd', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('language_speak')) && preg_match('/www\.|http:|https:/',$request->input('language_speak'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedLA', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('edu_statement')) && preg_match('/www\.|http:|https:/',$request->input('edu_statement'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedes', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('other_info')) && preg_match('/www\.|http:|https:/',$request->input('other_info'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedOI', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('current_employer')) && preg_match('/www\.|http:|https:/',$request->input('current_employer'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedCE', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('children')) && preg_match('/www\.|http:|https:/',$request->input('children'))){
            return redirect('apply-for-job')->with('UrlsNotAllowedChildren', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('nationality')) && preg_match('/www\.|http:|https:/',$request->input('nationality'))){
            return redirect('apply-for-job')->with('UrlsNotAllowednationality', 'URLs are not allowed.') ->withInput();
		}
		*/

	     $validator = Validator::make($request->all(), [
			'position' => 'required',
			'name' => 'required|regex:/^[`\'a-zA-Z\s]+$/',
			'gender' => 'required',
			'nationality' => 'required|regex:/^[`\'a-zA-Z\s]+$/',
			'dob' => 'required|date_format:"d-m-Y"',
			'photo' => 'required|mimes:jpeg,bmp,png,jpg',
			'marital_status' => 'required',
			'children' => 'required_if:marital_status,Married|sometimes|max:255',
			'cadd' => 'required',
			'landline' => 'required|numeric',
			'mob_no' => 'sometimes|numeric',
			//'pincode' => 'sometimes|numeric',
			'email_id' => 'required|email',
			'padd' => 'required',
			'passport_no' => 'sometimes|regex:/^[a-zA-Z0-9\s]+$/',
			'issue_date' => 'required_with:passport_no|date_format:"d-m-Y"',
			'issue_place' => 'required_with:passport_no|regex:/^[`\'a-zA-Z]+$/u',
			'expiry_date' => 'required_with:passport_no|date_format:"d-m-Y"',
			'internation_school_exp' => 'required',
			'experience_in' => 'required',
			'experience_year' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
			'current_employer' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
			'designation' => 'required_with:current_employer|regex:/(^[A-Za-z0-9 ]+$)+/|max:255',
			'joindt_curr_emp' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
			'current_sal' => 'required|regex:/(^[.A-Za-z0-9 ]+$)+/',
			'expected_sal' => 'required|regex:/(^[.A-Za-z0-9 ]+$)+/',
			'language_speak' => 'required|regex:/[^,;]+[a-zA-Z]+$/u|max:255',
			'newjoin_dt' => 'required',
			'job_captcha_code2'=>'required',
		/*	'edu_statement' => 'sometimes|max:255|regex:/(^[A-Za-z0-9 ]+$)+/',
			'other_info' => 'sometimes|max:255|regex:/(^[A-Za-z0-9 ]+$)+/'	*/
		]);

         if ($validator->fails()) {
            return redirect('apply-for-job')
                        ->withErrors($validator)
                        ->withInput();
        }

		$JobObj = new JobApplications;
		$JobObj->name = $request->input('name');
		$JobObj->dob = $request->input('dob');
		$JobObj->position = $request->input('position');
		if($request->hasFile('photo')) {
			$file = Input::file('photo');
			$timestamp = date('Ymdhi');
			$AppPic = $timestamp. '-' .$file->getClientOriginalName();
			$file->move(public_path().'/images/job-applications/',$AppPic);
			$JobObj->photo =$AppPic;
		}
		$JobObj->gender = $request->input('gender');
		$JobObj->nationality = $request->input('nationality');
		$JobObj->marital_status = $request->input('marital_status');
		$JobObj->children = $request->input('children');
		$JobObj->cadd = $request->input('cadd');
		$JobObj->padd = strtolower($request->input('padd'));
		$JobObj->pincode = strtolower($request->input('pincode'));
		$JobObj->mob_no = $request->input('mob_no');
		$JobObj->landline = $request->input('landline');
		$JobObj->email_id = strtolower($request->input('email_id'));
		$JobObj->passport_no = $request->input('passport_no');
		$JobObj->issue_date = $request->input('issue_date');
		$JobObj->issue_place = $request->input('issue_place');
		$JobObj->expiry_date = $request->input('expiry_date');
		$ExperienceIn="";
		if(!empty($request->input('experience_in'))){
    		foreach($request->input('experience_in') as $ExpIn){
    			if(isset($ExpIn) && !empty($ExpIn)){
    				$ExperienceIn.= $ExpIn.',';
    			}
    		}
		}
		$JobObj->experience_in = $ExperienceIn;
		$JobObj->experience_year = $request->input('experience_year');
		$JobObj->current_employer = $request->input('current_employer');
		$JobObj->designation = $request->input('designation');
		$JobObj->internation_school_exp = $request->input('internation_school_exp');
		$JobObj->joindt_curr_emp = $request->input('joindt_curr_emp');
		$JobObj->it_skills = $request->input('it_skills');
		$JobObj->language_speak = $request->input('language_speak');
		$JobObj->criminal_offence = $request->input('criminal_offence');
		$JobObj->hobbies = $request->input('hobbies');
		$JobObj->current_sal = $request->input('current_sal');
		$JobObj->expected_sal = $request->input('expected_sal');
		$JobObj->newjoin_dt = $request->input('newjoin_dt');
		$JobObj->edu_statement = $request->input('edu_statement');
		$JobObj->other_info = $request->input('other_info');
		$JobObj->save();
		$JAID = $JobObj->id;


		////Educational Details
		$Educount = count($request->input('university'));
		for($i=0;$i<$Educount;$i++){
			if($request->input('university')[$i] !=""){
				$EduObj = new EducationDetails;
				$EduObj->jaid=$JAID;
				$EduObj->university = $request->input('university')[$i];
				$EduObj->board = $request->input('board')[$i];
				$EduObj->sp_subject = $request->input('sp_subject')[$i];
				$EduObj->start_date = $request->input('start_date')[$i];
				$EduObj->completion_date = $request->input('completion_date')[$i];
				$EduObj->percentage = $request->input('percentage')[$i];
				$EduObj->grade = $request->input('grade')[$i];
				$EduObj->save();
			}
		}

		////Paper Reasearch
		$PRcount = count($request->input('prtitle'));
		for($i=0;$i<$PRcount;$i++){
			if($request->input('prtitle')[$i] !=""){
				$PRObj = new PaperResearch;
				$PRObj->jaid=$JAID;
				$PRObj->prtitle = $request->input('prtitle')[$i];
				$PRObj->publish_in = $request->input('publish_in')[$i];
				$PRObj->publish_date = $request->input('publish_date')[$i];
				$PRObj->presented_at = $request->input('presented_at')[$i];
				$PRObj->presentation_date = $request->input('presentation_date')[$i];
				$PRObj->save();
			}
		}

		////Professional Courses
		$PCcount = count($request->input('pattended'));
		for($i=0;$i<$PCcount;$i++){
			if($request->input('pattended')[$i] !=""){
				$PCObj = new ProfessionalCourses;
				$PCObj->jaid=$JAID;
				$PCObj->pattended = $request->input('pattended')[$i];
				$PCObj->pcourse = $request->input('pcourse')[$i];
				$PCObj->pstart_date = $request->input('pstart_date')[$i];
				$PCObj->pcompletion_date = $request->input('pcompletion_date')[$i];
				$PCObj->plocation = $request->input('plocation')[$i];
				$PCObj->save();
			}
		}

		////Professional Courses1
		$PCcount1 = count($request->input('pattended1'));
		for($i=0;$i<$PCcount1;$i++){
			if($request->input('pattended1')[$i] !=""){
				$PCObj1 = new ProfessionalCourses1;
				$PCObj1->jaid=$JAID;
				$PCObj1->pattended1 = $request->input('pattended1')[$i];
				$PCObj1->pcourse1 = $request->input('pcourse1')[$i];
				$PCObj1->pstart_date1 = $request->input('pstart_date1')[$i];
				$PCObj1->pcompletion_date1 = $request->input('pcompletion_date1')[$i];
				$PCObj1->plocation1 = $request->input('plocation1')[$i];
				$PCObj1->save();
			}
		}

		////Employement Details
		$EHcount = count($request->input('eh_name'));
		for($i=0;$i<$EHcount;$i++){
			if($request->input('eh_name')[$i] !=""){
				$EHObj = new EmploymentHistory;
				$EHObj->jaid=$JAID;
				$EHObj->eh_name = $request->input('eh_name')[$i];
				$EHObj->eh_designation = $request->input('eh_designation')[$i];
				$EHObj->period = $request->input('period')[$i];
				$EHObj->subjects = $request->input('subjects')[$i];
				$EHObj->curriculum = $request->input('curriculum')[$i];
				$EHObj->responsibility = $request->input('responsibility')[$i];
				$EHObj->leaving_reason = $request->input('leaving_reason')[$i];
				$EHObj->save();
			}
		}

		////Supervisor Reference
		$ERcount = count($request->input('ername'));
		for($i=0;$i<$ERcount;$i++){
			if($request->input('ername')[$i] !=""){
				$ERObj = new EmploymentReference;
				$ERObj->jaid=$JAID;
				$ERObj->ername = $request->input('ername')[$i];
				$ERObj->erdesignation = $request->input('erdesignation')[$i];
				$ERObj->erphone = $request->input('erphone')[$i];
				$ERObj->eremail = $request->input('eremail')[$i];
				$ERObj->eraddress = $request->input('eraddress')[$i];
				$ERObj->save();
			}
		}
		$message1= "";
		//////////////MAIL START///////////////
        $subject1 = "New Job Application for ".ucwords($request->input('position'));
        $message1.= "<h3>Job application details are as follows:</h3>";
        $message1.= "<b>Name:</b>&nbsp; ".$request->input('name')."<br>";
		$message1.= "<b>DOB:</b>&nbsp; ".strtolower($request->input('dob'))."<br>";
        $message1.= "<b>Email Id:</b>&nbsp; ".$request->input('email_id')."<br>";
        $message1.= "<b>Gender:</b>&nbsp; ".$request->input('gender')."<br>";
        $message1.= "<b>Contact No:</b>&nbsp; ".$request->input('mob_no').' | '.$request->input('landline')."<br>";
        $message1.= "<b>Position Applied For:</b>&nbsp; ".$request->input('position')."<br>";
        $message1.= "<b>Address:</b>&nbsp; ".$request->input('padd')."<br>";
        $message1.= "<b>Pincode:</b>&nbsp; ".$request->input('pincode')."<br>";
        $message1.= "<b>Nationality:</b>&nbsp; ".$request->input('nationality')."<br>";
        $message1.= "<b>Passport:</b>&nbsp; ".$request->input('passport_no')."<br>";
        $message1.= "<b>Issue Date:</b>&nbsp; ".$request->input('issue_date')."<br>";
        $message1.= "<b>Expiry Date:</b>&nbsp; ".$request->input('expiry_date')."<br>";
        $message1.= "<b>Issue Place:</b>&nbsp; ".$request->input('issue_place')."<br>";
        $message1.= "<b>Marital Status:</b>&nbsp; ".$request->input('marital_status')."<br>";
        $message1.= "<b>Children:</b>&nbsp; ".$request->input('children')."<br>";
        $message1.= "<b>Current Address:</b>&nbsp; ".$request->input('cadd')."<br>";
        $message1.= "<b>Current Employer:</b>&nbsp; ".$request->input('current_employer')."<br>";
        $message1.= "<b>Designation:</b>&nbsp; ".$request->input('designation')."<br>";
        $message1.= "<b>International School Experience:</b>&nbsp; ".$request->input('internation_school_exp')."<br>";
        $message1.= "<b>Current:</b>&nbsp; ".$request->input('current_employer')."<br>";
        $message1.= "<b>Permanant Address:</b>&nbsp; ".$request->input('padd')."<br>";
        $message1.= "<b>Experience Year:</b>&nbsp; ".$request->input('experience_year')."<br>";
        $message1.= "<b>Experience In:</b>&nbsp; ".$ExperienceIn."<br>";
        $message1.= "<b>Current Joining Date:</b>&nbsp; ".$request->input('joindt_curr_emp')."<br>";
        $message1.= "<b>Skills:</b>&nbsp; ".$request->input('it_skills')."<br>";
        $message1.= "<b>Language Speaks:</b>&nbsp; ".$request->input('language_speak')."<br>";
        $message1.= "<b>Hobbies:</b>&nbsp; ".$request->input('hobbies')."<br>";
        $message1.= "<b>Current Salary:</b>&nbsp; ".$request->input('current_sal')."<br>";
        $message1.= "<b>Expected Salary:</b>&nbsp; ".$request->input('expected_sal')."<br>";
        $message1.= "<b>New Joining date:</b>&nbsp; ".$request->input('newjoin_dt')."<br>";
        $message1.= "<b>Convicted of a criminal offence:</b>&nbsp; ".$request->input('criminal_offence')."<br>";


        if(count($request->input('university'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="7" class="text-center">Educational Details</th></tr>
			<tr>
			<th>University</th>
			<th>Board</th>
			<th>Specialist Subject area/s</th>
			<th>Start date</th>
			<th>Completion Date</th>
			<th>Grade</th>
			<th>Percentage</th>
			</tr>';
        for($i=0;$i<count($request->input('university'));$i++){
    		if($request->input('university')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('university')[$i].'</td>
    		        <td>'.$request->input('board')[$i].'</td>
    		         <td>'.$request->input('sp_subject')[$i].'</td>
    			    <td>'.$request->input('start_date')[$i].'</td>
    				<td>'.$request->input('completion_date')[$i].'</td>
    				<td>'.$request->input('percentage')[$i].'</td>
    			    <td>'.$request->input('grade')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('pattended'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="5" class="text-center">Professional Courses Attended</th></tr>
			<tr>
			<th>Attended</th>
			<th>Course Details</th>
			<th>Start date</th>
			<th>Completion Date</th>
			<th>Location</th>
			</tr>';
        for($i=0;$i<count($request->input('pattended'));$i++){
    		if($request->input('pattended')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('pattended')[$i].'</td>
    		        <td>'.$request->input('pcourse')[$i].'</td>
    			    <td>'.$request->input('pstart_date')[$i].'</td>
    				<td>'.$request->input('pcompletion_date')[$i].'</td>
    				<td>'.$request->input('plocation')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('pattended1'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="5" class="text-center">Professional Courses Conducted</th></tr>
			<tr>
			<th>Attended</th>
			<th>Course Details</th>
			<th>Start date</th>
			<th>Completion Date</th>
			<th>Location</th>
			</tr>';
        for($i=0;$i<count($request->input('pattended1'));$i++){
    		if($request->input('pattended1')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('pattended1')[$i].'</td>
    		        <td>'.$request->input('pcourse1')[$i].'</td>
    			    <td>'.$request->input('pstart_date1')[$i].'</td>
    				<td>'.$request->input('pcompletion_date1')[$i].'</td>
    				<td>'.$request->input('plocation1')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('prtitle'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="5" class="text-center">Paper Research</th></tr>
			<tr>
			<th>Paper/Research Title</th>
			<th>Published In</th>
			<th>Publish date</th>
			<th>Publish At</th>
			<th>Presentation Date</th>
			</tr>';
        for($i=0;$i<count($request->input('prtitle'));$i++){
    		if($request->input('prtitle')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('prtitle')[$i].'</td>
    		        <td>'.$request->input('publish_in')[$i].'</td>
    			    <td>'.$request->input('publish_date')[$i].'</td>
    				<td>'.$request->input('presented_at')[$i].'</td>
    				<td>'.$request->input('presentation_date')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('eh_name'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="6" class="text-center">Employment History</th></tr>
			<tr>
			<th>Employer Name</th>
			<th>Designation</th>
			<th>Period</th>
			<th>Subjects Tought</th>
			<th>Curriculum</th>
			<th>Resposibility</th>
			<th>Leaving Reason</th>
			</tr>';
        for($i=0;$i<count($request->input('eh_name'));$i++){
    		if($request->input('eh_name')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('eh_name')[$i].'</td>
    		        <td>'.$request->input('eh_designation')[$i].'</td>
    			    <td>'.$request->input('period')[$i].'</td>
    				<td>'.$request->input('subjects')[$i].'</td>
    				<td>'.$request->input('curriculum')[$i].'</td>
    				<td>'.$request->input('responsibility')[$i].'</td>
    				<td>'.$request->input('leaving_reason')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }


        if(count($request->input('ername'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="6" class="text-center">References</th></tr>
			<tr>
			<th>Name</th>
			<th>Designation</th>
			<th>Phone No.</th>
			<th>Email</th>
			<th>Address</th>
			</tr>';
        for($i=0;$i<count($request->input('ername'));$i++){
    		if($request->input('ername')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('ername')[$i].'</td>
    		        <td>'.$request->input('erdesignation')[$i].'</td>
    			    <td>'.$request->input('erphone')[$i].'</td>
    				<td>'.$request->input('eremail')[$i].'</td>
    				<td>'.$request->input('eraddress')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }


	    $message1.= "<b>Personal Education Statement:</b><br> ".$request->input('edu_statement')."<br>";
	    $message1.= "<b>Other Information:</b><br> ".$request->input('other_info')."<br>";


        $header1 = "From:noreply@ecolmondiale.org \r\n";
        $header1.= "MIME-Version: 1.0\r\n";
        $header1.= "Content-type: text/html\r\n";
        $subject2 = "Ecolemondiale.org - Thank you for applying job - ".ucwords($request->input('position'));
        mail ($request->input('email_id'),$subject2,$message1,$header1);
		//$to1 = 'ggavaskar@ecolemondiale.org';
        //$to2 = "sohail@matrixbricks.com";
		$to3 = 'cpereira@ecolemondiale.org';

       // mail ($to1,$subject1,$message1,$header1);
        //mail ($to2,$subject1,$message1,$header1);
        mail ($to3,$subject1,$message1,$header1);

		//////////////MAIL END////////////////

		$request->session()->flash('alert-success', 'Thank You for applying job, we will contact you soon');
		return redirect('thankyou');


	}

	public function searchcontent(Request $request){
		$request->input('search');
		$NewsList = DB::table('news')->where('title', 'like','%'.$request->input('search').'%')->get();
		$PagesList = DB::table('pages')->where('page_meta_keywords', 'like','%'.$request->input('search').'%')->get();
		return view('search',compact('NewsList','PagesList'));
	}

	public function newsletter(Request $request){
		if(!empty($request->input('filterby'))){
		    $filterby = $request->input('filterby');
		}else{
		    $filterby = 'nlid';
		}
		if(!empty($request->input('orderby'))){
		    $orderby = $request->input('orderby');
		}else{
		    $orderby = 'desc';
		}

		//$FilterNewsletters = DB::table('newsletter')->where('status','=','Active')->orderBy($filterby,$orderby)->paginate(1);
		return view('newsletter',compact('orderby','filterby'));
	}

	public function events(Request $request){

		$title = $request->input('title');
		$location = $request->input('location');
		if(!empty($request->input('date1'))){
		$Date1 = $request->input('date1');
		$date1 = date('Y-m-d',strtotime($Date1));
		}
		if(!empty($request->input('date2'))){
		$Date2 = $request->input('date2');
		$date2 = date('Y-m-d',strtotime($Date2));
		}


			if(!empty($title)){
				$EventData = DB::table('events')->where('title','like','%'.$title.'%')->orderBy('id','desc')->get();
			}
			if(!empty($location)){
				$EventData = DB::table('events')->where('location','like','%'.$location.'%')->orderBy('id','desc')->get();
			}
			if(!empty($request->input('date1'))){
				$EventData = DB::table('events')
					->where('start_date','>=',$date1)
					->orderBy('id','desc')->get();
			}
			if(!empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')->get();
			}
			if(!empty($title) && !empty($location)&& !empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && !empty($location)&& empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && !empty($location)&& !empty($request->input('date1'))&& empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->orderBy('id','desc')
					->get();
			}
			if(empty($title) && !empty($location)&& !empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(empty($title) && !empty($location)&& empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('location','like','%'.$location.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(empty($title) && !empty($location)&& !empty($request->input('date1'))&& empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& !empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('start_date','>=',$date1)
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& !empty($request->input('date1'))&& empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& empty($request->input('date1'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->orderBy('id','desc')
					->get();
			}

		return view('events',compact('EventData','title','location','Date1','Date2'));
	}




	public function applyjob123(Request $request){


	    //var_dump($_POST);die;
	    //Validation Start//
	    $errors = $this->validate($request, [
			'position' => 'required',
			'name' => 'required',
			'gender' => 'required',
			'nationality' => 'required|regex:/^[a-zA-Z]+$/u',
			'dob' => 'required|date_format:"d-m-Y"',
			'photo' => 'required|mimes:jpeg,bmp,png,jpg',
			'marital_status' => 'required',
			'children' => 'required_if:marital_status,Married|sometimes|regex:/^[a-zA-Z]+$/u|max:255',
			'cadd' => 'required',
			'landline' => 'required|numeric',
			'mob_no' => 'sometimes|numeric',
			'pincode' => 'sometimes|numeric',
			'email_id' => 'required|email',
			'padd' => 'required',
			'passport_no' => 'required|regex:/^[a-zA-Z0-9]+$/',
			'issue_date' => 'required_with:passport_no|date_format:"d-m-Y"',
			'issue_place' => 'required_with:passport_no|regex:/^[a-zA-Z]+$/u',
			'expiry_date' => 'required_with:passport_no|date_format:"d-m-Y"',
			'internation_school_exp' => 'required',
			'experience_in' => 'required',
			'experience_year' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
			'current_employer' => 'required|regex:/^[a-zA-Z0-9]+$/u',
			'designation' => 'required_with:current_employer|regex:/(^[A-Za-z0-9 ]+$)+/|max:255',
			'joindt_curr_emp' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
			'current_sal' => 'required|regex:/(^[.A-Za-z0-9 ]+$)+/',
			'expected_sal' => 'required|regex:/(^[.A-Za-z0-9 ]+$)+/',
			'language_speak' => 'required|regex:/[^,;]+[a-zA-Z]+$/u|max:255',
			'newjoin_dt' => 'required',
			'job_captcha_code2'=>'required',
		/*	'edu_statement' => 'sometimes|max:255|regex:/(^[A-Za-z0-9 ]+$)+/',
			'other_info' => 'sometimes|max:255|regex:/(^[A-Za-z0-9 ]+$)+/'	*/
		]);


		if(!empty($request->input('name')) && preg_match('/www\.|http:|https:/',$request->input('name'))){
            return redirect('apply-for-job123')->with('UrlsNotAllowedname', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('children')) && preg_match('/www\.|http:|https:/',$request->input('children'))){
            return redirect('apply-for-job123')->with('UrlsNotAllowed', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('cadd')) && preg_match('/www\.|http:|https:/',$request->input('cadd'))){
            return redirect('apply-for-job123')->with('UrlsNotAllowedcadd', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('padd')) && preg_match('/www\.|http:|https:/',$request->input('padd'))){
		   // echo 1004;die;
            return redirect('apply-for-job123')->with('UrlsNotAllowedpadd', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('language_speak')) && preg_match('/www\.|http:|https:/',$request->input('language_speak'))){
		   // echo 1007;die;
            return redirect('apply-for-job123')->with('UrlsNotAllowedLA', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('edu_statement')) && preg_match('/www\.|http:|https:/',$request->input('edu_statement'))){
            return redirect('apply-for-job123')->with('UrlsNotAllowedes', 'URLs are not allowed.') ->withInput();
		}
		if(!empty($request->input('other_info')) && preg_match('/www\.|http:|https:/',$request->input('other_info'))){
            return redirect('apply-for-job123')->with('UrlsNotAllowedOI', 'URLs are not allowed.') ->withInput();
		}


		$JobObj = new JobApplications;
		$JobObj->name = $request->input('name');
		$JobObj->dob = $request->input('dob');
		$JobObj->position = $request->input('position');
		if($request->hasFile('photo')) {
			$file = Input::file('photo');
			$timestamp = date('Ymdhi');
			$AppPic = $timestamp. '-' .$file->getClientOriginalName();
			$file->move(public_path().'/images/job-applications/',$AppPic);
			$JobObj->photo =$AppPic;
		}
		$JobObj->gender = $request->input('gender');
		$JobObj->nationality = $request->input('nationality');
		$JobObj->marital_status = $request->input('marital_status');
		$JobObj->children = $request->input('children');
		$JobObj->cadd = $request->input('cadd');
		$JobObj->padd = strtolower($request->input('padd'));
		$JobObj->pincode = strtolower($request->input('pincode'));
		$JobObj->mob_no = $request->input('mob_no');
		$JobObj->landline = $request->input('landline');
		$JobObj->email_id = strtolower($request->input('email_id'));
		$JobObj->passport_no = $request->input('passport_no');
		$JobObj->issue_date = $request->input('issue_date');
		$JobObj->issue_place = $request->input('issue_place');
		$JobObj->expiry_date = $request->input('expiry_date');
		$ExperienceIn="";
		if(!empty($request->input('experience_in'))){
    		foreach($request->input('experience_in') as $ExpIn){
    			if(isset($ExpIn) && !empty($ExpIn)){
    				$ExperienceIn.= $ExpIn.',';
    			}
    		}
		}
		$JobObj->experience_in = $ExperienceIn;
		$JobObj->experience_year = $request->input('experience_year');
		$JobObj->current_employer = $request->input('current_employer');
		$JobObj->designation = $request->input('designation');
		$JobObj->internation_school_exp = $request->input('internation_school_exp');
		$JobObj->joindt_curr_emp = $request->input('joindt_curr_emp');
		$JobObj->it_skills = $request->input('it_skills');
		$JobObj->language_speak = $request->input('language_speak');
		$JobObj->hobbies = $request->input('hobbies');
		$JobObj->current_sal = $request->input('current_sal');
		$JobObj->expected_sal = $request->input('expected_sal');
		$JobObj->newjoin_dt = $request->input('newjoin_dt');
		$JobObj->edu_statement = $request->input('edu_statement');
		$JobObj->other_info = $request->input('other_info');
		$JobObj->save();
		$JAID = $JobObj->id;


		////Educational Details
		$Educount = count($request->input('university'));
		for($i=0;$i<$Educount;$i++){
			if($request->input('university')[$i] !=""){
				$EduObj = new EducationDetails;
				$EduObj->jaid=$JAID;
				$EduObj->university = $request->input('university')[$i];
				$EduObj->board = $request->input('board')[$i];
				$EduObj->start_date = $request->input('start_date')[$i];
				$EduObj->completion_date = $request->input('completion_date')[$i];
				$EduObj->percentage = $request->input('percentage')[$i];
				$EduObj->grade = $request->input('grade')[$i];
				$EduObj->save();
			}
		}

		////Paper Reasearch
		$PRcount = count($request->input('prtitle'));
		for($i=0;$i<$PRcount;$i++){
			if($request->input('prtitle')[$i] !=""){
				$PRObj = new PaperResearch;
				$PRObj->jaid=$JAID;
				$PRObj->prtitle = $request->input('prtitle')[$i];
				$PRObj->publish_in = $request->input('publish_in')[$i];
				$PRObj->publish_date = $request->input('publish_date')[$i];
				$PRObj->presented_at = $request->input('presented_at')[$i];
				$PRObj->presentation_date = $request->input('presentation_date')[$i];
				$PRObj->save();
			}
		}

		////Professional Courses
		$PCcount = count($request->input('pattended'));
		for($i=0;$i<$PCcount;$i++){
			if($request->input('pattended')[$i] !=""){
				$PCObj = new ProfessionalCourses;
				$PCObj->jaid=$JAID;
				$PCObj->pattended = $request->input('pattended')[$i];
				$PCObj->pcourse = $request->input('pcourse')[$i];
				$PCObj->pstart_date = $request->input('pstart_date')[$i];
				$PCObj->pcompletion_date = $request->input('pcompletion_date')[$i];
				$PCObj->plocation = $request->input('plocation')[$i];
				$PCObj->save();
			}
		}

		////Professional Courses1
		$PCcount1 = count($request->input('pattended1'));
		for($i=0;$i<$PCcount1;$i++){
			if($request->input('pattended1')[$i] !=""){
				$PCObj1 = new ProfessionalCourses1;
				$PCObj1->jaid=$JAID;
				$PCObj1->pattended1 = $request->input('pattended1')[$i];
				$PCObj1->pcourse1 = $request->input('pcourse1')[$i];
				$PCObj1->pstart_date1 = $request->input('pstart_date1')[$i];
				$PCObj1->pcompletion_date1 = $request->input('pcompletion_date1')[$i];
				$PCObj1->plocation1 = $request->input('plocation1')[$i];
				$PCObj1->save();
			}
		}

		////Employement Details
		$EHcount = count($request->input('eh_name'));
		for($i=0;$i<$EHcount;$i++){
			if($request->input('eh_name')[$i] !=""){
				$EHObj = new EmploymentHistory;
				$EHObj->jaid=$JAID;
				$EHObj->eh_name = $request->input('eh_name')[$i];
				$EHObj->eh_designation = $request->input('eh_designation')[$i];
				$EHObj->period = $request->input('period')[$i];
				$EHObj->subjects = $request->input('subjects')[$i];
				$EHObj->curriculum = $request->input('curriculum')[$i];
				$EHObj->responsibility = $request->input('responsibility')[$i];
				$EHObj->leaving_reason = $request->input('leaving_reason')[$i];
				$EHObj->save();
			}
		}

		////Supervisor Reference
		$ERcount = count($request->input('ername'));
		for($i=0;$i<$ERcount;$i++){
			if($request->input('ername')[$i] !=""){
				$ERObj = new EmploymentReference;
				$ERObj->jaid=$JAID;
				$ERObj->ername = $request->input('ername')[$i];
				$ERObj->erdesignation = $request->input('erdesignation')[$i];
				$ERObj->erphone = $request->input('erphone')[$i];
				$ERObj->eremail = $request->input('eremail')[$i];
				$ERObj->eraddress = $request->input('eraddress')[$i];
				$ERObj->save();
			}
		}
		$message1= "";
		//////////////MAIL START///////////////
		$to1 = "enquiry@ecolemondiale.org";
        $subject1 = "New Job Application for ".ucwords($request->input('position'));
        $message1.= "<h3>Job application details are as follows:</h3>";
        $message1.= "<b>Name:</b>&nbsp; ".$request->input('name')."<br>";
		$message1.= "<b>DOB:</b>&nbsp; ".strtolower($request->input('dob'))."<br>";
        $message1.= "<b>Email Id:</b>&nbsp; ".$request->input('email_id')."<br>";
        $message1.= "<b>Gender:</b>&nbsp; ".$request->input('gender')."<br>";
        $message1.= "<b>Contact No:</b>&nbsp; ".$request->input('mob_no').' | '.$request->input('landline')."<br>";
        $message1.= "<b>Position Applied For:</b>&nbsp; ".$request->input('position')."<br>";
        $message1.= "<b>Address:</b>&nbsp; ".$request->input('padd')."<br>";
        $message1.= "<b>Pincode:</b>&nbsp; ".$request->input('pincode')."<br>";
        $message1.= "<b>Nationality:</b>&nbsp; ".$request->input('nationality')."<br>";
        $message1.= "<b>Passport:</b>&nbsp; ".$request->input('passport_no')."<br>";
        $message1.= "<b>Issue Date:</b>&nbsp; ".$request->input('issue_date')."<br>";
        $message1.= "<b>Expiry Date:</b>&nbsp; ".$request->input('expiry_date')."<br>";
        $message1.= "<b>Issue Place:</b>&nbsp; ".$request->input('issue_place')."<br>";
        $message1.= "<b>Marital Status:</b>&nbsp; ".$request->input('marital_status')."<br>";
        $message1.= "<b>Children:</b>&nbsp; ".$request->input('children')."<br>";
        $message1.= "<b>Current Address:</b>&nbsp; ".$request->input('cadd')."<br>";
        $message1.= "<b>Current Employer:</b>&nbsp; ".$request->input('current_employer')."<br>";
        $message1.= "<b>Designation:</b>&nbsp; ".$request->input('designation')."<br>";
        $message1.= "<b>International School Experience:</b>&nbsp; ".$request->input('internation_school_exp')."<br>";
        $message1.= "<b>Current:</b>&nbsp; ".$request->input('current_employer')."<br>";
        $message1.= "<b>Permanant Address:</b>&nbsp; ".$request->input('padd')."<br>";
        $message1.= "<b>Experience Year:</b>&nbsp; ".$request->input('experience_year')."<br>";
        $message1.= "<b>Experience In:</b>&nbsp; ".$ExperienceIn."<br>";
        $message1.= "<b>Current Joining Date:</b>&nbsp; ".$request->input('joindt_curr_emp')."<br>";
        $message1.= "<b>Skills:</b>&nbsp; ".$request->input('it_skills')."<br>";
        $message1.= "<b>Language Speaks:</b>&nbsp; ".$request->input('language_speak')."<br>";
        $message1.= "<b>Hobbies:</b>&nbsp; ".$request->input('hobbies')."<br>";
        $message1.= "<b>Current Salary:</b>&nbsp; ".$request->input('current_sal')."<br>";
        $message1.= "<b>Expected Salary:</b>&nbsp; ".$request->input('expected_sal')."<br>";
        $message1.= "<b>New Joining date:</b>&nbsp; ".$request->input('newjoin_dt')."<br>";


        if(count($request->input('university'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="7" class="text-center">Educational Details</th></tr>
			<tr>
			<th>University</th>
			<th>Board</th>
			<th>Start date</th>
			<th>Completion Date</th>
			<th>Grade</th>
			<th>Percentage</th>
			</tr>';
        for($i=0;$i<count($request->input('university'));$i++){
    		if($request->input('university')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('university')[$i].'</td>
    		        <td>'.$request->input('board')[$i].'</td>
    			    <td>'.$request->input('start_date')[$i].'</td>
    				<td>'.$request->input('completion_date')[$i].'</td>
    				<td>'.$request->input('percentage')[$i].'</td>
    			    <td>'.$request->input('grade')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('pattended'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="5" class="text-center">Professional Courses Attended</th></tr>
			<tr>
			<th>Attended</th>
			<th>Course</th>
			<th>Start date</th>
			<th>Completion Date</th>
			<th>Location</th>
			</tr>';
        for($i=0;$i<count($request->input('pattended'));$i++){
    		if($request->input('pattended')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('pattended')[$i].'</td>
    		        <td>'.$request->input('pcourse')[$i].'</td>
    			    <td>'.$request->input('pstart_date')[$i].'</td>
    				<td>'.$request->input('pcompletion_date')[$i].'</td>
    				<td>'.$request->input('plocation')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('pattended1'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="5" class="text-center">Professional Courses Conducted</th></tr>
			<tr>
			<th>Attended</th>
			<th>Course</th>
			<th>Start date</th>
			<th>Completion Date</th>
			<th>Location</th>
			</tr>';
        for($i=0;$i<count($request->input('pattended1'));$i++){
    		if($request->input('pattended1')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('pattended1')[$i].'</td>
    		        <td>'.$request->input('pcourse1')[$i].'</td>
    			    <td>'.$request->input('pstart_date1')[$i].'</td>
    				<td>'.$request->input('pcompletion_date1')[$i].'</td>
    				<td>'.$request->input('plocation1')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('prtitle'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="5" class="text-center">Paper Research</th></tr>
			<tr>
			<th>Paper/Research Title</th>
			<th>Published In</th>
			<th>Publish date</th>
			<th>Publish At</th>
			<th>Presentation Date</th>
			</tr>';
        for($i=0;$i<count($request->input('prtitle'));$i++){
    		if($request->input('prtitle')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('prtitle')[$i].'</td>
    		        <td>'.$request->input('publish_in')[$i].'</td>
    			    <td>'.$request->input('publish_date')[$i].'</td>
    				<td>'.$request->input('presented_at')[$i].'</td>
    				<td>'.$request->input('presentation_date')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }

        if(count($request->input('eh_name'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="6" class="text-center">Employment History</th></tr>
			<tr>
			<th>Employer Name</th>
			<th>Designation</th>
			<th>Period</th>
			<th>Subjects</th>
			<th>Curriculum</th>
			<th>Resposibility</th>
			<th>Leaving Reason</th>
			</tr>';
        for($i=0;$i<count($request->input('eh_name'));$i++){
    		if($request->input('eh_name')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('eh_name')[$i].'</td>
    		        <td>'.$request->input('eh_designation')[$i].'</td>
    			    <td>'.$request->input('period')[$i].'</td>
    				<td>'.$request->input('subjects')[$i].'</td>
    				<td>'.$request->input('curriculum')[$i].'</td>
    				<td>'.$request->input('responsibility')[$i].'</td>
    				<td>'.$request->input('leaving_reason')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }


        if(count($request->input('ername'))>0)
        {
		$message1.='<table width="100%" border="1" style="border:1px solid;padding-top:2px;">
			<tr><th colspan="6" class="text-center">References</th></tr>
			<tr>
			<th>Name</th>
			<th>Designation</th>
			<th>Phone No.</th>
			<th>Email</th>
			<th>Address</th>
			</tr>';
        for($i=0;$i<count($request->input('ername'));$i++){
    		if($request->input('ername')[$i] !=""){
    		$message1.='<tr>
        			<td>'.$request->input('ername')[$i].'</td>
    		        <td>'.$request->input('erdesignation')[$i].'</td>
    			    <td>'.$request->input('erphone')[$i].'</td>
    				<td>'.$request->input('eremail')[$i].'</td>
    				<td>'.$request->input('eraddress')[$i].'</td>
    				</tr>';
    			}
    		}
            $message1.='</table><br>';
	    }


	    $message1.= "<b>Personal Education Statement:</b><br> ".$request->input('edu_statement')."<br>";
	    $message1.= "<b>Other  Inforamtion:</b><br> ".$request->input('other_info')."<br>";


        $header1 = "From:noreply@ecolmondiale.org \r\n";
        $header1.= "MIME-Version: 1.0\r\n";
        $header1.= "Content-type: text/html\r\n";
        $subject2 = "Ecolemondiale.org - Thank you for applying job - ".ucwords($request->input('position'));
        mail ($request->input('email_id'),$subject2,$message1,$header1);
		//$to2 = "sohail@matrixbricks.com";
        //mail ($to2,$subject1,$message1,$header1);
		//////////////MAIL END////////////////

		$request->session()->flash('alert-success', 'Thank You for applying job, we will contact you soon');
		return redirect('thankyou');


	}



}
