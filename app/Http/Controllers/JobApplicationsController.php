<?php

namespace App\Http\Controllers;
use Auth;
use App\JobApplications; //iNCLUDE MODAL
use App\EducationDetails; //iNCLUDE MODAL
use App\ProfessionalCourses; //iNCLUDE MODAL
use App\PaperResearch; //iNCLUDE MODAL
use App\EmploymentHistory; //iNCLUDE MODAL
use App\EmploymentReference; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use File;
use Crypt; //For Encrption-Decryption

class JobApplicationsController extends Controller
{
	
	
    public function index(){
        
				$Data = DB::table('job_applications')
			//	->leftJoin('eduacation_details','eduacation_details.jaid','=','job_applications.id')
				// ->leftJoin('paper_research','paper_research.jaid','=','job_applications.id')
				// ->leftJoin('professional_courses','professional_courses.jaid','=','job_applications.id')
				// ->leftJoin('employment_history','employment_history.jaid','=','job_applications.id')
				->orderBy('job_applications.id','desc')
				// ->groupBy('job_applications.id')
				->get();
				
				return view('job-applications.index',compact('Data'));
		
    }
	
	public function view($id){
				$id = Crypt::decrypt($id);
				$Data = DB::table('job_applications')
			//	->leftJoin('eduacation_details','eduacation_details.jaid','=','job_applications.id')
				->leftJoin('paper_research','paper_research.jaid','=','job_applications.id')
				->leftJoin('professional_courses','professional_courses.jaid','=','job_applications.id')
				->where('job_applications.id', '=', $id)
				->get();
				
				$professional_courses = DB::table('professional_courses')
				->where('professional_courses.jaid', '=', $id)
				->get();
				
				$EduacationalDetails = DB::table('eduacation_details')
				->where('eduacation_details.jaid', '=', $id)
				->get();
				
				$PaperResearch = DB::table('paper_research')
				->where('paper_research.jaid', '=', $id)
				->get();
				
				$employment_history = DB::table('employment_history')
				->where('employment_history.jaid', '=', $id)
				->get();
				
				return view('job-applications/view',compact('Data','EduacationalDetails','professional_courses','PaperResearch','employment_history'));
	}
	
	public function addjob(Request $request){
				$Jobs = new Jobs;
				if($request->hasFile('thumbnail')) {
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('thumbnail')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					$thumbnail = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/jobs/',$thumbnail);
					$Jobs->thumbnail =$thumbnail;
				}
				$Jobs->status =stripslashes($request->input('status'));
				$Jobs->title =stripslashes($request->input('title'));
				$Jobs->short_desc =stripslashes($request->input('short_desc'));
				$Jobs->brief_desc =stripslashes($request->input('brief_desc'));
				$Jobs->save();
				$request->session()->flash('alert-success', 'Job added successfully!');
				return redirect('jobs');
	}	
	
	
	public function jobsstatus(Request $request){
			if ($request->ajax()) {
				$Jobs = new Jobs;
				$Data = array(
				'status' => $request->input('status'),
				);
				$Jobs->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
	}

	
	public function edit($id){
				$id = Crypt::decrypt($id);
				$Data = DB::table('job_applications')->where('job_applications.id', '=', $id)->get();
				
				$professional_courses = DB::table('professional_courses')
				->where('professional_courses.jaid', '=', $id)
				->get();
				
				$EduacationalDetails = DB::table('eduacation_details')
				->where('eduacation_details.jaid', '=', $id)
				->get();
				
				$PaperResearch = DB::table('paper_research')
				->where('paper_research.jaid', '=', $id)
				->get();
				
				$employment_history = DB::table('employment_history')
				->where('employment_history.jaid', '=', $id)
				->get();
				
				return view('job-applications/edit',compact('Data','EduacationalDetails','professional_courses','PaperResearch','employment_history'));
	}		
	
	public function updatejobapplication(Request $request){	
				$JAObj = new JobApplications;
				$JAData = array(
				'name' => $request->input('name'),
				'designation' => $request->input('designation'),
				'experience_year' => $request->input('experience_year'),
				'experience_in' => $request->input('experience_in'),
				'internation_school_exp' => $request->input('internation_school_exp'),
				'current_employer' => $request->input('current_employer'),
				'cadd' => $request->input('cadd'),
				'padd' => $request->input('padd'),
				'mob_no' => $request->input('mob_no'),
				'fax_no' => $request->input('fax_no'),
				'email_id' => $request->input('email_id'),
				'pincode' => $request->input('pincode'),
				'passport_no' => $request->input('passport_no'),
				'issue_date' => $request->input('issue_date'),
				'expiry_date' => $request->input('expiry_date'),
				'children' => $request->input('children'),
				);

				$NLData = DB::table('job_applications')->where('id', '=',$request->input('id'))->get();
				if($request->hasFile('photo') == 1) {
					if(!empty($NLData[0]->photo) || $NLData[0]->photo !="" || $NLData[0]->photo !=NULL){
						if(File::exists(public_path().'/images/job-applications/'.$NLData[0]->photo)){
							$Unlink = unlink(public_path().'/images/job-applications/'.$NLData[0]->photo);
						}
					}
					$file = Input::file('photo');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('photo')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					$photo = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/job-applications/',$photo);
					$JAData['photo']=$photo;
				}else{
					$JAData['photo']= $NLData[0]->photo;
				}
				
				$JAObj->where('id',$request->input('id'))->update($JAData);
				
				
				for($k=1; $k<=$request->input('kcount'); $k++){
					$EDObj = new EducationDetails;
					$EDData = array(
					'university' => $request->input('university'.$k),
					'board' => $request->input('board'.$k),
					'start_date' => $request->input('start_date'.$k),
					'completion_date' => $request->input('completion_date'.$k),
					'grade' => $request->input('grade'.$k),
					'percentage' => $request->input('percentage'.$k),
					);
					$EDObj->where('eid',$request->input('eid'.$k))->update($EDData);
				}
				
				for($l=1; $l<=$request->input('lcount'); $l++){
					$PCObj = new ProfessionalCourses;
					$PCData = array(
					'pattended' => $request->input('pattended'.$l),
					'pcourse' => $request->input('pcourse'.$l),
					'pstart_date' => $request->input('pstart_date'.$l),
					'pcompletion_date' => $request->input('pcompletion_date'.$l),
					'plocation' => $request->input('plocation'.$l),
					);
					$PCObj->where('pid',$request->input('pid'.$l))->update($PCData);
				}
				
				for($m=1; $m<=$request->input('mcount'); $m++){
					$PRObj = new PaperResearch;
					$PRData = array(
					'prtitle' => $request->input('prtitle'.$m),
					'publish_in' => $request->input('publish_in'.$m),
					'publish_date' => $request->input('publish_date'.$m),
					'presented_at' => $request->input('presented_at'.$m),
					'presentation_date' => $request->input('presentation_date'.$m),
					);
					$PRObj->where('prid',$request->input('prid'.$m))->update($PRData);
				}
				
				for($n=1; $n<=$request->input('ncount'); $n++){
					$EHObj = new EmploymentHistory;
					$EHData = array(
					'eh_designation' => $request->input('eh_designation'.$n),
					'period' => $request->input('period'.$n),
					'subjects' => $request->input('subjects'.$n),
					'curriculum' => $request->input('curriculum'.$n),
					'responsibility' => $request->input('responsibility'.$n),
					'leaving_reason' => $request->input('leaving_reason'.$n),
					);
					$EHObj->where('ehid',$request->input('ehid'.$n))->update($EHData);
				}
				
				$request->session()->flash('alert-success', 'Job application updated successfully!');
				return redirect('job-applications');
	}
	
		
	
	public function delete($id){
				$id = Crypt::decrypt($id);
				
				$NLData = DB::table('job_applications')->where('id', '=',$id)->get();
					if(!empty($NLData[0]->photo) || $NLData[0]->photo !="" || $NLData[0]->photo !=NULL){
						if(File::exists(public_path().'/images/job-applications/'.$NLData[0]->photo)){
							$Unlink = unlink(public_path().'/images/job-applications/'.$NLData[0]->photo);
						}
					}
				DB::table('job_applications')->where('id', '=', $id)->delete();
				DB::table('eduacation_details')->where('jaid', '=', $id)->delete();
				DB::table('professional_courses')->where('jaid', '=', $id)->delete();
				DB::table('paper_research')->where('jaid', '=', $id)->delete();
				DB::table('employment_history')->where('jaid', '=', $id)->delete();
				
				session()->flash('alert-success', 'Job has been deleted!');
				return redirect('job-applications');
	
	}

}
