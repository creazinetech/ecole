<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\IndexSliders; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class SlidersController extends Controller
{
	
    public function indexsliders(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','2')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$IndexSliders = DB::table('index_sliders')->get();
				return view('index-sliders',compact('IndexSliders'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function addissliders(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','2')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$IndexSliders = new IndexSliders;
				if($request->hasFile('is_img')) {
					$file = Input::file('is_img');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$is_img = $rand.$timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/slider-images/',$is_img);
					$IndexSliders->is_img =$is_img;
				}
				$IndexSliders->is_status ='Active';
				$IndexSliders->is_title1 =stripslashes($request->input('is_title1'));
				$IndexSliders->is_title2 =stripslashes($request->input('is_title2'));
				$IndexSliders->save();
				$request->session()->flash('alert-success', 'Images uploaded successfully!');
				return redirect('index-sliders');
			}else{
				return view('unauthorised');
			}
		}
	}
	
	public function isstatus(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$IndexSliders = new IndexSliders;
				$Data = array(
				'is_status' => $request->input('status'),
				);
				$IndexSliders->where('is_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function isorder(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$IndexSliders = new IndexSliders;
				$Data = array(
				'is_order' => $request->input('order'),
				);
				$IndexSliders->where('is_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function isdelete($id){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','2')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$is_id = Crypt::decrypt($id);
				DB::table('index_sliders')->where('is_id', '=', $is_id)->delete();
				session()->flash('alert-success', 'Image has been deleted!');
				return redirect('index-sliders');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	
}
