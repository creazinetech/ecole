<?php

namespace App\Http\Controllers;
use Auth;
use App\Admissions; //iNCLUDE MODAL
use App\ContactEnquiry; //iNCLUDE MODAL
use App\Orders; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class AdmissionController extends Controller
{

    public function index(){
			$Sliders = DB::table('index_sliders')->where('is_status','=','Active')->get();
			return view('index',compact('Sliders'));
    }

    public function messagefromprincipal(){
			return view('message-from-the-principal');
    }

    public function schoolcalendar(){
			return view('school-calendar');
    }


	public function sendasenquiry(Request $request){
		if ($request->ajax()) {
		   // echo '<pre>'.print_r($_GET).'</pre>';die;

    /*	    $ip = $_SERVER['REMOTE_ADDR'];
            $secretkey = '6LfniTsUAAAAABekDJEWHvzFX6mnSwc-0L1zksZQ';
            $captcha = $request->input('g-recaptcha-response');
            $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;
            $ch = curl_init($api_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $obj = json_decode($response);
            if($obj->success == true) {
    */
           /*     $Admissions = new Admissions;
        		$Admissions->name =stripslashes($request->input('name'));
        		$Admissions->dob =stripslashes($request->input('dob'));
        		$Admissions->address =stripslashes($request->input('address'));
        		$Admissions->nopskul =stripslashes($request->input('nopskul'));
        		$Admissions->grade =stripslashes($request->input('grade'));
        		$Admissions->gradetjoin =stripslashes($request->input('gradetjoin'));
        		$Admissions->year =stripslashes($request->input('year'));
        		$Admissions->nparents =stripslashes($request->input('nparents'));
        		$Admissions->designation =stripslashes($request->input('designation'));
        		$Admissions->organization =stripslashes($request->input('organization'));
        		$Admissions->occupation =stripslashes($request->input('occupation'));
        		$Admissions->offadd =stripslashes($request->input('offadd'));
        		$Admissions->city =stripslashes($request->input('city'));
        		$Admissions->state =stripslashes($request->input('state'));
        		$Admissions->country =stripslashes($request->input('country'));
        		$Admissions->pincode =stripslashes($request->input('pincode'));
        		$Admissions->restel =stripslashes($request->input('restel'));
        		$Admissions->offtel =stripslashes($request->input('offtel'));
        		$Admissions->mnfather =stripslashes($request->input('mnfather'));
        		$Admissions->mnmother =stripslashes($request->input('mnmother'));
        		$Admissions->emailf =stripslashes($request->input('emailf'));
        		$Admissions->emailm =stripslashes($request->input('emailm'));
        		$Admissions->passport =stripslashes($request->input('passport'));
        		$Admissions->ourskul =stripslashes($request->input('ourskul'));
        	    $Admissions->save();
        	*/
		$CEObj = new ContactEnquiry;
		$CEObj->name = ucwords($request->input('name'));
		$CEObj->dob = date('Y-m-d',strtotime($request->input('dob')));
		$CEObj->address = $request->input('address');
		$CEObj->nopskul = ucwords($request->input('nopskul'));
		$CEObj->grade = ucwords($request->input('grade'));
		$CEObj->gradetjoin = ucwords($request->input('gradetjoin'));
		$CEObj->year = $request->input('year');
		$CEObj->city = ucwords($request->input('city'));
		$CEObj->state = ucwords($request->input('state'));
		$CEObj->country = ucwords($request->input('country'));
		$CEObj->pincode = $request->input('pincode');
		$CEObj->whattoknow = $request->input('whattoknow');
		$CEObj->aboutschool = $request->input('ourskul');
		$CEObj->nationality = ucwords($request->input('nationality'));

		$CEObj->fname = ucwords($request->input('fname'));
		$CEObj->foccupation = ucwords($request->input('foccupation'));
		$CEObj->fdesignation = ucwords($request->input('fdesignation'));
		$CEObj->forganization = ucwords($request->input('forganization'));
		$CEObj->foffadd = ucwords($request->input('foffadd'));
		$CEObj->fofftel = $request->input('fofftel');
		$CEObj->fmobile = $request->input('fmobile');
		$CEObj->frestel = $request->input('frestel');
		$CEObj->femail = strtolower($request->input('femail'));

		$CEObj->mname = ucwords($request->input('mname'));
		$CEObj->moccupation = ucwords($request->input('moccupation'));
		$CEObj->mdesignation = ucwords($request->input('mdesignation'));
		$CEObj->morganization = ucwords($request->input('morganization'));
		$CEObj->moffadd = ucwords($request->input('moffadd'));
		$CEObj->mofftel = $request->input('mofftel');
		$CEObj->mmobile = $request->input('mmobile');
		$CEObj->mrestel = $request->input('mrestel');
		$CEObj->memail = strtolower($request->input('memail'));

			$CEObj->save();

    	    //Email
         	$subject = "Admission Form Enquiry - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
 			$message='<html>
				<head>
				<title>Admission Form Enquiry Details</title>
				</head>
				<body style="padding-left:20%;padding-right:20%;">
				<h3>Admission form details are as follows:</h3>
				<table width="100%">
					<tr>
					<td width="">
					<table width="100%" style="border:1px solid;padding-top:2px;">
					<tr>
					<td width="10%">
					<center>
					<img src="http://219.90.67.77/~ecolemondiale/admin_ecole/public/images/EM.png">
					</center>
					</td>
					<td width="">
					<center>
					<h2>École Mondiale World School</h2>
					<h3><u>STUDENT ADMISSION FORM</u></h3>
					</center>
					</td>
					<td width="10%">
					<center>
					<img src="http://219.90.67.77/~ecolemondiale/admin_ecole/public/images/IB.png">
					</center>
					</td>
					</tr>
					</table>
					</td>
					</tr>
						<tr>
						<td class="td-space">
						<strong>1) Student Name:</strong>'.ucwords($request->input('name')).'</td></tr>
						<tr><td class="td-space">
						<strong>2) Date of Birth:</strong>'.date('d F Y',strtotime($request->input('dob'))).'</td></tr>
						<tr><td class="td-space">
						<strong>3) Mobile No:</strong>'.$request->input('mobile').'</td></tr>
						<tr><td class="td-space">
						<strong>4) Resedential Telephone:</strong>'.$request->input('restel').'</td></tr>
						<tr><td class="td-space">
						<strong>5) Residential Address (Permanent):</strong>'.ucwords($request->input('address')).'</td></tr>
						<tr><td><strong>6) City:</strong>'.ucwords($request->input('city')).'
							&nbsp; <strong>State:</strong>'.ucwords($request->input('state')).'
							&nbsp; <strong>Country:</strong>'.ucwords($request->input('country')).'
						</td></tr>
						<tr><td><strong>7) Nationality:</strong>'.ucwords($request->input('nationality')).'</td></tr>
						<tr><td><strong>8) Name of Present School:</strong>'.ucwords($request->input('nopskul')).'</td></tr>
						<tr><td><strong>9) Current Grade:</strong>'.ucwords($request->input('grade')).'</td></tr>
						<tr><td><strong>10) Grade to Join in:</strong>'.ucwords($request->input('gradetjoin')).' In '.$request->input('year').'</td></tr>
						<tr>
						<td><strong>11) Parent Details:</strong>
							<table width="100%" border="1" style="margin-top:5px;margin-bottom:5px;">
								<thead>
								<tr>
									<th width="20%">Details</th>
									<th width="40%">Father</th>
									<th width="40%">Mother</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th class="td-space">Name:</th>
									<td>';
									if(!empty($request->input('fname'))){
									$message.=''.ucwords($request->input('fname'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mname'))){
									$message.=''.ucwords($request->input('mname'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Mobile No:</th>
									<td>';
									if(!empty($request->input('fmobile'))){
									$message.=''.ucwords($request->input('fmobile'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mmobile'))){
									$message.=''.ucwords($request->input('mmobile'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Residential Tel. No:</th>
									<td>';
									if(!empty($request->input('frestel'))){
									$message.=''.ucwords($request->input('frestel'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mrestel'))){
									$message.=''.ucwords($request->input('mrestel'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Occupation:</th>
									<td>';
									if(!empty($request->input('foccupation'))){
									$message.=''.ucwords($request->input('foccupation'));
									}
									$message.='</td><td>';
									if(!empty($request->input('moccupation'))){
									$message.=''.ucwords($request->input('moccupation'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Designation:</th>
									<td>';
									if(!empty($request->input('fdesignation'))){
									$message.=''.ucwords($request->input('fdesignation'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mdesignation'))){
									$message.=''.ucwords($request->input('mdesignation'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Name of the Organization:</th>
									<td>';
									if(!empty($request->input('forganization'))){
									$message.=''.ucwords($request->input('forganization'));
									}
									$message.='</td><td>';
									if(!empty($request->input('morganization'))){
									$message.=''.ucwords($request->input('morganization'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Official Address:</th>
									<td>';
									if(!empty($request->input('foffadd'))){
									$message.=''.ucwords($request->input('foffadd'));
									}
									$message.='</td><td>';
									if(!empty($request->input('foffadd'))){
									$message.=''.ucwords($request->input('foffadd'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Offic Tel:</th>
									<td>';
									if(!empty($request->input('fofftel'))){
									$message.=''.ucwords($request->input('fofftel'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mofftel'))){
									$message.=''.ucwords($request->input('mofftel'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Email:</th>
									<td>';
									if(!empty($request->input('femail'))){
									$message.=''.ucwords($request->input('femail'));
									}
									$message.='</td><td>';
									if(!empty($request->input('memail'))){
									$message.=''.ucwords($request->input('memail'));
									}
									$message.='</td>
								</tr>
								</tbody>
							</table>

						</td>
						</tr>
						<tr>
						<td><strong>10) What do you wish to know:</strong>'.ucwords($request->input('whattoknow')).'</td>
						</tr>
						<tr>
						<td><strong>11) From where did you hear about our School:</strong>'.ucwords($request->input('ourskul')).'</td>
						</tr>
				</table></body></html>';
            mail ($to1,$subject,$message,$headers);
            mail ($to,$subject,$message,$headers);
    	    //Email

        	    if(!empty($CEObj->id)){
        	        echo 'success';die;

        	    }else{
        	         echo 'failed';die;
        	    }

           /*  }else{
                echo 'captcha-failed';die;
             }*/

		}
	}


	public function addadmissions(Request $request){
	    //echo $request->input('g-recaptcha-response');die;
	   // var_dump($_POST);die;

	    //Validation Start//
        $errors = $this->validate($request, [
        	'name' => 'required',
        	'dob' => 'required',
        	'address' => 'required',
        	'nopskul' => 'required',
        	'grade' => 'required',
        	'gradetjoin' => 'required',
        	'year' => 'required',
        	'city' => 'required',
        	'state' => 'required',
        	'nationality' => 'required',
        	'country' => 'required',
        	'pincode' => 'required|numeric',

        	'fname' => 'required',
        	'foccupation' => 'required',
        	'fdesignation' => 'required',
        	'forganization' => 'required',
        	'foffadd' => 'required',
        	'fofftel' => 'required|numeric',
        	'femail' => 'required|email',
        	'fmobile' => 'required|numeric',
        	'frestel' => 'required|numeric',

        	'passport' => 'required',
        	'ourskul' => 'required',
        	'whattoknow' => 'required',
        //	'g-recaptcha-response' => 'required',
        ]);
        //Validation End//


     /*   $ip = $_SERVER['REMOTE_ADDR'];
        $secretkey = '6LfniTsUAAAAABekDJEWHvzFX6mnSwc-0L1zksZQ';
        $captcha = $request->input('g-recaptcha-response');
        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;

        $ch = curl_init($api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $obj = json_decode($response);
        if($obj->success == true) {	   */


    		$CEObj = new Admissions;
			$CEObj->name = ucwords($request->input('name'));
			$CEObj->dob = date('Y-m-d',strtotime($request->input('dob')));
			$CEObj->address = $request->input('address');
			$CEObj->nopskul = ucwords($request->input('nopskul'));
			$CEObj->grade = ucwords($request->input('grade'));
			$CEObj->gradetjoin = ucwords($request->input('gradetjoin'));
			$CEObj->year = $request->input('year');
			$CEObj->city = ucwords($request->input('city'));
			$CEObj->state = ucwords($request->input('state'));
			$CEObj->country = ucwords($request->input('country'));
			$CEObj->pincode = $request->input('pincode');
			$CEObj->passport = $request->input('passport');
			$CEObj->whattoknow = $request->input('whattoknow');
			$CEObj->ourskul = $request->input('ourskul');
			$CEObj->nationality = ucwords($request->input('nationality'));

			$CEObj->fname = ucwords($request->input('fname'));
			$CEObj->foccupation = ucwords($request->input('foccupation'));
			$CEObj->fdesignation = ucwords($request->input('fdesignation'));
			$CEObj->forganization = ucwords($request->input('forganization'));
			$CEObj->foffadd = ucwords($request->input('foffadd'));
			$CEObj->fofftel = $request->input('fofftel');
			$CEObj->fmobile = $request->input('fmobile');
			$CEObj->frestel = $request->input('frestel');
			$CEObj->femail = strtolower($request->input('femail'));

			$CEObj->mname = ucwords($request->input('mname'));
			$CEObj->moccupation = ucwords($request->input('moccupation'));
			$CEObj->mdesignation = ucwords($request->input('mdesignation'));
			$CEObj->morganization = ucwords($request->input('morganization'));
			$CEObj->moffadd = ucwords($request->input('moffadd'));
			$CEObj->mofftel = $request->input('mofftel');
			$CEObj->memail = strtolower($request->input('memail'));
			$CEObj->mmobile = $request->input('mmobile');
			$CEObj->mrestel = $request->input('mrestel');
    	    $CEObj->save();

    	    $AdmissionId = $CEObj->id;


    	    //Email
         	$subject = "Admission Form - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
			$message='<html>
				<head>
				<title>Admission Form Details</title>
				</head>
				<body style="padding-left:20%;padding-right:20%;">
				<h3>Admission form details are as follows:</h3>
				<table width="100%">
					<tr>
					<td width="">
					<table width="100%" style="border:1px solid;padding-top:2px;">
					<tr>
					<td width="10%">
					<center>
					<img src="http://219.90.67.77/~ecolemondiale/admin_ecole/public/images/EM.png">
					</center>
					</td>
					<td width="">
					<center>
					<h2>École Mondiale World School</h2>
					<h3><u>STUDENT ADMISSION FORM</u></h3>
					</center>
					</td>
					<td width="10%">
					<center>
					<img src="http://219.90.67.77/~ecolemondiale/admin_ecole/public/images/IB.png">
					</center>
					</td>
					</tr>
					</table>
					</td>
					</tr>
						<tr>
						<td class="td-space">
						<strong>1) Student Name:</strong>'.ucwords($request->input('name')).'</td></tr>
						<tr><td class="td-space">
						<strong>2) Date of Birth:</strong>'.date('d F Y',strtotime($request->input('dob'))).'</td></tr>
						<tr><td class="td-space">
						<strong>3) Residential Address (Permanent):</strong>'.ucwords($request->input('address')).'</td></tr>
						<tr><td><strong>4) City:</strong>'.ucwords($request->input('city')).'
							&nbsp; <strong>State:</strong>'.ucwords($request->input('state')).'
							&nbsp; <strong>Country:</strong>'.ucwords($request->input('country')).'
						</td></tr>
						<tr><td><strong>5) Nationality:</strong>'.ucwords($request->input('nationality')).'</td></tr>
						<tr><td><strong>6) Name of Present School:</strong>'.ucwords($request->input('nopskul')).'</td></tr>
						<tr><td><strong>7) Current Grade:</strong>'.ucwords($request->input('grade')).'</td></tr>
						<tr><td><strong>8) Grade to Join in:</strong>'.ucwords($request->input('gradetjoin')).' In '.$request->input('year').'</td></tr>
						<tr>
						<td><strong>9) Parent Details:</strong>
							<table width="100%" border="1" style="margin-top:5px;margin-bottom:5px;">
								<thead>
								<tr>
									<th width="20%">Details</th>
									<th width="40%">Father</th>
									<th width="40%">Mother</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th class="td-space">Name:</th>
									<td>';
									if(!empty($request->input('fname'))){
									$message.=''.ucwords($request->input('fname'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mname'))){
									$message.=''.ucwords($request->input('mname'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Mobile No:</th>
									<td>';
									if(!empty($request->input('fmobile'))){
									$message.=''.ucwords($request->input('fmobile'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mmobile'))){
									$message.=''.ucwords($request->input('mmobile'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Residential Tel. No:</th>
									<td>';
									if(!empty($request->input('frestel'))){
									$message.=''.ucwords($request->input('frestel'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mrestel'))){
									$message.=''.ucwords($request->input('mrestel'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Occupation:</th>
									<td>';
									if(!empty($request->input('foccupation'))){
									$message.=''.ucwords($request->input('foccupation'));
									}
									$message.='</td><td>';
									if(!empty($request->input('moccupation'))){
									$message.=''.ucwords($request->input('moccupation'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Designation:</th>
									<td>';
									if(!empty($request->input('fdesignation'))){
									$message.=''.ucwords($request->input('fdesignation'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mdesignation'))){
									$message.=''.ucwords($request->input('mdesignation'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Name of the Organization:</th>
									<td>';
									if(!empty($request->input('forganization'))){
									$message.=''.ucwords($request->input('forganization'));
									}
									$message.='</td><td>';
									if(!empty($request->input('morganization'))){
									$message.=''.ucwords($request->input('morganization'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Official Address:</th>
									<td>';
									if(!empty($request->input('foffadd'))){
									$message.=''.ucwords($request->input('foffadd'));
									}
									$message.='</td><td>';
									if(!empty($request->input('foffadd'))){
									$message.=''.ucwords($request->input('foffadd'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Offic Tel:</th>
									<td>';
									if(!empty($request->input('fofftel'))){
									$message.=''.ucwords($request->input('fofftel'));
									}
									$message.='</td><td>';
									if(!empty($request->input('mofftel'))){
									$message.=''.ucwords($request->input('mofftel'));
									}
									$message.='</td>
								</tr>
								<tr>
									<th class="td-space">Email:</th>
									<td>';
									if(!empty($request->input('femail'))){
									$message.=''.ucwords($request->input('femail'));
									}
									$message.='</td><td>';
									if(!empty($request->input('memail'))){
									$message.=''.ucwords($request->input('memail'));
									}
									$message.='</td>
								</tr>
								</tbody>
							</table>

						</td>
						</tr>
						<tr>
						<td><strong>10) What do you wish to know:</strong>'.ucwords($request->input('whattoknow')).'</td>
						</tr>
						<tr>
						<td><strong>11) From where did you hear about our School:</strong>'.ucwords($request->input('ourskul')).'</td>
						</tr>
				</table></body></html>';
           // mail ($to1,$subject,$message,$headers);
           // mail ($to,$subject,$message,$headers);


    		$Orders = DB::table('orders')->where('ordercode','!=','')->orderBy('orderid','desc')->take(1)->get();
    		//echo count($Orders);die;
    		if(count($Orders) == 0){
    			$NewOrderCode =1;
    		}else{
    		    //$xplode = explode('-',$Orders[0]->ordercode);
    			//$OldOrderCode = $xplode[1]+1;
    			//$NewOrderCode = "ecolem-".$OldOrderCode;
    			$NewOrderCode = $Orders[0]->ordercode+1;
    		}
    		if($request->input('passport') == 'Indian Passport'){ $Amount=10000;}
    		if($request->input('passport') == 'Non-Indian Passport'){ $Amount=16000;}

    		$Data = array(
    			'ordercode' => $NewOrderCode,
    			'AdmissionId'=>$AdmissionId,
    			'amount' => $Amount,
    			'name' => $request->input('name'),
    			'dob' => $request->input('dob'),
    			'address' => $request->input('address'),
    			'nopskul' => $request->input('nopskul'),
    			'grade' => $request->input('grade'),
    			'gradetjoin' => $request->input('gradetjoin'),
    			'year' => $request->input('year'),
    			'nparents' => $request->input('fname'),
    			'designation' => $request->input('fdesignation'),
    			'organization' => $request->input('forganization'),
    			'occupation' => $request->input('foccupation'),
    			'offadd' => $request->input('foffadd'),
    			'city' => $request->input('city'),
    			'state' => $request->input('state'),
    			'country' => $request->input('country'),
    			'pincode' => $request->input('pincode'),
    			'restel' => $request->input('frestel'),
    			'mobile' => $request->input('fmobile'),
    			'emailf' => strtolower($request->input('femail')),
    			'emailm' => strtolower($request->input('memail')),
    			'passport' => $request->input('passport'),
    			'ourskul' => $request->input('ourskul'),
    			'offtel' => $request->input('offtel'),
    			);
    		    return view('proceed-to-payment',compact('Data'));


	   /* }else{
            $VeriFailed = 'Captcha verification failed';
            return redirect('apply-for-admission')->with('CaptchaVerificaion', 'Captcha Verification Failed.') ->withInput();
	    }  */
	}

	public function checkout(Request $request){

        $ip = $_SERVER['REMOTE_ADDR'];
        $secretkey = '6LfniTsUAAAAABekDJEWHvzFX6mnSwc-0L1zksZQ';
        $captcha = $request->input('g-recaptcha-response');
        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip;
        /**
        $response = file_get_contents($api_url);
        /**/
        $ch = curl_init($api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $obj = json_decode($response);
        if($obj->success == true) {
        // if($request->input('AdmissionId') !='') {

    		$Orders = new Orders;
    		$Orders->admissionid =stripslashes($request->input('AdmissionId'));
    		$Orders->ordercode =stripslashes($request->input('order_id'));
    		$Orders->amount =stripslashes($request->input('amount'));
    		$Orders->billname =stripslashes($request->input('billing_name'));
    		$Orders->billaddress =stripslashes($request->input('billing_address'));
    		$Orders->billcountry =stripslashes($request->input('billing_country'));
    		$Orders->billstate =stripslashes($request->input('billing_state'));
    		$Orders->billcity =stripslashes($request->input('billing_city'));
    		$Orders->billpincode =stripslashes($request->input('billing_zip'));
    		$Orders->billtelephone =stripslashes($request->input('billing_tel'));
    		$Orders->billemail =strtolower(stripslashes($request->input('billing_email')));
    		$Orders->shipname =stripslashes($request->input('delivery_name'));
    		$Orders->shipaddress =stripslashes($request->input('delivery_address'));
    		$Orders->shipcountry =stripslashes($request->input('delivery_country'));
    		$Orders->shipstate =stripslashes($request->input('delivery_state'));
    		$Orders->shipcity =stripslashes($request->input('delivery_city'));
    		$Orders->shippincode =stripslashes($request->input('delivery_zip'));
    		$Orders->shiptelephone =stripslashes($request->input('shiptelephone'));
    		$Orders->deliverynotes =stripslashes($request->input('deliverynotes'));
    		$Orders->admission_date =date('Y-m-d');

    		$Orders->save();
    		$Orderid = $Orders->id;

    		$working_key='392DDCC8A10D81185E3F03EC15ABDF01';//Shared by CCAVENUES
    		$access_code='6J2BHM5KCK1HU8JN';//Shared by CCAVENUES
    		$merchant_id='11278';//Shared by CCAVENUES

    		//$checksum=$this->getchecksum($merchant_id,$request->input('amount'),stripslashes($request->input('ordercode')),stripslashes($request->input('Redirect_Url')),$working_key); // Method to generate checksum

    	//	$merchant_data= 'Merchant_Id='.$merchant_id.'&Amount='.$request->input('amount').'&Order_Id='.stripslashes($request->input('ordercode')).'&Redirect_Url='.stripslashes($request->input('Redirect_Url')).'&billing_cust_name='.stripslashes($request->input('billname')).'&billing_cust_address='.stripslashes($request->input('billaddress')).'&billing_cust_country='.stripslashes($request->input('billcountry')).'&billing_cust_state='.stripslashes($request->input('billstate')).'&billing_cust_city='.stripslashes($request->input('billcity')).'&billing_zip_code='.stripslashes($request->input('billpincode')).'&billing_cust_tel='.stripslashes($request->input('billtelephone')).'&billing_cust_email='.strtolower(stripslashes($request->input('billemail'))).'&delivery_cust_name='.stripslashes($request->input('shipname')).'&delivery_cust_address='.stripslashes($request->input('shipaddress')).'&delivery_cust_country='.stripslashes($request->input('shipcountry')).'&delivery_cust_state='.stripslashes($request->input('shipstate')).'&delivery_cust_city='.stripslashes($request->input('shipcity')).'&delivery_zip_code='.stripslashes($request->input('shippincode')).'&delivery_cust_tel='.stripslashes($request->input('shiptelephone')).'&billing_cust_notes='.stripslashes($request->input('deliverynotes'));
             $merchant_data='merchant_id='.urlencode($merchant_id).'&';
            foreach ($_POST as $key => $value){
    		$merchant_data.=$key.'='.urlencode($value).'&';
        	}

    	    $merchant_data1="tid=".$request->input('tid')."&merchant_id=11278&order_id=".$request->input('order_id')."&amount=".$request->input('amount')."&currency=".$request->input('currency')."&redirect_url=".$request->input('redirect_url')."&cancel_url=".$request->input('cancel_url')."&language=EN&billing_name=".$request->input('billing_name')."&billing_address=".$request->input('billing_address')."&billing_city=".$request->input('billing_city')."&billing_state=".$request->input('billing_state')."&billing_zip=".$request->input('billing_zip')."&billing_country=".$request->input('billing_country')."&billing_tel=".$request->input('billing_tel')."&billing_email=".$request->input('billing_email')."&delivery_name=".$request->input('delivery_name')."&delivery_address=".$request->input('delivery_address')."&delivery_city=".$request->input('delivery_city')."&delivery_state=".$request->input('delivery_state')."&delivery_zip=".$request->input('delivery_zip')."&delivery_country=".$request->input('delivery_country')."&delivery_tel=".$request->input('delivery_tel')."&merchant_param1=additional Info.&merchant_param2=additional Info.&merchant_param3=additional Info.&merchant_param4=additional Info.&merchant_param5=additional Info.&promo_code=&customer_identifier=&";

    		$encrypted_data=$this->encrypt($merchant_data1,$working_key); // Method for encrypting the data.

    		$Token =$request->input('_token');
    		return view('checkout',compact('encrypted_data','access_code','Token'));
        }else{
            $VeriFailed = 'Captcha verification failed';
            return redirect('proceed-to-payment')->with('CaptchaVerificaion', 'Captcha Verification Failed.') ->withInput();
	    }
	}


	public function success(Request $request){

	    $workingKey='392DDCC8A10D81185E3F03EC15ABDF01';		//Working Key should be provided here.
    	$encResponse=$request->input('encResp');			//This is the response sent by the CCAvenue Server
    	$rcvdString=$this->decrypt($encResponse,$workingKey);		//AES Decryption used as per the specified working key.


    	$AuthDesc="";
    	$MerchantId="11278";
    	$OrderId="";
    	$Amount=0;
    	$Checksum=0;
    	$veriChecksum=false;
    	$decryptValues=explode('&', $rcvdString);
    	$dataSize=sizeof($decryptValues);

        for($i = 0; $i < $dataSize; $i++)
	    {
    		$information=explode('=',$decryptValues[$i]);
    		if($i==0)	$OrderId=$information[1];
    		if($i==1)	$tracking_id=$information[1];
    		if($i==2)	$bank_ref_no=$information[1];
    		if($i==3)	$order_status=$information[1];
    		if($i==4)	$failure_message=$information[1];
    		if($i==5)	$payment_mode=$information[1];
    		if($i==10)	$Amount=$information[1];

    	}
    	$rcvdString=$MerchantId.'|'.$OrderId.'|'.$Amount.'|'.$AuthDesc.'|'.$workingKey;
    	$veriChecksum=$this->verifyChecksum($this->genchecksum($rcvdString), $Checksum);


    	if($order_status==="Success")
    	{


        	$Orders = new Orders;
    			$Data = array(
    			'orderstatus' => 'Paid',
    			'amount'=>$Amount
    			);
    		$Orders->where('ordercode',$OrderId)->update($Data);

        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.ordercode', '=',$OrderId)
    	            ->get();

        	foreach($Data as $rowp){}

        	$subject = "Admission Payment Successful - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billemail;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Admission Successful - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' >
            <tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr>
            <tr><td>PAYMENT STATUS</td><td>:</td><td>".$order_status."</td></tr>
            <tr><td>TOTAL AMOUNT</td><td>:</td><td>".number_format($Amount,2)."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billaddress."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billtelephone."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shippincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shiptelephone."</td></tr></table></body>
            </html>";

            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');

    		//Here you need to put in the routines for a successful
    		//transaction such as sending an email to customer,
    		//setting database status, informing logistics etc etc
    		$request->session()->flash('alert-success', 'Thank you. Your credit card has been charged and your transaction is successful.');
		    return redirect('thankyou');
    	}

        else if($order_status==="Aborted")
    	{

        	$Orders = new Orders;
    			$Data = array(
    			'orderstatus' => 'Aborted',
    			'amount'=>$Amount
    			);
    		$Orders->where('ordercode',$OrderId)->update($Data);

        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.ordercode', '=',$OrderId)
    	            ->get();

        	foreach($Data as $rowp){}

        	$subject = "Admission Aborted - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billemail;
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Admission Payment Aborted - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' >
            <tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr>
            <tr><td>PAYMENT STATUS</td><td>:</td><td>".$order_status."</td></tr>
            <tr><td>TOTAL AMOUNT</td><td>:</td><td>".number_format($Amount,2)."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billaddress."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billtelephone."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shippincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shiptelephone."</td></tr></table></body>
            </html>";

            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');

    	    //Delete Data
		  //  DB::table('admissions')->where('id', '=',$Data[0]->admissionid)->delete();
		  //  DB::table('orders')->where('ordercode', '=',$OrderId)->delete();

    		$request->session()->flash('alert-warning', 'You have aborted the transaction, Thank you!');
		    return redirect('thankyou');
    	}
    	else if($order_status==="Failure")
    	{
        	$Orders = new Orders;
    			$Data = array(
    			'orderstatus' => 'Failure',
    			'amount'=>$Amount
    			);
    		$Orders->where('ordercode',$OrderId)->update($Data);

        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.ordercode', '=',$OrderId)
    	            ->get();
        	foreach($Data as $rowp){}

        	$subject = "Admission Failed - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billemail;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Admission Payment Failed - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' >
            <tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr>
            <tr><td>PAYMENT STATUS</td><td>:</td><td>".$order_status."</td></tr>
            <tr><td>TOTAL AMOUNT</td><td>:</td><td>".number_format($Amount,2)."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billaddress."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billtelephone."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shippincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shiptelephone."</td></tr></table></body>
            </html>";

            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');

    	    //Delete Data
		   //  DB::table('admissions')->where('id', '=',$Data[0]->admissionid)->delete();
		   //  DB::table('orders')->where('ordercode', '=',$OrderId)->delete();

			$request->session()->flash('alert-info', 'Thank you for shopping with us.However,the transaction has been declined.');
		    return redirect('thankyou');
    	}
    	else
    	{
    	//	echo "<br>Security Error. Illegal access detected";

        	$Orders = new Orders;
    			$Data = array(
    			'orderstatus' => 'Failed',
    			'amount'=>$Amount
    			);
    		$Orders->where('ordercode',$OrderId)->update($Data);

        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.ordercode', '=',$OrderId)
    	            ->get();

        	foreach($Data as $rowp){}

        	$subject = "Admission Failed - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billemail;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Admission Payment Failed - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' >
            <tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr>
            <tr><td>PAYMENT STATUS</td><td>:</td><td>Failed</td></tr>
            <tr><td>TOTAL AMOUNT</td><td>:</td><td>".number_format($Amount,2)."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billaddress."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billtelephone."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shippincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shiptelephone."</td></tr></table></body>
            </html>";

            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');

    	    //Delete Data
		  //  DB::table('admissions')->where('id', '=',$Data[0]->admissionid)->delete();
		 //   DB::table('orders')->where('ordercode', '=',$OrderId)->delete();

    		$request->session()->flash('alert-danger', 'Security Error. Illegal access detected');
		    return redirect('thankyou');
    	}
	}




	public function addadmissions1(Request $request){
		$Admissions = new Admissions;
		$Admissions->name =stripslashes($request->input('name'));
		$Admissions->dob =stripslashes($request->input('dob'));
		$Admissions->address =stripslashes($request->input('address'));
		$Admissions->nopskul =stripslashes($request->input('nopskul'));
		$Admissions->grade =stripslashes($request->input('grade'));
		$Admissions->gradetjoin =stripslashes($request->input('gradetjoin'));
		$Admissions->year =stripslashes($request->input('year'));
		$Admissions->nparents =stripslashes($request->input('nparents'));
		$Admissions->designation =stripslashes($request->input('designation'));
		$Admissions->organization =stripslashes($request->input('organization'));
		$Admissions->occupation =stripslashes($request->input('occupation'));
		$Admissions->offadd =stripslashes($request->input('offadd'));
		$Admissions->city =stripslashes($request->input('city'));
		$Admissions->state =stripslashes($request->input('state'));
		$Admissions->country =stripslashes($request->input('country'));
		$Admissions->pincode =stripslashes($request->input('pincode'));
		$Admissions->restel =stripslashes($request->input('restel'));
		$Admissions->offtel =stripslashes($request->input('offtel'));
		$Admissions->mnfather =stripslashes($request->input('mnfather'));
		$Admissions->mnmother =stripslashes($request->input('mnmother'));
		$Admissions->emailf =stripslashes($request->input('emailf'));
		$Admissions->emailm =stripslashes($request->input('emailm'));
		$Admissions->passport =stripslashes($request->input('passport'));
		$Admissions->ourskul =stripslashes($request->input('ourskul'));
		// $Admissions->save();

		$Orders = DB::table('orders')->where('ordercode','!=','')->orderBy('ordercode','desc')->take(1)->get();
		//echo count($Orders);die;
		if(count($Orders) == 0){
			$NewOrderCode =1;
		}else{
		    //$xplode = explode('-',$Orders[0]->ordercode);
			//$OldOrderCode = $xplode[1]+1;
			//$NewOrderCode = "ecolem-".$OldOrderCode;
			$NewOrderCode = $Orders[0]->ordercode+1;
		}
		if($request->input('passport') == 'Indian Passport'){ $Amount=1;}
		if($request->input('passport') == 'Non-Indian Passport'){ $Amount=1;}

		$Data = array(
			'ordercode' => $NewOrderCode,
			'amount' => $Amount,
			'name' => $request->input('name'),
			'dob' => $request->input('dob'),
			'address' => $request->input('address'),
			'nopskul' => $request->input('nopskul'),
			'grade' => $request->input('grade'),
			'gradetjoin' => $request->input('gradetjoin'),
			'year' => $request->input('year'),
			'nparents' => $request->input('nparents'),
			'designation' => $request->input('designation'),
			'organization' => $request->input('organization'),
			'occupation' => $request->input('occupation'),
			'offadd' => $request->input('offadd'),
			'city' => $request->input('city'),
			'state' => $request->input('state'),
			'country' => $request->input('country'),
			'pincode' => $request->input('pincode'),
			'restel' => $request->input('restel'),
			'mnfather' => $request->input('mnfather'),
			'mnmother' => $request->input('mnmother'),
			'emailf' => strtolower($request->input('emailf')),
			'emailm' => strtolower($request->input('emailm')),
			'passport' => $request->input('passport'),
			'ourskul' => $request->input('ourskul'),
			'offtel' => $request->input('offtel'),
			);
		return view('proceed-to-payment1',compact('Data'));
	}


    public function checkout1(Request $request){

		$Admissions = new Admissions;
		$Admissions->name =stripslashes($request->input('name'));
		$Admissions->dob =stripslashes($request->input('dob'));
		$Admissions->address =stripslashes($request->input('address'));
		$Admissions->nopskul =stripslashes($request->input('nopskul'));
		$Admissions->grade =stripslashes($request->input('grade'));
		$Admissions->gradetjoin =stripslashes($request->input('gradetjoin'));
		$Admissions->year =stripslashes($request->input('year'));
		$Admissions->nparents =stripslashes($request->input('nparents'));
		$Admissions->designation =stripslashes($request->input('designation'));
		$Admissions->organization =stripslashes($request->input('organization'));
		$Admissions->occupation =stripslashes($request->input('occupation'));
		$Admissions->offadd =stripslashes($request->input('offadd'));
		$Admissions->city =stripslashes($request->input('city'));
		$Admissions->state =stripslashes($request->input('state'));
		$Admissions->country =stripslashes($request->input('country'));
		$Admissions->pincode =stripslashes($request->input('pincode'));
		$Admissions->restel =stripslashes($request->input('restel'));
		$Admissions->offtel =stripslashes($request->input('offtel'));
		$Admissions->mnfather =stripslashes($request->input('mnfather'));
		$Admissions->mnmother =stripslashes($request->input('mnmother'));
		$Admissions->emailf =strtolower(stripslashes($request->input('emailf')));
		$Admissions->emailm =strtolower(stripslashes($request->input('emailm')));
		$Admissions->passport =stripslashes($request->input('passport'));
		$Admissions->ourskul =stripslashes($request->input('ourskul'));
		$Admissions->save();
		$Admid = $Admissions->id;

		$Orders = new Orders;
		$Orders->admissionid =$Admid;
		$Orders->ordercode =stripslashes($request->input('order_id'));
		$Orders->amount =stripslashes($request->input('amount'));
		$Orders->billname =stripslashes($request->input('billing_name'));
		$Orders->billaddress =stripslashes($request->input('billing_address'));
		$Orders->billcountry =stripslashes($request->input('billing_country'));
		$Orders->billstate =stripslashes($request->input('billing_state'));
		$Orders->billcity =stripslashes($request->input('billing_city'));
		$Orders->billpincode =stripslashes($request->input('billing_zip'));
		$Orders->billtelephone =stripslashes($request->input('billing_tel'));
		$Orders->billemail =strtolower(stripslashes($request->input('billing_email')));
		$Orders->shipname =stripslashes($request->input('delivery_name'));
		$Orders->shipaddress =stripslashes($request->input('delivery_address'));
		$Orders->shipcountry =stripslashes($request->input('delivery_country'));
		$Orders->shipstate =stripslashes($request->input('delivery_state'));
		$Orders->shipcity =stripslashes($request->input('delivery_city'));
		$Orders->shippincode =stripslashes($request->input('delivery_zip'));
		$Orders->shiptelephone =stripslashes($request->input('shiptelephone'));
		$Orders->deliverynotes =stripslashes($request->input('deliverynotes'));
		$Orders->admission_date =date('Y-m-d',strtotime(stripslashes($request->input('admission_date'))));

		$Orders->save();
		$Orderid = $Orders->id;

		$working_key='392DDCC8A10D81185E3F03EC15ABDF01';//Shared by CCAVENUES
		$access_code='6J2BHM5KCK1HU8JN';//Shared by CCAVENUES
		$merchant_id='11278';//Shared by CCAVENUES

		//$checksum=$this->getchecksum($merchant_id,$request->input('amount'),stripslashes($request->input('ordercode')),stripslashes($request->input('Redirect_Url')),$working_key); // Method to generate checksum

	//$merchant_data= 'Merchant_Id='.$merchant_id.'&Amount='.$request->input('amount').'&Order_Id='.stripslashes($request->input('ordercode')).'&Redirect_Url='.stripslashes($request->input('Redirect_Url')).'&billing_cust_name='.stripslashes($request->input('billname')).'&billing_cust_address='.stripslashes($request->input('billaddress')).'&billing_cust_country='.stripslashes($request->input('billcountry')).'&billing_cust_state='.stripslashes($request->input('billstate')).'&billing_cust_city='.stripslashes($request->input('billcity')).'&billing_zip_code='.stripslashes($request->input('billpincode')).'&billing_cust_tel='.stripslashes($request->input('billtelephone')).'&billing_cust_email='.strtolower(stripslashes($request->input('billemail'))).'&delivery_cust_name='.stripslashes($request->input('shipname')).'&delivery_cust_address='.stripslashes($request->input('shipaddress')).'&delivery_cust_country='.stripslashes($request->input('shipcountry')).'&delivery_cust_state='.stripslashes($request->input('shipstate')).'&delivery_cust_city='.stripslashes($request->input('shipcity')).'&delivery_zip_code='.stripslashes($request->input('shippincode')).'&delivery_cust_tel='.stripslashes($request->input('shiptelephone')).'&billing_cust_notes='.stripslashes($request->input('deliverynotes'));


      $merchant_data='merchant_id='.urlencode($merchant_id).'&';
        foreach ($_POST as $key => $value){
		$merchant_data.=$key.'='.urlencode($value).'&';
    	}

    	$merchant_data1="tid=".$request->input('tid')."&merchant_id=11278&order_id=".$request->input('order_id')."&amount=".$request->input('amount')."&currency=".$request->input('currency')."&redirect_url=".$request->input('redirect_url')."&cancel_url=".$request->input('cancel_url')."&language=EN&billing_name=".$request->input('billing_name')."&billing_address=".$request->input('billing_address')."&billing_city=".$request->input('billing_city')."&billing_state=".$request->input('billing_state')."&billing_zip=".$request->input('billing_zip')."&billing_country=".$request->input('billing_country')."&billing_tel=".$request->input('billing_tel')."&billing_email=".$request->input('billing_email')."&delivery_name=".$request->input('delivery_name')."&delivery_address=".$request->input('delivery_address')."&delivery_city=".$request->input('delivery_city')."&delivery_state=".$request->input('delivery_state')."&delivery_zip=".$request->input('delivery_zip')."&delivery_country=".$request->input('delivery_country')."&delivery_tel=".$request->input('delivery_tel')."&merchant_param1=additional Info.&merchant_param2=additional Info.&merchant_param3=additional Info.&merchant_param4=additional Info.&merchant_param5=additional Info.&promo_code=&customer_identifier=&";

	    /*var_dump($merchant_data);
	    echo '<br><br>';
	    var_dump($merchant_data1);
	    die; */
		$encrypted_data=$this->encrypt($merchant_data1,$working_key); // Method for encrypting the data.

		$Token =$request->input('_token');
		return view('checkout1',compact('encrypted_data','access_code','Token'));
	}




function encrypt($plainText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
	  	$openMode = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
	  	$blockSize = @mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad = $this->pkcs5_pad($plainText, $blockSize);
	  	if (@mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
		{
	      $encryptedText = @mcrypt_generic($openMode, $plainPad);
  	      @mcrypt_generic_deinit($openMode);

		}
		return bin2hex($encryptedText);
	}

	function decrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
	  	$openMode = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		@mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
	 	@mcrypt_generic_deinit($openMode);
		return $decryptedText;

	}
	//*********** Padding Function *********************

	 function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

	//********** Hexadecimal to Binary function for php 4.0 version ********

	function hextobin($hexString)
   	{
        	$length = strlen($hexString);
        	$binString="";
        	$count=0;
        	while($count<$length)
        	{
        	    $subString =substr($hexString,$count,2);
        	    $packedString = pack("H*",$subString);
        	    if ($count==0)
		    {
				$binString=$packedString;
		    }

		    else
		    {
				$binString.=$packedString;
		    }

		    $count+=2;
        	}
  	        return $binString;
    }


	function getchecksum($MerchantId,$Amount,$OrderId ,$URL,$WorkingKey)
	{
		$str ="$order_id|$OrderId|$Amount|$URL|$WorkingKey";
		$adler = 1;
		$adler = $this->adler32($adler,$str);
		return $adler;
	}

	function genchecksum($str)
	{
		$adler = 1;
		$adler = $this->adler32($adler,$str);
		return $adler;
	}

	function verifyChecksum($getCheck, $avnChecksum)
	{
		$verify=false;
		if($getCheck==$avnChecksum) $verify=true;
		return $verify;
	}

	function adler32($adler , $str)
	{
		$BASE =  65521 ;
		$s1 = $adler & 0xffff ;
		$s2 = ($adler >> 16) & 0xffff;
		for($i = 0 ; $i < strlen($str) ; $i++)
		{
			$s1 = ($s1 + Ord($str[$i])) % $BASE ;
			$s2 = ($s2 + $s1) % $BASE ;
		}
		return $this->leftshift($s2 , 16) + $s1;
	}

	function leftshift($str , $num)
	{

		$str = DecBin($str);

		for( $i = 0 ; $i < (64 - strlen($str)) ; $i++)
			$str = "0".$str ;

		for($i = 0 ; $i < $num ; $i++)
		{
			$str = $str."0";
			$str = substr($str , 1 ) ;
			//echo "str : $str <BR>";
		}
		return $this->cdec($str) ;
	}

	function cdec($num)
	{
		$dec=0;
		for ($n = 0 ; $n < strlen($num) ; $n++)
		{
		   $temp = $num[$n] ;
		   $dec =  $dec + $temp*pow(2 , strlen($num) - $n - 1);
		}

		return $dec;
	}


}
