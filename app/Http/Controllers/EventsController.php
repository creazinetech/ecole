<?php

namespace App\Http\Controllers;
use Auth;
use App\Events; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class EventsController  extends Controller
{
	
	
    public function index(){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('events')->orderBy('id','desc')->get();
				return view('events.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function addevent(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$News = new Events;
				if($request->hasFile('thumbnail')) {
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('thumbnail')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					$thumbnail = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/events/',$thumbnail);
					$News->thumbnail =$thumbnail;
				}
				$News->status =stripslashes($request->input('status'));
				$News->title =stripslashes($request->input('title'));
				$News->short_desc =stripslashes($request->input('short_desc'));
				$News->brief_desc =stripslashes($request->input('brief_desc'));
				$News->save();
				$request->session()->flash('alert-success', 'News added successfully!');
				return redirect('events');
			}else{
				return view('unauthorised');
			}
		}
	}	
	
	
	public function eventstatus(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Event= new Events;
				$Data = array(
				'status' => $request->input('status'),
				);
				$Event->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deleteevent($id){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','10')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('events')->where('id', '=', $id)->get();
				
				if(!empty($Data[0]->thumbnail) || $Data[0]->thumbnail !="" || $Data[0]->thumbnail !=NULL){
					unlink(public_path().'/images/events/'.$Data[0]->thumbnail);
				}
				DB::table('events')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'Event has been deleted!');
				return redirect('events');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function editevent($id){
		if(Auth::user()->role_id =="2"  && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('events')
					->where('id', '=', $id)
					->get();
				return view('events/editevent',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}		
	
	public function updateevent(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$NLData = DB::table('events')->where('id', '=',$request->input('id'))->get();
				
				$News = new Events;
				$Data = array(
				'title' => $request->input('title'),
				'status' => $request->input('status'),
				'short_desc' => $request->input('short_desc'),
				'brief_desc' => $request->input('brief_desc'),
				);				
				
				if($request->hasFile('thumbnail') == 1) {
					
					if(!empty($NLData[0]->thumbnail) || $NLData[0]->thumbnail !="" || $NLData[0]->thumbnail !=NULL){
						$Unlink = unlink(public_path().'/images/events/'.$NLData[0]->thumbnail);
					}
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('thumbnail')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					// $pdf = 'Ecole-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$thumbnail = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/events/',$thumbnail);
					$Data['thumbnail']=$thumbnail;
				}else{
					$Data['thumbnail']= $NLData[0]->thumbnail;
				}
				
				
				$News->where('id',$request->input('id'))->update($Data);
				
				$request->session()->flash('alert-success', 'News updated successfully!');
				return redirect('events');
			}else{
				return view('unauthorised');
			}
		}
	}

}
