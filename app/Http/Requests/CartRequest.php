<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class CartRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'cart_id' => '',
            'dist_email' => 'required',
            'company_id' => 'required',
            'prod_id' => 'required',
            'quantity' => 'required',
            'prod_price' => 'required',
            'disc_price' => 'required',
            'discount' => 'required',
            'totalprice' => 'required',
            'prod_code' => 'required',
            'created_at' => '',
            'updated_at' => '',
        ];
    }
}
