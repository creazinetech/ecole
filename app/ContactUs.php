<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contactus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['c_id','c_address1','c_address2','c_contact1','c_contact2','c_emai','faxno','c_status','map'];

}
