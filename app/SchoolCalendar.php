<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class SchoolCalendar extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'school_calendar';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','pdf','status','pdfname'];

}
