<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'alumni';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['aid','firstname','lastname','home_address','graduate_yr','city','state','zip','contact1','contact2','emailid','website','study_type','university','programme','last_university','last_programme','last_address','occupation','organisation','designation','off_address','off_contact','off_fax','off_email','off_website','work_type','gender','dob','alumni_ambassador','personal','professional','hobbies','vacation'];

}
