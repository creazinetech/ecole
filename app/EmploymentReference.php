<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class EmploymentReference extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employment_refrence';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['erid','jaid','ername','erdesignation','erphone','eremail','eraddress'];

}
