<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class IndexSliders extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'index_sliders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['is_id','is_img','is_status','is_order','is_title1','is_title2'];

}
