<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['prod_name','prod_desc','prod_features','cat_id','company_id','prod_img','prod_images','prod_price','prod_for','prod_specs','prod_brand','prod_code','bunch_piece','bunch_price'];

}
