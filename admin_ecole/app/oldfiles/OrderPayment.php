<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_payment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_code','amount','paid_amt','bal_amt','po_status','order_dt','cancel_dt','rejected_dt','dist_email','company_id','pay_status','due_date','disp_name','disp_con','total_bundle','btcode','disp_img'];

}
