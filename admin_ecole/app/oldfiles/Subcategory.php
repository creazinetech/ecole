<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subcategory';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['scat_name','scat_status','cat_id'];

}
