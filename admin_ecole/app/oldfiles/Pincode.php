<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Pincode extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pincode';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pin_code','city_id','pin_status'];

}
