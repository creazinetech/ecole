<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class MediaImages extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'media_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','category_id','img','img_status','img_title'];

}
