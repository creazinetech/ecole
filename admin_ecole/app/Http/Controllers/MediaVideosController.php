<?php

namespace App\Http\Controllers;
use Auth;
use App\MediaCategory; //iNCLUDE MODAL
use App\MediaVideos; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class MediaVideosController extends Controller
{

	public $mediaVideos = array();

	public function __construct()
	{
		$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','27')->get();
		foreach($roles as $Roles){ array_push($this->mediaVideos, $Roles->action); }
	}

	public function index($categoryId = null)
	{
		if(Auth::user()->status =="Active")
		{
			if(in_array('listing',$this->mediaVideos) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('media_videos');
				$Data = $Data->get();

				// $Data = DB::table('gallery')->leftJoin('pages','pages.page_id','=','gallery.page_id')->orderby('pages.page_title')->get();
				$Pages = DB::table('media_category')->orderby('catname')->get();
				return view('media.videos.index',compact('Data','Pages'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}


	public function addVideos(Request $request){
		if(Auth::user()->status =="Active")
		{
			if(in_array('add',$this->mediaVideos) || Auth::user()->added_by== 0)
			{
				$ID="";

					$TableObj = new MediaVideos;
					$TableObj->vid_status ='Active';
					$TableObj->vid_title =stripslashes($request->input('vid_title'));
					$TableObj->url =stripslashes($request->input('url'));
					$TableObj->save();
				$request->session()->flash('alert-success', 'Videos uploaded successfully!');
				return redirect()->back();//('gallery');
			}else{
				return view('unauthorised');
			}
		}
	}

	public function changeStatus(Request $request)
	{
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Gallery = new MediaVideos;
				$Data = array(
				'img_status' => $request->input('status'),
				);
				$Gallery->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}


	public function deleteVideo($id, Request $request){
		if(Auth::user()->status =="Active")
		{
				$id = Crypt::decrypt($id);
				DB::table('media_videos')->where('id', '=', $id)->delete();
				$request->session()->flash('alert-success', 'Video removed successfully!');
				return redirect()->back();
		}else{
				return view('unauthorised');
		}
	}


	public function deleteAllVideos(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$id = $request->input('cid');
				$Data = DB::table('media_videos')->where('category_id', '=', $id)->get();
				foreach($Data as $OData){
					if(!empty($OData->img) || $OData->img !="" || $OData->img !=NULL){
				        if (file_exists(public_path().'/videos/media/'.$OData->img)) {
							unlink(public_path().'/videos/media/'.$OData->img);
				        }
					}
				}
				DB::table('media_videos')->where('category_id', '=', $id)->delete();

				$Data1 = DB::table('media_videos')->get();
				$GData='';
				$path= $request->input('path');
				foreach($Data1 as $row){
				$GData.='<div class="col-md-55">
								<div class="thumbnail">
								  <div class="image view view-first">
									<img style="width: 100%; height:100%; display: block;" src="videos/media/'.$row->img.'" alt="image" />
									<div class="mask">
									  <p>'.$row->img_title.'</p>
									  <div class="tools tools-bottom">
										<select id="img_status" name="img_status" class="input-xs img_status" style="color:black;font-size:12px;" data-id="'.$row->id.'">
										<option value="Active"';
										if($row->img_status=='Active'){ $GData.='selected';}
										$GData.='>Active</option>
										<option value="Inactive"';
										if($row->img_status=='Inactive'){ $GData.='selected';}
										$GData.='>Inactive</option>
										</select>
										<a class="delete_img" data-id="'.$row->id.'" style="color:red;"><i class="fa fa-times"></i></a>
									  </div>
									</div>
								  </div>
								</div>
							</div>';
				}

				echo $GData;
				die;
			}
			else
			{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}
}
