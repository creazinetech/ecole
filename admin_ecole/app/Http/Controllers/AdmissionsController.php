<?php

namespace App\Http\Controllers;
use Auth;
use App\Admissions; //iNCLUDE MODAL
use App\Orders; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use PDF;
use Crypt; //For Encrption-Decryption

class AdmissionsController extends Controller
{
	
    public function index(){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('admissions')
				        //->leftJoin('orders','orders.admissionid','=','admissions.id')
				        ->orderBy('id','desc')
				        ->get();
				return view('admissions/index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function view($id){
		if(Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				
				$AdmObj = new Admissions;
				$Data = array(
				'viewed' =>1,
				);
				$AdmObj->where('id',$aid)->update($Data);	
				
				$Data = DB::table('admissions')->where('admissions.id', '=', $aid)->get();
				return view('admissions/view',compact('Data'));
				
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

	public function downloadPDF($id,$type){
		$RoleArray = [];
		$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
		foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
		if(in_array('print',$RoleArray) || Auth::user()->added_by== 0)
		{
			$aid = Crypt::decrypt($id);
			$Data = DB::table('admissions')->where('admissions.id', '=',$aid)->get();
			$pdf = PDF::loadView('admissions/pdf', compact('Data','type'));
			if($type==1){
				return $pdf->download('ecolemondiale.pdf');
			}if($type==2){
				return $pdf->download('New.pdf');
			}if($type==3){
				return $pdf->download(''.$Data[0]->name.'.pdf');
			}
		}else{
			return view('unauthorised');
		}
    }	
	
	public function viewPDF($id,$type){
		$RoleArray = [];
		$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
		foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
		if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
		{
			$aid = Crypt::decrypt($id);
			$Data = DB::table('admissions')->where('admissions.id', '=',$aid)->get();
			$pdf =PDF::loadView('admissions/pdf', compact('Data','type'));
			if($type==1){
				return $pdf->stream('ecolemondiale.pdf');
			}if($type==2){
				return $pdf->stream('New.pdf');
			}if($type==3){
				return $pdf->stream(''.$Data[0]->name.'.pdf');
			}
		}else{
			return view('unauthorised');
		}
    }

	public function admissiondelete($id){
		if(Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				DB::table('admissions')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'Admission entry has been deleted!');
				return redirect('admissions');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

	public function edit($id){
		if(Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				$Data = DB::table('admissions')->where('admissions.id', '=', $aid)->get();
				return view('admissions/edit',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	

	public function updateadmissions(Request $request){
		if(Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$AdmObj = new Admissions;
				$Data = array(
				'name' => ucwords($request->input('name')),
				'dob' => $request->input('dob'),
				'address' => $request->input('address'),
				'nopskul' => $request->input('nopskul'),
				'grade' => $request->input('grade'),
				'gradetjoin' => $request->input('gradetjoin'),
				'year' => $request->input('year'),
				'city' => ucwords($request->input('city')),
				'state' => ucwords($request->input('state')),
				'country' => ucwords($request->input('country')),
				'pincode' => $request->input('pincode'), 
				'frestel' => $request->input('frestel'),
				'fmobile' => $request->input('fmobile'),
				'passport' => $request->input('passport'),
				'ourskul' => $request->input('ourskul'),
				'fname' => $request->input('fname'),
				'foccupation' => $request->input('foccupation'),
				'fdesignation' => $request->input('fdesignation'),
				'forganization' => $request->input('forganization'),
				'foffadd' => $request->input('foffadd'),
				'femail' => $request->input('femail'),
				'mname' => $request->input('mname'),
				'moccupation' => $request->input('moccupation'),
				'mdesignation' => $request->input('mdesignation'),
				'morganization' => $request->input('morganization'),
				'moffadd' => $request->input('moffadd'),
				'memail' => $request->input('memail'),
				'mrestel' => $request->input('mrestel'),
				'mmobile' => $request->input('mmobile'),
				);
				
    	
				$AdmObj->where('id',$request->input('id'))->update($Data);				
				$request->session()->flash('alert-success', 'Admission details updated successfully!');
				return redirect('admissions');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
}
