<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\QuickEnquiry; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class QuickEnquiryController extends Controller
{
	
    public function quickenquiry(){
		if(Auth::user()->status =="Active" )
		{
		    $RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','4')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
			    
		    	$QuickEnquiries = DB::table('quick_enquiry')->orderBy('qe_id','desc')->get();
		    	return view('quick-enquiry',compact('QuickEnquiries'));
			}else{
				return view('unauthorised');
			}	
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function qedelete($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','4')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$qe_id = Crypt::decrypt($id);
				DB::table('quick_enquiry')->where('qe_id', '=', $qe_id)->delete();
				session()->flash('alert-success', 'Enquiry has been deleted!');
				return redirect('quick-enquiry');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function viewdqenquiry (Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$QuickEnquiry = new QuickEnquiry;
				$Data = array(
				'viewed' =>$request->input('status'),
				);
				$QuickEnquiry->where('qe_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
}
