<?php

namespace App\Http\Controllers;
use Auth;
use App\News; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class NewsController extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','10')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('news')->orderBy('id','desc')->get();
				return view('news.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function addnews(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','10')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$News = new News;
				if($request->hasFile('thumbnail')) {
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('thumbnail')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					$thumbnail = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/news/',$thumbnail);
					$News->thumbnail =$thumbnail;
				}
				$News->status =stripslashes($request->input('status'));
				$News->title =stripslashes($request->input('title'));
				$News->short_desc =stripslashes($request->input('short_desc'));
				$News->brief_desc =stripslashes($request->input('brief_desc'));
				$News->save();
				$request->session()->flash('alert-success', 'News added successfully!');
				return redirect('news');
			}else{
				return view('unauthorised');
			}
		}
	}	
	
	
	public function newsstatus(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$News = new News;
				$Data = array(
				'status' => $request->input('status'),
				);
				$News->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deletenews($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','10')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('news')->where('id', '=', $id)->get();
				
				if(!empty($Data[0]->thumbnail) || $Data[0]->thumbnail !="" || $Data[0]->thumbnail !=NULL){
				    if (file_exists(public_path().'/images/news/'.$Data[0]->thumbnail)) {
				    	unlink(public_path().'/images/news/'.$Data[0]->thumbnail);
				    }
				}
				DB::table('news')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'News has been deleted!');
				return redirect('news');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function editnews($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','10')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('news')
					->where('id', '=', $id)
					->get();
				return view('news/editnews',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}		
	
	public function updatenews(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','10')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$NLData = DB::table('news')->where('id', '=',$request->input('id'))->get();
				
				$News = new News;
				$Data = array(
				'title' => $request->input('title'),
				'status' => $request->input('status'),
				'short_desc' => $request->input('short_desc'),
				'brief_desc' => $request->input('brief_desc'),
				);				
				
				if($request->hasFile('thumbnail') == 1) {
					
					if(!empty($NLData[0]->thumbnail) || $NLData[0]->thumbnail !="" || $NLData[0]->thumbnail !=NULL){
						if (file_exists(public_path().'/images/news/'.$NLData[0]->thumbnail)) {
							$Unlink = unlink(public_path().'/images/news/'.$NLData[0]->thumbnail);
						}
					}
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('thumbnail')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					// $pdf = 'Ecole-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$thumbnail = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/news/',$thumbnail);
					$Data['thumbnail']=$thumbnail;
				}else{
					$Data['thumbnail']= $NLData[0]->thumbnail;
				}
				
				
				$News->where('id',$request->input('id'))->update($Data);
				
				$request->session()->flash('alert-success', 'News updated successfully!');
				return redirect('news');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

}
