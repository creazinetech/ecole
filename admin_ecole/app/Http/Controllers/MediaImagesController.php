<?php

namespace App\Http\Controllers;
use Auth;
use App\MediaCategory; //iNCLUDE MODAL
use App\MediaImages; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class MediaImagesController extends Controller
{

	public $mediaImages = array();

	public function __construct()
	{
		$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','26')->get();
		foreach($roles as $Roles){ array_push($this->mediaImages, $Roles->action); }
	}

	public function index($categoryId = null)
	{
		echo $categoryId;
		if(Auth::user()->status =="Active")
		{
			if(in_array('listing',$this->mediaImages) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('media_images');
				if($categoryId !=null)
					$Data = $Data->where('category_id',$categoryId);
				$Data = $Data->get();

				// $Data = DB::table('gallery')->leftJoin('pages','pages.page_id','=','gallery.page_id')->orderby('pages.page_title')->get();
				$Pages = DB::table('media_category')->orderby('catname')->get();
				return view('media.images.index',compact('Data','Pages'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}


	public function addImages(Request $request){
		if(Auth::user()->status =="Active")
		{
			if(in_array('add',$this->mediaImages) || Auth::user()->added_by== 0)
			{
				$ID="";
				if($request->hasFile('img')) {
					foreach(Input::file("img") as $file) {
					$timestamp = date('Yhis');
					$ext = $file->guessClientExtension();
					$newname = 'media_'.rand(4,'99999').date('is').'.'.$ext;
					//$ProdImages = $timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/media/',$newname);
					$TableObj = new MediaImages;
					$TableObj->img =$newname;
					$TableObj->img_status ='Active';
					$TableObj->category_id =$request->input('category_id');
					$TableObj->img_title =stripslashes($request->input('img_title'));
					$TableObj->save();
					}
				}
				$request->session()->flash('alert-success', 'Images uploaded successfully!');
				return redirect()->back();//('gallery');
			}else{
				return view('unauthorised');
			}
		}
	}

	public function changeStatus(Request $request)
	{
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Gallery = new MediaImages;
				$Data = array(
				'img_status' => $request->input('status'),
				);
				$Gallery->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}


	public function deleteImage(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$id = $request->input('cid');
				$Data = DB::table('media_images')->where('id', '=', $id)->get();
				if(!empty($Data[0]->img) || $Data[0]->img !="" || $Data[0]->img !=NULL){
				    if (file_exists(public_path().'/images/media/'.$Data[0]->img)) {
					    unlink(public_path().'/images/media/'.$Data[0]->img);
				    }
				}
				DB::table('media_images')->where('id', '=', $id)->delete();

				$Data1 = DB::table('media_images')->get();
				$GData='';
				$path= $request->input('path');
				foreach($Data1 as $row){
				$GData.='<div class="col-md-55">
								<div class="thumbnail">
								  <div class="image view view-first">
									<img style="width: 100%; height:100%; display: block;" src="images/media/'.$row->img.'" alt="image" />
									<div class="mask">
									  <p>'.$row->img_title.'</p>
									  <div class="tools tools-bottom">
										<select id="img_status" name="img_status" class="input-xs img_status" style="color:black;font-size:12px;" data-id="'.$row->id.'">
										<option value="Active"';
										if($row->img_status=='Active'){ $GData.='selected';}
										$GData.='>Active</option>
										<option value="Inactive"';
										if($row->img_status=='Inactive'){ $GData.='selected';}
										$GData.='>Inactive</option>
										</select>
										<a class="delete_img" data-id="'.$row->id.'" style="color:red;"><i class="fa fa-times"></i></a>
									  </div>
									</div>
								  </div>
								</div>
							</div>';
				}

				echo $GData;
				die;
			}
			else
			{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}


	public function deleteAllImages(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$id = $request->input('cid');
				$Data = DB::table('media_images')->where('category_id', '=', $id)->get();
				foreach($Data as $OData){
					if(!empty($OData->img) || $OData->img !="" || $OData->img !=NULL){
				        if (file_exists(public_path().'/images/media/'.$OData->img)) {
							unlink(public_path().'/images/media/'.$OData->img);
				        }
					}
				}
				DB::table('media_images')->where('category_id', '=', $id)->delete();

				$Data1 = DB::table('media_images')->get();
				$GData='';
				$path= $request->input('path');
				foreach($Data1 as $row){
				$GData.='<div class="col-md-55">
								<div class="thumbnail">
								  <div class="image view view-first">
									<img style="width: 100%; height:100%; display: block;" src="images/media/'.$row->img.'" alt="image" />
									<div class="mask">
									  <p>'.$row->img_title.'</p>
									  <div class="tools tools-bottom">
										<select id="img_status" name="img_status" class="input-xs img_status" style="color:black;font-size:12px;" data-id="'.$row->id.'">
										<option value="Active"';
										if($row->img_status=='Active'){ $GData.='selected';}
										$GData.='>Active</option>
										<option value="Inactive"';
										if($row->img_status=='Inactive'){ $GData.='selected';}
										$GData.='>Inactive</option>
										</select>
										<a class="delete_img" data-id="'.$row->id.'" style="color:red;"><i class="fa fa-times"></i></a>
									  </div>
									</div>
								  </div>
								</div>
							</div>';
				}

				echo $GData;
				die;
			}
			else
			{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}
}
