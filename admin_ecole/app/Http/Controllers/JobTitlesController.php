<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\Roles; //iNCLUDE MODAL
use App\Rolesctrl; //iNCLUDE MODAL
use App\JobTitles; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class JobTitlesController extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','24')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
			$Data = DB::table('job_titles')->orderBy('id', 'desc')->get();
				return view('jobtitles.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function savejobtitle(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','24')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){
				
				$titles = explode(',',$request->input('title'));
				foreach($titles as $title){
				$JobTitles = new JobTitles;
				$JobTitles->title = ucwords($title);
				$JobTitles->status ='Active';
				$JobTitles->save();
				}
				$request->session()->flash('alert-success', 'Job title added successfully!');
				return redirect('jobtitles');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function deletejobtitle($id) 
	{
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','24')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$ID = Crypt::decrypt($id);
				DB::table('job_titles')->where('id', '=', $ID)->delete();
				session()->flash('alert-success', 'job title has been deleted!');
				return redirect('jobtitles');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function editjobtitle($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','24')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$ID = Crypt::decrypt($id);
				$Data = JobTitles::where('id',$ID)->first();
				return view('jobtitles.edit',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function updatejobtitle(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','24')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				
				$DistT = new JobTitles;
				$Data = array(
				'title' => ucwords($request->input('title')),
				'status' => $request->input('status'),
				);
				$DistT->where('id',$request->input('id'))->update($Data);
				$request->session()->flash('alert-success', 'Job Title updated successfully!');
				return redirect('jobtitles');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function jobtitlestatus(Request $request){
		if(Auth::user()->status =="Active" )
		{
			if ($request->ajax()) {
				$DistT = new JobTitles;
				$Data = array(
				'status' => $request->input('status'),
				);
				$DistT->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
}
?>