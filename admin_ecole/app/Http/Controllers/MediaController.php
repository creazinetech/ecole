<?php

namespace App\Http\Controllers;
use Auth;
use App\MediaCategory; //iNCLUDE MODAL
use App\YoutubeVideo; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class MediaCategoryController extends Controller
{

	public function categoryIndex()
	{
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','25')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('media_category')->orderBy('id','desc')->get();
				return view('media.category.category_index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}


	public function addCategory(Request $request){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','25')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$mediaCategory = new MediaCategory;
				if($request->hasFile('thumbnail')) {
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$thumbnail = 'media-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/media/',$thumbnail);
					$mediaCategory->thumbnail =$thumbnail;
				}
				$mediaCategory->yc_status ='Active';
				$mediaCategory->catname =stripslashes($request->input('catname'));
				$mediaCategory->save();
				$request->session()->flash('alert-success', 'Category added successfully!');
				return redirect('media/category');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

	public function ycategorystatus(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$YoutubeCategory = new MediaCategory;
				$Data = array(
				'yc_status' => $request->input('status'),
				);
				$YoutubeCategory->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}

	public function editcategory($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$page_id = Crypt::decrypt($id);
				$Data = DB::table('youtube_category')
					->where('ycatid', '=', $page_id)
					->get();
				return view('youtube/editcategory',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
}
