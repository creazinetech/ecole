<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\SocialMedia; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class SocialLinkController extends Controller
{
	
    public function socialmedia(){
		if(Auth::user()->status =="Active")
		{
	    	$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','1')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
		    	$SocialMedia = DB::table('social_media')->get();
		    	return view('social-media',compact('SocialMedia'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function updatesocial(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$SocialMedia = new SocialMedia;
				$Data = array(
				'social_link' => $request->input('Value'),
				);
				$SocialMedia->where('social_id',$request->input('Sid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function socialstatus(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$SocialMedia = new SocialMedia;
				$Data = array(
				'social_status' => $request->input('status'),
				);
				$SocialMedia->where('social_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
}
