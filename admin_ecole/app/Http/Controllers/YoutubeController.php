<?php

namespace App\Http\Controllers;
use Auth;
use App\YoutubeCategory; //iNCLUDE MODAL
use App\YoutubeVideo; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class YoutubeController extends Controller
{
	
	
    public function category(){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('youtube_category')->orderBy('ycatid','desc')->get();
				return view('youtube.category',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function addyoutubecat(Request $request){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$YoutubeCategory = new YoutubeCategory;
				if($request->hasFile('thumbnail')) {
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$thumbnail = 'youtube-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/youtube/',$thumbnail);
					$YoutubeCategory->thumbnail =$thumbnail;
				}
				$YoutubeCategory->yc_status ='Active';
				$YoutubeCategory->catname =stripslashes($request->input('catname'));
				$YoutubeCategory->save();
				$request->session()->flash('alert-success', 'Category added successfully!');
				return redirect('youtube/category');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function ycategorystatus(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$YoutubeCategory = new YoutubeCategory;
				$Data = array(
				'yc_status' => $request->input('status'),
				);
				$YoutubeCategory->where('ycatid',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deleteycategory($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('youtube_category')->where('ycatid', '=', $id)->get();
				
				if(!empty($Data[0]->thumbnail) || $Data[0]->thumbnail !="" || $Data[0]->thumbnail !=NULL){
					unlink(public_path().'/images/youtube/'.$Data[0]->thumbnail);
				}
				DB::table('youtube_category')->where('ycatid', '=', $id)->delete();
				session()->flash('alert-success', 'Category has been deleted!');
				return redirect('youtube/category');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function editcategory($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$page_id = Crypt::decrypt($id);
				$Data = DB::table('youtube_category')
					->where('ycatid', '=', $page_id)
					->get();
				return view('youtube/editcategory',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}		
	
	public function updateycategory(Request $request){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$YCData = DB::table('youtube_category')->where('ycatid', '=',$request->input('ycatid'))->get();
				
				$YoutubeCategory = new YoutubeCategory;
				$Data = array(
				'catname' => $request->input('catname'),
				'yc_status' => $request->input('yc_status'),
				);				
				
				if($request->hasFile('thumbnail') == 1) {
					
					if(!empty($YCData[0]->thumbnail) || $YCData[0]->thumbnail !="" || $YCData[0]->thumbnail !=NULL){
						$Unlink = unlink(public_path().'/images/youtube/'.$YCData[0]->thumbnail);
					}
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$thumbnail = 'youtube-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/youtube/',$thumbnail);
					$Data['thumbnail']=$thumbnail;
				}else{
					$Data['thumbnail']= $YCData[0]->thumbnail;
				}
				
				
				$YoutubeCategory->where('ycatid',$request->input('ycatid'))->update($Data);
				
				$request->session()->flash('alert-success', 'Category added successfully!');
				return redirect('youtube/category');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
    public function videos(){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('youtube_video')
				->leftJoin('youtube_category', 'youtube_category.ycatid', '=', 'youtube_video.ycatid')
				->orderBy('yid','desc')
				->get();
				
				return view('youtube.videos',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function addyoutubevideo(Request $request){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$YVideo = new YoutubeVideo;
				$YVideo->ycatid =$request->input('ycatid');
				$YVideo->ystatus =$request->input('ystatus');
				$YVideo->yurl =stripslashes($request->input('yurl'));
				$YVideo->save();
				$request->session()->flash('alert-success', 'Video added successfully!');
				return redirect('youtube/videos');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function deletevideos($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				DB::table('youtube_video')->where('yid', '=', $id)->delete();
				session()->flash('alert-success', 'Video has been deleted!');
				return redirect('youtube/videos');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function yvideostatus(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$YoutubeVideo = new YoutubeVideo;
				$Data = array(
				'ystatus' => $request->input('status'),
				);
				$YoutubeVideo->where('yid',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}	
	
}
