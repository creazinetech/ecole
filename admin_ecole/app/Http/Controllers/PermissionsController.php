<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\Rolesctrl; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class PermissionsController extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$CtrlRoles = DB::table('roles')->where('panel_id','=','2')->get();
						
				return view('permissions.index',compact('CtrlRoles'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
    public function editpermissions($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				
				$Controllers = DB::table('controller')->where('panel_id','2')->orderBy('ctrl_name', 'asc')->get();
				
				$CtrlRoles = DB::table('role_ctrl')
						->select('role_id', 'ctrl_id', 'action')
						->where('role_id',$id)
						->get();
						
				$permissions = array();
				foreach ($CtrlRoles as $val) {
					$permissions[$val->ctrl_id][] = $val->action;
				}				
				
				return view('permissions.edit-permissions',compact('Controllers','permissions','id'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function addpermissions(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$input = Input::all();
			
			DB::table('role_ctrl')->where('role_id', '=', $request->input('roleid'))->delete();
			
			foreach ($input as $key => $value) { 
				if($key != '_token' && $key !='roleid'){
					$RC = new Rolesctrl;
					$RC->role_id = $request->input('roleid');
					$RC->ctrl_id = $key;
					$RC->action = $value;
					$RC->save();
					//echo $key.' = '.$value.'<br>';
				}	
			}	//die;	
			$request->session()->flash('alert-success', 'Permissions updated successfully!');
			return redirect('permissions');
		}else{
			return redirect('auth/logout');
		}
	}

	
}
