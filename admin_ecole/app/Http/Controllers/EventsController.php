<?php

namespace App\Http\Controllers;
use Auth;
use App\Events; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class EventsController  extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('events')
				//->leftJoin('users','users.id','=','events.added_by')
				->orderBy('events.id','desc')
				->get();
				
				return view('event.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
    public function addevent(){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				return view('event.addevent',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function saveevent(Request $request){
		if(Auth::user()->status =="Active"){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$News = new Events;
				$News->added_by =Auth::user()->id;
				$News->status =ucwords($request->input('status'));
				$News->timing =stripslashes($request->input('timing'));
				$News->title =stripslashes($request->input('title'));
				$News->location =stripslashes($request->input('location'));
				$News->location_address =stripslashes($request->input('location_address'));
				$News->brief_desc =stripslashes($request->input('brief_desc'));
				$News->start_date =date('Y-m-d',strtotime($request->input('start_date')));
				$News->end_date =date('Y-m-d',strtotime($request->input('end_date')));
				$News->save();
				$request->session()->flash('alert-success', 'Event added successfully!');
				return redirect('event');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	
	public function eventstatus(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Event= new Events;
				$Data = array(
				'status' => $request->input('status'),
				);
				$Event->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deleteevent($id){
		if(Auth::user()->status =="Active"){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','10')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('events')->where('id', '=', $id)->get();
				DB::table('events')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'Event has been deleted!');
				return redirect('event');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function editevent($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('events')
					->where('id', '=', $id)
					->get();
				return view('event/editevent',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}		
	
	public function updateevent(Request $request){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','11')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$NLData = DB::table('events')->where('id', '=',$request->input('id'))->get();
				
				$News = new Events;
				$Data = array(
				'added_by' => Auth::user()->id,
				'title' => $request->input('title'),
				'timing' => $request->input('timing'),
				'status' => $request->input('status'),
				'location' => $request->input('location'),
				'location_address' => $request->input('location_address'),
				'brief_desc' => $request->input('brief_desc'),
				'start_date' =>date('Y-m-d',strtotime($request->input('start_date'))),
				'end_date' =>date('Y-m-d',strtotime($request->input('end_date'))),
				);
				
				$News->where('id',$request->input('id'))->update($Data);
				
				$request->session()->flash('alert-success', 'Event updated successfully!');
				return redirect('event');
			}else{
				return view('unauthorised');
			}
		}
	}

}

?>