<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\Testimonials; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class TestimonialsController extends Controller
{
	
    public function testimonials(){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','18')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Testimonials = DB::table('testimonials')->get();
				return view('testimonials',compact('Testimonials'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function addtestimonials(Request $request){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','18')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Content = str_replace("</p>", "<br />",stripslashes($request->input('t_content')));
				$Testimonials = new Testimonials;
				$Testimonials->t_status ='Active';
				$Testimonials->t_content =str_replace("<p>", "",$Content);
				$Testimonials->tby =ucwords($request->input('tby'));
				$Testimonials->tdetails =stripslashes($request->input('tdetails'));
				$Testimonials->save();
				$request->session()->flash('alert-success', 'Testimonials uploaded successfully!');
				return redirect('testimonials');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function tstatus(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Testimonials = new Testimonials;
				$Data = array(
				't_status' => $request->input('status'),
				);
				$Testimonials->where('t_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function showindex(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Testimonials = new Testimonials;
				$Data = array(
				't_soindex' => 1,
				);
				$Testimonials->where('t_id',$request->input('tid'))->update($Data);
				
				$Data1 = array(
				't_soindex' => 0,
				);
				$Testimonials->where('t_id','!=',$request->input('tid'))->update($Data1);
				
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function showsidebar(Request $request){
		if(Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Testimonials = new Testimonials;
				$Data = array(
				'onsidebar' => 1,
				);
				$Testimonials->where('t_id',$request->input('tid'))->update($Data);
				
				$Data1 = array(
				'onsidebar' => 0,
				);
				$Testimonials->where('t_id','!=',$request->input('tid'))->update($Data1);
				
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}	

	public function tdelete($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','18')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$t_id = Crypt::decrypt($id);
				DB::table('testimonials')->where('t_id', '=', $t_id)->delete();
				session()->flash('alert-success', 'Testimonials has been deleted!');
				return redirect('testimonials');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	
}
