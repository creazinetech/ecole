<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\Roles; //iNCLUDE MODAL
use App\Rolesctrl; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class RolesController extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
			$Data = DB::table('roles')->where('panel_id','=','2')->orderBy('role_name', 'Asc')->get();
				return view('roles.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function saverole(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){
				
				$RoleNames = explode(',',$request->input('role_name'));
				foreach($RoleNames as $RoleName){
				$DistT = new Roles;
				$DistT->role_name = ucwords($RoleName);
				$DistT->panel_id = 2;
				$DistT->role_status = $request->input('role_status');
				$DistT->save();
				}
				$request->session()->flash('alert-success', 'Role added successfully!');
				return redirect('roles');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function deleterole($id) 
	{
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$ID = Crypt::decrypt($id);
				DB::table('roles')->where('role_id', '=', $ID)->delete();
				session()->flash('alert-success', 'Role has been deleted!');
				return redirect('roles');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function editrole($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$ID = Crypt::decrypt($id);
				$Data = Roles::where('role_id',$ID)->first();
				return view('roles.edit',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function updaterole(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$DistT = new Roles;
				$Data = array(
				'role_name' => ucwords($request->input('role_name')),
				'role_status' => $request->input('role_status'),
				);
				$DistT->where('role_id',$request->input('role_id'))->update($Data);
				$request->session()->flash('alert-success', 'Role updated successfully!');
				return redirect('roles');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function rolestatus(Request $request){
		if(Auth::user()->status =="Active" )
		{
			if ($request->ajax()) {
				$DistT = new Roles;
				$Data = array(
				'role_status' => $request->input('status'),
				);
				$DistT->where('role_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
}
?>