<?php

namespace App\Http\Controllers;
use Auth;
use App\ContactEnquiry; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use PDF;
use Crypt; //For Encrption-Decryption

class EnquiryController extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('contact_enquiry')->orderBy('id','desc')->get();
				return view('enquiry/index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	
	public function downloadPDF($id){
		if(Auth::user()->status =="Active"){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('print',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				$Data = DB::table('contact_enquiry')->where('contact_enquiry.id', '=',$aid)->get();
				$pdf =PDF::loadView('enquiry/enquiry-pdf', compact('Data'));
				return $pdf->stream(''.$Data[0]->name.'.pdf');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }	
    
	public function view($id){
		if(Auth::user()->status =="Active"){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				
				$AdmObj = new ContactEnquiry;
				$Data = array(
				'viewed' =>1,
				);
				$AdmObj->where('id',$aid)->update($Data);	
				
				$Data = DB::table('contact_enquiry')->where('contact_enquiry.id', '=',$aid)->get();
				return view('enquiry/view',compact('Data'));
				// $pdf =PDF::loadView('alumni-pdf-view', compact('Data'));
				// return $pdf->stream('alumni.pdf');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	
	public function enquirydelete($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				DB::table('contact_enquiry')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'Enquiry has been deleted!');
				return redirect('enquiry');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function alumniview($id){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				$Data = DB::table('contact_enquiry')->where('contact_enquiry.id', '=', $aid)->get();
				return view('alumni-view',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function edit($id){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				$Data = DB::table('contact_enquiry')->where('contact_enquiry.id', '=', $aid)->get();
				return view('enquiry/edit',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function enquiryupdate(Request $request){
		if(Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Alumni = new ContactEnquiry;
				$Data = array(
				'name' => ucwords($request->input('name')),
				'dob' => $request->input('dob'),
				'address' => $request->input('address'),
				'nopskul' => $request->input('nopskul'),
				'grade' => $request->input('grade'),
				'gradetjoin' => $request->input('gradetjoin'),
				'year' => $request->input('year'),
				'nparents' => ucwords($request->input('nparents')),
				'designation' => $request->input('designation'),
				'organization' => $request->input('organization'),
				'occupation' => $request->input('occupation'),
				'offadd' => $request->input('offadd'),
				'city' => ucwords($request->input('city')),
				'state' => ucwords($request->input('state')),
				'country' => ucwords($request->input('country')),
				'pincode' => $request->input('pincode'),
				'restel' => $request->input('restel'),
				'offtel' => $request->input('offtel'),
				'mobile' => $request->input('mobile'),
				'email' => strtolower($request->input('email')),
				'whattoknow' => $request->input('whattoknow'),
				'aboutschool' => $request->input('aboutschool'),
				);
				
				$Alumni->where('id',$request->input('id'))->update($Data);				
				$request->session()->flash('alert-success', 'Enquiry updated successfully!');
				return redirect('enquiry');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
}
