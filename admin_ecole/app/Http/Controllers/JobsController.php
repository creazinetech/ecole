<?php

namespace App\Http\Controllers;
use Auth;
use App\Jobs; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class JobsController extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','14')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('jobs')->orderBy('id','desc')->get();
				return view('jobs.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function addjob(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','14')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Jobs = new Jobs;
				if($request->hasFile('thumbnail')) {
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('thumbnail')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					$thumbnail = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/jobs/',$thumbnail);
					$Jobs->thumbnail =$thumbnail;
				}
				$Jobs->status =stripslashes($request->input('status'));
				$Jobs->title =stripslashes($request->input('title'));
				$Jobs->short_desc =stripslashes($request->input('short_desc'));
				$Jobs->brief_desc =stripslashes($request->input('brief_desc'));
				$Jobs->save();
				$request->session()->flash('alert-success', 'Job added successfully!');
				return redirect('jobs');
			}else{
				return view('unauthorised');
			}
		}
	}	
	
	
	public function jobsstatus(Request $request){
		if(Auth::user()->status =="Active" )
		{
			if ($request->ajax()) {
				$Jobs = new Jobs;
				$Data = array(
				'status' => $request->input('status'),
				);
				$Jobs->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deletejob($id){
		if(Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','14')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('jobs')->where('id', '=', $id)->get();
				
				if(!empty($Data[0]->thumbnail) || $Data[0]->thumbnail !="" || $Data[0]->thumbnail !=NULL){
					unlink(public_path().'/images/jobs/'.$Data[0]->thumbnail);
				}
				DB::table('jobs')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'Job has been deleted!');
				return redirect('jobs');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function editjob($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','14')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('jobs')
					->where('id', '=', $id)
					->get();
				return view('jobs/editjob',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}		
	
	public function updatejob(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','14')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$NLData = DB::table('jobs')->where('id', '=',$request->input('id'))->get();
				
				$Jobs = new Jobs;
				$Data = array(
				'title' => $request->input('title'),
				'status' => $request->input('status'),
				'short_desc' => $request->input('short_desc'),
				'brief_desc' => $request->input('brief_desc'),
				);				
				
				if($request->hasFile('thumbnail') == 1) {
					
					if(!empty($NLData[0]->thumbnail) || $NLData[0]->thumbnail !="" || $NLData[0]->thumbnail !=NULL){
						$Unlink = unlink(public_path().'/images/jobs/'.$NLData[0]->thumbnail);
					}
					$file = Input::file('thumbnail');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('thumbnail')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					// $pdf = 'Ecole-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$thumbnail = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/images/jobs/',$thumbnail);
					$Data['thumbnail']=$thumbnail;
				}else{
					$Data['thumbnail']= $NLData[0]->thumbnail;
				}
				
				
				$Jobs->where('id',$request->input('id'))->update($Data);
				
				$request->session()->flash('alert-success', 'Job details updated successfully!');
				return redirect('jobs');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

}
