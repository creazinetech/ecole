<?php

namespace App\Http\Controllers;
use Auth;
use App\Newsletter; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class NewslettersController extends Controller
{
	
	
    public function index(){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','9')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('newsletter')->orderBy('nlid','desc')->get();
				return view('newsletter.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function addnewsletter(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','9')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Newsletter = new Newsletter;
				if($request->hasFile('pdf')) {
					$file = Input::file('pdf');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('pdf')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					$onlyName1 = str_replace(' ','-',$onlyName[0]);
					$pdf = $onlyName1.'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/downloads/',$pdf);
					$Newsletter->pdf =$pdf;
					$Newsletter->pdfsize = ($request->file('pdf')->getClientSize()* .0009765625) * .0009765625;
				}
				$Newsletter->status ='Active';
				$Newsletter->pdfname =stripslashes($request->input('pdfname'));
				$Newsletter->content =stripslashes($request->input('content'));
				$Newsletter->stock =stripslashes($request->input('newsletter_type'));
				$Newsletter->save();
				$request->session()->flash('alert-success', 'Newsletter added successfully!');
				return redirect('newsletter/index');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	
	public function newsletterstatus(Request $request){
		if(Auth::user()->status =="Active" )
		{
			if ($request->ajax()) {
				$Newsletter = new Newsletter;
				$Data = array(
				'status' => $request->input('status'),
				);
				$Newsletter->where('nlid',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deletenewsletter($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','9')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('newsletter')->where('nlid', '=', $id)->get();
				
				if(!empty($Data[0]->pdf) || $Data[0]->pdf !="" || $Data[0]->pdf !=NULL){
				    if (file_exists(public_path().'/downloads/'.$Data[0]->pdf)) {
				    	unlink(public_path().'/downloads/'.$Data[0]->pdf);
				    }
				}
				DB::table('newsletter')->where('nlid', '=', $id)->delete();
				session()->flash('alert-success', 'Newsletter has been deleted!');
				return redirect('newsletter');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function editnewsletter($id){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','9')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$page_id = Crypt::decrypt($id);
				$Data = DB::table('newsletter')
					->where('nlid', '=', $page_id)
					->get();
				return view('newsletter/editnewsletter',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}		
	
	public function updatenewsletter(Request $request){
		if(Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','9')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$NLData = DB::table('newsletter')->where('nlid', '=',$request->input('nlid'))->get();
				
				$Newsletter = new Newsletter;
				$Data = array(
				'pdfname' => $request->input('pdfname'),
				'status' => $request->input('status'),
				'content' => $request->input('content'),
				);				
				
				if($request->hasFile('pdf') == 1) {
					
					if(!empty($NLData[0]->pdf) || $NLData[0]->pdf !="" || $NLData[0]->pdf !=NULL){
					    if (file_exists(public_path().'/downloads/'.$NLData[0]->pdf)) {
					    	$Unlink = unlink(public_path().'/downloads/'.$NLData[0]->pdf);
					    }
					}
					$file = Input::file('pdf');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('pdf')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					// $pdf = 'Ecole-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$pdf = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/downloads/',$pdf);
					$Data['pdf']=$pdf;
					$Data['pdfsize']= ($request->file('pdf')->getClientSize()* .0009765625) * .0009765625;
				}else{
					$Data['pdf']= $NLData[0]->pdf;
					$Data['pdfsize']= $NLData[0]->pdfsize;
				}
				
				
				$Newsletter->where('nlid',$request->input('nlid'))->update($Data);
				
				$request->session()->flash('alert-success', 'Newsletter updated successfully!');
				return redirect('newsletter');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

}
