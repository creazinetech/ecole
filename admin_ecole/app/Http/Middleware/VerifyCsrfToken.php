<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
       'reports/searchcompany',
       'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=files',
       'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=images',
       'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=flash',
       'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=files',
       'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=images',
       'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=flash'
       
    ];
}
