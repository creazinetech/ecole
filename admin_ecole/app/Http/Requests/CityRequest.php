<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class CityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'city_id' => '',
            'city_name' => 'required',
            'state_status' => 'required',
            'state_id' => 'required',
            'created_at' => '',
            'updated_at' => '',
        ];
    }
}
