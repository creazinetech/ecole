<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class CompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'company_id' => '',
            'company_name' => 'required',
            'company_email' => 'required',
            'company_type' => 'required',
            'company_contact1' => 'required',
            'company_contact2' => '',
            'company_website' => 'required',
            'company_password' => 'required',
            'company_id' => 'required',
            'company_address' => '',
            'company_logo' => '',
            'created_at' => '',
            'updated_at' => '',
        ];
    }
}
