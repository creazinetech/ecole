<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class CountryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'country_id' => '',
            'country_name' => 'required',
            'country_status' => 'required',
            'created_at' => '',
            'updated_at' => '',
        ];
    }
}
