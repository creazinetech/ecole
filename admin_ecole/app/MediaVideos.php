<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class MediaVideos extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'media_videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
