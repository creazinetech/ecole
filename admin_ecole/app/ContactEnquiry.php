<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ContactEnquiry extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_enquiry';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'dob', 'address', 'nopskul', 'grade', 'gradetjoin', 'year', 'city', 'state', 'country', 'pincode', 'mobile', 'whattoknow', 'aboutschool', 'nationality', 'fname', 'foccupation', 'fdesignation', 'forganization', 'foffadd', 'fofftel', 'femail', 'fmobile', 'frestel', 'mname', 'moccupation', 'mdesignation', 'morganization', 'moffadd', 'mofftel', 'memail', 'mmobile', 'mrestel','viewed'];

}
