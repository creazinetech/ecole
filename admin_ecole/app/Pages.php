<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['page_id','page_title','page_img','page_banner','short_desc','brief_desc','page_heading','page_meta_title','page_meta_description','page_meta_keywords','banner_title','parent','brief_desc2','brief_desc3','brief_desc4'];


}
