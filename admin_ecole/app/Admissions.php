<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admissions extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
      protected $fillable = ['id','name','dob','address','nopskul','grade','gradetjoin','year','city','state','country','pincode','passport','ourskul','nationality','viewed', 'fname', 'foccupation', 'fdesignation', 'forganization', 'foffadd', 'fofftel', 'femail', 'fmobile', 'frestel', 'mname', 'moccupation', 'mdesignation', 'morganization', 'moffadd', 'mofftel', 'memail','mmobile','mrestel'];

}
