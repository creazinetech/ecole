<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class YoutubeVideo extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'youtube_video';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['yid','yurl','yembedd','ystatus'];

}
