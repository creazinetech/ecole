<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class JobApplications extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name','dob','position','photo','gender','nationality','marital_status','children','cadd','padd','pincode','mob_no','landline','email_id','passport_no','issue_date','issue_place','expiry_date','experience_in','experience_year','current_employer','designation','internation_school_exp','joindt_curr_emp','it_skills','language_speak','hobbies','current_sal','expected_sal','newjoin_dt','edu_statement','other_info','viewed','criminal_offence'];


}
