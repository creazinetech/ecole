<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Rolesctrl extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_ctrl';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role_id','ctrl_id','action'];

}
