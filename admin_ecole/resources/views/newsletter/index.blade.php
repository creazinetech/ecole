@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','9')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }

if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>News Letter List</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					<?php if(Auth::user()->added_by== 0 || in_array('add',$RoleArray)){?>
					<a style="margin-right:5px;" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#Add_PA_Modal">
			         Add </a>
				<?php } ?>
                     
					<table id="datatable-fixed-header" class="table table-striped table-bordered table-hover table-condensed table-responsive datatable-responsive" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Title</th>
                          <!--<th>Size</th>-->
                          <th>PDF</th>
                          <th>Downloads/Viewed</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
                          <?php } ?>
						  <?php if(in_array('delete',$RoleArray) || in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						  <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						foreach($Data as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $row->pdfname; ?></td>
						
					<!--	<td><?php 
					//	echo $this->human_filesize(filesize(public_path().'/downloads/'.$row->pdf));
						echo $row->pdfsize; 
						?></td> -->
						<td>
						<a class="fillink" download="<?php echo $row->pdf; ?>" target="_blank" href="{!! \Config::get('app.url_base') !!}/<?php echo 'downloads/'.$row->pdf; ?>"><?php echo $row->pdf; ?></a> 
						</td>
						<td><?php  echo $row->downloads; ?></td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<select id="status" name="status" class="form-group nl_status" data-id="<?php echo $row->nlid; ?>">
						  <option value="Active" <?php if($row->status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->status == "Inactive"){echo "selected";} ?>>Inactive</option>
						</select>
						</td>
						<?php } ?>
						<?php if(in_array('edit',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						
						<a href="{{URL::to('newsletter/editnewsletter',array(Crypt::encrypt($row->nlid)))}}" class="btn btn-xs btn-success EditBtn" >
						<i class="fa fa-edit"></i></a>
						
						<?php } if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<a href="{{URL::to('newsletter/deletenewsletter',array(Crypt::encrypt($row->nlid)))}}" class="btn btn-danger btn-xs">
						<i class="fa fa-trash" title="delete"></i></a>
						<?php } ?>
						</td>
						<?php }?>
						</tr>
						<?php $i++; }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
	<!---FILE SIZE FUNCTION START --->
	<?php
/*	function human_filesize($bytes, $decimals = 2) {
		$size = array('B','KB','MB','GB','TB','PB','EB','ZB','YB');
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
	}	*/
	
	}
	?>
	<!---FILE SIZE FUNCTION END--->	
	<!-- Add model start -->
	<div id="Add_PA_Modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Add Newsletter</h4>
		  </div>
		  <form class="form-horizontal form-label-left" action="{{URL::route('addnewsletter')}}" enctype="multipart/form-data" method="post">
		  <div class="modal-body">
		      
				<div class="form-group">
				    <label class="col-md-2 col-sm-2 col-xs-12">Type :</label>
			         <div class="col-md-2 col-sm-2 col-xs-12">
				        <select id="newsletter_type" name="newsletter_type" class="form-control input-sm"  required="required">
				            <option value="1" selected>Pdf Letter</option>
				            <option value="2">Content Letter</option>
			            </select>
				    </div>
				</div>
				
				<div class="form-group">
				    <label class="col-md-2 col-sm-2 col-xs-12">Title :</label>
			         <div class="col-md-8 col-sm-8 col-xs-12">
				        <input type="text" name="pdfname" id="pdfname"  placeholder="Newsletter Title" class="form-control input-sm">
				        <label style="display:none;" class="pdfnameerr red">Please enter title.</label>
				    </div>
				</div>
				
				<div class="form-group pdfdiv">
				    <label class="col-md-2 col-sm-2 col-xs-12">PDF File :</label>
			         <div class="col-md-8 col-sm-8 col-xs-12">
				        <input type="file" name="pdf" id="pdf"  placeholder="PDF File" class="form-control input-sm">
				        <label style="display:none;" class="pdferr red">Please select pdf file.</label>
				    </div>
				</div>
				
				<div class="form-group contentdiv" style="display:none;">
				    <label class="col-md-2 col-sm-2 col-xs-12">Content :</label>
			         <div class="col-md-12 col-sm-12 col-xs-12">
				        <textarea name="content" id="content" class="ckeditor"></textarea>
				        <label style="display:none;" class="contenterr red">Please enter content.</label>
				    </div>
				</div>
				
				{{ csrf_field() }}
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-success btn-sm savenewsletter">Save</button>
			<button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>			
	<!-- Add model end -->
	

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
	$(document).ready(function(){
	    
        $('body').on('click','#savenewsletter', function(){
            var type = $('#newsletter_type').val();
	        var pdfname = $('#pdfname').val();
	        var pdf = $('#pdf').val();
	        var content = $('#content').val();
	       
            if(pdfname == '' || pdfname == 'undefined'){
                $('.pdfnameerr').show();
		        return false;
            }else{
                $('.pdfnameerr').hide();
            }
	       if(type==1){
	            if(pdf == '' || pdf == 'undefined'){
                    $('.pdfnameerr').show();
		            return false;
                }else{
                     $('.pdfnameerr').hide();
                } 
	       }
	       if(type==2){
	            if(content == '' || content == 'undefined'){
                    $('.contenterr').show();
		            return false;
                }else{
                     $('.contenterr').hide();
                } 	           
	       }
	          
	    });
	    
	    $('body').on('change','#newsletter_type', function(){
	        var type = $(this).val();
	        var pdf = $('#pdf').val();
	        var content = $('#content').val();
	        
	       if(type==1){
	            $('.pdfdiv').show();
	            $('.contentdiv').hide(); 
	       }
	       if(type==2){
	            $('.contentdiv').show();  
	            $('.pdfdiv').hide();
	       }
	        
	    });
		
		$('.nl_status').change(function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'newsletter/newsletterstatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		
	});
	</script>
  @include('included.footer')