@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','11')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Events List</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					<?php 
					if(Auth::user()->added_by== 0 || in_array('add',$RoleArray)){?>
					<a href="event/addevent" class="btn btn-primary btn-sm pull-right">
			         Add Event </a>
				<?php } ?>
                     
					<table id="datatable" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Title</th>
                          <th>Location</th>
                          <th>Start Date</th>
                          <th>End Date</th>
                          <th>Added By</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
                          <?php } ?>
						  <?php if(in_array('delete',$RoleArray) || in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						  <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						foreach($Data as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $row->title; ?></td>
						<td><?php echo $row->location; ?></td>
						<td><?php echo $row->start_date; ?></td>
						<td><?php echo $row->end_date; ?></td>
						<td>
						    <?php 
						    $Data1 = DB::table('users')->where('id','=',$row->added_by)->get();
						    echo $Data1[0]->name; ?>
						</td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<select id="status" name="status" class="form-group status" data-id="<?php echo $row->id; ?>">
						  <option value="Active" <?php if($row->status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->status == "Inactive"){echo "selected";} ?>>Inactive</option>
						</select>
						</td>
						<?php } ?>
						<?php if(in_array('add',$RoleArray) ||in_array('edit',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<?php  if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						
						<a href="{{URL::to('event/editevent',array(Crypt::encrypt($row->id)))}}" class="btn btn-xs btn-success EditBtn" >
						<i class="fa fa-edit"></i></a>
						
						<?php } if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						
						<a href="{{URL::to('event/deleteevent',array(Crypt::encrypt($row->id)))}}" class="btn btn-danger btn-xs">
						<i class="fa fa-trash" title="delete"></i></a>
						<?php } ?>
						</td>
						<?php }?>
						</tr>
						<?php $i++; }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>	
<script>
	$(document).ready(function(){
		
		$('.status').change(function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'event/eventstatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		
	});
	</script>
  @include('included.footer')