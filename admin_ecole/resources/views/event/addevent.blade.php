@include('included.header')
@include('included.super-admin-sidebar')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">

	<div class="clearfix"></div>
	<div class="flash-message">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
	  @if(Session::has('alert-' . $msg))

	  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
	  @endif
	@endforeach
	</div>
	
	<div class="flash-message1" style="display:none;">
	  <p class="alert alert-dismissable status_result"></p>
	</div>
	
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
			<h2>Add Event</h2>
			<div class="clearfix"></div>
		  </div>
			<div class="x_content" style="">
			<div class="clearfix"></div>
			<div class="col-md-12 col-sm-12 col-xs-12">
			<form class="form-horizontal form-label-left"  action="{{URL::route('saveevent')}}" enctype="multipart/form-data" method="post">
			
				<div class="col-md-3"> 
				
				<div class="form-group">
					<P>Title</P>
					<input type="text" name="title" id="title"  placeholder="Event Title" class="form-control input-sm"  required="required">
					<label style="display:none;" class="titleerr red">Please enter title.</label>
				</div>
				
				<div class="form-group">
					<P>Location Area</P>
					<input type="text" name="location" id="location" placeholder="Location Area" class="form-control input-sm">
					<label style="display:none;" class="locationerr red">Please enter location area.</label>
				</div>
				
				<div class="form-group">
					<P>Location Address</P>
					<textarea name="location_address" id="location_address" rows="10"  placeholder="Location Address" class="form-control input-sm"  required="required"></textarea>
					<label style="display:none;" class="location_addresserr red">Please enter location address.</label>
				</div>
				
				<div class="form-group">
					<P>Timing</P>
					<input type="text" name="timing" id="timing" placeholder="11:00AM to 12:00PM" class="form-control input-sm">
					<label style="display:none;" class="timingerr red">Please enter timing.</label>
				</div>
				
				<div class="form-group">
					<P>Start Date</P>
					<input type="text" name="start_date" id="start_date" placeholder="Start Date(dd-mm-yyyy)" class="datepicker form-control input-sm">
					<label style="display:none;" class="start_dateerr red">Please enter start date.</label>
				</div>
				
				<div class="form-group">
					<P>End Date</P>
					<input type="text" name="end_date" id="end_date" placeholder="End Date(dd-mm-yyyy)" class="datepicker form-control input-sm">
					<label style="display:none;" class="end_dateerr red">Please enter end date.</label>
				</div>
				
				<div class="form-group">
					<P>Status</P>
					<select id="status" name="status" class="form-control input-sm">
					  <option value="Active">Active</option>
					  <option value="Inactive">Inactive</option>
					</select>
					<label style="display:none;" class="statuserr red">Please select status.</label>
				</div>
				
				</div>
				
				<div class="col-md-9"> 
				<div class="form-group">
					<P>Content</P>
					<label style="display:none;" class="brief_descerr red">Please enter event content.</label>
					<textarea name="brief_desc" class="ckeditor" id="brief_desc" placeholder="Event Content" required="required"></textarea>
				</div>
				</div>
				
				
				
				<div class="col-md-12"> 
				{{ csrf_field() }}

				<button type="submit" class="btn btn-success btn-sm">Save</button>
				<a href="{!! \Config::get('app.url_base') !!}/event" class="btn btn-danger btn-sm">Cancel</a>
				</div>

			</form>
		  </div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!-- /page content -->

<script>
		
	$('.savenews').on('click',function(){
		var title = $('#title').val();
		var short_desc = $('#short_desc').val();
		var brief_desc = $('#brief_desc').val();
		var thumbnail = $('#thumbnail').val();
		var status = $('#status').val();
		var ext = thumbnail.split('.').pop().toLowerCase();
		if(title ==""){
			$('.titleerr').show();
			return false;
		}else{
			$('.titleerr').hide();
		}
		// if(brief_desc ==""){
			// $('.brief_descerr').show();
			// return false;
		// }else{
			// $('.brief_descerr').hide();
		// }
		if(status ==""){
			$('.statuserr').show();
			return false;
		}else{
			$('.statuserr').hide();
		}
		if(thumbnail !=""){
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				$('.thumbnailerr').show();
				return false;
			}else{
				$('.thumbnailerr').hide();
			}
		}else{
			$('.thumbnailerr').hide();
		}
	});
</script>
	
		
@include('included.footer')