@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','4')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('lisitng',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Quick Enquiries</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
                     
					<table id="datatable-fixed-header" class="table table-striped table-bordered table-condensed table-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Entry Id</th>
                          <th>Name</th>
                          <th>Contact</th>
                          <th>Email</th>
                          <th>Date</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
                           <?php } ?>
                          <?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
                          <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						if(count($QuickEnquiries) > 0){ 
						foreach($QuickEnquiries as $row){ 
						?>
						<tr class="qerow<?php echo $row->qe_id; ?>" style="<?php if($row->viewed==0){ echo 'font-weight:bold;background:rgba(0,0,0,0.1)';}?>">
						<td><?php echo $i; ?></td>
						<td><?php echo $row->qe_id; ?></td>
						<td><?php echo $row->qe_name; ?></td>
						<td><?php echo $row->qe_mobile; ?></td>
						<td><?php echo $row->qe_email; ?></td>
						<td><?php echo date('d-m-Y H:i A',strtotime($row->created_at)); ?></td>
						
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<select id="viewed" name="viewed" class="form-group viewed" data-id="<?php echo $row->qe_id; ?>">
						  <option value="1" <?php if($row->viewed == "1"){echo "selected";} ?>>Contacted</option>
						  <option value="0" <?php if($row->viewed == "0"){echo "selected";} ?>>Not contacted</option>
						</select>
						</td>
						<?php } ?>
						
						<?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<a href="{{URL::to('quick-enquiry/qedelete',array(Crypt::encrypt($row->qe_id)))}}" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm('Are you sure want to delete this?')"><i class="fa fa-trash"></i></a>
						</td>
						<?php } ?>
						</tr>
						<?php $i++; }} ?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	
			$('.viewed').on('change',function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'quick-enquiry-viewed',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					//	$('qerow'+cid).css('font-weight','normal');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
	</script>
	
  @include('included.footer')