 @include('included.header')
 @include('included.super-admin-sidebar')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Change Password</h2>
					 <?php 
					if(isset($updated) && $updated=='Updated')
						
					{
						 // header('location:{{URL::route("logout")}}');
					 // }
					 ?>
					 <script type="text/javascript">
						window.location.href = '{{URL::route("logout")}}';
					</script>
					<input type="hidden" id="updated" name="updated" value="<?php echo $updated;?>">
					<?php 
					}
					?>
					
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					
                    <div class="col-md-6 col-md-offset-3">
					
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('updatepassword')}}" enctype="multipart/form-data" method="post">
                       
						
						<div class="col-md-12">
							<div class="form-group">
							  <label for="name">Name</label>
							  <input type="text" class="form-control" required="required" id="name" name="name" value="<?php echo Auth::user()->name;?>">
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
							  <label for="email">Email</label>
							  <input type="email" class="form-control" required="required" id="email" name="email" value="<?php echo Auth::user()->email;?>" readonly>
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
							  <label for="new_password">New Password</label>
							  <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password">
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group has-confirmpassword">
							  <label for="password_confirmation">Confirm Password</label>
							  <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
							  <label class="pass_err red" style="display:none;"><i class="fa fa-alert"></i> Confirm password mismatched..!</label>
							</div>
						</div>
						
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
                        <div class="col-md-12 col-md-offset-3">
							<button id="savedata" type="submit" class="btn btn-success">Submit</button>
							<a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/home">Cancel</a>
                        </div>
                      </div>
                    </form>
					
					</div>
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
		
<script>
$(document).ready(function(){
	$('#savedata').click(function(){
		var confirm_password = $('#password_confirmation').val();
		var password = $('#new_password').val();
		if(password != confirm_password){
				$('.pass_err').show();
				$('.has-confirmpassword').addClass('has-error');
				return false;
		}else{
				$('.pass_err').hide();
				$('.has-confirmpassword').removeClass('has-error');
		}
	});
});
</script>	
		
  @include('included.footer')