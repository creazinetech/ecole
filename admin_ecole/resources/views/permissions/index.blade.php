 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result">
			  </p>
			</div>
			
            <div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel tile">
					<div class="x_title">
					   <h2><i class="fa fa-lock "></i> Manage Permissions <small> ( Manage permissions for other users )</small></h2>
					   <div class="clearfix"></div>
					</div>
					<div class="x_content">
						
						<?php 
						$i=1;
						foreach($CtrlRoles as $row){ ?>
						<h4>
						<a href="{{URL::to('permissions/edit-permissions',array(Crypt::encrypt($row->role_id)))}}" title="Edit Roles">
						<?php echo $i.'. '.$row->role_name; ?>
						</a>
						</h4>
						<hr>
						<?php $i++;}?>
		
					</div>
				</div>
			</div>
              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>
  @include('included.footer')