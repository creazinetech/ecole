 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result">
			  </p>
			</div>
			
            <div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                   <h2>Manage Permissions <small> ( Manage permissions for other users )</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
				<form name="notification_settings_form" method="post" id="notification_settings_form" action="{{URL::route('addpermissions')}}">			
					<input type="hidden" name="roleid" id="roleid" value="<?php echo $id; ?>">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="">
								<table class="table table-striped table-bordered table-condensed table-hover" id="">
									<thead>
										<tr>
											<th> Functionalities </th>
											<th colspan="6"> Permission </th>
										</tr>
										<tr>
											<th> </th>
											<th> Listing </th>                        
											<th> Add </th>                        
											<th> Edit </th>
											<th> Delete </th>
											<th> View </th>
											<th> Print </th>
										</tr>
									</thead>
									<tbody>							
									<?php
									$i = 0;
									if($Controllers != "") {
									foreach ($Controllers as $ctrl) {
									?>
									<tr>
									<td class="border-right">
									<b> <?php echo ucfirst($ctrl->ctrl_name); ?></b>
									</td> 
									<td>
										<input type="checkbox" name="<?php  echo $ctrl->ctrl_id . "_listing"; ?>" id="<?php  echo $ctrl->ctrl_id . "_listing"; ?>" value="listing" 
										<?php if(isset($permissions[$ctrl->ctrl_id]) && in_array('listing',$permissions[$ctrl->ctrl_id])){echo "checked=checked";} ?> class="icheck" />
									</td>
									<td>
										<input type="checkbox" name="<?php  echo $ctrl->ctrl_id . "_add"; ?>" id="<?php  echo $ctrl->ctrl_id . "_add"; ?>" value="add" 
										<?php if(isset($permissions[$ctrl->ctrl_id]) && in_array('add',$permissions[$ctrl->ctrl_id])){echo "checked=checked";} ?> class="icheck"/>
									</td>
									<td>
										<input type="checkbox" name="<?php  echo $ctrl->ctrl_id . "_edit"; ?>" id="<?php  echo $ctrl->ctrl_id . "_edit"; ?>" value="edit" 
										<?php if(isset($permissions[$ctrl->ctrl_id]) && in_array('edit',$permissions[$ctrl->ctrl_id])){echo "checked=checked";} ?> class="icheck"/>
									</td>
									<td>
										<input type="checkbox" name="<?php  echo $ctrl->ctrl_id . "_delete"; ?>" id="<?php  echo $ctrl->ctrl_id . "_delete"; ?>" value="delete" 
										<?php if(isset($permissions[$ctrl->ctrl_id]) && in_array('delete',$permissions[$ctrl->ctrl_id])){echo "checked=checked";} ?> class="icheck"/>
									</td>
									<td>
										<input type="checkbox" name="<?php echo $ctrl->ctrl_id . "_view"; ?>" id="<?php  echo $ctrl->ctrl_id . "_view"; ?>" value="view"
										<?php if(isset($permissions[$ctrl->ctrl_id]) && in_array('view',$permissions[$ctrl->ctrl_id])){echo "checked=checked";} ?> class="icheck"/>
									</td>
									<td>
										<input type="checkbox" name="<?php echo $ctrl->ctrl_id . "_print"; ?>" id="<?php  echo $ctrl->ctrl_id . "_print"; ?>" value="print"
										<?php if(isset($permissions[$ctrl->ctrl_id]) && in_array('print',$permissions[$ctrl->ctrl_id])){echo "checked=checked";} ?> class="icheck"/>
									</td>
								</tr>
							<?php  } }  ?>		
									</tbody>
								</table>
							</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-3">
								 {{ csrf_field() }}
									<button type="submit" class="btn btn-success">Update</button>
									<a href="{!! \Config::get('app.url_base') !!}/permissions" class="btn btn-primary">Cancel</a>
								</div>
							</div>
						</div>
				</form>	 				  
                </div>
              </div>
            </div>
              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>
  @include('included.footer')