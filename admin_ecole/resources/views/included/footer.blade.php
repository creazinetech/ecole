        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <small> Developed by <a href="https://www.matrixbricks.com">Matrix Bricks Infotech</a> </small>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- FastClick -->
    <script src="{!! \Config::get('app.url') !!}/plugins/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="{!! \Config::get('app.url') !!}/plugins/nprogress/nprogress.js"></script>
    
    <!-- bootstrap-progressbar -->
    <script src="{!! \Config::get('app.url') !!}/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="{!! \Config::get('app.url') !!}/plugins/iCheck/icheck.min.js"></script>
    
    <!-- DateJS -->
    <script src="{!! \Config::get('app.url') !!}/plugins/DateJS/build/date.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="{!! \Config::get('app.url') !!}/plugins/moment/min/moment.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	
    <!-- validator -->
    <script src="{!! \Config::get('app.url') !!}/plugins/validator/validator.js"></script>
    
	
    <!-- Datatables -->
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{!! \Config::get('app.url') !!}/plugins/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    
    
    <!-- Parsley Js -->
    <script src="{!! \Config::get('app.url') !!}/plugins/parsley.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="{!! \Config::get('app.url') !!}/plugins/build/js/custom.min.js"></script>
        <script language="Javascript">
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;
 
          return true;
       }
       //-->
    </script>


  
  </body>
</html>