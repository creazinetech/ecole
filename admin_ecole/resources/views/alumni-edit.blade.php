@include('included.header')
@include('included.super-admin-sidebar')
<?php foreach($Data as $row){} ?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="page-title">
	  <div class="title_left">
		<h2>Edit Alumni</h2>
	  </div>
	</div>

	<div class="clearfix"></div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel" style="padding:0px;">
		  <div class="x_content">
				<form class="form-horizontal form-label-left" novalidate action="{{URL::route('alumniupdate')}}" method="post">
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr class="bg-blue"><th colspan="3" class="text-center">Student Details</th></tr>
							<tr><th width="110px">Student Name</th><td width="1px"> : </td>
							<td>
							<input class="form-control input-sm" type="hidden" name="aid" value="<?php echo ucwords($row->aid); ?>">
							<input type="text" name="firstname" value="<?php echo ucwords($row->firstname); ?>"  placeholder="First Name" class="form-control input-sm">
							<input type="text" name="lastname" value="<?php echo ucwords($row->lastname); ?>"   placeholder="Last Name" class="form-control input-sm">
							</td></tr>
							<tr><th>Contact</th><td> : </td>
							<td>
							<input type="text" name="contact1" value="<?php echo $row->contact1; ?>" placeholder="Phone No." class="form-control input-sm">
							<input type="text" name="contact2" value="<?php echo $row->contact2; ?>" placeholder="Mobile No." class="form-control input-sm">
							</td></tr>
							<tr><th>Graduated Yr.</th><td> : </td>
							<td>							
							<select class="form-control input-sm" id="graduate_yr" name="graduate_yr">
							<option value="">Year?</option>
							<?php 
							$start_year=date('Y')-35;
							$end_year= date('Y')+5;
							$start_year1=$start_year+1;
							$end_year1=$end_year+1;
							for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
							<option value="<?php echo $i.'-'.$j; ?>" <?php if($row->graduate_yr == $i.'-'.$j){ echo "selected";} ?>>
							<?php echo $i.'-'.$j;?>
							</option>
							<?php } ?>
							</select>
							</td></tr>
							<tr><th>Home Address</th><td> : </td>
							<td>
							<textarea class="form-control input-sm" type="text" name="home_address" placeholder="Home Address"><?php echo $row->home_address; ?></textarea>
							</td></tr>
							<tr><th>City</th><td> : </td>
							<td><input class="form-control input-sm" type="text" name="city" value="<?php echo $row->city; ?>" placeholder="City"></td></tr>
							<tr><th>State</th><td> : </td>
							<td><input class="form-control input-sm" type="text" name="state" value="<?php echo $row->state; ?>" placeholder="State"></td></tr>
							<tr><th>Zipcode</th><td> : </td>
							<td><input class="form-control input-sm" type="text" name="zip" value="<?php echo $row->zip; ?>" placeholder="Zip"></td></tr>
							<tr><th>Email Id</th><td> : </td>
							<td><input class="form-control input-sm" type="text" name="emailid" value="<?php echo $row->emailid; ?>" placeholder="Email Id"></td></tr>
							<tr><th>Website</th><td> : </td>
							<td><input class="form-control input-sm" type="text" name="website" value="<?php echo $row->website; ?>" placeholder="Website"></td></tr>
						</table>
					</div>
					
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr class="bg-green"><th colspan="3" class="text-center">Current Details</th></tr>
							<tr><th width="110px">Studying?</th><td width="1px"> : </td>
							<td>
							<select name="study_type" class="form-control input-sm">
								<option value="">Study Type?</option>
								<option value="Full Time" <?php if($row->study_type == "Full Time"){echo 'selected';} ?>>Full Time</option>
								<option value="Part Time" <?php if($row->study_type == "Part Time"){echo 'selected';} ?>>Part Time</option>
							</select>
							</td></tr>
							<tr><th>University</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="university" value="<?php echo $row->university; ?>" placeholder="University">
							</td></tr>
							<tr><th>Programme</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="programme" value="<?php echo $row->programme; ?>" placeholder="Programme">
							</td></tr>
							<tr><th>Prev. University</th><td> : </td>
							<td><?php echo ucwords($row->last_university); ?>
							<input class="form-control input-sm" type="text" name="last_university" value="<?php echo $row->last_university; ?>" placeholder="Prev. University">
							</td></tr>
							<tr><th>Prev. Programme</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="last_programme" value="<?php echo $row->last_programme; ?>" placeholder="Prev. Programme">
							</td></tr>
							<tr><th>Address</th><td> : </td>
							<td>
							<textarea class="form-control input-sm" type="text" name="last_address" placeholder="Address"><?php echo $row->last_address; ?></textarea>
							</td></tr>
						</table>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr style="background-color: #8cd417 ;color:#ffffff"><th colspan="3" class="text-center">Extra Details</th></tr>
							<tr><th width="110px">Occupation</th><td width="1px"> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="occupation" value="<?php echo $row->occupation; ?>" placeholder="Occupation">
							</td></tr>
							<tr><th>Organisation</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="organisation" value="<?php echo $row->organisation; ?>" placeholder="Organisation">
							</td></tr>
							<tr><th>Designation</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="designation" value="<?php echo $row->designation; ?>" placeholder="Designation">
							</td></tr>
							<tr><th>Address</th><td> : </td>
							<td>
							<textarea class="form-control input-sm" type="text" name="off_address" placeholder="Office Address"><?php echo $row->off_address; ?></textarea>
							</td></tr>
							<tr><th>Contact</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="off_contact" value="<?php echo $row->off_contact; ?>" placeholder="Office Contact">
							</td></tr>
							<tr><th>Fax</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="off_fax" value="<?php echo $row->off_fax; ?>" placeholder="Office Fax">
							</td></tr>
							<tr><th>Email</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="off_email" value="<?php echo $row->off_email; ?>" placeholder="Office Email">
							</td></tr>
							<tr><th>Website</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="off_website" value="<?php echo $row->off_website; ?>" placeholder="Office Website">
							</td></tr>
						</table>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr style="background-color:#ffc300;color:#ffffff"><th colspan="3" class="text-center">Survey Details</th></tr>
							<tr><th>Working?</th><td> : </td>
							<td>
							<select name="work_type" class="form-control input-sm">
								<option value="">Working Type?</option>
								<option value="Full Time" <?php if($row->work_type == "Full Time"){echo 'selected';} ?>>Full Time</option>
								<option value="Part Time" <?php if($row->work_type == "Part Time"){echo 'selected';} ?>>Part Time</option>
							</select>
							</td></tr>
							<tr><th>Gender</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="gender" value="<?php echo $row->gender; ?>" placeholder="Gender">
							</td></tr>
							<tr><th>DOB <br> <small class="red">( yyyy-mm-dd )</small></th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="dob" value="<?php echo $row->dob; ?>" placeholder="yyyy-mm-dd">
							</td></tr>
							<tr style="font-weight:bold;" class="red"><th>Alumni Ambassador</th><td> : </td>
							<td>
							<select name="alumni_ambassador" class="form-control input-sm">
								<option value="">Alumni Ambassador?</option>
								<option value="Yes" <?php if($row->alumni_ambassador == "Yes"){echo 'selected';} ?>>Yes</option>
								<option value="No" <?php if($row->alumni_ambassador == "No"){echo 'selected';} ?>>No</option>
							</select>
							</td></tr>
						</table>
					</div>
					<div class="col-md-8 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr style="background-color:#fd8e00;color:#ffffff"><th colspan="3" class="text-center">Alumni News</th></tr>
							<tr><th>Personal</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="personal" value="<?php echo $row->personal; ?>" placeholder="Personal" style="width:100%;">
							</td></tr>
							<tr><th>Professional</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="professional" value="<?php echo $row->professional; ?>" placeholder="Professional" style="width:100%;">
							</td></tr>
							<tr><th>Hobbies</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="hobbies" value="<?php echo $row->hobbies; ?>" placeholder="Hobbies" style="width:100%;">
							</td></tr>
							<tr><th>Vaction</th><td> : </td>
							<td>
							<input class="form-control input-sm" type="text" name="vacation" value="<?php echo $row->vacation; ?>" placeholder="Vacation" style="width:100%;">
							</td></tr>
						</table>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
					  {{ csrf_field() }}
                        <div class="">
							<button id="savedata" type="submit" class="btn btn-success btn-sm">Submit</button>
							<a class="btn btn-danger  btn-sm" href="{!! \Config::get('app.url_base') !!}/alumni">Cancel</a>
                        </div>
                    </div>
					  
				</form>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!-- /page content -->
@include('included.footer')