 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','8')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add Job Title</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
                     
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('savejobtitle')}}" enctype="multipart/form-data" method="post">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Job Title(s) <span class="required">*</span></label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                          <input id="title" name="title" class="form-control" placeholder="Job Title" required="required" type="text">
                        </div>
					  </div>
						 <div class="clearfix"></div>
						<p class="red col-md-offset-3">Use comma sepearted value for multiple data eg. Title1,Title2.</p>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_status">Status  <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label><input type="radio" name="status" required="required" id="status" class="icheck" value="Active" checked> Active</label>
							<label><input type="radio" name="status" required="required" id="status" class="icheck" value="Inactive"> Inactive</label>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
                        <div class="col-md-6 col-md-offset-3">
                          <button id="savedata" type="submit" class="btn btn-success">Submit</button>
                          <a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/jobtitles">Cancel</a>
                        </div>
                      </div>
                    </form>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php }?>
		
  @include('included.footer')