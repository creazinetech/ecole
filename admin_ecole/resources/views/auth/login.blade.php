@include('included.login-header')

  <body class="login">
    <div>
	  
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            
            <img src="{!! \Config::get('app.url_base') !!}/images/mbi-lg.png" class="" style="height:80px;width:250px;">
			<p>Developed by Matrix Bricks Infotech.</p>
			
            <form id="sign_in" method="POST" action="{!! \Config::get('app.url_base') !!}/auth/login">
			{!! csrf_field() !!}
              <h1>Sign In</h1> 
			  <div class="form-group has-feedback">
				<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			  </div>
			  <div class="form-group has-feedback">
				 <input type="hidden" class="form-control" name="role_id" id="role_id" value="2" required>
				 <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			  </div>
              <div>
                <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
                <!--<a href="#">I forgot my password</a>
				<a href="/auth/register" class="to_register"> Register account? </a>-->
              </div>

              <div class="clearfix"></div>
@include('included.login-footer')