 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','3')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add User</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
                     
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('adduser')}}" enctype="multipart/form-data" method="post">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input id="name" name="name" class="form-control col-md-7 col-xs-12" placeholder="Enter user name" required="required" type="text">
						  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email ID <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="user@email.com" required="required" type="text">
						  <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>				  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-password">
                          <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12" required="required">
						  <span class="fa fa-lock form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Role
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<select id="role_id" name="role_id" class="form-control" required="required">
							<option value="">Role?</option>
							<?php 
							if(count($Data)>0){
							foreach($Data as $RoleData){
							?>
							<option value="{{$RoleData->role_id}}">{{$RoleData->role_name}}</option>
							<?php } } ?>
						  </select>
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label><input type="radio" name="status" required="required" id="status" class="flat" value="Active" checked> Active</label>
							<label><input type="radio" name="status" required="required" id="status" class="flat" value="Inactive"> Inactive</label>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
						<!--<input id="_token" name="_token" type="hidden" value="{{ csrf_field() }}">--->
                        <div class="col-md-6 col-md-offset-3">
                          <button id="savedata" type="submit" class="btn btn-success">Submit</button>
                          <a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/users">Cancel</a>
                        </div>
                      </div>
                    </form>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>
  @include('included.footer')