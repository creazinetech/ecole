<?php 
foreach ($Data as $row){ }
?>
<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <title><?php echo ucwords($row->name); ?></title>
  </head><body>
<?php
  if($type == 1){
  ?>
    <table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5">
		<tr>
		<td><b>Entry Id:</b>&nbsp;&nbsp;<?php echo ucwords($row->id); ?></td>
		</tr>
		<tr style="background-color:#edf6fe;">
		<td><b>Student's Name:</b>&nbsp;&nbsp;<?php echo ucwords($row->name); ?></td>
		</tr>
		<tr>
		<td><b>Date Of Birth: (DD-MM-YYYY)</b>&nbsp;&nbsp;<?php echo date('d-m-Y', strtotime($row->dob)); ?></td>
		</tr>
		<tr style="background-color:#edf6fe;">
		<td><b>Residential Address:</b>&nbsp;&nbsp;<?php echo ucwords($row->address); ?></td>
		</tr>
		<tr>
		<td><b>City:</b>&nbsp;&nbsp;<?php echo ucwords($row->city); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>State:</b>&nbsp;&nbsp;<?php echo ucwords($row->state); ?></td>
		</tr>
		<tr>
		<td><b>Pincode:</b>&nbsp;&nbsp;<?php echo ucwords($row->pincode); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Passport Type:</b>&nbsp;&nbsp;<?php echo ucwords($row->passport); ?></td>
		</tr>
		<tr>
		<td><b>Nationality:</b>&nbsp;&nbsp; <?php echo $row->nationality; ?> </td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Name Of Present School:</b>&nbsp;&nbsp;<?php echo ucwords($row->nopskul); ?></td>
		</tr>
		<tr>
		<td><b>Present Grade:</b>&nbsp;&nbsp;<?php echo ucwords($row->grade); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Grade To Join & (Year to Join):</b>&nbsp;<?php echo ucwords($row->gradetjoin); ?>&nbsp;(<?php echo $row->year; ?>)</td>
		</tr>
		<tr>
		<td><b>Parent Details:</b><br>
            <table class="table table-bordered tabled-stripped table-hover" border="1" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
				<tr>
					<th width="20%">Details</th>
					<th width="40%">Father</th>
					<th width="40%">Mother</th>
				</tr>
				<tr>
					<th>Name:</th>
					<td><?php if(!empty($row->fname)){echo ucwords($row->fname); } ?></td>
					<td><?php if(!empty($row->mname)){echo ucwords($row->mname); } ?></td>
				</tr>
				<tr>
					<th>Mobile No:</th>
					<td><?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?></td>
					<td><?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?></td>
				</tr>
				<tr>
					<th>Residential Tel:</th>
					<td><?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?></td>
					<td><?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?></td>
				</tr>
				<tr>
					<th>Email:</th>
					<td><?php if(!empty($row->femail)){echo $row->femail; } ?></td>
					<td><?php if(!empty($row->memail)){echo $row->memail; } ?></td>
				</tr>
				<tr>
					<th>Occupation:</th>
					<td><?php if(!empty($row->foccupation)){echo ucwords($row->foccupation); } ?></td>
					<td><?php if(!empty($row->moccupation)){echo ucwords($row->moccupation); } ?></td>
				</tr>
				<tr>
					<th>Designation:</th>
					<td><?php if(!empty($row->fdesignation)){echo ucwords($row->fdesignation); } ?></td>
					<td><?php if(!empty($row->mdesignation)){echo ucwords($row->mdesignation); } ?></td>
				</tr>
				<tr>
					<th>Name of the Organization:</th>
					<td><?php if(!empty($row->forganization)){echo ucwords($row->forganization); } ?></td>
					<td><?php if(!empty($row->morganization)){echo ucwords($row->morganization); } ?></td>
				</tr>
				<tr>
					<th>Official Address:</th>
					<td><?php if(!empty($row->foffadd)){echo ucwords($row->foffadd); } ?></td>
					<td><?php if(!empty($row->moffadd)){echo ucwords($row->moffadd); } ?></td>
				</tr>
				<tr>
					<th>Office Tel:</th>
					<td><?php if(!empty($row->fofftel)){echo ucwords($row->fofftel); } ?></td>
					<td><?php if(!empty($row->mofftel)){echo ucwords($row->mofftel); } ?></td>
				</tr>
			</table>
		</td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Tell us how you heard about our School:</b><br><?php echo ucwords($row->ourskul); ?></td>
		</tr>
		
	</table>
  <?php 
  }
  if($type==2){
  ?>
  
	<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
		<tr>
		<td><b>Entry Id:</b><br>
		<input type="text" value="<?php echo ucwords($row->id); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Student's Name:</b><br>
		<input type="text" value="<?php echo ucwords($row->name); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Date Of Birth: (DD-MM-YYYY)</b>
		<br>
		<input type="text" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Residential Address:</b><br>
		<input type="text" value="<?php echo ucwords($row->address); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>City:</b><br>
		<input type="text" value="<?php echo ucwords($row->city); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>State:</b><br>
		<input type="text" value="<?php echo ucwords($row->state); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Pincode:</b><br>
		<input type="text" value="<?php echo ucwords($row->pincode); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Passport Type:</b><br>
		<input type="text" value="<?php echo $row->passport; ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Nationality:</b><br>
		<input type="text" value="<?php echo $row->nationality; ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Name Of Present School:</b><br>
		<input type="text" value="<?php echo ucwords($row->nopskul); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Present Grade:</b><br>
		<input type="text" value="<?php echo ucwords($row->grade); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Grade To Join:</b><br>
		<input type="text" value="<?php echo ucwords($row->gradetjoin); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Academic Year For Which Admission Is Sought:</b><br>
		<input type="text" value="<?php echo ucwords($row->year); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		
		<tr>
		    <td>
		    <b>Parents Details:</b><br>
            <table class="table table-responsive table-bordered table-hover" width="100%" border="0"> 
				<tr>
					<th width="20%">Details</th>
					<th width="40%">Father</th>
					<th width="40%">Mother</th>
				</tr>
				<tr>
					<th>Name:</th>
					<td><input type="text" value="<?php if(!empty($row->fname)){echo ucwords($row->fname); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->mname)){echo ucwords($row->mname); } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Mobile No:</th>
					<td><input type="text" value="<?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Residential Tel:</th>
					<td><input type="text" value="<?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Email:</th>
					<td><input type="text" value="<?php if(!empty($row->femail)){echo $row->femail; } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->memail)){echo $row->memail; } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Occupation:</th>
					<td><input type="text" value="<?php if(!empty($row->foccupation)){echo ucwords($row->foccupation); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->moccupation)){echo ucwords($row->moccupation); } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Designation:</th>
					<td><input type="text" value="<?php if(!empty($row->fdesignation)){echo ucwords($row->fdesignation); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->mdesignation)){echo ucwords($row->mdesignation); } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Name of the Organization:</th>
					<td><input type="text" value="<?php if(!empty($row->forganization)){echo ucwords($row->forganization); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->morganization)){echo ucwords($row->morganization); } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Official Address:</th>
					<td><input type="text" value="<?php if(!empty($row->foffadd)){echo ucwords($row->foffadd); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->moffadd)){echo ucwords($row->moffadd); } ?>" style="width:100%;height:30px;"></td>
				</tr>
				<tr>
					<th>Office Tel:</th>
					<td><input type="text" value="<?php if(!empty($row->fofftel)){echo ucwords($row->fofftel); } ?>" style="width:100%;height:30px;"></td>
					<td><input type="text" value="<?php if(!empty($row->mofftel)){echo ucwords($row->mofftel); } ?>" style="width:100%;height:30px;"></td>
				</tr>
			</table>
		</td>
		</tr>
		<tr >
		<td><b>Tell us how you heard about our School:</b><br>
		<input type="text" value="<?php echo ucwords($row->ourskul); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		
	</table>
  <?php } if($type==3){?>
  
    <table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5">
		<tr>
		<td><b>Entry Id:</b>&nbsp;&nbsp;<?php echo ucwords($row->id); ?></td>
		</tr>
		<tr>
		<td><b>Student's Name:</b>&nbsp;&nbsp;<?php echo ucwords($row->name); ?></td>
		</tr>
		<tr>
		<td><b>Date Of Birth: (DD-MM-YYYY)</b>&nbsp;&nbsp;<?php echo date('d-m-Y', strtotime($row->dob)); ?></td>
		</tr>
		<tr>
		<td><b>Residential Address:</b>&nbsp;&nbsp;<?php echo ucwords($row->address); ?></td>
		</tr>
		<tr>
		<td><b>City:</b>&nbsp;&nbsp;<?php echo ucwords($row->city); ?></td>
		</tr>
		<tr>
		<td><b>State:</b>&nbsp;&nbsp;<?php echo ucwords($row->state); ?></td>
		</tr>
		<tr>
		<td><b>Pincode:</b>&nbsp;&nbsp;<?php echo ucwords($row->pincode); ?></td>
		</tr>
		<tr>
		<td><b>Passport Type:</b>&nbsp;&nbsp;<?php echo ucwords($row->passport); ?></td>
		</tr>
		<tr>
		<td><b>Nationality:</b>&nbsp;&nbsp; <?php echo $row->nationality; ?> </td>
		</tr>
		<tr>
		<td><b>Name Of Present School:</b>&nbsp;&nbsp;<?php echo ucwords($row->nopskul); ?></td>
		</tr>
		<tr>
		<td><b>Present Grade:</b>&nbsp;&nbsp;<?php echo ucwords($row->grade); ?></td>
		</tr>
		<tr>
		<td><b>Grade To Join & (Year to Join):</b>&nbsp;<?php echo ucwords($row->gradetjoin); ?>&nbsp;(<?php echo $row->year; ?>)</td>
		</tr>
		
		<tr>
		<td><b>Parent Details:</b><br>
            <table class="table table-bordered tabled-stripped table-hover" border="1" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
				<tr>
					<th width="20%">Details</th>
					<th width="40%">Father</th>
					<th width="40%">Mother</th>
				</tr>
				<tr>
					<th>Name:</th>
					<td><?php if(!empty($row->fname)){echo ucwords($row->fname); } ?></td>
					<td><?php if(!empty($row->mname)){echo ucwords($row->mname); } ?></td>
				</tr>
				<tr>
					<th>Mobile No:</th>
					<td><?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?></td>
					<td><?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?></td>
				</tr>
				<tr>
					<th>Residential Tel:</th>
					<td><?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?></td>
					<td><?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?></td>
				</tr>
				<tr>
					<th>Email:</th>
					<td><?php if(!empty($row->femail)){echo $row->femail; } ?></td>
					<td><?php if(!empty($row->memail)){echo $row->memail; } ?></td>
				</tr>
				<tr>
					<th>Occupation:</th>
					<td><?php if(!empty($row->foccupation)){echo ucwords($row->foccupation); } ?></td>
					<td><?php if(!empty($row->moccupation)){echo ucwords($row->moccupation); } ?></td>
				</tr>
				<tr>
					<th>Designation:</th>
					<td><?php if(!empty($row->fdesignation)){echo ucwords($row->fdesignation); } ?></td>
					<td><?php if(!empty($row->mdesignation)){echo ucwords($row->mdesignation); } ?></td>
				</tr>
				<tr>
					<th>Name of the Organization:</th>
					<td><?php if(!empty($row->forganization)){echo ucwords($row->forganization); } ?></td>
					<td><?php if(!empty($row->morganization)){echo ucwords($row->morganization); } ?></td>
				</tr>
				<tr>
					<th>Official Address:</th>
					<td><?php if(!empty($row->foffadd)){echo ucwords($row->foffadd); } ?></td>
					<td><?php if(!empty($row->moffadd)){echo ucwords($row->moffadd); } ?></td>
				</tr>
				<tr>
					<th>Office Tel:</th>
					<td><?php if(!empty($row->fofftel)){echo ucwords($row->fofftel); } ?></td>
					<td><?php if(!empty($row->mofftel)){echo ucwords($row->mofftel); } ?></td>
				</tr>
			</table>
		</td>
		</tr>
		<tr>
		<td><b>Tell us how you heard about our School:</b><br><?php echo ucwords($row->ourskul); ?></td>
		</tr>
	</table>
  <?php } ?>
  </body></html>