@include('included.header')
@include('included.super-admin-sidebar')
<?php 
foreach($Data as $row){} 
?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">

	<div class="clearfix"></div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
			<h2>Edit Admission</h2>
			<a href="{!! \Config::get('app.url_base') !!}/admissions" class="btn btn-danger btn-sm pull-right"><i class="fa  fa-close"></i> Close</a>
			<div class="clearfix"></div>
		  </div>
		  <div class="x_content">
				  
		  <?php 
		  foreach ($Data as $row){ } 
		  ?>
		  <form class="form-horizontal form-label-left" novalidate action="{{URL::route('updateadmissions')}}" enctype="multipart/form-data" method="post">
		  <div class="col-md-6 col-sm-6 col-xs-12">
		        <h3>&nbsp;Student Details:</h3> 
		     <div class="col-md-6 col-sm-6 col-xs-12">
			<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
				<input type="hidden" value="<?php echo ucwords($row->id); ?>" name="id" id="id" class="form-control input-sm">
				<td><b>Student's Name:</b><br>
				<input type="text" value="<?php echo ucwords($row->name); ?>" name="name" id="name" class="form-control">
				<span id="qnameerr" style="color:red;display:none;"> Please Enter name.</span>
				</td>
				</tr>
				<tr>
				<td><b>Date Of Birth: (DD-MM-YYYY)</b>
				<br>
				<input type="text" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" name="dob" id="dob" class="form-control datepicker">
				<span id="qdateerr" style="color:red;display:none;"> Please Enter Birth Date.</span>
				</td>
				</tr>
				<tr>
				<td><b>Name Of Present School:</b><br>
				<input type="text" value="<?php echo ucwords($row->nopskul); ?>" name="nopskul" id="nopskul" class="form-control">
				<span id="qnskulerr" style="color:red;display:none;"> Name of Present School?</span>
				</td>
				</tr>
				<tr >
				<td><b>Present Grade:</b><br>
				<input type="text" value="<?php echo ucwords($row->grade); ?>" name="grade" id="grade" class="form-control">
				<span id="qgradeerr" style="color:red;display:none;"> Enter present grade.</span>
				</td>
				</tr>
				<tr>
				<td><b>Grade To Join:</b><br>
				<input type="text" value="<?php echo ucwords($row->gradetjoin); ?>" name="gradetjoin" id="gradetjoin" class="form-control">
				<span id="qjoinerr" style="color:red;display:none;"> Grade to join</span>
				</td>
				</tr>
				<tr >
				<td><b>Academic Year For Admission:</b><br>
				<select class='form-control input-sm' id='year' name='year'>
					<option value=''>Admission Year?</option>
					<?php 
					$start_year=date('Y')-1;
					$end_year= date('Y')+2;
					$start_year1=$start_year+1;
					$end_year1=$end_year+1;
					for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ 
					$Year = $i.'-'.$j;
					?>
					<option value='<?php echo $i.'-'.$j; ?>' <?php if($row->year == $Year){echo 'selected';} ?>><?php echo $Year;?></option>
					<?php } ?>
				</select>
				<span id="qayearerr" style="color:red;display:none;"> Select Academic Year.</span>
				</td>
				</tr>
				<tr>
				<td><b>Nationality:</b><br>
				<input type="text" value="<?php echo ucwords($row->nationality); ?>" name="nationality" id="nationality" class="form-control">
				<span id="nationalityerr" style="color:red;display:none;"> Enter Nationality.</span>
				</td>
				</tr>
				<tr>
				<td><b>Passport Type:</b><br>
				<select class="form-control" id="passport" name="passport">
					<option value="Indian Passport" <?php if($row->state == 'Indian Passport'){ echo 'selected';} ?>>Indian Passport</option>
					<option value="Non-Indian Passport" <?php if($row->state == 'Non-Indian Passport'){ echo 'selected';} ?>>Non-Indian Passport</option>
				</select>
				<span id="qpassporterr" style="color:red;display:none;"> Select Passport Type.</span>
				</td>
				</tr>
				</table>
				</div> 
				
		        <div class="col-md-6 col-sm-6 col-xs-12">
			    <table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
				<tr>
				<td><b>How you heard about our School:</b><br>
				<textarea name="ourskul" id="ourskul" class="form-control" rows="4"><?php echo ucwords($row->ourskul); ?></textarea>
				<span id="ourskulerr" style="color:red;display:none;"> How you heard about our School.</span>
				</td>
				</tr>
				<tr>
				<td><b>Residential Address:</b><br>
				<textarea name="address" id="address" class="form-control" rows="5"><?php echo ucwords($row->address); ?></textarea>
				<span id="addresserr" style="color:red;display:none;"> Enter Residential Address.</span>
				</td>
				</tr>
				<tr >
				<td><b>City:</b><br>
				<input type="text" value="<?php echo ucwords($row->city); ?>" name="city" id="city" class="form-control">
				<span id="qcityerr" style="color:red;display:none;"> Please Enter City.</span>
				</td>
				</tr>
				<tr>
				<td><b>State:</b><br>
					<select class="form-control" id="state" name="state">
					  <option value=''>Select State</option>
					  <option value='Andhra Pradesh' <?php if($row->state == 'Andhra Pradesh'){ echo 'selected';} ?>>Andhra Pradesh</option>
					  <option value='Arunachal Pradesh' <?php if($row->state == 'Arunachal Pradesh'){ echo 'selected';} ?>>Arunachal Pradesh</option>
					  <option value='Assam' <?php if($row->state == 'Assam'){ echo 'selected';} ?>>Assam </option>
					  <option value='Bihar' <?php if($row->state == 'Bihar'){ echo 'selected';} ?>>Bihar</option>
					  <option value='Chhattisgarh' <?php if($row->state == 'Chhattisgarh'){ echo 'selected';} ?>>Chhattisgarh    </option>
					  <option value='Goa' <?php if($row->state == 'Goa'){ echo 'selected';} ?>>Goa</option>
					  <option value='Gujarat' <?php if($row->state == 'Gujarat'){ echo 'selected';} ?>>Gujarat</option>
					  <option value='Haryana' <?php if($row->state == 'Haryana'){ echo 'selected';} ?>>Haryana </option>
					  <option value='Himachal Pradesh' <?php if($row->state == 'Himachal Pradesh'){ echo 'selected';} ?>>Himachal Pradesh</option>
					  <option value='Jammu & Kashmir' <?php if($row->state == 'Jammu & Kashmir'){ echo 'selected';} ?>>Jammu & Kashmir</option>
					  <option value='Jharkhand' <?php if($row->state == 'Jharkhand'){ echo 'selected';} ?>>Jharkhand </option>
					  <option value='Karnataka' <?php if($row->state == 'Karnataka'){ echo 'selected';} ?>>Karnataka </option>
					  <option value='Kerla' <?php if($row->state == 'Kerla'){ echo 'selected';} ?>>Kerla </option>
					  <option value='Madhya Pradesh' <?php if($row->state == 'Madhya Pradesh'){ echo 'selected';} ?>>Madhya Pradesh</option>
					  <option value='Maharashtra' <?php if($row->state == 'Maharashtra'){ echo 'selected';} ?>>Maharashtra  </option>
					  <option value='Manipur' <?php if($row->state == 'Manipur'){ echo 'selected';} ?>>Manipur </option>
					  <option value='Meghalaya' <?php if($row->state == 'Meghalaya'){ echo 'selected';} ?>>Meghalaya</option>
					  <option value='Mizoram' <?php if($row->state == 'Mizoram'){ echo 'selected';} ?>> Mizoram</option>
					  <option value='Nagaland' <?php if($row->state == 'Nagaland'){ echo 'selected';} ?>>Nagaland</option>
					  <option value='Orissa' <?php if($row->state == 'Orissa'){ echo 'selected';} ?>>Orissa </option>
					  <option value='Punjab' <?php if($row->state == 'Punjab'){ echo 'selected';} ?>>Punjab </option>
					  <option value='Sikkim' <?php if($row->state == 'Sikkim'){ echo 'selected';} ?>>Sikkim </option>
					  <option value='Tamilnadu' <?php if($row->state == 'Tamilnadu'){ echo 'selected';} ?>>Tamilnadu</option>
					  <option value='Tripura' <?php if($row->state == 'Tripura'){ echo 'selected';} ?>>Tripura </option>
					  <option value='Uttar Pradesh' <?php if($row->state == 'Uttar Pradesh'){ echo 'selected';} ?>>Uttar Pradesh </option>
					  <option value='Uttaranchal' <?php if($row->state == 'Uttaranchal'){ echo 'selected';} ?>> Uttaranchal </option>
					  <option value='West Bengal' <?php if($row->state == 'West Bengal'){ echo 'selected';} ?>>West Bengal</option>
					  <option value='Outside India' <?php if($row->state == 'Outside India'){ echo 'selected';} ?>>Outside India</option>
				  </select>
				  <span id="qstaterr" style="color:red;display:none;"> Please Select State.</span>
				</td>
				</tr>
				<tr >
				<td><b>Country:</b><br>
				<input type="text" value="<?php echo ucwords($row->country); ?>" name="country" id="country" class="form-control">
				<span id="qcountryerr" style="color:red;display:none;"> Please Enter Country.</span>
				</td>
				</tr>
				<tr >
				<td><b>Pincode:</b><br>
				<input type="text" value="<?php echo ucwords($row->pincode); ?>" name="pincode" id="pincode" class="form-control">
				<span id="qpincodeerr" style="color:red;display:none;"> Please Enter 6 digit Pincode.</span>
				</td>
				</tr>
			</table>
			
			</div>
			</div>
			
			<div class="col-md-6 col-sm-6 col-xs-12">
			    <h3>&nbsp;Parents Details:</h3>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    <table>
			    <tr>
				<td><b>Father Name:</b><br>
				<input type="text" value="<?php echo ucwords($row->fname); ?>" name="fname" id="fname" class="form-control">
				<span id="fnameerr" style="color:red;display:none;"> Enter Father's Name?</span>
				</td>
				</tr>
				<tr>
				<td><b>Father's Mobile No:</b><br>
				<input type="text" value="<?php echo ucwords($row->fmobile); ?>" name="fmobile" id="fmobile" class="form-control">
				<span id="fmobileerr" style="color:red;display:none;"> Mother's Mobile No.?</span>
				</td>
				</tr>
				<tr>
				<td><b>Father's Res. Tel:</b><br>
				<input type="text" value="<?php echo ucwords($row->frestel); ?>" name="frestel" id="frestel" class="form-control">
				<span id="frestelerr" style="color:red;display:none;"> Mother's Office Tel.?</span>
				</td>
				</tr>
				<tr>
				<td><b>Father's Email Id:</b><br>
				<input type="text" value="<?php echo $row->femail; ?>" name="femail" id="femail" class="form-control">
				<span id="femailerr" style="color:red;display:none;"> Enter Father's Email id.</span>
				</td>
				</tr>
				<tr >
				<td><b>Father's Designation:</b><br>
				<input type="text" value="<?php echo ucwords($row->fdesignation); ?>" name="fdesignation" id="fdesignation" class="form-control">
				<span id="fdesignationerr" style="color:red;display:none;"> Father's Designation?</span>
				</td>
				</tr>
				<tr>
				<td><b>Father's Organization:</b><br>
				<input type="text" value="<?php echo ucwords($row->forganization); ?>" name="forganization" id="forganization" class="form-control">
				<span id="forganizationerr" style="color:red;display:none;"> Father's Organization?</span>
				</td>
				</tr>
				<tr >
				<td><b>Father's Occupation:</b><br>
				<input type="text" value="<?php echo ucwords($row->foccupation); ?>" name="foccupation" id="foccupation" class="form-control">
				<span id="foccupationerr" style="color:red;display:none;"> Father's Occupation?</span>
				</td>
				</tr>
				<tr>
				<td><b>Father's Official Address:</b><br>
				<textarea name="foffadd" id="foffadd" class="form-control"><?php echo ucwords($row->foffadd); ?></textarea>
				<span id="foffadderr" style="color:red;display:none;"> Father's Official Address?</span>
				</td>
				</tr>
				<tr>
				<td><b>Father's Office Tel:</b><br>
				<input type="text" value="<?php echo ucwords($row->fofftel); ?>" name="fofftel" id="fofftel" class="form-control">
				<span id="fofftelerr" style="color:red;display:none;"> Enter 10 digit Office Tel.</span>
				</td>
				</tr>
			    </table>
			    </div>
			    
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    <table>
			    <tr>
				<td><b>Mother Name:</b><br>
				<input type="text" value="<?php echo ucwords($row->mname); ?>" name="mname" id="mname" class="form-control">
				<span id="mnameerr" style="color:red;display:none;"> Mother's Name?</span>
				</td>
				</tr>
				<tr>
				<td><b>Mother's Mobile No:</b><br>
				<input type="text" value="<?php echo ucwords($row->mmobile); ?>" name="mmobile" id="mmobile" class="form-control">
				<span id="mmobileerr" style="color:red;display:none;"> Mother's Mobile No.?</span>
				</td>
				</tr>
				<tr>
				<td><b>Mother's Res. Tel:</b><br>
				<input type="text" value="<?php echo ucwords($row->mrestel); ?>" name="mrestel" id="mrestel" class="form-control">
				<span id="mrestelerr" style="color:red;display:none;"> Mother's Office Tel.?</span>
				</td>
				</tr>
				<tr>
				<td><b>Mother's Email Id:</b><br>
				<input type="text" value="<?php echo $row->memail; ?>" name="memail" id="memail" class="form-control">
				<span id="memailerr" style="color:red;display:none;"> Mother's Email id?</span>
				</td>
				</tr>
				<tr >
				<td><b>Mother's Designation:</b><br>
				<input type="text" value="<?php echo ucwords($row->mdesignation); ?>" name="mdesignation" id="mdesignation" class="form-control">
				<span id="mdesignationerr" style="color:red;display:none;">Mother's Designation?</span>
				</td>
				</tr>
				<tr>
				<td><b>Mother's Organization:</b><br>
				<input type="text" value="<?php echo ucwords($row->morganization); ?>" name="morganization" id="morganization" class="form-control">
				<span id="morganizationerr" style="color:red;display:none;">Mother's Organization?</span>
				</td>
				</tr>
				<tr >
				<td><b>Mother's Occupation:</b><br>
				<input type="text" value="<?php echo ucwords($row->moccupation); ?>" name="moccupation" id="moccupation" class="form-control">
				<span id="moccupationerr" style="color:red;display:none;"> Mother's Occupation?</span>
				</td>
				</tr>
				<tr>
				<td><b>Mother's Official Address:</b><br>
				<textarea name="moffadd" id="moffadd" class="form-control"><?php echo ucwords($row->moffadd); ?></textarea>
				<span id="moffadderr" style="color:red;display:none;"> Mother's Official Address?.</span>
				</td>
				</tr>
				<tr>
				<td><b>Mother's Office Tel:</b><br>
				<input type="text" value="<?php echo ucwords($row->mofftel); ?>" name="mofftel" id="mofftel" class="form-control">
				<span id="mofftelerr" style="color:red;display:none;"> Mother's Office Tel.?</span>
				</td>
				</tr>
			    </table>
			    </div>
			    
			</div>
			
			
				<div class="form-group">
					{{ csrf_field() }}
					<div class="col-md-6 col-md-offset-3">
					<button id="admission-btn" type="submit" class="btn btn-success">Submit</button>
					<a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/admissions">Cancel</a>
					</div>
				</div>
			</form>
  
			</div>
		  </div>
		</div>
	  </div>
	</div>
	</div>
	
<script type="text/javascript">

$(document).ready(function(){
	$('#admission-btn').click(function(){
	var name = $('#name').val();
	var dob = $('#dob').val();
	var address = $('#address').val();
	var nopskul = $('#nopskul').val();
	var grade = $('#grade').val();
	var gradetjoin = $('#gradetjoin').val();
	var year = $('#year').val();
	var fname = $('#fname').val();
	var fdesignation = $('#fdesignation').val();
	var forganization = $('#forganization').val();
	var foccupation = $('#foccupation').val();
	var foffadd = $('#foffadd').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	var fofftel = $('#fofftel').val();
	var fmobile = $('#fmobile').val();
	var frestel = $('#frestel').val();
	var mmobile = $('#mmobile').val();
	var mrestel = $('#mrestel').val();
	var femail = $('#femail').val();
	var memail = $('#memail').val();
	var passport = $('#passport').val();
	var ourskul = $('#ourskul').val();
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

	    if(name==""){
			$('#qnameerr').css('display','block'); 
			$('#name').focus(); 
			return false;
		}else{
			$('#qnameerr').css('display','none'); 
		}
		if(dob==""){
			$('#qdateerr').css('display','block'); 
			$('#dob').focus(); 
			return false;
		}else{
			$('#qdateerr').css('display','none'); 
		}
		if(nopskul==""){
			$('#qnskulerr').css('display','block'); 
			$('#nopskul').focus(); 
			return false;
		}else{
			$('#qnskulerr').css('display','none'); 
		}
		if(grade==""){
			$('#qgradeerr').css('display','block'); 
			$('#grade').focus(); 
			return false;
		}else{
			$('#qgradeerr').css('display','none'); 
		}
		if(gradetjoin==""){
			$('#qjoinerr').css('display','block'); 
			$('#gradetjoin').focus(); 
			return false;
		}else{
			$('#qjoinerr').css('display','none'); 
		}
		if(year==""){
			$('#qayearerr').css('display','block'); 
			$('#year').focus(); 
			return false;
		}else{
			$('#qayearerr').css('display','none'); 
		}
		if(nationality==""){
			$('#nationalityerr').css('display','block'); 
			$('#nationality').focus(); 
			return false;
		}else{
			$('#nationalityerr').css('display','none'); 
		}
		if(passport==""){
			$('#qpassporterr').css('display','block'); 
			$('#passport').focus(); 
			return false;
		}else{
			$('#qpassporterr').css('display','none'); 
		}
		if(ourskul==""){
			$('#ourskulerr').css('display','block'); 
			$('#ourskul').focus(); 
			return false;
		}else{
			$('#ourskulerr').css('display','none'); 
		}
		if(address==""){
			$('#addresserr').css('display','block'); 
			$('#address').focus(); 
			return false;
		}else{
			$('#addresserr').css('display','none'); 
		}
		if(city==""){
			$('#qcityerr').css('display','block'); 
			$('#city').focus(); 
			return false;
		}else{
			$('#qcityerr').css('display','none'); 
		}
		if(state==""){
			$('#qstaterr').css('display','block'); 
			$('#state').focus(); 
			return false;
		}else{
			$('#qstaterr').css('display','none'); 
		}
		if(country==""){
			$('#qcountryerr').css('display','block'); 
			$('#country').focus(); 
			return false;
		}else{
			$('#qcountryerr').css('display','none'); 
		}
		if(pincode=="" || pincode.length<6){
			$('#qpincodeerr').css('display','block'); 
			$('#pincode').focus(); 
			return false;
		}else{
			$('#qpincodeerr').css('display','none'); 
		}
		
		if(fname==""){
			$('#fnameerr').css('display','block'); 
			$('#fname').focus(); 
			return false;
		}else{
			$('#fnameerr').css('display','none'); 
		}
		if(frestel=="" || frestel.length<10 || frestel.length>15){
			$('#frestelerr').css('display','block'); 
			$('#frestel').focus(); 
			return false;
		}else{
			$('#frestelerr').css('display','none'); 
		}
		
		if(fmobile=="" || fmobile.length<10 || fmobile.length>15){
			$('#fmobileerr').css('display','block'); 
			$('#fmobile').focus(); 
			return false;
		}else{
			$('#fmobileerr').css('display','none'); 
		}
		if(fdesignation==""){
			$('#fdesignationerr').css('display','block'); 
			$('#fdesignation').focus(); 
			return false;
		}else{
			$('#fdesignationerr').css('display','none'); 
		}
		if(forganization==""){
			$('#forganizationerr').css('display','block'); 
			$('#forganization').focus(); 
			return false;
		}else{
			$('#forganizationerr').css('display','none'); 
		}
		if(foccupation==""){
			$('#foccupationerr').css('display','block'); 
			$('#foccupation').focus(); 
			return false;
		}else{
			$('#foccupationerr').css('display','none'); 
		}
		if(foffadd==""){
			$('#foffadderr').css('display','block'); 
			$('#foffadd').focus(); 
			return false;
		}else{
			$('#foffadderr').css('display','none'); 
		}
		if(fofftel=="" || fofftel.length<10 || fofftel.length>15){
			$('#fofftelerr').css('display','block'); 
			$('#fofftel').focus(); 
			return false;
		}else{
			$('#fofftelerr').css('display','none'); 
		}
		if(femail=="" || !email_regex.test(femail)){
			$('#femailerr').css('display','block'); 
			$('#femail').focus(); 
			return false;
		}else{
			$('#femailerr').css('display','none'); 
		}
		
		
		if(mmobile!=""){
		    if(mmobile.length<10 || mmobile.length>15){
    			$('#mmobileerr').css('display','block'); 
    			$('#mmobile').focus(); 
    			return false;
		    }else{
		    	$('#mmobileerr').css('display','none');
		    }
		}
		if(mrestel!=""){
		    if(mrestel.length<10 || mrestel.length>15){
			    $('#mrestelerr').css('display','block'); 
			    $('#mrestel').focus(); 
			    return false;
		    }else{
		    	$('#mrestelerr').css('display','none'); 
		    }
		}
		if(memail!=""){
		    if(!email_regex.test(memail)){
			    $('#memailerr').css('display','block'); 
			    $('#memail').focus(); 
			return false;
	    	}else{
		    	$('#memailerr').css('display','none'); 
		    }
        }
		if(mofftel !=""){
		    if(fofftel.length<10 || fofftel.length>15){
		    	$('#mofftelerr').css('display','block'); 
			    $('#mofftel').focus(); 
			return false;
    		}else{
    			$('#mofftelerr').css('display','none'); 
    		}
	    
	    }
		/*
		if(mname==""){
			$('#mnameerr').css('display','block'); 
			$('#mname').focus(); 
			return false;
		}else{
			$('#mnameerr').css('display','none'); 
		}
		if(mdesignation==""){
			$('#mdesingnationerr').css('display','block'); 
			$('#mdesignation').focus(); 
			return false;
		}else{
			$('#mdesingnationerr').css('display','none'); 
		}
		if(morganization==""){
			$('#morganizationerr').css('display','block'); 
			$('#morganization').focus(); 
			return false;
		}else{
			$('#morganizationerr').css('display','none'); 
		}
		if(moccupation==""){
			$('#moccupationerr').css('display','block'); 
			$('#moccupation').focus(); 
			return false;
		}else{
			$('#moccupationerr').css('display','none'); 
		}
		if(moffadd==""){
			$('#moffadderr').css('display','block'); 
			$('#moffadd').focus(); 
			return false;
		}else{
			$('#moffadderr').css('display','none'); 
		}
		*/
		
		

	});	
});	
</script> 


<script>
$('.datepicker').datepicker({
	autoclose: true
});
</script>
	<!-- /page content -->
@include('included.footer')