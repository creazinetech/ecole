@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }

foreach($Data as $row){} 
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Admission Details</h2>
					<a href="{!! \Config::get('app.url_base') !!}/admissions" class="btn btn-danger btn-sm pull-right"><i class="fa  fa-close"></i> Close</a>
                    <div class="clearfix"></div>
                  </div>
				  <div class="x_content">
					
					<div class="col-md-12 col-sm-12 col-xs-12">
					  
						<div class="col-md-2 col-sm-2 col-xs-2"></div>
						
						<section id="EcolePrint" style="padding:0px;display:block;" >
						<div class="col-md-8 col-sm-8 col-xs-8" style="border:1px solid #ececec;color:black;">
						<br>
							<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5">
								<tr>
								<td><b>Entry Id:</b>&nbsp;<?php echo ucwords($row->id); ?></td>
								</tr>
								<tr style="background-color:#edf6fe;">
								<td><b>Student's Name:</b>&nbsp;<?php echo ucwords($row->name); ?></td>
								</tr>
								<tr>
								<td><b>Date Of Birth: (DD-MM-YYYY)</b>&nbsp;<?php echo date('d-m-Y', strtotime($row->dob)); ?></td>
								</tr>
								<tr style="background-color:#edf6fe;">
								<td><b>Residential Address:</b><br><?php echo ucwords($row->address); ?></td>
								</tr>
								<tr>
								<td><b>City:</b>&nbsp;<?php echo ucwords($row->city); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>State:</b>&nbsp;<?php echo ucwords($row->state); ?></td>
								</tr>
								<tr>
								<td><b>Pincode:</b>&nbsp;<?php echo ucwords($row->pincode); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Passport Type:</b>&nbsp;<?php echo ucwords($row->passport); ?></td>
								</tr>
								<tr>
								<td><b>Nationality:</b>&nbsp;<?php echo ucwords($row->nationality); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Name Of Present School:</b>&nbsp;<?php echo ucwords($row->nopskul); ?></td>
								</tr>
								<tr>
								<td><b>Present Grade:</b>&nbsp;<?php echo ucwords($row->grade); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Grade To Join & (Year to Join):</b>&nbsp;<?php echo ucwords($row->gradetjoin); ?>&nbsp;(<?php echo $row->year; ?>)</td>
								</tr>
								
								<tr>
								<td>
								    <b>Parent Details:<br>
								<table class="table table-responsive table-bordered table-hover" width="100%"> 
								<thead>
								<tr>
									<th width="20%">Details</th>
									<th width="40%">Father</th>
									<th width="40%">Mother</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th>Name:</th>
									<td><?php if(!empty($row->fname)){echo ucwords($row->fname); } ?></td>
									<td><?php if(!empty($row->mname)){echo ucwords($row->mname); } ?></td>
								</tr>
								<tr>
									<th>Mobile No:</th>
									<td><?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?></td>
									<td><?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?></td>
								</tr>
								<tr>
									<th>Residential Tel:</th>
									<td><?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?></td>
									<td><?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?></td>
								</tr>
								<tr>
									<th>Email:</th>
									<td><?php if(!empty($row->femail)){echo $row->femail; } ?></td>
									<td><?php if(!empty($row->memail)){echo $row->memail; } ?></td>
								</tr>
								<tr>
									<th>Occupation:</th>
									<td><?php if(!empty($row->foccupation)){echo ucwords($row->foccupation); } ?></td>
									<td><?php if(!empty($row->moccupation)){echo ucwords($row->moccupation); } ?></td>
								</tr>
								<tr>
									<th>Designation:</th>
									<td><?php if(!empty($row->fdesignation)){echo ucwords($row->fdesignation); } ?></td>
									<td><?php if(!empty($row->mdesignation)){echo ucwords($row->mdesignation); } ?></td>
								</tr>
								<tr>
									<th>Name of the Organization:</th>
									<td><?php if(!empty($row->forganization)){echo ucwords($row->forganization); } ?></td>
									<td><?php if(!empty($row->morganization)){echo ucwords($row->morganization); } ?></td>
								</tr>
								<tr>
									<th>Official Address:</th>
									<td><?php if(!empty($row->foffadd)){echo ucwords($row->foffadd); } ?></td>
									<td><?php if(!empty($row->moffadd)){echo ucwords($row->moffadd); } ?></td>
								</tr>
								<tr>
									<th>Office Tel:</th>
									<td><?php if(!empty($row->fofftel)){echo ucwords($row->fofftel); } ?></td>
									<td><?php if(!empty($row->mofftel)){echo ucwords($row->mofftel); } ?></td>
								</tr>
								</tbody>
							</table>
								   
								</td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Tell us how you heard about our School:</b>&nbsp;<?php echo ucwords($row->ourskul); ?></td>
								</tr>
								
							</table>
						</div>
						</section>
						
						<section id="NewPrint" style="padding:0px;display:none;" >
						<div class="col-md-8 col-sm-8 col-xs-8" style="border:1px solid #ececec;color:black;">
						<br>
							<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
								<tr>
								<td><b>Entry Id:</b><br>
								<input type="text" value="<?php echo ucwords($row->id); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Student's Name:</b><br>
								<input type="text" value="<?php echo ucwords($row->name); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Date Of Birth: (DD-MM-YYYY)</b>
								<br>
								<input type="text" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Residential Address:</b><br>
								<input type="text" value="<?php echo ucwords($row->address); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Name Of Present School:</b><br>
								<input type="text" value="<?php echo ucwords($row->nopskul); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Present Grade:</b><br>
								<input type="text" value="<?php echo ucwords($row->grade); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Grade To Join:</b><br>
								<input type="text" value="<?php echo ucwords($row->gradetjoin); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Academic Year For Which Admission Is Sought:</b><br>
								<input type="text" value="<?php echo ucwords($row->year); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								
								<tr>
<table class="table table-responsive table-bordered table-hover" width="100%"> 
								<thead>
								<tr>
									<th width="20%">Details</th>
									<th width="40%">Father</th>
									<th width="40%">Mother</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th>Name:</th>
									<td><?php if(!empty($row->fname)){echo ucwords($row->fname); } ?></td>
									<td><?php if(!empty($row->mname)){echo ucwords($row->mname); } ?></td>
								</tr>
								
								<tr>
									<th>Mobile No:</th>
									<td><?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?></td>
									<td><?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?></td>
								</tr>
								<tr>
									<th>Residential Tel:</th>
									<td><?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?></td>
									<td><?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?></td>
								</tr>
								<tr>
									<th>Occupation:</th>
									<td><?php if(!empty($row->foccupation)){echo ucwords($row->foccupation); } ?></td>
									<td><?php if(!empty($row->moccupation)){echo ucwords($row->moccupation); } ?></td>
								</tr>
								<tr>
									<th>Designation:</th>
									<td><?php if(!empty($row->fdesignation)){echo ucwords($row->fdesignation); } ?></td>
									<td><?php if(!empty($row->mdesignation)){echo ucwords($row->mdesignation); } ?></td>
								</tr>
								<tr>
									<th>Name of the Organization:</th>
									<td><?php if(!empty($row->forganization)){echo ucwords($row->forganization); } ?></td>
									<td><?php if(!empty($row->morganization)){echo ucwords($row->morganization); } ?></td>
								</tr>
								<tr>
									<th>Official Address:</th>
									<td><?php if(!empty($row->foffadd)){echo ucwords($row->foffadd); } ?></td>
									<td><?php if(!empty($row->moffadd)){echo ucwords($row->moffadd); } ?></td>
								</tr>
								<tr>
									<th>Office Tel:</th>
									<td><?php if(!empty($row->fofftel)){echo ucwords($row->fofftel); } ?></td>
									<td><?php if(!empty($row->mofftel)){echo ucwords($row->mofftel); } ?></td>
								</tr>
								<tr>
									<th>Email:</th>
									<td><?php if(!empty($row->femail)){echo ucwords($row->femail); } ?></td>
									<td><?php if(!empty($row->memail)){echo ucwords($row->memail); } ?></td>
								</tr>
								<tr>
									<th>Mobile Number:</th>
									<td><?php if(!empty($row->mobile)){echo ucwords($row->mobile); } ?></td>
									<td>-</td>
								</tr>
								</tbody>
							    </table>
								</tr>
								
								<tr>
								<td><b>City:</b><br>
								<input type="text" value="<?php echo ucwords($row->city); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>State:</b><br>
								<input type="text" value="<?php echo ucwords($row->state); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Pincode:</b><br>
								<input type="text" value="<?php echo ucwords($row->pincode); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Passport Type:</b><br>
								<input type="text" value="<?php echo $row->passport; ?>" style="width:100%;height:30px;">
								</td>
								</tr>
                        		<tr>
                        		<td><b>Nationality:</b><br>
                        		<input type="text" value="<?php echo $row->nationality; ?>" style="width:100%;height:30px;">
                        		</td>
                        		</tr>
								<tr >
								<td><b>Tell us how you heard about our School:</b><br>
								<input type="text" value="<?php echo ucwords($row->ourskul); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								
							</table>
						</div>
						</section>
						
						<section id="StudPrint" style="padding:0px;display:none;" >
						<div class="col-md-8 col-sm-8 col-xs-8" style="border:1px solid #ececec;color:black;">
						<br>
							<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5" style="font-style:Verdana, sans-serif;">
								<tr>
								<td><b>Entry Id:</b><br>
								<?php echo ucwords($row->id); ?>
								</td>
								</tr>
								<tr>
								<td><b>Student's Name:</b><br>
								<?php echo ucwords($row->name); ?>
								</td>
								</tr>
								<tr>
								<td><b>Date Of Birth: (DD-MM-YYYY)</b>
								<br>
								<?php echo date('d-m-Y', strtotime($row->dob)); ?>
								</td>
								</tr>
								<tr>
								<td><b>Residential Address:</b><br>
								<?php echo ucwords($row->address); ?>
								</td>
								</tr>
								<tr>
								<td><b>Name Of Present School:</b><br>
								<?php echo ucwords($row->nopskul); ?>
								</td>
								</tr>
								<tr >
								<td><b>Present Grade:</b><br>
								<?php echo ucwords($row->grade); ?>
								</td>
								</tr>
								<tr>
								<td><b>Grade To Join:</b><br>
								<?php echo ucwords($row->gradetjoin); ?>
								</td>
								</tr>
								<tr >
								<td><b>Academic Year For Which Admission Is Sought:</b><br>
								<?php echo ucwords($row->year); ?>
								</td>
								</tr>
								
								<tr><td><b>Father's Name:</b><br><?php echo ucwords($row->fname); ?></td></tr>
								<tr><td><b>Father's Designation:</b><br><?php echo ucwords($row->fdesignation); ?></td></tr>
								
								
								<tr>
									<td><b>Father's Mobile No:</b><br><?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?></td>
								</tr>
								<tr>
									<td><b>Father's Residential Tel:</b><br><?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?></td>
								</tr>
								<tr>
								<td><b>Father's Organization:</b><br><?php echo ucwords($row->forganization); ?></td></tr>
								<tr>
								<td><b>Father's Occupation:</b><br><?php echo ucwords($row->foccupation); ?></td></tr>
								<tr><td><b>Father's Official Address:</b><br><?php echo ucwords($row->foffadd); ?></td></tr>
								<tr><td><b>Father's Office Tel:</b><br><?php echo ucwords($row->fofftel); ?></td></tr>
								
								
								
								<tr><td><b>Mother's Name:</b><br><?php echo ucwords($row->mname); ?></td></tr>
								
								
								<tr>
									<td><b>Mother's Mobile No:</b> <br><?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?></td>
								</tr>
								<tr>
									<td><b>Mother's Residential Tel:</b> <br><?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?></td>
								</tr>
								<tr><td><b>Mother's Designation:</b><br><?php echo ucwords($row->mdesignation); ?></td></tr>
								<tr>
								<td><b>Mother's Organization:</b><br><?php echo ucwords($row->morganization); ?></td></tr>
								<tr>
								<td><b>Mother's Occupation:</b><br><?php echo ucwords($row->moccupation); ?></td></tr>
								<tr><td><b>Mother's Official Address:</b><br><?php echo ucwords($row->moffadd); ?></td></tr>
								<tr><td><b>Mother's Office Tel:</b><br><?php echo ucwords($row->mofftel); ?></td></tr>
								
								<tr>
								<td><b>City:</b><br>
								<?php echo ucwords($row->city); ?>
								</td>
								</tr>
								<tr>
								<td><b>State:</b><br>
								<?php echo ucwords($row->state); ?>
								</td>
								</tr>
								<tr >
								<td><b>Pincode:</b><br>
								<?php echo ucwords($row->pincode); ?>
								</td>
								</tr>
								<tr>
								<td><b>Email Address Father:</b><br>
								<?php echo $row->femail; ?>
								</td>
								</tr>
								<tr >
								<td><b>Email Address Mother:</b><br>
								<?php echo ucwords($row->memail); ?>
								</td>
								</tr>
								<tr>
								<td><b>Passport Type:</b><br>
								<?php echo $row->passport; ?>
								</td>
								</tr>
                        		<tr>
                        		<td><b>Nationality:</b><br>
                        		<?php echo $row->nationality; ?>
                        		</td>
                        		</tr>
								<tr >
								<td><b>Tell us how you heard about our School:</b><br>
								<?php echo ucwords($row->ourskul); ?>
								</td>
								</tr>
								
							</table>
						</div>
						</section>
						
						<div class="col-md-2 col-sm-2 col-xs-2">
						<?php if(in_array('print',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<p>
						<?php $type=1; ?>
						<a href="{{URL::to('admissions/viewPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs btn-primary" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Ecole Mondiale</a>
						</p>
						<p>
						<?php $type=2; ?>
						<a href="{{URL::to('admissions/viewPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs btn-primary" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> New</a>
						</p>
						<p>
						<?php $type=3; ?>
						<a href="{{URL::to('admissions/viewPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs btn-primary" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> <?php echo ucwords($row->name); ?></a>
						</p>
						<?php  } ?>
						</div>
					  
					  
					</div>
					  
                  </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <!-- /page content -->
<script type="text/javascript">
function PrintDoc() {
  var toPrint = document.getElementById('EcolePrint');
  var popupWin = window.open('', '_blank', 'width=800 ,height=500');
  var CSSLINK = '<link rel="stylesheet"media="print" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
  popupWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head><title>Print</title><style>@page { size: auto;  margin: 0mm;}@media print { body{-webkit-print-color-adjust:exact;font-style:"Work Sans", Arial, Helvetica, sans-serif;} }</style></head><body style="padding:20px;" onload="window.print()">'+toPrint.innerHTML+'</body></html>')
  popupWin.document.close();
  //popupWin.print();
}

function PrintDoc1() {
  var toPrint = document.getElementById('NewPrint');
  var popupWin = window.open('', '_blank', 'width=800 ,height=500');
  var CSSLINK = '<link rel="stylesheet"media="print" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
  popupWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head><title>Print</title><style>@page { size: auto;  margin:5mm;}@media print { body{-webkit-print-color-adjust:exact;font-style:Verdana, sans-serif;} }</style></head><body style="padding:20px;" onload="window.print()">'+toPrint.innerHTML+'</body></html>')
  popupWin.document.close();
  //popupWin.print();
}
function PrintDoc2() {
  var toPrint = document.getElementById('StudPrint');
  var popupWin = window.open('', '_blank', 'width=800 ,height=500');
  var CSSLINK = '<link rel="stylesheet"media="print" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
  popupWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head><title>Print</title><style>@page { size: auto;  margin:5mm;}@media print { body{-webkit-print-color-adjust:exact;font-style:Verdana, sans-serif;} }</style></head><body style="padding:20px;" onload="window.print()">'+toPrint.innerHTML+'</body></html>')
  popupWin.document.close();
  //popupWin.print();
}
</script>

@include('included.footer')