 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result">
			  </p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Admissions</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">				
					<table id="datatable-fixed-header" class="table table-striped table-bordered table-condesed table-hover" cellspacing="0" width="100%">
                      <thead>
                          <th>Sr No</th>
                          <th>Entry Id</th>
                          <th>Name</th>
                          <th width="9%">DOB</th>
                          <th width="18%">Order Details</th>
                          <th width="12%">Present School</th>
                          <th width="9%">Date</th>
						  <?php if(in_array('print',$RoleArray) || in_array('delete',$RoleArray) || in_array('view',$RoleArray)  || in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
                      <tbody>
					    <?php $i = 1; ?>
						<?php foreach ($Data as $row){ ?>
                        <tr style="<?php if($row->viewed==0){ echo 'font-weight:bold;background:rgba(0,0,0,0.1)';}?>">
                          <td><?php echo $i; ?></td>
                          <td><?php echo $row->id; ?></td>
                          <td>
                              <?php echo ucwords($row->name); ?> </td>
                          <td><?php echo date('d-m-Y', strtotime($row->dob)); ?></td>
                          <td>
                            <?php 
                                $OrderDetails = DB::table('orders')
			                        ->where('admissionid','=',$row->id)
				                    ->get();
				                if(count($OrderDetails)>0){
				                foreach($OrderDetails as $OrderDetail){}
                            ?>
                              <p><b>OrderCode:</b> <?php echo $OrderDetail->ordercode; ?> </p>
                              <p><b>Amt:</b> <?php echo number_format($OrderDetail->amount,2); ?> </p>
                              <p><b>Status:</b> <?php echo ucwords($OrderDetail->orderstatus); ?> </p>
                              <p><b>Date:</b> <?php echo date('d-m-Y h:i A',strtotime($OrderDetail->created_at)); ?> </p>
                            <?php }else{ echo '-'; } ?>
                          </td>
                          
                          <td><?php echo $row->nopskul; ?></td>
                          <td><?php echo date('d-m-Y',strtotime($row->created_at)); ?></td>
                          
						 
						  <?php if(in_array('print',$RoleArray) || in_array('edit',$RoleArray) ||in_array('view',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <td>
						  
						  <?php if(in_array('view',$RoleArray) || Auth::user()->added_by== 0) { ?>
						  <a href="{{URL::to('admissions/view',array(Crypt::encrypt($row->id)))}}" class="btn btn-success btn-xs" data-toggle="tooltip" title="View PDF"> View</a>
						  <?php  } ?>
						  
						  <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0) { ?>
						  <a href="{{URL::to('admissions/edit',array(Crypt::encrypt($row->id)))}}" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Edit Record"> Edit</a>
						  <?php  } ?>
						  
						  <?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0) { ?>
						  <a href="{{URL::to('admissions/delete',array(Crypt::encrypt($row->id)))}}" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Record"> Delete</a>
						  <?php  } ?>
						  
						  <?php if(in_array('print',$RoleArray) || Auth::user()->added_by== 0) { ?>
							<div class="dropdown" data-toggle="tooltip" title="Download PDF's">
							  <button class="btn btn-xs btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
							  Downloads <span class="caret"></span></button>
							  <ul class="dropdown-menu">
								<li>
								<?php $type=1; ?>
								<a href="{{URL::to('admissions/downloadPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs pull-left" title="Download PDF"><i class="fa fa-file-pdf-o"></i> Ecole Mondiale</a>
								</li>
								<li>
								<?php $type=2; ?>
								<a href="{{URL::to('admissions/downloadPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs pull-left" title="Download PDF"><i class="fa fa-file-pdf-o"></i> New</a>
								<li>
								<?php $type=3; ?>
								<a href="{{URL::to('admissions/downloadPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs pull-left" title="Download PDF"><i class="fa fa-file-pdf-o"></i> <?php echo ucwords($row->name); ?></a>
							  </ul>
							</div> 
						  <?php  } ?>
						  
						  </td>
						  <?php } ?>
                        </tr>
						<?php  $i++; } ?>
                      </tbody>
                    </table>	
					
					<section id="EcolePrint" style="padding:0px;display:none;" >
					</section>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	$(document).ready(function(){
		$('.getpdf').click(function(){
			var cid = $(this).attr('data-id');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'admissions/getpdfdata',
				data:'cid='+ cid,
				success: function (data) {
					$('#EcolePrint').html(data);
					var toPrint = document.getElementById('EcolePrint');
					var popupWin = window.open('', '_blank', 'width=800 ,height=500');
					var CSSLINK = '<link rel="stylesheet"media="print" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
					popupWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head><title>Print</title><style>@page { size: auto;  margin:5mm;}@media print { body{-webkit-print-color-adjust:exact;font-style:Verdana, sans-serif;} }</style></head><body style="padding:20px;" onload="window.print()">'+data+'</body></html>')
					popupWin.document.close();
					
				}
			});	
		});
	});
	</script>
	
@include('included.footer')