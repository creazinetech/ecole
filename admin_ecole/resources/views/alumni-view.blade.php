@include('included.header')
@include('included.super-admin-sidebar')

 <?php foreach($Data as $row){} ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ucwords($row->firstname.' '.$row->lastname); ?> - Alumni Details</h2>
					<a href="{!! \Config::get('app.url_base') !!}/alumni" class="btn btn-danger btn-sm pull-right"><i class="fa  fa-close"></i> Close</a>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr class="bg-blue"><th colspan="3" class="text-center">Student Details</th></tr>
							<tr><th width="110px">Student Name</th><td width="1px"> : </td><td><?php echo ucwords($row->firstname.' '.$row->lastname); ?></td></tr>
							<tr><th>Contact</th><td> : </td><td><?php echo $row->contact1.' '.$row->contact1; ?></td></tr>
							<tr><th>Graduated Yr.</th><td> : </td><td><?php echo $row->graduate_yr; ?></td></tr>
							<tr><th>Home Address</th><td> : </td><td><?php echo ucwords($row->home_address); ?></td></tr>
							<tr><th>City</th><td> : </td><td><?php echo ucwords($row->city); ?></td></tr>
							<tr><th>State</th><td> : </td><td><?php echo ucwords($row->state); ?></td></tr>
							<tr><th>Zipcode</th><td> : </td><td><?php echo $row->zip; ?></td></tr>
							<tr><th>Email Id</th><td> : </td><td><?php echo $row->emailid; ?></td></tr>
							<tr><th>Website</th><td> : </td><td><?php echo $row->website; ?></td></tr>
						</table>
					</div>
					
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr class="bg-green"><th colspan="3" class="text-center">Current Details</th></tr>
							<tr><th width="110px">Studying?</th><td width="1px"> : </td><td><?php echo ucwords($row->study_type); ?></td></tr>
							<tr><th>University</th><td> : </td><td><?php echo ucwords($row->university); ?></td></tr>
							<tr><th>Programme</th><td> : </td><td><?php echo $row->programme; ?></td></tr>
							<tr><th>Prev. University</th><td> : </td><td><?php echo ucwords($row->last_university); ?></td></tr>
							<tr><th>Prev. Programme</th><td> : </td><td><?php echo ucwords($row->last_programme); ?></td></tr>
							<tr><th>Address</th><td> : </td><td><?php echo ucwords($row->last_address); ?></td></tr>
						</table>
					</div>
					
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr style="background-color: #8cd417 ;color:#ffffff"><th colspan="3" class="text-center">Extra Details</th></tr>
							<tr><th width="110px">Occupation</th><td width="1px"> : </td><td><?php echo ucwords($row->occupation); ?></td></tr>
							<tr><th>Organisation</th><td> : </td><td><?php echo ucwords($row->organisation); ?></td></tr>
							<tr><th>Programme</th><td> : </td><td><?php echo $row->programme; ?></td></tr>
							<tr><th>Address</th><td> : </td><td><?php echo ucwords($row->off_address); ?></td></tr>
							<tr><th>Contact</th><td> : </td><td><?php echo ucwords($row->off_contact); ?></td></tr>
							<tr><th>Fax</th><td> : </td><td><?php echo ucwords($row->off_fax); ?></td></tr>
							<tr><th>Email</th><td> : </td><td><?php echo $row->off_email; ?></td></tr>
							<tr><th>Website</th><td> : </td><td><?php echo $row->off_website; ?></td></tr>
						</table>
					</div>
					<div class="clearfix"></div>
					
					<div class="col-md-4 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr style="background-color:#ffc300;color:#ffffff"><th colspan="3" class="text-center">Survey Details</th></tr>
							<tr><th>Working?</th><td> : </td><td><?php echo ucwords($row->work_type); ?></td></tr>
							<tr><th>Gender</th><td> : </td><td><?php echo ucwords($row->gender); ?></td></tr>
							<tr><th>DOB</th><td> : </td><td><?php echo ucwords($row->dob); ?></td></tr>
							<tr style="font-weight:bold;" class="red"><th>Alumni Ambassador</th><td> : </td><td><?php echo ucwords($row->alumni_ambassador); ?></td></tr>
						</table>
					</div>
					
					<div class="col-md-8 col-sm-12 col-xs-12" style="overflow-x:auto;">
						<table class="table table-responsive table-stripped">
							<tr style="background-color:#fd8e00;color:#ffffff"><th colspan="3" class="text-center">Alumni News</th></tr>
							<tr><th>Personal</th><td> : </td><td><?php echo ucwords($row->personal); ?></td></tr>
							<tr><th>Professional</th><td> : </td><td><?php echo ucwords($row->professional); ?></td></tr>
							<tr><th>Hobbies</th><td> : </td><td><?php echo ucwords($row->hobbies); ?></td></tr>
							<tr><th>Vaction</th><td> : </td><td><?php echo ucwords($row->vacation); ?></td></tr>
						</table>
					</div>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@include('included.footer')