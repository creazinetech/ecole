<!DOCTYPE html>
<html>
    <head>
		<link rel="shortcut icon" href="{!! \Config::get('app.url') !!}/images/ico/favicon.png">
        <title>404 Page Not Found</title>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #f4a137;
                display: table;
                font-weight: 100;
                font-family: 'Raleway', sans-serif;
				background-color:#2A3F54;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
			.details {
				text-align: center;
                font-size:18px;
                margin-bottom: 40px;
            }
			.button {
				text-align: center;
                font-size:16px;
                margin-bottom: 40px;
				text-decoration:none;
				background-color:#f4a137;
				color:#fff;
				border-radius:5px;
				padding:8px 18px;
            }
			.button:hover {
				text-align: center;
                font-size:16px;
                margin-bottom: 40px;
				text-decoration:none;
				background-color:#e4860e;
				color:#fff;
				border-radius:5px;
				padding:8px 18px;
            }
			.fa-undo:hover {
				webkit-transform: rotate(90deg);
				-ms-transform: rotate(90deg);
				transform: rotate(90deg);
			}
			.fa-rocket:hover {
				webkit-transform: rotate(134deg);
				-ms-transform: rotate(134deg);
				transform: rotate(134deg);
			}
			.fa-rocket {
				webkit-transform: rotate(-45deg);
				-ms-transform: rotate(-45deg);
				transform: rotate(-45deg);
			}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title"><i class="fa fa-rocket"></i> Opps..404</div>
                <div class="details">
				Page are you looking is not found!
				<br><br><br>
				<a class="button btn-warning btn-md" href="{{ URL::previous() }}"><i class="fa fa-rotate-180 fa-undo"></i> Go Back !</a>
				</div>
            </div>
        </div>
    </body>
</html>
