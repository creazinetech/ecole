@include('included.header')
@include('included.super-admin-sidebar')
<?php
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','27')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
					<div class="x_title">
						<h2>Gallery <small> gallery videos </small></h2>
						<?php if(Auth::user()->added_by== 0 || in_array('add',$RoleArray)){?>
						<a style="margin-right:5px;" class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#Add_PA_Modal">
						Add
						</a>
						<?php } ?>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

			            <div class="row">
			              <div class="col-md-12 col-sm-12 col-xs-12">
			                <div class="x_panel">
			                  <div class="x_title">
			                    <h2>Media Videos</h2>
			                    <div class="clearfix"></div>

			                  </div>
								<div class="x_content" style="overflow-x:auto;">


								<table class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
			                      <thead>
			                        <tr>
			                          <th>Sr No</th>
			                          <th>URL</th>
			                          <th>Title</th>
									  <?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
			                          <th>Action</th>
			                          <?php } ?>
			                        </tr>
			                      </thead>
								  <tbody>
									<?php
									$i=1;
									if(count($Data) > 0){
									foreach($Data as $row){
									?>
									<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $row->url; ?></td>
									<td><?php echo $row->vid_title?></td>
									<?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
									<td>
									<a href="{{URL::to('media/videos/delete-video',array(Crypt::encrypt($row->id)))}}" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm('Are you sure want to delete this?')"><i class="fa fa-trash"></i></a>
									</td>
									<?php } ?>
									</tr>
									<?php $i++; }}else{?>
									<tr>
									<td colspan="5" class="text-center">No records found..!</td>
									</tr>
									<?php }?>
								  </tbody>
			                    </table>

								</div>
			                </div>
			              </div>
			            </div>
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>
	<!-- Add model start -->
	<div id="Add_PA_Modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Add Video</h4>
		  </div>
		  <form class="" action="{{URL::route('addMediaVideos')}}"  enctype="multipart/form-data" method="post" >
		  <div class="modal-body">
				<div class="form-group">
				<input type="url" required name="url" id="url" placeholder="Video URL" class="form-control input-sm">
				</div>
				<div class="form-group">
				<input type="text" required name="vid_title" id="vid_title" placeholder="Video Title" class="form-control input-sm">
				</div>
				{{ csrf_field() }}
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-success btn-sm">Save</button>
			<button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	<!-- Add model end -->
 	<script>
	//$(document).ready(function(){
		$('.delete_img').on('click',function(){
			var cid = $(this).attr('data-id');
			var path = $(this).attr('data-path');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'images/delete-image',
				data: 'cid='+ cid+'&path='+ path,
				success: function (data,status) {
					if(status == 'success'){
						//$('.image_row').html(data);
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Image deleted successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						location.reload(1200);
					}
					if(status != 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to delete image! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});
		});

		$('.deleteall').on('click',function(){
			var cid = $('#category_id1').val();
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'images/delete-all-images',
				data: 'cid='+cid,
				success: function (data,status) {
					if(status == 'success'){
						//$('.image_row').html(data);
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Images deleted successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						location.reload(1200);
					}
					if(status != 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to delete images! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});
		});


	//});
	</script>
  @include('included.footer')