@include('included.header')
@include('included.super-admin-sidebar')
<?php
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','25')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
foreach($Data as $row){ }
?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">

	<div class="clearfix"></div>
	<div class="flash-message">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
	  @if(Session::has('alert-' . $msg))

	  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
	  @endif
	@endforeach
	</div>

	<div class="flash-message1" style="display:none;">
	  <p class="alert alert-dismissable status_result"></p>
	</div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
			<h2>Edit Category</h2>
			<div class="clearfix"></div>
		  </div>
			<div class="x_content" style="overflow-x:auto;height:420px;">
			<div class="clearfix"></div>
			<div class="col-md-8 col-sm-12 col-xs-12">
			<form class="form-horizontal form-label-left"  action="{{URL::route('updateMediaCategory')}}" enctype="multipart/form-data" method="post">
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category Name</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
					  <input type="hidden" id="id" name="id" class="form-control input-sm" value="<?php echo $row->id; ?>" >
					  <input type="text" id="catname" name="catname" class="form-control input-sm" value="<?php echo $row->catname; ?>" >
					</div>
				</div>
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Thumbnail</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
					  <input type="file" id="thumbnail" name="thumbnail" class="form-control  input-sm">
					  <?php echo $row->thumbnail; ?>
					</div>
				</div>
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
					 <select id="yc_status" name="yc_status" class="form-group yc_status  input-sm">
						  <option value="Active" <?php if($row->yc_status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->yc_status == "Inactive"){echo "selected";} ?>>Inactive</option>
					</select>
					</div>
				</div>

					{{ csrf_field() }}

				<button type="submit" class="btn btn-success btn-sm">Save</button>
				<a href="{!! \Config::get('app.url_base') !!}/youtube/category" class="btn btn-danger btn-sm">Cancel</a>

			</form>
		  </div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!-- /page content -->

@include('included.footer')