@include('included.header')
@include('included.super-admin-sidebar')

 <?php foreach($Data as $row){} ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Job Application - <?php echo ucwords($row->position); ?></h2>
					<a href="{!! \Config::get('app.url_base') !!}/job-applications" class="btn btn-danger btn-sm pull-right"><i class="fa  fa-close"></i> Close</a>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					
					<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{!! \Config::get('app.pdfimg') !!}/images/job-applications/<?php echo $row->photo; ?>" style="height: 250px; width: 220px;">
                        </div>
                      </div>
                      <h3><?php echo ucwords($row->name); ?></h3>
                      <p><i class="fa fa-calendar"></i> Applied Date: <?php echo date('d-m-Y',strtotime($row->created_at)); ?></span></p>
                      <h5><i class="fa fa-briefcase user-profile-icon"></i> <?php echo ucwords($row->designation); ?></h5>

                      <ul class="list-unstyled user_data">
                        <li><strong>Experience :</strong> <?php echo $row->experience_year; ?></li>
                        <li><strong>Experience In :</strong> <?php echo $row->experience_in; ?></li>
                        <li><strong>Internation School Experience :</strong> <?php echo $row->internation_school_exp; ?></li>
                        <li><strong>Current Employer :</strong> <?php echo $row->current_employer; ?></li>
                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo ucwords($row->cadd); ?></li>
						<?php if(!empty($row->padd) && $row->padd !="same" && $row->padd !="SAME"&& $row->padd !="Same"){ ?>
                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo ucwords($row->padd); ?></li>
						<?php } ?>
                        <li> <i class="fa fa-phone user-profile-icon"></i><?php echo $row->mob_no; ?> </li>
						
                        <li class="m-top-xs"> <i class="fa fa-envelope user-profile-icon"></i>
                          <a href="mailto:<?php echo $row->email_id; ?>" ><?php echo $row->email_id; ?></a>
                        </li>
						
                        <li class="m-top-xs"> <i class="fa fa-map-signs user-profile-icon"></i> <?php echo $row->pincode; ?></li>
						
						<li class="m-top-xs"> <i class="fa fa-dot-circle-o"></i> </i>Criminal Offence : <?php echo $row->criminal_offence; ?></li>
						
                      </ul>
					  </div>
					
					<div class="col-md-9 col-sm-12 col-xs-12" style="/*overflow-x:auto;height:auto*/">
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped " width="100%" border="1">
							<tr style="background-color:#4568DC;color:#ffffff;"><th class="text-center">Personal Details</th></tr>
							<tr>
							<td>
							<p style="font-weight:bold;">Children</p>
							<?php echo ucwords($row->children); ?>
							</td>
							</tr>
							<tr>
							<td>
							<p style="font-weight:bold;">Passport No :</p>
							<p><?php echo $row->passport_no; ?></p>
							<p style="font-weight:bold;">Issue Date :</p>
							<p><?php echo $row->issue_date; ?></p>
							<p style="font-weight:bold;">Expiry Date :</p>
							<p><?php echo $row->expiry_date; ?></p>
							</td>
							</tr>
						</table>
					</div>
					
					<?php if(count($EduacationalDetails)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr class="bg-blue"><th colspan="8" class="text-center">Educational Details</th></tr>
							<tr>
							<th>Sr No</th>
							<th>University</th>
							<th>Board</th>
							<th>Specialist Subject area/s</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Grade</th>
							<th>Percentage</th>
							</tr>
							<?php 
							$i=1;
							foreach($EduacationalDetails as $row1){ ?>
							<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo ucwords($row1->university); ?></td>
							<td><?php echo ucwords($row1->board); ?></td>
							<td><?php echo $row1->sp_subject; ?></td>
							<td><?php echo $row1->start_date; ?></td>
							<td><?php echo $row1->completion_date; ?></td>
							<td><?php echo $row1->grade; ?></td>
							<td><?php echo $row1->percentage; ?></td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					
					<?php if(count($professional_courses)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr class="bg-green"><th colspan="7" class="text-center">Professional Courses Attended</th></tr>
							<tr>
							<th>Sr No</th>
							<th>Attended</th>
							<th>Course Description</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Location</th>
							</tr>
							<?php 
							$i=1;
							foreach($professional_courses as $row2){ ?>
							<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo ucwords($row2->pattended); ?></td>
							<td><?php echo ucwords($row2->pcourse); ?></td>
							<td><?php echo $row2->pstart_date; ?></td>
							<td><?php echo $row2->pcompletion_date; ?></td>
							<td><?php echo $row2->plocation; ?></td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					
					<?php if(count($professional_courses1)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr class="bg-green"><th colspan="7" class="text-center">Professional Courses Conducted</th></tr>
							<tr>
							<th>Sr No</th>
							<th>Conducted</th>
							<th>Course Description</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Location</th>
							</tr>
							<?php 
							$i1=1;
							foreach($professional_courses1 as $row21){ ?>
							<tr>
							<td><?php echo $i1; ?></td>
							<td><?php echo ucwords($row21->pattended1); ?></td>
							<td><?php echo ucwords($row21->pcourse1); ?></td>
							<td><?php echo $row21->pstart_date1; ?></td>
							<td><?php echo $row21->pcompletion_date1; ?></td>
							<td><?php echo $row21->plocation1; ?></td>
							</tr>
							<?php $i1++; } ?>
						</table>
					</div>
					<?php } ?>
					
					<?php if(count($PaperResearch)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr style="background-color: #8cd417 ;color:#ffffff">
							<th colspan="7" class="text-center">Paper Research</th>
							</tr>
							<tr>
							<th>Sr No</th>
							<th>Paper/Research Title</th>
							<th>Published In</th>
							<th>Publish date</th>
							<th>Publish At</th>
							<th>Presentation Date</th>
							</tr>
							<?php 
							$i=1;
							foreach($PaperResearch as $row3){ ?>
							<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo ucwords($row3->prtitle); ?></td>
							<td><?php echo ucwords($row3->publish_in); ?></td>
							<td><?php echo $row3->publish_date; ?></td>
							<td><?php echo $row3->presented_at; ?></td>
							<td><?php echo $row3->presentation_date; ?></td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					
					<?php if(count($employment_history)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr style="background-color:#ffc300;color:#ffffff">
							<th colspan="7" class="text-center">Employment History</th>
							</tr>
							<tr>
							<th>Sr No</th>
							<th>Designation</th>
							<th>Period</th>
							<th>Subjects Taught</th>
							<th>Curriculum</th>
							<th>Resposibility</th>
							<th>Leaving Reason</th>
							</tr>
							<?php 
							$i=1;
							foreach($employment_history as $row4){ ?>
							<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo ucwords($row4->eh_designation); ?></td>
							<td><?php echo $row4->period; ?></td>
							<td><?php echo $row4->subjects; ?></td>
							<td><?php echo $row4->curriculum; ?></td>
							<td><?php echo $row4->responsibility; ?></td>
							<td><?php echo $row4->leaving_reason; ?></td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					<div class="clearfix"></div>
					
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
        <!-- /page content -->
@include('included.footer')