<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <title></title>
  </head><body style="">
  <?php
  foreach ($Data as $row){ }
  ?>
	<b style="padding-left:5px;">Apply Now: Entry # <?php echo ucwords($row->id); ?></b>

	<p style="padding-left:5px;">
	    <b>Position applying for:</b>
		<br><?php echo ucwords($row->position); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Name:</b>
		<br><?php echo ucwords($row->name); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Date Of Birth:</b>
		<br><?php echo date('d/m/Y',strtotime($row->dob)); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Upload Photograph:</b>
		<br><img style="width:180px; height:200px; display: block;" src="{!! \Config::get('app.pdfimg') !!}/images/job-applications/<?php echo $row->photo; ?>" alt="image" />
	</p>
	
	<p style="padding-left:5px;">
        <b>Gender:</b>
		<br><?php echo $row->gender; ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Nationality:</b>
		<br><?php echo ucwords($row->nationality); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Marital Status:</b>
		<br><?php echo ucwords($row->marital_status); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Children:</b>
		<br><?php echo ucwords($row->children); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Current Address:</b>
		<br><?php echo ucwords($row->cadd); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Pincode:</b>
		<br><?php echo $row->pincode; ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Landline no.</b>
		<br><?php echo $row->landline; ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Mobile no.</b>
		<br><?php echo $row->mob_no; ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Email Id</b>
		<br><?php echo $row->email_id; ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Permanent Address (Write ‘same’ if the same as current address)</b>
		<br><?php echo ucwords($row->padd); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Passport no.</b>
		<br><?php echo $row->passport_no; ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Date of issue:</b>
		<br><?php echo $row->issue_date; ?>
	</p>
	
	
	<p style="padding-left:5px;">
	    <b>Place of Issue:</b>
		<br><?php echo ucwords($row->issue_place); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Expiry Date:</b>
		<br><?php echo $row->expiry_date; ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>List your academic history below starting from the one completed last:</b>
		<br>
		<?php if(count($Data1)>0){ ?>
		<table width="100%" class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0">
		
		<tr style="background-color:#F1F1F1;">
		<th>Name of School/ College/ University</th>
		<th>Board/Degree/Diploma</th>
		<th>Starting Date</th>
		<th>Completion Date</th>
		<th>Grade</th>
		<th>Percentage</th>
		</tr>
		
		
		<?php foreach($Data1 as $row1){ ?>
		<tr>
		<td><?php echo ucwords($row1->university); ?></td>
		<td><?php echo ucwords($row1->board); ?></td>
		<td><?php echo ucwords($row1->start_date); ?></td>
		<td><?php echo ucwords($row1->completion_date); ?></td>
		<td><?php echo ucwords($row1->grade); ?></td>
		<td><?php echo ucwords($row1->percentage); ?></td>
		</tr>		
		<?php } ?>
		
		</table>	
		<?php }else{echo 'NA';} ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>List your academic history below starting from the one completed last:</b>
		<br>
		<?php if(count($Data2)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		
		<tr style="background-color:#F1F1F1;">
		<th>Attended</th>
		<th>Name of Course</th>
		<th>Starting Date</th>
		<th>Completion Date</th>
		<th>Location</th>
		</tr>
		
		
		<?php foreach($Data2 as $row2){ ?>
		<tr>
		<td><?php echo ucwords($row2->pattended); ?></td>
		<td><?php echo ucwords($row2->pcourse); ?></td>
		<td><?php echo ucwords($row2->pstart_date); ?></td>
		<td><?php echo ucwords($row2->pcompletion_date); ?></td>
		<td><?php echo ucwords($row2->plocation); ?></td>
		</tr>		
		<?php } ?>
		
		</table>	
		<?php }else{echo 'NA';} ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>List the professional development courses conducted by you, if any</b>
		<br>
		<?php if(count($Data21)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		
		<tr style="background-color:#F1F1F1;">
		<th>Conducted</th>
		<th>Name of Course</th>
		<th>Starting Date</th>
		<th>Completion Date</th>
		<th>Location</th>
		</tr>
		
		
		<?php foreach($Data21 as $row21){ ?>
		<tr>
		<td><?php echo ucwords($row21->pattended1); ?></td>
		<td><?php echo ucwords($row21->pcourse1); ?></td>
		<td><?php echo ucwords($row21->pstart_date1); ?></td>
		<td><?php echo ucwords($row21->pcompletion_date1); ?></td>
		<td><?php echo ucwords($row21->plocation1); ?></td>
		</tr>		
		<?php } ?>
		
		</table>	
		<?php }else{echo 'NA';} ?>
	</p>
	
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Publications/Research Papers/ Documents produced (in the last 5 years):</b>
		<br>
		<?php if(count($Data3)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		
		<tr style="background-color:#F1F1F1;">
		<th>Title</th>
		<th>Published in</th>
		<th>Date of Publishing</th>
		<th>Presented at</th>
		<th>Date of Presentation</th>
		</tr>
		
		
		<?php foreach($Data3 as $row3){ ?>
		<tr>
		<td><?php echo ucwords($row3->prtitle); ?></td>
		<td><?php echo ucwords($row3->publish_in); ?></td>
		<td><?php echo ucwords($row3->publish_date); ?></td>
		<td><?php echo ucwords($row3->presented_at); ?></td>
		<td><?php echo ucwords($row3->presentation_date); ?></td>
		</tr>		
		<?php } ?>
		
		</table>	
		<?php }else{echo 'NA';} ?>	
	</p>
	
	<p style="padding-left:5px;">
	    <b>Experience in International Schools:</b>
		<br><?php echo ucwords($row->internation_school_exp); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Experience in Type of International Education:</b>
		<br>
		<?php 
		if(isset($row->experience_in) && !empty($row->experience_in)){
		$exp_explode = explode(',',$row->experience_in);
		echo '<ul>';
		for($i=0;$i<count($exp_explode)-1;$i++){
			echo '<li>'.$exp_explode[$i].'</li>';
		?>
		<?php 
		} 
		echo '</ul>';
		} ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Total Experience in years:</b>
		<br><?php echo ucwords($row->experience_year); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Current Employer:</b>
		<br><?php echo ucwords($row->current_employer); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Designation:</b>
		<br><?php echo ucwords($row->designation); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Joining Date of Current Job:</b>
		<br><?php echo ucwords($row->joindt_curr_emp); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Employment History (Start with the most Recent One First):</b>
		<br>
		<?php if(count($Data4)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		
		<tr style="background-color:#F1F1F1;">
		<th>Name of Employer</th>
		<th>Designation</th>
		<th>Period of Employment  From - To</th>
		<th>Subject Taught (with Grade Levels)</th>
		<th>Curriculum Taught (IB/IGCSE/ ICSE)</th>
		<th>Additional Responsibility</th>
		<th>Reason for Leaving the Job</th>
		</tr>
		<?php foreach($Data4 as $row4){ ?>
		<tr>
		<td><?php echo ucwords($row4->eh_name); ?></td>
		<td><?php echo ucwords($row4->eh_designation); ?></td>
		<td><?php echo ucwords($row4->period); ?></td>
		<td><?php echo ucwords($row4->subjects); ?></td>
		<td><?php echo ucwords($row4->curriculum); ?></td>
		<td><?php echo ucwords($row4->responsibility); ?></td>
		<td><?php echo ucwords($row4->leaving_reason); ?></td>
		</tr>		
		<?php } ?>
		</table>	
		<?php }else{echo 'NA';} ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>I.T. Skills: (Tell us about the IT skills you are good at):</b>
		<br><?php echo ucwords($row->it_skills); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Language Skills:</b>
		<br><?php echo ucwords($row->language_speak); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Hobbies/ Interests:</b>
		<br><?php echo ucwords($row->hobbies); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Current Monthly Salary:</b>
		<br><?php echo ucwords($row->current_sal); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Expected Monthly Salary:</b>
		<br><?php echo ucwords($row->expected_sal); ?>
	</p>
	
	<p style="padding-left:5px;">
	    <b>Earliest Joining Date:</b>
		<br><?php if(!empty($row->newjoin_dt)){echo $row->newjoin_dt;}else{echo 'NA';} ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Employment Reference:</b>
		<br>
		<?php if(count($Data5)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		<tr style="background-color:#F1F1F1;">
		<th>Name Of Reference</th>
		<th>Designation</th>
		<th>Phone</th>
		<th>E-mail</th>
		<th>Address</th>
		</tr>
		<?php foreach($Data5 as $row5){ ?>
		<tr>
		<td><?php echo ucwords($row5->ername); ?></td>
		<td><?php echo ucwords($row5->erdesignation); ?></td>
		<td><?php echo ucwords($row5->erphone); ?></td>
		<td><?php echo strtolower($row5->eremail); ?></td>
		<td><?php echo ucwords($row5->eraddress); ?></td>
		</tr>		
		<?php } ?>
		
		</table>	
		<?php }else{echo 'NA';} ?>
	</p>
		
	<p style="padding-left:5px;">
	    <b>Personal Statement of Education:</b><br>
		<?php echo ucwords($row->edu_statement); ?>
	</p>
	
	<p style="background-color:#edf6fe;padding-left:5px;">
	    <b>Any other information, if required:</b><br>
	    <?php echo ucwords($row->other_info); ?>
	</p>
  </body></html>