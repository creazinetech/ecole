@include('included.header')
@include('included.super-admin-sidebar')
<style>
input{
	width:100%;
}
table{
	width:100%;
	table-layout:fixed !important;
}
.div_responsive{
	overflow-x:auto;
}
</style>
 <?php foreach($Data as $row){} ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Job Application</h2>
					<a href="{!! \Config::get('app.url_base') !!}/job-applications" class="btn btn-danger btn-sm pull-right">
					<i class="fa  fa-close"></i> Close</a> 
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					<form class="form-horizontal form-label-left" novalidate action="{{URL::route('updatejobapplication')}}" enctype="multipart/form-data" method="post">
					<div class="col-md-3 col-sm-12 col-xs-12 profile_left">
					  <center>
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{!! \Config::get('app.pdfimg') !!}/images/job-applications/<?php echo $row->photo; ?>" style="height: 250px; width: 220px;">
						  <!--<input class="form-control" type="file" id="photo" name="photo" value="">-->
                        </div>
						
                      </div>
					  </center>
					  <div class="clearfix"></div>
					  <div class="col-md-12 col-sm-6 col-xs-12">
                      <strong>Name</strong>
					  <input class="form-control" type="hidden" id="id" name="id" value="<?php echo ucwords($row->id); ?>">
					  <input class="form-control" type="text" id="name" name="name" value="<?php echo ucwords($row->name); ?>">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
                      <strong>Designation</strong>
					  <input class="form-control" type="text" id="designation" name="designation" value="<?php echo ucwords($row->designation); ?>">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
                     <strong>Experience :</strong>
						<input class="form-control" type="text" id="experience_year" name="experience_year" value="<?php echo $row->experience_year; ?>">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Experience In :</strong>
						<input class="form-control" type="text" id="experience_in" name="experience_in" value="<?php echo $row->experience_in; ?>"> 
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Internation School Experience :</strong>
						<input class="form-control" type="text" id="internation_school_exp" name="internation_school_exp" value="<?php echo $row->internation_school_exp; ?>"> 
					  </div>
					  
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Current Employer :</strong>
						<input class="form-control" type="text" id="current_employer" name="current_employer" value="<?php echo $row->current_employer; ?>">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Current Address :</strong>
						<input class="form-control" type="text" id="cadd" name="cadd" value="<?php echo ucwords($row->cadd); ?>">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Permanent Address :</strong>
						<input class="form-control" type="text" id="padd" name="padd" value="<?php  echo ucwords($row->padd); ?>">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Mobile No :</strong>
						<input class="form-control" type="text" id="mob_no" name="mob_no" value="<?php echo $row->mob_no; ?> ">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Email Id :</strong>
						<input class="form-control" type="text" id="email_id" name="email_id" value="<?php echo $row->email_id; ?> ">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Pincode :</strong>
						<input class="form-control" type="text" id="pincode" name="pincode" value="<?php  echo $row->pincode; ?>">
					  </div>
					  
					  <div class="col-md-12 col-sm-6 col-xs-12">
						<strong>Criminal Offence :</strong> <br>
						Yes <input type="radio" class="pull-left" name="criminal_offence" id="criminal_offence" value="Yes" <?php if($row->criminal_offence == 'Yes'){ echo 'checked';} ?>> 
						<br>
						No<input type="radio" class="pull-left" name="criminal_offence" id="criminal_offence" value="No"  <?php if($row->criminal_offence == 'No'){ echo 'checked';} ?>>  
					  </div>
					  
					</div>
					
					
					<br>
					<div class="col-md-9 col-sm-12 col-xs-12 " style="">
					
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-striped table-bordered" cellspacing="0"  style="">
							<tr style="background-color:#4568DC;color:#ffffff;"><th class="text-center">Personal Details</th></tr>
							<tr>
							<td>
							<p style="font-weight:bold;">Children</p>
							<textarea class="form-control" style="width:100%;" rows="4" id="children" name="children"><?php  echo $row->children; ?></textarea>
							</td>
							</tr>
							<tr>
							<td>
							<p style="font-weight:bold;">Passport No :</p>
							<p><input class="form-control" type="text" id="passport_no" name="passport_no" value="<?php  echo $row->passport_no; ?>"> </p>
							<p style="font-weight:bold;">Issue Date :</p>
							<p><input class="form-control" type="text" id="issue_date" name="issue_date" value="<?php  echo $row->issue_date; ?>"></p>
							<p style="font-weight:bold;">Expiry Date :</p>
							<p><input class="form-control" type="text" id="expiry_date" name="expiry_date" value="<?php  echo $row->expiry_date; ?>"></p>
							</td>
							</tr>
						</table>
					</div>
				
					<?php
					echo '<h3>Details</h3>';
					if(count($EduacationalDetails)>0){ ?>
					<br>
					<div class="div_responsive" style="padding:0px">
					    	<hr>
						<table class="table table-responsive table-striped table-bordered" cellspacing="0"  style="">
							<thead>
							<tr class="bg-blue">
							<th colspan="7" class="text-center">Educational Details</th></tr>
							<tr>
							<th>University</th>
							<th>Board</th>
							<th>Specialist Subject area/s</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Grade</th>
							<th>Percentage</th>
							</tr>
							</thead>
							<tbody>
							<?php 
							$k=0;
							foreach($EduacationalDetails as $row1){ 
							$k++;
							?>
							<tr>
							<td>
							<input class="form-control" type="hidden" id="eid" name="eid{{$k}}" value="<?php  echo ucwords($row1->eid); ?>">
							<input class="form-control" type="text" id="university" name="university{{$k}}" value="<?php  echo ucwords($row1->university); ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="board" name="board{{$k}}" value="<?php  echo ucwords($row1->board); ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="sp_subject" name="sp_subject{{$k}}" value="<?php  echo ucwords($row1->sp_subject); ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="start_date" name="start_date{{$k}}" value="<?php  echo $row1->start_date; ?>"></td>
							<td>
							<input class="form-control"type="text" id="completion_date" name="completion_date{{$k}}" value="<?php echo $row1->completion_date; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="grade" name="grade{{$k}}" value="<?php echo $row1->grade; ?>" >
							</td>
							<td>
							<input class="form-control" type="text" id="percentage" name="percentage{{$k}}" value="<?php echo $row1->percentage; ?>">
							</td>
							</tr>
							<?php  } ?>
							</tbody>
						</table>
						<input class="form-control" type="hidden" id="kcount" name="kcount" value="<?php echo $k; ?>">
					</div>
					<?php } ?>
					
					<?php if(count($professional_courses)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12 div_responsive" style="padding:0px;">
						<table class="table table-responsive table-striped table-bordered" width="100%">
							<tr class="bg-green"><th colspan="5" class="text-center">Professional Courses Attended</th></tr>
							<tr>
							<th>Attended</th>
							<th>Course</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Location</th>
							</tr>
							<?php 
							$l=0;
							foreach($professional_courses as $row2){ 
							$l++;
							?>
							<tr>
							<td>
							<input class="form-control" type="hidden" id="pid" name="pid{{$l}}" value="<?php  echo ucwords($row2->pid); ?>">
							<input class="form-control" type="text" id="pattended" name="pattended{{$l}}" value="<?php  echo ucwords($row2->pattended); ?>"> 
							</td>
							<td>
							<input class="form-control" type="text" id="pcourse" name="pcourse{{$l}}" value="<?php  echo ucwords($row2->pcourse); ?>"> 
							</td>
							<td>
							<input class="form-control" type="text" id="pstart_date" name="pstart_date{{$l}}" value="<?php  echo $row2->pstart_date; ?>"> 
							</td>
							<td>
							<input class="form-control" type="text" id="pcompletion_date" name="pcompletion_date{{$l}}" value="<?php  echo $row2->pcompletion_date; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="plocation" name="plocation{{$l}}" value="<?php  echo $row2->plocation; ?>">
							</td>
							</tr>
							<?php } ?>
						</table>
						<input class="form-control" type="hidden" id="lcount" name="lcount" value="<?php echo $l; ?>">
					</div>
					<?php } ?>
					
					<?php if(count($professional_courses1)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12 div_responsive" style="padding:0px;">
						<table class="table table-responsive table-striped table-bordered" width="100%">
							<tr class="bg-green"><th colspan="5" class="text-center">Professional Courses Conducted</th></tr>
							<tr>
							<th>Conducted</th>
							<th>Course</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Location</th>
							</tr>
							<?php 
							$l1=0;
							foreach($professional_courses1 as $row21){ 
							$l1++;
							?>
							<tr>
							<td>
							<input class="form-control" type="hidden" id="pid1" name="pid1{{$l1}}" value="<?php  echo ucwords($row21->pid1); ?>">
							<input class="form-control" type="text" id="pattended1" name="pattended1{{$l1}}" value="<?php  echo ucwords($row21->pattended1); ?>"> 
							</td>
							<td>
							<input class="form-control" type="text" id="pcourse1" name="pcourse1{{$l1}}" value="<?php  echo ucwords($row21->pcourse1); ?>"> 
							</td>
							<td>
							<input class="form-control" type="text" id="pstart_date1" name="pstart_date1{{$l1}}" value="<?php  echo $row21->pstart_date1; ?>"> 
							</td>
							<td>
							<input class="form-control" type="text" id="pcompletion_date1" name="pcompletion_date1{{$l1}}" value="<?php  echo $row21->pcompletion_date1; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="plocation1" name="plocation1{{$l1}}" value="<?php  echo $row21->plocation1; ?>">
							</td>
							</tr>
							<?php } ?>
						</table>
						<input class="form-control" type="hidden" id="lcount1" name="lcount1" value="<?php echo $l1; ?>">
					</div>
					<?php } ?>
					
					
					
					<?php if(count($PaperResearch)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12 div_responsive" style="padding:0px;">
						<table class="table table-responsive table-striped table-bordered" width="100%">
							<tr style="background-color: #8cd417 ;color:#ffffff">
							<th colspan="5" class="text-center">Paper Research</th>
							</tr>
							<tr>
							<th>Paper/Research Title</th>
							<th>Published In</th>
							<th>Publish date</th>
							<th>Publish At</th>
							<th>Presentation Date</th>
							</tr>
							<?php 
							$m=0;
							foreach($PaperResearch as $row3){ 
							$m++;
							?>
							<tr>
							<td>
							<input class="form-control" type="hidden" id="prid" name="prid{{$m}}" value="<?php  echo ucwords($row3->prid); ?>">
							<input class="form-control" type="text" id="prtitle" name="prtitle{{$m}}" value="<?php  echo ucwords($row3->prtitle); ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="publish_in" name="publish_in{{$m}}" value="<?php echo ucwords($row3->publish_in); ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="publish_date" name="publish_date{{$m}}" value="<?php  echo $row3->publish_date; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="presented_at" name="presented_at{{$m}}" value="<?php  echo $row3->presented_at; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="presentation_date" name="presentation_date{{$m}}" value="<?php  echo $row3->presentation_date; ?>">
							</td>
							</tr>
							<?php  } ?>
						</table>
						<input class="form-control" type="hidden" id="mcount" name="mcount" value="<?php echo $m; ?>">
					</div>
					<?php } ?>
					
					<?php if(count($employment_history)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12 div_responsive" style="padding:0px;">
						<table class="table table-responsive table-striped table-bordered" width="100%">
							<tr style="background-color:#ffc300;color:#ffffff">
							<th colspan="6" class="text-center">Employment History</th>
							</tr>
							<tr>
							<th>Designation</th>
							<th>Period</th>
							<th>Subjects</th>
							<th>Curriculum</th>
							<th>Resposibility</th>
							<th>Leaving Reason</th>
							</tr>
							<?php 
							$n=0;
							foreach($employment_history as $row4){
							$n++;
							?>
							<tr>
							<td>
							<input class="form-control" type="hidden" id="ehid" name="ehid{{$n}}" value="<?php  echo ucwords($row4->ehid); ?>">
							<input class="form-control" type="text" id="eh_designation" name="eh_designation{{$n}}" value="<?php  echo $row4->eh_designation; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="period" name="period{{$n}}" value="<?php  echo $row4->period; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="subjects" name="subjects{{$n}}" value="<?php  echo $row4->subjects; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="curriculum" name="curriculum{{$n}}" value="<?php  echo $row4->curriculum; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="responsibility" name="responsibility{{$n}}" value="<?php  echo $row4->responsibility; ?>">
							</td>
							<td>
							<input class="form-control" type="text" id="leaving_reason" name="leaving_reason{{$n}}" value="<?php  echo $row4->leaving_reason; ?>">
							</td>
							</tr>
							<?php } ?>
						</table>
						<input class="form-control" type="hidden" id="ncount" name="ncount" value="<?php echo $n; ?>">
					</div>
					<?php } ?>
					<div class="clearfix"></div>
					
					</div>
					
						<div class="ln_solid"></div>
						<div class="form-group">
							{{ csrf_field() }}
							<div class="col-md-6 col-md-offset-3">
							<button id="savedata" type="submit" class="btn btn-success">Submit</button>
							<a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/job-applications">Cancel</a>
							</div>
						</div>
					
					</form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@include('included.footer')