@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','2')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('lisitng',$RoleArray) || Auth::user()->added_by== 0)
{
	foreach($ContactUs as $ConEdit){ }
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Contact US</h2>
					<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
					<button class="btn btn-primary btn-sm pull-right edit_con">Edit Contact Us</button>
					<?php } ?>
                    <div class="clearfix"></div>
					
                  </div>
					<div class="x_content" style="overflow-x:auto;">
					
					<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
					<form class="form-horizontal form-label-left" novalidate action="{{URL::route('updatecontactus')}}" enctype="multipart/form-data" method="post" style="display:none;" id="con_form">
                        
					<div class="col-md-8 col-sm-8 t_content">
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="c_address1"> Address1 <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
							  
							  <input id="c_id" name="c_id"  required="required" type="hidden" value="<?php echo $ConEdit->c_id; ?>" >
							  <input id="c_address1" name="c_address1" class="form-control col-md-7 col-xs-12" placeholder="Please Enter Address" required="required" type="text"  value="<?php echo $ConEdit->c_address1; ?>" >
							  <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="c_address2"> Address2 <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
							  <input id="c_address2" name="c_address2" class="form-control col-md-7 col-xs-12" placeholder="Please Enter Address" required="required" type="text" value="<?php echo $ConEdit->c_address2; ?>" >
							  <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="c_contact1"> Contact1 <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
							  <input id="c_contact1" name="c_contact1" class="form-control col-md-7 col-xs-12" placeholder="Please Enter Contact No." required="required" type="text" value="<?php echo $ConEdit->c_contact1; ?>" > 
							  <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="c_contact2"> Contact2 <span class=""></span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
							  <input id="c_contact2" name="c_contact2" class="form-control col-md-7 col-xs-12" placeholder="Please Enter Contact No."  type="text" value="<?php echo $ConEdit->c_contact2; ?>" > 
							  <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="faxno"> Fax Number <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
							  <input id="faxno" name="faxno" class="form-control col-md-7 col-xs-12" placeholder="Please Enter Fax No." required="required" type="text" value="<?php echo $ConEdit->faxno; ?>" >  
							  <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="c_email"> Email<span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
							  <input id="c_email" name="c_email" class="form-control col-md-7 col-xs-12" placeholder="Please Enter Email" required="required" type="text" value="<?php echo $ConEdit->c_email; ?>" > 
							  <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
							</div>
						</div>
						
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="map"> Map<span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
							  <textarea id="map" name="map" class="form-control col-md-7 col-xs-12" placeholder="Map Iframe" required="required" ><?php echo $ConEdit->map; ?></textarea>
							</div>
						</div>
						
					
					
					  
					<div class="col-md-12 col-sm-6 col-xs-12" style="margin-top:5px;">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-sm btn-success pull-left">Update</button>
						<button type="reset" class="btn btn-sm btn-danger pull-right cancel">Cancel</button>
					</div>
					</div>
					</form>
					<?php } ?>
                     
					<table class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Address</th>
                          <th>contact</th>
                          <th>Fax No</th>
                          <th>Email</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          
						  <?php } ?>
						  <?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                         
                          <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						if(count($ContactUs) > 0){ 
						foreach($ContactUs as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>					
						<td><?php echo $row->c_address1; ?><?php echo $row->c_address2; ?></td>
						<td><?php echo $row->c_contact1; ?></td>
						<td><?php echo $row->faxno; ?></td>
						<td><?php echo $row->c_email; ?></td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						
						
						<?php } ?>
						
						</tr>
						<?php $i++; }}else{?>
						<tr>
						<td colspan="5" class="text-center">No records found..!</td>
						</tr>
						<?php }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
		$(document).ready(function(){
			$('.edit_con').click(function(){
				$('#con_form').slideDown();
			});
			$('.cancel').click(function(){
				$('#con_form').slideUp();
			});
				
		});
	</script>

  @include('included.footer')