 @include('included.header')
 @include('included.super-admin-sidebar')
 <?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','7')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>

 <?php foreach($Compdata as $row){} ?>
 <?php foreach($UserData as $user){}?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Company</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					
                    <div class="col-md-8">
					
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('updateprofile')}}" enctype="multipart/form-data" method="post">
                        <input id="company_id" name="company_id" type="hidden" value="{{$row->company_id}}">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_name">Company Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input id="company_name" name="company_name" class="form-control col-md-7 col-xs-12" placeholder="Example PVT LTD." required="required" type="text" value="{{$row->company_name}}">
						  <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Company Type
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="company_type" name="company_type" readonly class="form-control col-md-7 col-xs-12">
							<option value="">Company Type?</option>
							<option value="Distributor" <?php if($row->company_type=='Distributor'){echo 'selected';} ?>>Distributor</option>
							<option value="Manufacturer" <?php if($row->company_type=='Manufacturer'){echo 'selected';} ?>>Manufacturer</option>
							<option value="Retailer" <?php if($row->company_type=='Retailer'){echo 'selected';} ?>>Retailer</option>
							<option value="Wholeseller" <?php if($row->company_type=='Wholeseller'){echo 'selected';} ?>>Wholeseller</option>
						  </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="email" id="company_email" name="company_email" required="required" placeholder="example@mail.com" class="form-control col-md-7 col-xs-12" readonly value="{{ $row->company_email }}">
						  <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_contact1">Company Contact1 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" id="company_contact1" name="company_contact1" maxlength="12" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value="{{ $row->company_contact1 }}">
						  <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_contact2">Company Contact2
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-1 has-feedback">
                          <input type="text" id="company_contact2" name="company_contact2" maxlength="12" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12"  value="{{ $row->company_contact2 }}">
						  <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">Company Website <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="url" id="company_website" name="company_website" required="required" placeholder="www.website.com" class="form-control col-md-7 col-xs-12" value="{{ $row->company_website }}">
						  <span class="fa fa-globe form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_address">Company Address <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="company_address" required="required" name="company_address" placeholder="Company Address" class="form-control col-md-7 col-xs-12">{{ $row->company_address }}</textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Company Logo
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" id="company_logo" name="company_logo" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_status">Status
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label><input type="radio" name="company_status" required="required" id="company_status" class="flat" value="Active" <?php if($row->company_status =="Active"){echo "checked";} ?> > Active</label>
							<label><input type="radio" name="company_status" required="required" id="company_status" class="flat" value="Inactive" <?php if($row->company_status =="Inactive"){echo "checked";} ?>> Inactive</label>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
						<!--<input id="_token" name="_token" type="hidden" value="{{ csrf_field() }}">-->
                        <div class="col-md-6 col-md-offset-3">
                          <a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/home">Cancel</a>
                          <button id="savedata" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
					
					</div>
					
					<div class="col-md-4">
					<?php if($row->company_logo !=""){ ?>
					<img src="{!! \Config::get('app.url') !!}/images/company_logo/<?php echo $row->company_logo;?>" class="thumbnail">
					<?php } else{ ?>
					<img src="{!! \Config::get('app.url') !!}/images/company_logo/nologo.png" class="thumbnail">	
					<?php }?>
					</div>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>
		
  @include('included.footer')