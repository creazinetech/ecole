@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','2')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('lisitng',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Footer Slider Images</h2>
					<?php if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){ ?>
					<button class="btn btn-primary btn-sm pull-right add_fs">Add Images</button>
					<?php } ?>
                    <div class="clearfix"></div>
					
                  </div>
					<div class="x_content" >
					
					<?php if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){ ?>
					<form class="form-horizontal form-label-left" novalidate action="{{URL::route('addfssliders')}}" enctype="multipart/form-data" method="post" id="slider_form" style="display:none;">
					<div class="col-md-3 col-sm-6 pull-right">
					
					<div class="item form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                          <input id="i
						  fs_title1" name="fs_title1" class="form-control col-md-7 col-xs-12" type="text" placeholder="Title">
						  <span class="fa fa-pencil form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
					
					<div class="item form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                          <input type="file" name="fs_img" id="fs_img" multiple class="input-sm form-control" placeholder="image(s)">
						  <span class="fa fa-image form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
					  
					<div class="col-md-12 col-sm-6 col-xs-12" style="margin-top:5px;">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-sm btn-success pull-left">Upload</button>
						<button type="reset" class="btn btn-sm btn-danger pull-right cancel">Cancel</button>
					</div>
					</div>
					</form>
					<?php } ?>
                     
					<table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Image</th>
                          <th>Title</th>
                          <th>Order</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
						  <?php } ?>
						  <?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
                          <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						if(count($FooterSliders) > 0){ 
						foreach($FooterSliders as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>
						<td>
						<img src="{!! \Config::get('app.url_base') !!}/images/footerslider-images/<?php echo $row->fs_img; ?>" style="height:45px;width:120px;">
						</td>						
						<td><?php echo $row->fs_title1; ?></td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<select id="fs_order" name="fs_order" class="form-group fs_order" data-id="<?php echo $row->fs_id; ?>">
						<option value="0" >?</option>
						<?php for($j=0;$j <= count($FooterSliders); $j++){?>
						  <option value="<?php echo $j; ?>" <?php if($row->fs_order == $j){echo "selected";} ?>><?php echo $j; ?></option>
						<?php } ?>
						</select>
						</td>
						<td>
						<select id="fs_status" name="fs_status" class="form-group fs_status" data-id="<?php echo $row->fs_id; ?>">
						  <option value="Active" <?php if($row->fs_status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->fs_status == "Inactive"){echo "selected";} ?>>Inactive</option>
						</select>
						</td>
						<?php } ?>
						<?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<a href="{{URL::to('footer-sliders/fsdelete',array(Crypt::encrypt($row->fs_id)))}}" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm('Are you sure want to delete this?')"><i class="fa fa-trash"></i></a>
						</td>
						<?php } ?>
						</tr>
						<?php $i++; }}else{?>
						<tr>
						<td colspan="5" class="text-center">No records found..!</td>
						</tr>
						<?php }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
	$(document).ready(function(){
		$('.add_fs').click(function(){
			$('#slider_form').slideDown();
		});
		$('.cancel').click(function(){
			$('#slider_form').slideUp();
		});
		
		$('.fs_status').change(function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'footer-sliders/fsstatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});		
		
		$('.fs_order').change(function(){
			var order = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'footer-sliders/fsorder',
				data: 'order='+order+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
	});
	</script>
		
  @include('included.footer')