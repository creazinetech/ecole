<!DOCTYPE html>
 <?php foreach ($Data as $row){ } ?>
<html><head>
    <meta charset="utf-8">
    <title><?php echo ucwords($row->name); ?> - Enquiry PDF</title>
	<style>
	.td-space{
		margin-left:10px;
	}
	table{
		border-collapse: collapse;
	}
	th, td {
		padding-left:8px;
		padding-right:8px;
		text-align: left;
	}
	body{
		color:black;
		font-family:Helvetica, Arial, sans-serif;
		line-height:30px;
		border:1px solid grey;
		font-size:13px;
	}
	</style>
  </head><body><table width="100%">
					<tr>
					<td width="">
					<table width="100%" style="border:1px solid;padding-top:2px;"> 
					<tr>
					<td width="10%">
					<center>
					<img src="http://219.90.67.77/~ecolemondiale/admin_ecole/public/images/EM.png">
					</center>
					</td>
					<td width="">
					<center>
					<h2>École Mondiale World School</h2>
					<h3><u>Student Enquiry Form</u></h3>
					</center>
					</td>
					<td width="10%">
					<center>
					<img src="http://219.90.67.77/~ecolemondiale/admin_ecole/public/images/IB.png">
					</center>
					</td>
					</tr>
					</table>
					</td>
					</tr><tr>
						<td class="td-space">
						<strong>1) Student Name:</strong> <?php echo ucwords($row->name); ?>
						</td></tr>
						<tr><td class="td-space"><strong>2) Date of Birth:</strong> <?php echo date('d F Y',strtotime($row->dob)); ?></td></tr>
						<tr><td class="td-space"><strong>3) Residential Address (Permanent):</strong> <?php echo ucwords($row->address); ?></td></tr>
						<tr><td><strong>4) City:</strong> <?php echo ucwords($row->city); ?>
							&nbsp; <strong>State:</strong> <?php echo ucwords($row->state); ?>
							&nbsp; <strong>Country:</strong> <?php echo ucwords($row->country); ?>
						</td></tr>
						<tr><td><strong>5) Nationality:</strong> <?php echo ucwords($row->nationality); ?></td></tr>
						<tr><td><strong>6) Name of Present School:</strong> <?php echo ucwords($row->nopskul); ?></td></tr>
						<tr><td><strong>7) Current Grade:</strong> <?php echo ucwords($row->grade); ?></td></tr>
						<tr><td><strong>8) Grade to Join in:</strong> <?php echo $row->year.' :'.$row->gradetjoin; ?></td></tr>
						<tr>
						<td><strong>9) Parent Details:</strong>
							<table width="100%" border="1" style="margin-top:5px;margin-bottom:5px;"> 
								<tr>
									<th width="20%">Details</th>
									<th width="40%">Father</th>
									<th width="40%">Mother</th>
								</tr>
								<tr>
									<th class="td-space">Name:</th>
									<td><?php if(!empty($row->fname)){echo ucwords($row->fname); } ?></td>
									<td><?php if(!empty($row->mname)){echo ucwords($row->mname); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Occupation:</th>
									<td><?php if(!empty($row->foccupation)){echo ucwords($row->foccupation); } ?></td>
									<td><?php if(!empty($row->moccupation)){echo ucwords($row->moccupation); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Designation:</th>
									<td><?php if(!empty($row->fdesignation)){echo ucwords($row->fdesignation); } ?></td>
									<td><?php if(!empty($row->mdesignation)){echo ucwords($row->mdesignation); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Name of the Organization:</th>
									<td><?php if(!empty($row->forganization)){echo ucwords($row->forganization); } ?></td>
									<td><?php if(!empty($row->morganization)){echo ucwords($row->morganization); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Official Address:</th>
									<td><?php if(!empty($row->foffadd)){echo ucwords($row->foffadd); } ?></td>
									<td><?php if(!empty($row->moffadd)){echo ucwords($row->moffadd); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Offic Tel:</th>
									<td><?php if(!empty($row->fofftel)){echo ucwords($row->fofftel); } ?></td>
									<td><?php if(!empty($row->mofftel)){echo ucwords($row->mofftel); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Email:</th>
									<td><?php if(!empty($row->femail)){echo ucwords($row->femail); } ?></td>
									<td><?php if(!empty($row->memail)){echo ucwords($row->memail); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Mobile Number:</th>
									<td><?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?></td>
									<td><?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?></td>
								</tr>
								<tr>
									<th class="td-space">Residence Tel:</th>
									<td><?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?></td>
									<td><?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?></td>
								</tr>
							</table>
							
						</td>
						</tr>
						<tr>
						<td><strong>10) What do you wish to know:</strong> <?php echo ucwords($row->whattoknow); ?></td>
						</tr>
						<tr>
						<td><strong>11) From where did you hear about our School:</strong> <?php echo ucwords($row->aboutschool); ?></td>
						</tr>
						</table>
  </body></html>