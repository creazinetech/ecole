@include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('view',$RoleArray) || Auth::user()->added_by== 0){
foreach ($Data as $row){ }
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
		<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Enquiry View</h2>
				<div class="clearfix"></div>
				<?php if(in_array('print',$RoleArray) || Auth::user()->added_by== 0) { ?>
				<a href="{{URL::to('enquiry/enquiry-pdf',array(Crypt::encrypt($row->id)))}}" class="btn btn-success btn-xs" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Print PDF</a>
				<?php }?>
				<a href="{{URL::to('enquiry')}}" class="btn btn-danger btn-xs" title="Back"><i class="fa fa-close"></i> Back</a>
			  </div>
				<div class="x_content">	
				
				<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-2 col-sm-12 col-xs-12"></div>
				<div class="col-md-8 col-sm-12 col-xs-12" style="border:1px solid grey;">
					<br>
					<div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid grey;">
						<div class="col-md-2 col-sm-2 col-xs-2">
							<img class="img-responsive avatar-view" src="{!! \Config::get('app.url_base') !!}/images/EM.png">
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<h3>École Mondiale World School</h3>
						<h3><u>Student Enquiry Form</u></h3>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<img class="img-responsive avatar-view" src="{!! \Config::get('app.url_base') !!}/images/IB.png">
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="font-size:14px;">
					<br><br>
						<p><strong>1) Student Name:</strong> <?php echo ucwords($row->name); ?></p>
						<p><strong>2) Date of Birth:</strong> <?php echo date('d F Y',strtotime($row->dob)); ?></p>
						<p><strong>3) Residential Address (Permanent):</strong> <?php echo ucwords($row->address); ?></p>
						<p><strong>4) City:</strong> <?php echo ucwords($row->city); ?>
							&nbsp; <strong>State:</strong> <?php echo ucwords($row->state); ?>
							&nbsp; <strong>Country:</strong> <?php echo ucwords($row->country); ?>
						</p>
						<p><strong>5) Nationality:</strong> <?php echo ucwords($row->nationality); ?></p>
						<p><strong>6) Name of Present School:</strong> <?php echo ucwords($row->nopskul); ?></p>
						<p><strong>7) Current Grade:</strong> <?php echo ucwords($row->grade); ?></p>
						<p><strong>8) Grade to Join in:</strong> <?php echo $row->year.' :'.$row->gradetjoin; ?></p>
						<p><strong>9) Parent Details:</strong>
							<table class="table table-responsive table-bordered table-hover" width="100%"> 
								<thead>
								<tr>
									<th width="20%">Details</th>
									<th width="40%">Father</th>
									<th width="40%">Mother</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th>Name:</th>
									<td><?php if(!empty($row->fname)){echo ucwords($row->fname); } ?></td>
									<td><?php if(!empty($row->mname)){echo ucwords($row->mname); } ?></td>
								</tr>
								<tr>
									<th>Occupation:</th>
									<td><?php if(!empty($row->foccupation)){echo ucwords($row->foccupation); } ?></td>
									<td><?php if(!empty($row->moccupation)){echo ucwords($row->moccupation); } ?></td>
								</tr>
								<tr>
									<th>Designation:</th>
									<td><?php if(!empty($row->fdesignation)){echo ucwords($row->fdesignation); } ?></td>
									<td><?php if(!empty($row->mdesignation)){echo ucwords($row->mdesignation); } ?></td>
								</tr>
								<tr>
									<th>Name of the Organization:</th>
									<td><?php if(!empty($row->forganization)){echo ucwords($row->forganization); } ?></td>
									<td><?php if(!empty($row->morganization)){echo ucwords($row->morganization); } ?></td>
								</tr>
								<tr>
									<th>Official Address:</th>
									<td><?php if(!empty($row->foffadd)){echo ucwords($row->foffadd); } ?></td>
									<td><?php if(!empty($row->moffadd)){echo ucwords($row->moffadd); } ?></td>
								</tr>
								<tr>
									<th>Office Tel:</th>
									<td><?php if(!empty($row->fofftel)){echo ucwords($row->fofftel); } ?></td>
									<td><?php if(!empty($row->mofftel)){echo ucwords($row->mofftel); } ?></td>
								</tr>
								<tr>
									<th>Email:</th>
									<td><?php if(!empty($row->femail)){echo $row->femail; } ?></td>
									<td><?php if(!empty($row->memail)){echo $row->memail; } ?></td>
								</tr>
								<tr>
									<th>Mobile Number:</th>
									<td><?php if(!empty($row->fmobile)){echo ucwords($row->fmobile); } ?></td>
									<td><?php if(!empty($row->mmobile)){echo ucwords($row->mmobile); } ?></td>
								</tr>
								<tr>
									<th>Residence Tel:</th>
									<td><?php if(!empty($row->frestel)){echo ucwords($row->frestel); } ?></td>
									<td><?php if(!empty($row->mrestel)){echo ucwords($row->mrestel); } ?></td>
								</tr>
								</tbody>
							</table>
						</p>
						<p><strong>10) What do you wish to know:</strong> <?php echo ucwords($row->whattoknow); ?></p>
						<p><strong>11) From where did you hear about our School:</strong> <?php echo ucwords($row->aboutschool); ?></p>
					</div>
				
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12"></div>
				</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
        <!-- /page content -->
<?php } ?>
@include('included.footer')