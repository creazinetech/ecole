 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result">
			  </p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Enquiry List</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">				
					<table id="datatable-fixed-header" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                      <thead>
                          <th width="3%">Sr No</th>
                          <th>Entry Id</th>
                          <th>Name</th>
                          <th>Date OF Birth</th>
                          <th>Enquiry Date</th>
						  <?php if(in_array('print',$RoleArray) ||in_array('edit',$RoleArray) ||in_array('view',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
                      <tbody>
					    <?php $i = 1; ?>
						<?php foreach ($Data as $row){ ?>
                        <tr style="<?php if($row->viewed==0){ echo 'font-weight:bold;background:rgba(0,0,0,0.1)';}?>">
                          <td> <?php echo $i; ?></td>
                          <td> <?php echo ucwords($row->id); ?> </td>
                          
                           <td>
                              <?php  echo ucwords($row->name); ?> 
                          </td>                          
                          
                          <td> <?php echo date('d-m-Y',strtotime($row->dob)); ?> </td>
						  <td><?php echo date('d-m-Y',strtotime($row->created_at)); ?></td>
						  <?php if(in_array('print',$RoleArray) ||in_array('edit',$RoleArray) ||in_array('view',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <td>
						  <?php if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
						  { ?>
						  <a href="{{URL::to('enquiry/view',array(Crypt::encrypt($row->id)))}}" class="btn btn-warning btn-xs" title="View"><i class="fa fa-file"></i></a>
						  <?php 
						  }
						  if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
						  { ?>
						  <a href="{{URL::to('enquiry/edit',array(Crypt::encrypt($row->id)))}}" class="btn btn-primary btn-xs" title="Edit"><i class="fa fa-edit"></i></a>
						  <?php 
						  }
						  if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
						  { ?>
						  <a href="{{URL::to('enquiry/delete',array(Crypt::encrypt($row->id)))}}" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash"></i></a>
						  <?php } ?>
						  </td>
						  <?php } ?>
                        </tr>
						<?php  $i++; } ?>
                      </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php 
}
?>

<script>
// $('.table').dataTable( {
  // "pageLength":50
// });
</script>		
@include('included.footer')