/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license 
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = '../public/plugins/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = '../public/plugins/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = '../public/plugins/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = '../public/plugins/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = '../public/plugins/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = '../public/plugins/kcfinder/upload.php?opener=ckeditor&type=flash';
    
    /*
    config.filebrowserBrowseUrl = 'http://www.ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = 'http://www.ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = 'http://www.ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = 'http://www.ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = 'http://www.ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = 'http://www.ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=flash';
    
    
    config.filebrowserBrowseUrl = 'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = 'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = 'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = 'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = 'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = 'http://219.90.67.77/~ecolemondiale/admin_ecole/public/plugins/kcfinder/upload.php?opener=ckeditor&type=flash';
    */
};
