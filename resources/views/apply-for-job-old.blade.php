<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','39')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
        <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/faculty-and-staff">Faculty & Career Opening</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo ucwords($Page->page_heading);} ?> 
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
		
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<div id="tg-content" class="tg-content">
			<section class="tg-sectionspace tg-haslayout">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin-bottom: 10px;">
				<div class="tg-contactus tg-contactusvone">
					<div class="tg-titleborder tg-content">
						<h2>Career Opening</h2>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
							<?php echo $Page->brief_desc; ?>
					</div>
					
				</div>
			 
				<div class="tg-titleborder tg-content">
				   <h2>Apply Now</h2>
				  <!--<h3 class="progress-bar-striped">Step 1 of 6</h3>
				  <div id="Progressbar1">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:16%;height: 20px;border-radius:5px;"> 16% </div>
				
				  </div>-->
				</div>
	    		</div>
	    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin-bottom: 10px;display:none;">
	    		@if(count($errors))
            	    <div class="alert alert-danger">
                		<ul>
                			@foreach($errors->all() as $error)
                			<li>{{ $error }}</li>
                			@endforeach
                		</ul>
            	    </div>
                 @endif
                 
                </div>
                @if(Session::get('CaptchaVerificaion') !='')
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;">
                <div class="alert alert-danger">{{ Session::get('CaptchaVerificaion') }} </div>
                </div>
                @endif
                
				<hr>
				
				<p  class="tg-content-text">Fill up the application form below. The mandatory fields are marked with an *</p>
				
				<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id=""   enctype='multipart/form-data'  action="{{ url('applyjob') }}" >
				<div class="alert alert-info">Personal Details</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="padding:0px;">
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
					  <select class="form-control" name="position" id="position">
                        <option value='' >Position applying for?</option>
                    
                        <?php
                        $JobTitles = DB::table('job_titles')->where('status','=','Active')->orderBy('title', 'asc')->get();
                        if(count($JobTitles)>0){
                            foreach($JobTitles as $JobTitle){ ?>
                            <option value='<?php echo $JobTitle->title; ?>' @if (Input::old('position') == $JobTitle->title) selected="selected" @endif><?php echo $JobTitle->title; ?></option>
                        <?php    }
                        }
                        ?>
						</select>
						<span id="positionerr" style="color:red;display:none;"> Please Select Position applying for</span>
						@if ($errors->has('position'))
							<div class="error" style="color:red;">{{ $errors->first('position') }}</div>
						@endif
					</div>
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name*" value="{{ old('name') }}">
						<span id="nameerr" style="color:red;display:none;"> Please Enter Name.</span>
						@if ($errors->has('name'))
							<div class="error" style="color:red;">{{ $errors->first('name') }}</div>
						@endif
					</div>
					
				   <div class="form-group col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="gender" id="gender">
							<option value="">Gender?</option>
							<option value="Male" @if (Input::old('gender') == 'Male') selected="selected" @endif>Male</option>
							<option value="Female" @if (Input::old('gender') == 'Female') selected="selected" @endif>Female</option>
						</select>
						<span id="gendererr" style="color:red;display:none;"> Please Enter Gender</span>
						@if ($errors->has('gender'))
							<div class="error" style="color:red;">{{ $errors->first('gender') }}</div>
						@endif
					</div>
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="nationality" id="nationality" placeholder="Nationality*" value="{{ old('nationality') }}" >
						<span id="nationalityerr" style="color:red;display:none;"> Please Enter Nationality</span>
						@if ($errors->has('nationality'))
							<div class="error" style="color:red;">{{ $errors->first('nationality') }}</div>
						@endif
					</div>
					
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
						<label>DD-MM-YYYY</label>
						<input type="text" class="datepicker form-control" id="dob" name="dob" placeholder="Date of Birth*" value="{{ old('dob') }}">
						<span id="doberr" style="color:red;display:none;"> Please Enter Date of Birth</span>
						@if ($errors->has('dob'))
							<div class="error" style="color:red;">{{ $errors->first('dob') }}</div>
						@endif
					</div>
					
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
						<label>Upload Photograph<small>(Accepted file types: jpg, gif, png, jpeg, bmp.)</small></label>
						<input type="file" name="photo" id="photo" class="form-control" placeholder="Upload Photograph" value="{{ old('photo') }}">
						<span id="photoerr" style="color:red;display:none;"> Please Upload Photograph.</span>
						<span id="photoerr1" style="color:red;display:none;">Please upload 'jpeg', 'jpg', 'png', 'bmp' format image.</span>
						@if ($errors->has('photo'))
							<div class="error" style="color:red;">{{ $errors->first('photo') }}</div>
						@endif
					</div>		
					
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<select class="form-control" name="marital_status" id="marital_status">
							<option value="">Marital Status?</option>
							<option value="Married" @if (Input::old('marital_status') == 'Married') selected="selected" @endif>Married</option>
							<option value="Unmarried" @if (Input::old('marital_status') == 'Unmarried') selected="selected" @endif>Unmarried</option>
							<option value="Other" @if (Input::old('marital_status') == 'Other') selected="selected" @endif>Other</option>
						</select>
						<span id="marital_statuserr" style="color:red;display:none;"> Please Select Marital Status.</span>
						@if ($errors->has('marital_status'))
							<div class="error" style="color:red;">{{ $errors->first('marital_status') }}</div>
						@endif
				   </div>

				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<textarea type="text" class="form-control" name="children" id="children" placeholder="Children's Name - Age">{{ old('children') }}</textarea>
						<span id="childrenerr" style="color:red;display:none;"> Please Enter Children's Names - Age.</span>
						@if ($errors->has('children'))
							<div class="error" style="color:red;">{{ $errors->first('children') }}</div>
						@endif
				   </div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="cadd" id="cadd" placeholder="Current Address*" value="{{ old('cadd') }}">
						<span id="cadderr" style="color:red;display:none;"> Please Enter Current Address.</span>
						@if ($errors->has('cadd'))
							<div class="error" style="color:red;">Please Enter Current Address</div>
						@endif
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" max-length="6" class="form-control" name="pincode" id="pincode" placeholder="Zipcode" onkeypress="return isNumberKey(event)" value="{{ old('pincode') }}">
						<span id="pincodeerr" style="color:red;display:none;"> Please Enter 6 Digit Zipcode.</span>
						@if ($errors->has('Zipcode'))
							<div class="error" style="color:red;">Please Enter 6 Digit Zipcode</div>
						@endif
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" max-length="10" class="form-control" name="landline" id="landline" placeholder="Landline no.*" onkeypress="return isNumberKey(event)" value="{{ old('landline') }}">
						<span id="landlineerr" style="color:red;display:none;"> Please Enter Landline no.</span>
						@if ($errors->has('landline'))
							<div class="error" style="color:red;">Please Enter Landline no.</div>
						@endif
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" max-length="10" class="form-control" name="mob_no" id="mob_no" placeholder="Mobile no." onkeypress="return isNumberKey(event)" value="{{ old('mob_no') }}">
						<span id="mob_noerr" style="color:red;display:none;"> Please Enter 10 Digit Mobile no.</span>
						@if ($errors->has('mob_no'))
							<div class="error" style="color:red;">Please Enter 10 Digit Mobile no.</div>
						@endif
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="email_id" id="email_id" placeholder="Email id*" value="{{ old('email_id') }}">
						<span id="email_iderr" style="color:red;display:none;"> Please Enter Email Id.</span>
						@if ($errors->has('email_id'))
							<div class="error" style="color:red;">{{ $errors->first('email_id') }}</div>
						@endif
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="padd" id="padd" placeholder="Permanent Address (Write ‘same’ if the same as current address)*" value="{{ old('padd') }}">
						<span id="padderr" style="color:red;display:none;"> Please Enter Permanent Address.</span>
						@if ($errors->has('padd'))
							<div class="error" style="color:red;">Please Enter Permanent Address.</div>
						@endif
				   </div>
				   
				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
						<input type="text" class="datepicker form-control" id="newjoin_dt" name="newjoin_dt" value="{{ old('newjoin_dt') }}" placeholder="New Joining Date*">
						<span id="newjoin_dterr" style="color:red;display:none;"> Please Enter Date of Birth</span>
						@if ($errors->has('newjoin_dterr'))
							<div class="error" style="color:red;">{{ $errors->first('newjoin_dt') }}</div>
						@endif
					</div>
							
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				   <div class="alert alert-info">Passport Details</div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="passport_no" id="passport_no" placeholder="Passport no.*" value="{{ old('passport_no') }}">
						<span id="passport_noerr" style="color:red;display:none;"> Please Enter Passport no.</span>
						@if ($errors->has('passport_no'))
							<div class="error" style="color:red;">Please Enter Passport no.</div>
						@endif
				   </div>	
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="issue_date" id="issue_date" placeholder="Passport issue date (dd-mm-yyyy)*" value="{{ old('issue_date') }}">
						<span id="issue_dateerr" style="color:red;display:none;"> Please Enter Passport issue date.</span>
						@if ($errors->has('issue_date'))
							<div class="error" style="color:red;">Please Enter Passport issue date.</div>
						@endif
				   </div>	
				   	
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="issue_place" id="issue_place" placeholder="Issue Place*" value="{{ old('issue_place') }}">
						<span id="issue_placeerr" style="color:red;display:none;"> Please Enter Issue Place.</span>
						@if ($errors->has('issue_place'))
							<div class="error" style="color:red;">Please Enter Issue Place.</div>
						@endif
					</div>	
				   	
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="expiry_date" id="expiry_date" placeholder="Expiry Date (dd-mm-yyyy)*" value="{{ old('expiry_date') }}">
						<span id="expiry_dateerr" style="color:red;display:none;"> Please Enter Expiry Date.</span>
						@if ($errors->has('expiry_date'))
							<div class="error" style="color:red;">Please Enter Expiry Date.</div>
						@endif
					</div>					   
				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="overflow-x:auto">
				   <div class="alert alert-info">Educational Details</div>
				<table class="table table-responsive table-condensed table-hover table-bordered input_fields_wrap">
				<tr style="font-weight:200;font-size:12px;">
				<th>University</th>
				<th>Board</th>
				<th>Start Date</th>
				<th>Complition Date</th>
				<th>Grade</th>
				<th>Percentage</th>
				<th></th>
				</tr>
				<?php 
				$Universities = old('university'); 
				$board = old('board');
				$start_date = old('start_date');
				$completion_date = old('completion_date');
				$grade = old('grade');
				$percentage = old('percentage');
				if(!empty($Universities)){
					for($i=0; $i<count($Universities); $i++ ){
				?>
				<tr>
				<td><input type="text" class="form-control" name="university[]" id="university" placeholder="University*" value="<?= $Universities[$i]; ?>"></td>
				<td><input type="text" class="form-control" name="board[]" id="board" placeholder="Board*" value="<?= $board[$i]; ?>"></td>
				<td><input type="text" class="form-control" name="start_date[]" id="start_date" placeholder="Start Date*" value="<?= $start_date[$i]; ?>"></td>
				<td><input type="text" class="form-control" name="completion_date[]" id="completion_date" placeholder="Complition Date*" value="<?= $completion_date[$i]; ?>"></td>
				<td><input type="text" class="form-control" name="grade[]" id="grade" placeholder="Grade*" value="<?= $grade[$i]; ?>"></td>
				<td><input type="text" class="form-control" name="percentage[]" id="percentage" placeholder="Percentage*" value="<?= $percentage[$i]; ?>"></td>
				<td>
				    <?php if($i == 0){ ?>
					<span class="btn btn-primary btn-sm green" id="add_attributes" name="add_attributes"><i class="fa fa-plus"></i></span>
					<?php }else{ ?>
					<a href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm" data-id="<?= $i;?>"><i class="fa fa-close"></i></a>
					<?php } ?>
			    </td>
				</tr>
				<?php } }else{ ?>
				<tr>
				<td><input type="text" class="form-control" name="university[]" id="university" placeholder="University*"></td>
				<td><input type="text" class="form-control" name="board[]" id="board" placeholder="Board*"></td>
				<td><input type="text" class="form-control" name="start_date[]" id="start_date" placeholder="Start Date*"></td>
				<td><input type="text" class="form-control" name="completion_date[]" id="completion_date" placeholder="Complition Date*"></td>
				<td><input type="text" class="form-control" name="grade[]" id="grade" placeholder="Grade*"></td>
				<td><input type="text" class="form-control" name="percentage[]" id="percentage" placeholder="Percentage*"></td>
				<td><span class="btn btn-primary btn-sm green" id="add_attributes" name="add_attributes"><i class="fa fa-plus"></i></span></td>
				</tr>
					
				<?php }?>
				</table>			   
				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="overflow-x:auto">
					<div class="alert alert-info">List the professional development courses attended by you, if any</div>
					<table class="table table-responsive table-condensed table-hover table-bordered pctbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Attended</th>
					<th>Course</th>
					<th>Start Date</th>
					<th>Complition Date</th>
					<th>Location</th>
					<th></th>
					</tr>
					<?php 
					$pattended = old('pattended'); 
					$pcourse = old('pcourse');
					$pstart_date = old('pstart_date');
					$pcompletion_date = old('pcompletion_date');
					$plocation = old('plocation');
					if(!empty($pattended) && count($pattended) >0){
						for($i=0; $i<count($pattended); $i++ ){
					?>					
					<tr>
					<td><input type="text" class="form-control" name="pattended[]" id="pattended" placeholder="Attended*" value="<?= $pattended[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="pcourse[]" id="pcourse" placeholder="Course*" value="<?= $pcourse[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="pstart_date[]" id="pstart_date" placeholder="Start Date*" value="<?= $pstart_date[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="pcompletion_date[]" id="pcompletion_date" placeholder="Completion Date*" value="<?= $pcompletion_date[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="plocation[]" id="plocation" placeholder="Location*" value="<?= $plocation[$i]; ?>"></td>
					<td>
				    <?php if($i == 0){ ?>
					<span class="btn btn-primary btn-sm green" id="addpc" name="addpc"><i class="fa fa-plus"></i></span>
					<?php }else{ ?>
					<a href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm" data-id="<?= $i;?>"><i class="fa fa-close"></i></a>
					<?php } ?>
				    </td>
					</tr>
					<?php }}else{?>
					<tr>
					<td><input type="text" class="form-control" name="pattended[]" id="pattended" placeholder="Attended*"></td>
					<td><input type="text" class="form-control" name="pcourse[]" id="pcourse" placeholder="Course*"></td>
					<td><input type="text" class="form-control" name="pstart_date[]" id="pstart_date" placeholder="Start Date*"></td>
					<td><input type="text" class="form-control" name="pcompletion_date[]" id="pcompletion_date" placeholder="Completion Date*"></td>
					<td><input type="text" class="form-control" name="plocation[]" id="plocation" placeholder="Location*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addpc" name="addpc"><i class="fa fa-plus"></i></span></td>
					</tr>
					<?php } ?>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="overflow-x:auto">
					<div class="alert alert-info">List the professional development courses conducted by you, if any</div> 
					<table class="table table-responsive table-condensed table-hover table-bordered pctbl1">
					<tr style="font-weight:200;font-size:12px;">
					<th>Conducted</th>
					<th>Course</th>
					<th>Start Date</th>
					<th>Complition Date</th>
					<th>Location</th>
					<th></th>
					</tr>
					</tr>
					<?php 
					$pattended1 = old('pattended1'); 
					$pcourse1 = old('pcourse1');
					$pstart_date1 = old('pstart_date1');
					$pcompletion_date1 = old('pcompletion_date1');
					$plocation1 = old('plocation1');
					if(!empty($pattended1)){
						for($i=0; $i<count($pattended1); $i++ ){
					?>					
					<tr>
					<td><input type="text" class="form-control" name="pattended1[]" id="pattended1" placeholder="Counducted*" value="<?= $pattended[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="pcourse1[]" id="pcourse1" placeholder="Course*" value="<?= $pcourse[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="pstart_date1[]" id="pstart_date1" placeholder="Start Date*" value="<?= $pstart_date[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="pcompletion_date1[]" id="pcompletion_date1" placeholder="Completion Date*" value="<?= $pcompletion_date[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="plocation1[]" id="plocation1" placeholder="Location*" value="<?= $plocation[$i]; ?>"></td>
					<td>
				    <?php if($i == 0){ ?>
					<span class="btn btn-primary btn-sm green" id="addpc1" name="addpc1"><i class="fa fa-plus"></i></span>
					<?php }else{ ?>
					<a href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm" data-id="<?= $i;?>"><i class="fa fa-close"></i></a>
					<?php } ?>
					</td>
					</tr>
					<?php }}else{?>
					<tr>
					<td><input type="text" class="form-control" name="pattended1[]" id="pattended1" placeholder="Counducted*"></td>
					<td><input type="text" class="form-control" name="pcourse1[]" id="pcourse1" placeholder="Course*"></td>
					<td><input type="text" class="form-control" name="pstart_date1[]" id="pstart_date1" placeholder="Start Date*"></td>
					<td><input type="text" class="form-control" name="pcompletion_date1[]" id="pcompletion_date1" placeholder="Completion Date*"></td>
					<td><input type="text" class="form-control" name="plocation1[]" id="plocation1" placeholder="Location*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addpc1" name="addpc1"><i class="fa fa-plus"></i></span></td>
					</tr>
					<?php } ?>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="overflow-x:auto">
					<div class="alert alert-info">Publications/Research Papers/ Documents produced (in the last 5 years)</div>
					<table class="table table-responsive table-condensed table-hover table-bordered prtbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Title</th>
					<th>Published in</th>
					<th>Date of Publishing</th>
					<th>Presented at</th>
					<th>Date of Presentation</th>
					<th></th>
					</tr>
					<?php 
					$prtitle = old('prtitle'); 
					$publish_in = old('publish_in'); 
					$publish_date = old('publish_date');
					$presented_at = old('presented_at');
					$presentation_date = old('presentation_date');
					if(!empty($prtitle)){
						for($i=0; $i<count($prtitle); $i++ ){
					?>							
					<tr>
					<td><input type="text" class="form-control" name="prtitle[]" id="prtitle" placeholder="Title*" value="<?= $prtitle[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="publish_in[]" id="publish_in" placeholder="Published In*" value="<?= $publish_in[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="publish_date[]" id="publish_date" placeholder="Publish Date*" value="<?= $publish_date[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="presented_at[]" id="presented_at" placeholder="Presented At*" value="<?= $presented_at[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="presentation_date[]" id="presentation_date" placeholder="Presentation Date*" value="<?= $presentation_date[$i]; ?>"></td>
					<td>
				    <?php if($i == 0){ ?> 
					<span class="btn btn-primary btn-sm green" id="addpr" name="addpr"><i class="fa fa-plus"></i></span>
					<?php }else{ ?>
					<a href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm" data-id="<?= $i;?>"><i class="fa fa-close"></i></a>
					<?php } ?>
				    </td>
					</tr>
					<?php }}else{?>
					<tr>
					<td><input type="text" class="form-control" name="prtitle[]" id="prtitle" placeholder="Title*"></td>
					<td><input type="text" class="form-control" name="publish_in[]" id="publish_in" placeholder="Published In*"></td>
					<td><input type="text" class="form-control" name="publish_date[]" id="publish_date" placeholder="Publish Date*"></td>
					<td><input type="text" class="form-control" name="presented_at[]" id="presented_at" placeholder="Presented At*"></td>
					<td><input type="text" class="form-control" name="presentation_date[]" id="presentation_date" placeholder="Presentation Date*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addpr" name="addpr"><i class="fa fa-plus"></i></span></td>
					</tr>
					<?php } ?>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				   <div class="alert alert-info">Other Details</div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="internation_school_exp" name="internation_school_exp" placeholder="Experience in International Schools*"  value="{{ old('internation_school_exp') }}">
						<span id="internation_school_experr" style="color:red;display:none;">Experience in International Schools?</span>
						@if ($errors->has('internation_school_exp'))
							<div class="error" style="color:red;">Please Enter Experience in International Schools.</div>
						@endif
				   </div>	
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
				   <label>Experience In</label>
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="IB"  @if(is_array(old('experience_in')) && in_array('IB',old('experience_in'))) checked @endif> IB
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="IGCSE"  @if(is_array(old('experience_in')) && in_array('IGCSE',old('experience_in'))) checked @endif> IGCSE
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="A Level"  @if(is_array(old('experience_in')) && in_array('A Level',old('experience_in'))) checked @endif> A Level
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="None"  @if(is_array(old('experience_in')) && in_array('None',old('experience_in'))) checked @endif> None
				   </div>
				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="experience_year" name="experience_year" placeholder="Experience in Years*" value="{{ old('experience_year') }}">
						<span id="experience_yearerr" style="color:red;display:none;">Experience in Years?</span>
						@if ($errors->has('experience_year'))
							<div class="error" style="color:red;">Please Enter Experience in Years.</div>
						@endif
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="current_employer" name="current_employer" placeholder="Current Employer*" value="{{ old('current_employer') }}">
						<span id="current_employererr" style="color:red;display:none;">Please Enter Current Employer.</span>
						@if ($errors->has('current_employer'))
							<div class="error" style="color:red;">Please Enter Current Employer.</div>
						@endif
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="designation" name="designation" placeholder="Designation*" value="{{ old('designation') }}">
						<span id="designationerr" style="color:red;display:none;">Please Enter Designation.</span>
						@if ($errors->has('designation'))
							<div class="error" style="color:red;">Please Enter Designation.</div>
						@endif
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="joindt_curr_emp" name="joindt_curr_emp" placeholder="Joining Date of Current Job*" value="{{ old('joindt_curr_emp') }}">
						<span id="joindt_curr_emperr" style="color:red;display:none;">Please Enter Joining Date of Current Job.</span>
						@if ($errors->has('joindt_curr_emp'))
							<div class="error" style="color:red;">Please Enter Joining Date of Current Job.</div>
						@endif
					</div>				   
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="overflow-x:auto">
					<div class="alert alert-info">Employment Details</div>
					<table class="table table-responsive table-condensed table-hover table-bordered ehtbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Employer Name</th>
					<th>Designation</th>
					<th>Period</th>
					<th>Subjects</th>
					<th>Curriculum</th>
					<th>Responsibility</th>
					<th>Leaving Reason</th>
					<th></th>
					</tr>
					
					<?php 
					$eh_name = old('eh_name'); 
					$eh_designation = old('eh_designation');
					$period = old('period');
					$subjects = old('subjects');
					$curriculum = old('curriculum');
					$responsibility = old('responsibility');
					$leaving_reason = old('leaving_reason');
					if(!empty($eh_name)){
						for($i=0; $i<count($eh_name); $i++ ){
					?>					
					
					<tr>
					<td><input type="text" class="form-control" name="eh_name[]" id="eh_name" placeholder="Employer Name*" value="<?= $eh_name[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="eh_designation[]" id="eh_designation" placeholder="Designation*" value="<?= $eh_designation[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="period[]" id="period" placeholder="Period*" value="<?= $period[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="subjects[]" id="subjects" placeholder="Subjects*" value="<?= $subjects[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="curriculum[]" id="curriculum" placeholder="Curriculum*" value="<?= $curriculum[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="responsibility[]" id="responsibility" placeholder="Responsibility*" value="<?= $responsibility[$i]; ?>"></td>
					<td><input type="text" class="form-control" name="leaving_reason[]" id="leaving_reason" placeholder="Leaving Reason*" value="<?= $leaving_reason[$i]; ?>"></td>
					<td>
				    <?php if($i == 0){ ?>
					<span class="btn btn-primary btn-sm green" id="addeh" name="addeh"><i class="fa fa-plus"></i></span>
					<?php }else{ ?>
					<a href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm" data-id="<?= $i;?>"><i class="fa fa-close"></i></a>
					<?php } ?>
					</td>
					</tr>
					<?php }}else{?>
					<tr>
					<td><input type="text" class="form-control" name="eh_name[]" id="eh_name" placeholder="Employer Name*"></td>
					<td><input type="text" class="form-control" name="eh_designation[]" id="eh_designation" placeholder="Designation*"></td>
					<td><input type="text" class="form-control" name="period[]" id="period" placeholder="Period*"></td>
					<td><input type="text" class="form-control" name="subjects[]" id="subjects" placeholder="Subjects*"></td>
					<td><input type="text" class="form-control" name="curriculum[]" id="curriculum" placeholder="Curriculum*"></td>
					<td><input type="text" class="form-control" name="responsibility[]" id="responsibility" placeholder="Responsibility*"></td>
					<td><input type="text" class="form-control" name="leaving_reason[]" id="leaving_reason" placeholder="Leaving Reason*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addeh" name="addeh"><i class="fa fa-plus"></i></span></td>
					</tr>
					<?php } ?>
					
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				   <div class="alert alert-info">Skills & Interests</div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="it_skills" name="it_skills" placeholder="Skills" value="{{ old('it_skills') }}">
						<span id="it_skillserr" style="color:red;display:none;"> Please Enter Skills In You're Good.</span>
						@if ($errors->has('it_skills'))
							<div class="error" style="color:red;">Please Enter Skills In You're Good.</div>
						@endif
				   </div>	
				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="hobbies" name="hobbies" placeholder="Hobbies" value="{{ old('hobbies') }}">
						<span id="hobbieserr" style="color:red;display:none;"> Please Enter Hobbies.</span>
						@if ($errors->has('hobbies'))
							<div class="error" style="color:red;">Please Enter Hobbies.</div> 
						@endif
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="current_sal" name="current_sal" placeholder="Current Salary*  eg. 2 Lac per annum OR 20000/month" value="{{ old('current_sal') }}">
						<span id="current_salerr" style="color:red;display:none;"> Please Enter Current Salary.</span>
						@if ($errors->has('current_sal'))
							<div class="error" style="color:red;">Please Enter Current Salary eg. 2 Lac per annum OR 20000/month.</div>
						@endif
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="expected_sal" name="expected_sal" placeholder="Expected Salary*  eg. 2 Lac per annum OR 20000/month" value="{{ old('expected_sal') }}">
						<span id="expected_salerr" style="color:red;display:none;"> Please Enter Expected Salary.</span>
						@if ($errors->has('expected_sal'))
							<div class="error" style="color:red;">Please Enter Expected Salary eg. 2 Lac per annum OR 20000/month.</div>
						@endif
					</div>	

				   <div class="form-group col-md-12 col-sm-6 col-xs-12"> 
						<textarea type="text" class="form-control" id="language_speak" name="language_speak" placeholder="Language Skills*">{{ old('language_speak') }}</textarea>
						<span id="language_speakerr" style="color:red;display:none;"> Please Enter Language Known?</span>
						@if ($errors->has('language_speak'))
							<div class="error" style="color:red;">Please Enter Language Known?</div>
						@endif
				   </div>	
				   					
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="overflow-x:auto">
					<div class="alert alert-info">Confidential Referees Contact Details</div>
					<table class="table table-responsive table-condensed table-hover table-bordered ertbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Reference Name</th>
					<th>Designation</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Address</th>
					<th></th>
					</tr>
					<tr>
					<td><input type="text" class="form-control" name="ername[]" id="ername" placeholder="Reference Name*"></td>
					<td><input type="text" class="form-control" name="erdesignation[]" id="erdesignation" placeholder="Designation*"></td>
					<td><input type="text" class="form-control" name="erphone[]" id="erphone" placeholder="Phone No*"></td>
					<td><input type="text" class="form-control" name="eremail[]" id="eremail" placeholder="Email*"></td>
					<td><input type="text" class="form-control" name="eraddress[]" id="eraddress" placeholder="Address*"></td>
					<td><span class="btn btn-primary btn-sm green" id="adder" name="adder"><i class="fa fa-plus"></i></span></td>
					</tr>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				
				   <div class="form-group col-md-6 col-sm-6 col-xs-12" style="padding:0px;"> 
						<textarea type="text" class="form-control" id="edu_statement" name="edu_statement" placeholder="Personal Statement of Education">{{ old('edu_statement') }}</textarea>
						<span id="edu_statementerr" style="color:red;display:none;"> Please Enter Personal Statement of Education.</span>
						@if ($errors->has('edu_statement'))
							<div class="error" style="color:red;">Please Enter Personal Statement of Education.</div>
						@endif
				   </div>	
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"  style="padding:0px;"> 
						<textarea type="text" class="form-control" id="other_info" name="other_info" placeholder="Any other information, if required">{{ old('other_info') }}</textarea>
						@if ($errors->has('other_info'))
							<div class="error" style="color:red;">Please Enter Personal Any Other Information.</div>
						@endif
					</div>

					
				   
				</div>
				
                <!--<div class="form-group col-md-6 col-sm-6 col-xs-7">
                    <div class="g-recaptcha" data-sitekey="6LdRDDwUAAAAAK_LeKgqYCiGJ-hPFNPJ6Ircafmo"></div>
                    @if ($errors->has('g-recaptcha-response'))
							<div class="error" style="color:red;">Please Enter Captcha.</div>
				    @endif
                </div>-->
                
                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
						<input type="hidden" id="rannumber" name="rannumber" class="captcha">
						<div class="captcha col-md-8 col-sm-10 col-xs-10" style="border: 2px solid green;line-height:1.4; height: 45px; text-align: center; vertical-align: middle; font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">
						</div>
						<div class="col-md-1">
						  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;" >
						  <i class="fa fa-refresh"></i>
						  </a>
						  <br><br>
						</div>
					  
					</div>	
					
					 <div class="form-group col-md-3 col-sm-6 col-xs-12"  style="padding:0px;"> 
					 <div class="col-md-12">
						<input class="form-control" id="job_captcha_code2" name="job_captcha_code2" class="mb-22"  placeholder="Enter Captcha*" type="text">
						<span id="job_captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
						@if ($errors->has('job_captcha_code2'))
							<div class="error" style="color:red;">Captcha field is required.</div> 
						@endif
					 </div>
				   </div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
			
				<button  type="submit" class="button" id="job-submit1">Submit</button>
				<button  type="reset" class="button">Clear</button>
				</div>
				
				</form> 
			</section>
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    <hr>
			    <p><strong>Related Links</strong></p>
			    <a class="button" href="{!! \Config::get('app.url_base') !!}/overview-profile-training-growth/" style="margin:5px;">Overview & Training , Development & Growth</a>
		    	<a class="button" href="{!! \Config::get('app.url_base') !!}/recognition-promotion/" style="margin:5px;">Recognition &amp; Promotion</a>
                <a class="button" href="{!! \Config::get('app.url_base') !!}/opportunities/" style="margin:5px;">Opportunities</a>
                <a class="button" href="{!! \Config::get('app.url_base') !!}/in-service-training/" style="margin:5px;">In Service Training</a>
                <a class="button" href="{!! \Config::get('app.url_base') !!}/teacher-profile/" style="margin:5px;">Teacher Profile</a>
            </div>
                                
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			@include('includes.index-sidebar')
		</div>	
	</main>
	


<script type="text/javascript">


$(document).ready(function(){
    
    $("#photo").change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            $('#photoerr1').show();
            $("#photo").val('');
        }else{
            $('#photoerr1').hide();
        }
    });

	$('#job-submit1').click(function(){
	var position = $('#position').val();
	var name = $('#name').val();
	var gender = $('#gender').val();
	var nationality = $('#nationality').val();
	var dob = $('#dob').val();
	var photo = $('#photo').val();
	var marital_status = $('#marital_status').val();
	var children = $('#childrenXXX').val();
	var cadd = $('#cadd').val();
	var pincode = $('#pincode').val();
	var landline = $('#landline').val();
	var mob_no = $('#mob_no').val();
	var email_id = $('#email_id').val();
	var padd = $('#padd').val();
	var newjoin_dt = $('#newjoin_dt').val();
	var passport_no = $('#passport_no').val();
	var issue_date = $('#issue_date').val();
	var issue_place = $('#issue_place').val();
	var expiry_date = $('#expiry_date').val();
	var internation_school_exp = $('#internation_school_exp').val();
	var experience_in = $('#experience_in').val();
	var experience_year = $('#experience_year').val();
	var current_employer = $('#current_employer').val();
	var designation = $('#designation').val();
	var joindt_curr_emp = $('#joindt_curr_emp').val();
	var it_skills = $('#it_skills').val();
	var hobbies = $('#hobbies').val();
	var current_sal = $('#current_sal').val();
	var expected_sal = $('#expected_sal').val();
	var language_speak = $('#language_speak').val();
	var edu_statement = $('#edu_statementXXX').val();
	var job_captcha_code2 = $('#job_captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
	
	    if(position==""){
			$('#positionerr').css('display','block'); 
			$('#position').focus(); 
			return false;
		}else{
			$('#positionerr').css('display','none'); 
		}
		if(name==""){
			$('#nameerr').css('display','block'); 
			$('#name').focus(); 
			return false;
		}else{
			$('#nameerr').css('display','none'); 
		}
		if(gender==""){
			$('#gendererr').css('display','block'); 
			$('#gender').focus(); 
			return false;
		}else{
			$('#gendererr').css('display','none'); 
		}
		if(nationality==""){
			$('#nationalityerr').css('display','block'); 
			$('#nationality').focus(); 
			return false;
		}else{
			$('#nationalityerr').css('display','none'); 
		}
		if(dob==""){
			$('#doberr').css('display','block'); 
			$('#dob').focus(); 
			return false;
		}else{
			$('#doberr').css('display','none'); 
		}
		if(photo==""){
			$('#photoerr').css('display','block'); 
			$('#photo').focus(); 
			$('html,body').animate({
            scrollTop: $(".contact-form").offset().top},
            'slow');
			return false;
		}else{
			$('#photoerr').css('display','none'); 
		}
		if(marital_status==""){
			$('#marital_statuserr').css('display','block'); 
			$('#marital_status').focus(); 
			return false;
		}else{
			$('#marital_statuserr').css('display','none'); 
		}
		if(children==""){
			$('#childrenerr').css('display','block'); 
			$('#children').focus(); 
			return false;
		}else{
			$('#childrenerr').css('display','none'); 
		}
		if(cadd==""){
			$('#cadderr').css('display','block'); 
			$('#cadd').focus(); 
			return false;
		}else{
			$('#cadderr').css('display','none'); 
		}
	/*	if(pincode=="" || pincode.length<6){
			$('#pincodeerr').css('display','block'); 
			$('#pincode').focus(); 
			return false;
		}else{
			$('#pincodeerr').css('display','none'); 
		} */
		if(landline=="" || landline.length<10 || landline.length>15){
			$('#landlineerr').css('display','block'); 
			$('#landline').focus(); 
			return false;
		}else{
			$('#landlineerr').css('display','none'); 
		}
	/*	if(mob_no=="" || mob_no.length<10 || mob_no.length>15){
			$('#mob_noerr').css('display','block'); 
			$('#mob_no').focus(); 
			return false;
		}else{
			$('#mob_noerr').css('display','none'); 
		} */
		
		if(email_id=="" || !email_regex.test(email_id)){
			$('#email_iderr').css('display','block'); 
			$('#email_id').focus(); 
			return false;
		}else{
			$('#email_iderr').css('display','none'); 
		}
		
		if(padd==""){
			$('#padderr').css('display','block'); 
			$('#padd').focus(); 
			return false;
		}else{
			$('#padderr').css('display','none'); 
		}
		if(newjoin_dt==""){
			$('#newjoin_dterr').css('display','block'); 
			$('#newjoin_dt').focus(); 
			return false;
		}else{
			$('#newjoin_dterr').css('display','none'); 
		}
		if(passport_no==""){
			$('#passport_noerr').css('display','block'); 
			$('#passport_no').focus(); 
			return false;
		}else{
			$('#passport_noerr').css('display','none'); 
		}
		if(issue_date==""){
			$('#issue_dateerr').css('display','block'); 
			$('#issue_date').focus(); 
			return false;
		}else{
			$('#issue_dateerr').css('display','none'); 
		}
		if(issue_place==""){
			$('#issue_placeerr').css('display','block'); 
			$('#issue_place').focus(); 
			return false;
		}else{
			$('#issue_placeerr').css('display','none'); 
		}
		if(expiry_date==""){
			$('#expiry_dateerr').css('display','block'); 
			$('#expiry_date').focus(); 
			return false;
		}else{
			$('#expiry_dateerr').css('display','none'); 
		}
		if(internation_school_exp==""){
			$('#internation_school_experr').css('display','block'); 
			$('#internation_school_exp').focus(); 
			return false;
		}else{
			$('#internation_school_experr').css('display','none'); 
		}
		if(experience_in==""){
			$('#experience_inerr').css('display','block'); 
			$('#experience_in').focus(); 
			return false;
		}else{
			$('#experience_inerr').css('display','none'); 
		}
		if(experience_year==""){
			$('#experience_yearerr').css('display','block'); 
			$('#experience_year').focus(); 
			return false;
		}else{
			$('#experience_yearerr').css('display','none'); 
		}
		if(current_employer==""){
			$('#current_employererr').css('display','block'); 
			$('#current_employer').focus(); 
			return false;
		}else{
			$('#current_employererr').css('display','none'); 
		}
		if(designation==""){
			$('#designationerr').css('display','block'); 
			$('#designation').focus(); 
			return false;
		}else{
			$('#designationerr').css('display','none'); 
		}
		if(joindt_curr_emp==""){
			$('#joindt_curr_emperr').css('display','block'); 
			$('#joindt_curr_emp').focus(); 
			return false;
		}else{
			$('#joindt_curr_emperr').css('display','none'); 
		}
	/*	if(it_skills==""){
			$('#it_skillserr').css('display','block'); 
			$('#it_skills').focus(); 
			return false;
		}else{
			$('#it_skillserr').css('display','none'); 
		}
		if(hobbies==""){
			$('#hobbieserr').css('display','block'); 
			$('#hobbies').focus(); 
			return false;
		}else{
			$('#hobbieserr').css('display','none'); 
		}
		*/
		if(current_sal==""){
			$('#current_salerr').css('display','block'); 
			$('#current_sal').focus(); 
			return false;
		}else{
			$('#current_salerr').css('display','none'); 
		}
		if(expected_sal==""){
			$('#expected_salerr').css('display','block'); 
			$('#expected_sal').focus(); 
			return false;
		}else{
			$('#expected_salerr').css('display','none'); 
		} 
		if(language_speak==""){
			$('#language_speakerr').css('display','block'); 
			$('#language_speak').focus(); 
			return false;
		}else{
			$('#language_speakerr').css('display','none'); 
		}
		if(edu_statement==""){
			$('#edu_statementerr').css('display','block'); 
			$('#edu_statement').focus(); 
			return false;
		}else{
			$('#edu_statementerr').css('display','none'); 
		}
		
		if(job_captcha_code2==""){
			$('#job_captcha_code2err').css('display','block'); 
			$('#job_captcha_code2').focus(); 
			$('html,body').animate({
            scrollTop: $(".captcha").offset().top},
            'slow');
			return false;
		}else{
			$('#job_captcha_code2err').css('display','none'); 
		}
		if(job_captcha_code2 != rannumber){
			$('#job_captcha_code2err').css('display','block'); 
			$('#job_captcha_code2').focus(); 
			$('html,body').animate({
            scrollTop: $(".captcha").offset().top},
            'slow');
			return false;
		}else{
			$('#job_captcha_code2err').css('display','none'); 
		}

	});	
});	
</script> 

<script type="text/javascript">
//$(document).ready(function() {
	
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 0; //initlal text box count
    $("#add_attributes").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<tr class="'+x+'"><td><input type="text" class="form-control" name="university[]" id="university'+x+'" placeholder="University*"></td><td><input type="text" class="form-control" name="board[]" id="board'+x+'" placeholder="Board*"></td><td><input type="text" class="form-control" name="start_date[]" id="start_date'+x+'" placeholder="Start Date*"></td><td><input type="text" class="form-control" name="completion_date[]" id="completion_date'+x+'" placeholder="Complition Date*"></td><td><input type="text" class="form-control" name="grade[]" id="grade'+x+'" placeholder="Grade*"></td><td><input type="text" class="form-control" name="percentage[]" id="percentage'+x+'" placeholder="Percentage*"></td><td><a data-id="'+x+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	var x1 = 0;
    $("#addpc").click(function(e){ //on add input button click
        e.preventDefault();
        if(x1 < max_fields){ //max input box allowed
            x1++; //text box increment
            $('.pctbl').append('<tr class="'+x1+'"><td><input type="text" class="form-control" name="pattended[]" id="pattended'+x1+'" placeholder="Attended*"></td><td><input type="text" class="form-control" name="pcourse[]" id="pcourse'+x1+'" placeholder="Course*"></td><td><input type="text" class="form-control" name="pstart_date[]" id="pstart_date'+x1+'" placeholder="Start Date*"></td><td><input type="text" class="form-control" name="pcompletion_date[]" id="pcompletion_date'+x1+'" placeholder="Complition Date*"></td><td><input type="text" class="form-control" name="plocation[]" id="plocation'+x1+'" placeholder="Location*"></td><td><a data-id="'+x1+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.pctbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
    
    
	var x11 = 0;
    $("#addpc1").click(function(e){ //on add input button click
        e.preventDefault();
        if(x11 < max_fields){ //max input box allowed
            x11++; //text box increment
            $('.pctbl1').append('<tr class="'+x11+'"><td><input type="text" class="form-control" name="pattended1[]" id="pattended1'+x11+'" placeholder="Conducted*"></td><td><input type="text" class="form-control" name="pcourse1[]" id="pcourse1'+x11+'" placeholder="Course*"></td><td><input type="text" class="form-control" name="pstart_date1[]" id="pstart_date1'+x11+'" placeholder="Start Date*"></td><td><input type="text" class="form-control" name="pcompletion_date1[]" id="pcompletion_date1'+x11+'" placeholder="Complition Date*"></td><td><input type="text" class="form-control" name="plocation1[]" id="plocation1'+x11+'" placeholder="Location*"></td><td><a data-id="'+x11+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
	
    $('.pctbl1').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
    
	
	var x2 = 0;
    $("#addpr").click(function(e){ //on add input button click
        e.preventDefault();
        if(x2 < max_fields){ //max input box allowed
            x2++; //text box increment
            $('.prtbl').append('<tr class="'+x2+'"><td><input type="text" class="form-control" name="prtitle[]" id="prtitle'+x2+'" placeholder="Title*"></td><td><input type="text" class="form-control" name="publish_in[]" id="publish_in'+x2+'" placeholder="Published In*"></td><td><input type="text" class="form-control" name="publish_date[]" id="publish_date'+x2+'" placeholder="Publish Date*"></td><td><input type="text" class="form-control" name="presented_at[]" id="presented_at'+x2+'" placeholder="Presented At*"></td><td><input type="text" class="form-control" name="presentation_date[]" id="presentation_date'+x2+'" placeholder="Presentation Date*"></td><td><a data-id="'+x2+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.prtbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	
	var x3 = 0;
    $("#addeh").click(function(e){ //on add input button click
        e.preventDefault();
        if(x3 < max_fields){ //max input box allowed
            x3++; //text box increment
            $('.ehtbl').append('<tr class="'+x3+'"><td><input type="text" class="form-control" name="eh_name[]" id="eh_name'+x3+'" placeholder="Employer Name*"></td><td><input type="text" class="form-control" name="eh_designation[]" id="eh_designation'+x3+'" placeholder="Designation*"></td><td><input type="text" class="form-control" name="period[]" id="period'+x3+'" placeholder="Period*"></td><td><input type="text" class="form-control" name="subjects[]" id="subjects'+x3+'" placeholder="Subjects*"></td><td><input type="text" class="form-control" name="curriculum[]" id="curriculum'+x3+'" placeholder="Curriculum*"></td><td><input type="text" class="form-control" name="responsibility[]" id="responsibility'+x3+'" placeholder="Responsibility*"></td><td><input type="text" class="form-control" name="leaving_reason[]" id="leaving_reason'+x3+'" placeholder="Leaving Reason*"></td><td><a data-id="'+x3+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.ehtbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	
	var x4 = 0;
    $("#adder").click(function(e){ //on add input button click
        e.preventDefault();
        if(x4 < max_fields){ //max input box allowed
            x4++; //text box increment
            $('.ertbl').append('<tr class="'+x4+'"><td><input type="text" class="form-control" name="ername[]" id="ername'+x4+'" placeholder="Reference Name*"></td><td><input type="text" class="form-control" name="erdesignation[]" id="erdesignation'+x4+'" placeholder="Designation*"></td><td><input type="text" class="form-control" name="erphone[]" id="erphone'+x4+'" placeholder="Phone No*"></td><td><input type="text" class="form-control" name="eremail[]" id="eremail'+x4+'" placeholder="Email*"></td><td><input type="text" class="form-control" name="eraddress[]" id="eraddress'+x4+'" placeholder="Address*"></td><td><a data-id="'+x4+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.ertbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	
//});
</script>	
	
<script type="text/javascript" src="js/custom.js"></script>	
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')