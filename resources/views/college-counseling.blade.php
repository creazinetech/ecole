<?php
	$Pages = DB::table('pages')->where('page_id','=','13')->get();
	$Gallery = DB::table('gallery')->where('page_id','=','13')->where('img_status','=','Active')->paginate(20);
	$Countries = DB::table('universities')->where('country_status','=','Active')->orderBy('country_name','asc')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/academics">Academics</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?>
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tg-content-text">
										    <?php echo $Page->brief_desc; ?>
										</div>
										
										<?php 
										  $i=1;
										  foreach($Countries as $Country){?>
										 <!-- <li class="<?php if($i == 1){echo 'active';} ?>">
										  <a href="#<?php echo $i; ?>" data-toggle="tab"><?php echo ucwords($Country->country_name); ?></a>
										  </li>-->
										<?php $i++;}?>
										  
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 bhoechie-tab-menu">
											  <div class="list-group">
												<?php 
												  $i=1;
												  foreach($Countries as $Country){?>
												<a href="#<?php echo $Country->country_id; ?>" class="list-group-item text-center <?php if($i == 1){echo 'active';} ?>">
												  <p class="ecole-text <?php echo $Country->country_id; ?>"><?php echo ucwords($Country->country_name); ?></p>
												</a>
												<?php $i++;}?>
											  </div>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 bhoechie-tab  tg-content-text"  style="background-color:#DCF2F7;">
												<?php 
												$j=1;
												foreach($Countries as $Country){?>
												<div class="ecole-text bhoechie-tab-content <?php if($j == 1){echo 'active';} ?>" style="background-color:#DCF2F7;">
													<h1 class="<?php echo $Country->country_id; ?>" style="color:#000000"></h1>
													<?php echo $Country->country_content; ?>
												</div>
												<?php $j++;}?>
												<br><br>
											</div>
										</div>
									</div>
									</div>
								<?php if(count($Gallery)>0){?>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder">
											<h2>Gallery</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php foreach($Gallery as $GalImg){?>
											<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3" >
												<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox-group="collegecounselling" data-darkbox-description="<?php echo ucwords($GalImg->img_title); ?>" class="img-responsive thumbnail">
											</div>
										<?php } ?>
										<br>
										</div>
										<div class="ecole-text text-center"><?php echo $Gallery->render(); ?></div>
									</div>
								</div>
								<?php } ?>
								
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
                					    <hr>
                					    <p><strong>Related Links</strong></p>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/playschool">Play School</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/early-yrndprimary-yr">Early Years & Primary Years</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/middle-years-11-16-yrs">Middle Years</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/diploma-16-19-yrs">Diploma</a>
            					</div>
            					
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>

<script>
$(document).ready(function() {
	$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
		e.preventDefault();
		$(this).siblings('a.active').removeClass("active");
		$(this).addClass("active");
		var index = $(this).index();
		$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
		$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
	});
});
</script>
@include('includes.index-footer')