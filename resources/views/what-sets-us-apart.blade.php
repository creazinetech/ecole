<?php 
	$Pages = DB::table('pages')->where('page_id','=','52')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) ){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	 
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	<style>
	    .whatscontnet h1 {
            font-family: 'Rokkitt',serif;
            font-size: 54px;
            text-transform:none;
        }
        .whatscontnet h2 {
            font-family: 'Rokkitt',serif;
            font-size: 36px;
            font-weight: lighter;
            color: #474747;
            text-transform:none;
        }
        .whatscontnet h3 {
            font-family: 'Rokkitt',serif;
            font-weight: lighter;
            font-size: 28px;
            color: #ffffff;
            text-transform:none;
        }
        .whatscontnet div{
            padding:0px;
        }
        .whatscontnet img{
            padding:2px;
        }
        .whatscontnet .btndiv{
            font-family: 'Rokkitt',serif;
        }
        .btndiv:hover{
            background-color:#008CE0;
        }
	</style>
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/about-us">About Us</a>
                / <a  class="breadcrum-text"><?php echo ucwords($Page->page_heading); ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone tg-content-text whatscontnet descdiv"><br>
								
											<?php echo $Page->brief_desc; ?>
											
									</div>
								</div>
							</section>
						</div>
						
					</div>	
	</main>
@include('includes.index-footer')