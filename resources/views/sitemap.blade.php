<?php
    $Pages = DB::table('pages')->where('page_id','=','48')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.url') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.url') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
		    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php echo ucwords($Page->page_heading); ?></a>
                <hr>
	        </div>
		    
		    
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<ul class="sitemap-ul">
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/index">Home</a></li>
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/admissions">Admissions</a></li>
												<ul>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/admission-procedure"> Admission Procedure</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/schedule-a-visit"> Schedule a Visit</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/faq"> FAQs</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/school-calendar"> School Calendar</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/fee-structure"> Fee Structure</a></li>
												</ul>
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/academics">Academics</a></li>
												<ul>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/toddler"> Toddler</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/playschool"> Play School</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/early-yrndprimary-yr"> Early Years & Primary Years</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/middle-years-11-16-yrs"> Middle Years</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/diploma-16-19-yrs"> Diploma</a></li>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/college-counseling"> College Counseling</a></li>
												</ul>
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/campus-life">Campus Life</a></li>
												<ul>
    												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/library"> Libraries</a></li>
            										<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/computer-centres"> Computer Centers</a></li>
            										<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/science-technology-labs"> Science & Technology Labs</a></li>
            										<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/sports"> Sports & Recreation</a></li>
            										<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/music-dance-theatre"> Music, Dance & Theatre</a></li>
            										<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/visual-arts"> Visual Arts</a></li>
            										<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/emun"> Ecole Model United Nations (EMUN)</a></li>
            										<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/expeditions-camps"> CAS Programme</a></li>
												</ul>												
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/events">Events</a></li>
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/about-us">About Us</a></li>
												<ul>
													<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/message-from-the-principal"> Message from the Principal</a></li>
													<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/overview"> Overview</a></li>
												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/what-sets-us-apart"> What sets us apart</a></li>
												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/our-mission"> Mission, Vision & Aims</a></li>
												</ul>
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/alumni"> Alumni</a></li>
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/faculty-and-staff"> Faculty & Staff</a></li>
												<ul>
												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/recognition-promotion"> Recognition & Promotion</a></li>
												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/opportunities"> Opportunities</a></li>
												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/in-service-training"> In Service Training</a></li>
												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/overview-profile-training-growth"> Overview & Training, Development & Growth</a></li>
												<li type="circle"><a href="{!! \Config::get('app.url_base') !!}/training-development-growth"> Career Opening</a></li>
												</ul>
											<li type="disc">
											    <a href="{!! \Config::get('app.url_base') !!}/news"> News</a>
											    </li>
												<ul>
												<?php 
												$News = DB::table('news')->where('created_at','>=',date('Y-m-01').' 00:00:00')->where('created_at','<=',date('Y-m-t').' 23:59:59')->where('status','=','Active')->get();
												foreach($News as $New){
												?>
												<li type="circle">
												    <a href="{{URL::to('view-news',array($New->id))}}" class="">
												    <?php echo ' '.ucwords($New->title); ?>
												    </a></li>
												<?php  } ?>
												</ul>
											<li type="disc"><a href="{!! \Config::get('app.url_base') !!}/contact-us"> Contact Us</a></li>
										</ul>
										
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')