<?php
	$Pages = DB::table('pages')->where('page_id','=','2')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>">
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>">
	<?php } ?>

	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>

    </div>

	<!--************************************
			Home Slider End
	*************************************-->

	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">

		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/admissions">Admissions</a>
                / <a  class="breadcrum-text"><?php echo ucwords($Page->page_heading); ?></a>
                <hr>
	        </div>

			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text descdiv">
											<?php echo $Page->brief_desc; ?>
										</div>
									</div>
								</div>
								<br><br>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
								     <!--<p>What's Next</p><br>-->
							    	<a class="button big-button" href="{!! \Config::get('app.url_base') !!}/apply-for-admission" style="font-size:19px;padding:21px;">Apply For Admission <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							    	<a class="button big-button" href="{!! \Config::get('app.url_base') !!}/contact-us" style="font-size:19px;padding:21px;">Enquiry Now <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
								    <br>
								    <p>Related Links</p>
							    	<a class="button" href="{!! \Config::get('app.url_base') !!}/admission-procedure">Admission Procedure</a>
    						    	<a class="button" href="{!! \Config::get('app.url_base') !!}/school-calendar">School Calendar</a>
    						    	<a class="button" href="{!! \Config::get('app.url_base') !!}/faq">FAQ</a>
    						    	<a class="button" href="{!! \Config::get('app.url_base') !!}/fee-structure">Fee Structure</a>
								</div>
							</section>
						</div>

					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>
	</main>
@include('includes.index-footer')