<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','44')->get();
	foreach($Pages as $Page){ }
	$ContactUs = DB::table('contactus')->get();
	foreach($ContactUs as $Contact){ }


?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>">
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>">
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>

	<!--************************************
			Home Slider End
	*************************************-->

	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">

		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo ucwords($Page->page_heading);} ?>
                    </a>
                <hr>
	        </div>

			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content-text">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<div class="row">

											<div class="clearfix"></div>
											<div class="flash-message tg-content">
											@foreach (['danger', 'warning', 'success', 'info'] as $msg)
											  @if(Session::has('alert-' . $msg))

											  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
											  @endif
											@endforeach
											</div>

											<?php
											if(isset($CaptchaVerificaion)){

											    echo '<div class="alert alert-danger">'.$CaptchaVerificaion.'</div>';
											}
											?>

											<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id="contactpageform"   enctype='multipart/form-data'  action="{{ url('addcontactusenquiry') }}" >
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
												<div class="alert alert-info"><strong>Student's Detail</strong></div>
												<fieldset>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Student Name*">
															<span id="qnameerr" style="color:red;display:none;"> Please Enter name.</span>
															@if ($errors->has('name'))
                                        						<div class="error" style="color:red;">{{ $errors->first('name') }}</div>
                                        					@endif
														</div>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="datepicker form-control" id="dob" name="dob" value="{{ old('dob') }}" placeholder="Date Of Birth* DD-MM-YYYY">
															<span id="qdateerr" style="color:red;display:none;"> Please Select Date Of Birth.</span>
															@if ($errors->has('dob'))
                                        						<div class="error" style="color:red;"> Please Enter Date Of Birth.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="Residential Address*">
															<span id="qraddresserr" style="color:red;display:none;"> Please Enter Residential Address.</span>
															@if ($errors->has('address'))
                                        						<div class="error" style="color:red;"> Please Enter Residential Address.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}" placeholder="City:*">
															<span id="cityerr" style="color:red;display:none;"> Please Enter City.</span>
															@if ($errors->has('city'))
                                        						<div class="error" style="color:red;">Please Enter City.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<select class="form-control" id="state" name="state">
                                                                <option value=''>Select State</option>
                                                                <option value='Andhra Pradesh' @if (Input::old('state') == 'Andhra Pradesh') selected="selected" @endif>Andhra Pradesh</option>
                                                                <option value='Arunachal Pradesh' @if (Input::old('state') == 'Arunachal Pradesh') selected="selected" @endif >Arunachal Pradesh</option>
                                                                <option value='Assam' @if (Input::old('state') == 'Assam') selected="selected" @endif >Assam </option>
                                                                <option value='Bihar' @if (Input::old('state') == 'Bihar') selected="selected" @endif >Bihar</option>
                                                                <option value='Chhattisgarh' @if (Input::old('state') == 'Chhattisgarh') selected="selected" @endif >Chhattisgarh    </option>
                                                                <option value='Goa' @if (Input::old('state') == 'Goa') selected="selected" @endif >Goa</option>
                                                                <option value='Gujarat' @if (Input::old('state') == 'Gujarat') selected="selected" @endif >Gujarat</option>
                                                                <option value='Haryana' @if (Input::old('state') == 'Haryana') selected="selected" @endif >Haryana </option>
                                                                <option value='Himachal Pradesh' @if (Input::old('state') == 'Himachal Pradesh') selected="selected" @endif >Himachal Pradesh</option>
                                                                <option value='Jammu & Kashmir' @if (Input::old('state') == 'Jammu & Kashmir') selected="selected" @endif >Jammu & Kashmir</option>
                                                                <option value='Jharkhand' @if (Input::old('state') == 'Jharkhand') selected="selected" @endif >Jharkhand </option>
                                                                <option value='Karnataka' @if (Input::old('state') == 'Karnataka') selected="selected" @endif >Karnataka </option>
                                                                <option value='Kerla' @if (Input::old('state') == 'Kerla') selected="selected" @endif >Kerla </option>
                                                                <option value='Madhya Pradesh' @if (Input::old('state') == 'Madhya Pradesh') selected="selected" @endif >Madhya Pradesh</option>
                                                                <option value='Maharashtra' @if (Input::old('state') == 'Maharashtra') selected="selected" @endif >Maharashtra  </option>
                                                                <option value='Manipur' @if (Input::old('state') == 'Manipur') selected="selected" @endif >Manipur </option>
                                                                <option value='Meghalaya' @if (Input::old('state') == 'Meghalaya') selected="selected" @endif >Meghalaya</option>
                                                                <option value='Mizoram' @if (Input::old('state') == 'Mizoram') selected="selected" @endif > Mizoram</option>
                                                                <option value='Nagaland' @if (Input::old('state') == 'Nagaland') selected="selected" @endif >Nagaland</option>
                                                                <option value='Orissa' @if (Input::old('state') == 'Orissa') selected="selected" @endif >Orissa </option>
                                                                <option value='Punjab' @if (Input::old('state') == 'Punjab') selected="selected" @endif >Punjab </option>
                                                                <option value='Sikkim' @if (Input::old('state') == 'Sikkim') selected="selected" @endif >Sikkim </option>
                                                                <option value='Tamilnadu' @if (Input::old('state') == 'Tamilnadu') selected="selected" @endif >Tamilnadu</option>
                                                                <option value='Tripura' @if (Input::old('state') == 'Tripura') selected="selected" @endif >Tripura </option>
                                                                <option value='Uttar Pradesh' @if (Input::old('state') == 'Uttar Pradesh') selected="selected" @endif >Uttar Pradesh </option>
                                                                <option value='Uttaranchal' @if (Input::old('state') == 'Uttaranchal') selected="selected" @endif > Uttaranchal </option>
                                                                <option value='West Bengal' @if (Input::old('state') == 'West Bengal') selected="selected" @endif >West Bengal</option>
                                                                <option value='Outside India' @if (Input::old('state') == 'Outside India') selected="selected" @endif >Outside India</option>
														  </select>
														  <span id="stateerr" style="color:red;display:none;"> Please Select State.</span>
															@if ($errors->has('state'))
                                        						<div class="error" style="color:red;">Please Select State.</div>
                                        					@endif
														</div>


														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" name="country" id="country" value="{{ old('country') }}" placeholder="country:*">
															<span id="qcountryerr" style="color:red;display:none;"> Please Enter Country.</span>
															@if ($errors->has('country'))
                                        						<div class="error" style="color:red;">{{ $errors->first('country') }}</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" max-length="6" class="form-control" id="pincode" name="pincode" value="{{ old('pincode') }}" placeholder="Pincode:*" onkeypress="return isNumberKey(event)">
															<span id="pincodeerr" style="color:red;display:none;"> Please Enter 6 Digit Pincode.</span>
															@if ($errors->has('pincode'))
                                        						<div class="error" style="color:red;">{{ $errors->first('pincode') }}</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="nopskul" id="nopskul" value="{{ old('nopskul') }}" placeholder="Name of Present School:*">
															<span id="qnskulerr" style="color:red;display:none;"> Please Enter Name of Present School.</span>
															@if ($errors->has('nopskul'))
                                        						<div class="error" style="color:red;"> Please Enter Name of Present School.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" id="grade" name="grade" value="{{ old('grade') }}" placeholder="Present grade*">
															<span id="qgradeerr" style="color:red;display:none;"> Please Enter present grade.</span>
															@if ($errors->has('grade'))
                                        						<div class="error" style="color:red;">Please Enter present grade.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="gradetjoin" name="gradetjoin" value="{{ old('gradetjoin') }}" placeholder="Grade to join*">
															<span id="gradetjoinerr" style="color:red;display:none;">Please Enter Grade To Join</span>
															@if ($errors->has('gradetjoin'))
                                        						<div class="error" style="color:red;"> Grade to join</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<select class="form-control" id="year" name="year">
															<option value=""> Academic Year for which admission is sought</option>
																<?php
																$start_year=date('Y');
																$end_year= date('Y')+1;
																$start_year1=$start_year+1;
																$end_year1=$end_year+1;
																for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
																<option value='<?php echo $i.'-'.$j; ?>'><?php echo $i.'-'.$j;?></option>
																<?php } ?>
															</select>
															<span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
															@if ($errors->has('year'))
                                        						<div class="error" style="color:red;">Please Enter Academic Year.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" class="form-control" id="nationality" name="nationality" value="{{ old('nationality') }}" placeholder="Nationality*">
														  <span id="nationalityerr" style="color:red;display:none;">Please Enter Nationality.</span>
															@if ($errors->has('nationality'))
                                        						<div class="error" style="color:red;">Please Enter Nationality.</div>
                                        					@endif
														</div>

													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
													<div class="alert alert-info"><strong>Parent Detail</strong></div>

													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding:0px;">

														<div style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" name="fname"  name="fname" value="{{ old('fname') }}" placeholder="Father Name*">
															<span id="fnameerr" style="color:red;display:none;"> Please Enter Father Name.</span>
															@if ($errors->has('fname'))
                                        						<div class="error" style="color:red;">Please Enter Father Name.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="foccupation" name="foccupation" value="{{ old('foccupation') }}" placeholder="Father's Occupation*">
															<span id="foccupationerr" style="color:red;display:none;"> Enter Father's Occupation</span>
															@if ($errors->has('foccupation'))
                                        						<div class="error" style="color:red;">Enter Father's Occupation</div>
                                        					@endif
														</div>

														<div style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="fdesignation" name="fdesignation" value="{{ old('fdesignation') }}" placeholder="Father's Designation*">
															<span id="fdesignationerr" style="color:red;display:none;"> Enter Father's Designation.</span>
															@if ($errors->has('fdesignation'))
                                        						<div class="error" style="color:red;">Enter Father's Designation.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="forganization" name="forganization" value="{{ old('forganization') }}" placeholder="Father's Organization*">
															<span id="forganizationerr" style="color:red;display:none;">Enter Father's Organisation Name.</span>
															@if ($errors->has('forganization'))
                                        						<div class="error" style="color:red;">Enter Father's Organisation Name.</div>
                                        					@endif
														</div>


														<div  style="padding:0px;" class="form-group  col-md-12 col-sm-12">
															<input type="text" class="form-control" id="foffadd" name="foffadd" value="{{ old('foffadd') }}" placeholder="Father's Official Address:*">
															<span id="foffadderr" style="color:red;display:none;">Father's Official Address?</span>
															@if ($errors->has('foffadd'))
                                        						<div class="error" style="color:red;">Father's Official Address?</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="fofftel" name="fofftel" value="{{ old('fofftel') }}" placeholder="Father's Office Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="fofftelerr" style="color:red;display:none;"> 10 Digits Office Telelphone.</span>
															@if ($errors->has('fofftel'))
                                        						<div class="error" style="color:red;">10 Digits Office Telelphone.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="email" class="form-control" id="femail" name="femail" value="{{ old('femail') }}" placeholder="Father's Email*">
															<span id="femailerr" style="color:red;display:none;">Father's Email Id ?</span>
															@if ($errors->has('femail'))
                                        						<div class="error" style="color:red;">Father's Email Id ?</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="fmobile" name="fmobile" value="{{ old('fmobile') }}" placeholder="Father's Mobile No:*" onkeypress="return isNumberKey(event)">
														  <span id="fmobileerr" style="color:red;display:none;">10 Digit Mobile Number?</span>
															@if ($errors->has('fmobile'))
                                        						<div class="error" style="color:red;">10 Digit Mobile Number?</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="frestel" name="frestel" value="{{ old('frestel') }}" placeholder="Father's Residence Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="frestelerr" style="color:red;display:none;">10 Digit Residence Tel.?</span>
															@if ($errors->has('frestel'))
                                        						<div class="error" style="color:red;">10 Digit Residence Tel.?</div>
                                        					@endif
														</div>

													</div>

													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding:0px;">

														<div style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" name="mname"  name="mname" value="{{ old('mname') }}" placeholder="Mother Name*">
															<span id="mnameerr" style="color:red;display:none;"> Please Enter Mother Name.</span>
															@if ($errors->has('mname'))
                                        						<div class="error" style="color:red;">Please Enter Mother Name.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="moccupation" name="moccupation" value="{{ old('moccupation') }}" placeholder="Mother's Occupation*">
															<span id="moccupationerr" style="color:red;display:none;"> Enter Mother's Occupation</span>
															@if ($errors->has('moccupation'))
                                        						<div class="error" style="color:red;">Enter Mother's Occupation</div>
                                        					@endif
														</div>

														<div style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="mdesignation" name="mdesignation" value="{{ old('mdesignation') }}" placeholder="Mother's Designation*">
															<span id="mdesignationerr" style="color:red;display:none;"> Enter Mother's Designation.</span>
															@if ($errors->has('mdesignation'))
                                        						<div class="error" style="color:red;">Enter Mother's Designation.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="morganization" name="morganization" value="{{ old('morganization') }}" placeholder="Mother's Organization*">
															<span id="morganizationerr" style="color:red;display:none;">Enter Mother's Organisation Name.</span>
															@if ($errors->has('morganization'))
                                        						<div class="error" style="color:red;">Enter Mother's Organisation Name.</div>
                                        					@endif
														</div>


														<div  style="padding:0px;" class="form-group  col-md-12 col-sm-12">
															<input type="text" class="form-control" id="moffadd" name="moffadd" value="{{ old('moffadd') }}" placeholder="Mother's Official Address:*">
															<span id="moffadderr" style="color:red;display:none;">Mother's Official Address?</span>
															@if ($errors->has('moffadd'))
                                        						<div class="error" style="color:red;">Mother's Official Address?</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="mofftel" name="mofftel" value="{{ old('mofftel') }}" placeholder="Mother's Office Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="mofftelerr" style="color:red;display:none;"> 10 Digits Office Telelphone.</span>
															@if ($errors->has('mofftel'))
                                        						<div class="error" style="color:red;">10 Digits Office Telelphone.</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="email" class="form-control" id="memail" name="memail" value="{{ old('memail') }}" placeholder="Mother's Email*">
															<span id="memailerr" style="color:red;display:none;">Mother's Email Id ?</span>
															@if ($errors->has('memail'))
                                        						<div class="error" style="color:red;">Mother's Email Id ?</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="mmobile" name="mmobile" value="{{ old('mmobile') }}" placeholder="Mother's Mobile No:*" onkeypress="return isNumberKey(event)">
														  <span id="mmobileerr" style="color:red;display:none;">10 Digit Mobile Number?</span>
															@if ($errors->has('mmobile'))
                                        						<div class="error" style="color:red;">10 Digit Mobile Number?</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="mrestel" name="mrestel" value="{{ old('mrestel') }}" placeholder="Mother's Residence Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="mrestelerr" style="color:red;display:none;">10 Digit Residence Tel.?</span>
															@if ($errors->has('mrestel'))
                                        						<div class="error" style="color:red;">10 Digit Residence Tel.?</div>
                                        					@endif
														</div>

													</div>

													</div>

													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
													<div class="alert alert-info"><strong>Other Information</strong></div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" class="form-control" id="whattoknow" name="whattoknow" value="{{ old('whattoknow') }}" placeholder="What do you wish to know*">
														  <span id="whattoknowerr" style="color:red;display:none;">What do you wish to know.</span>
															@if ($errors->has('whattoknow'))
                                        						<div class="error" style="color:red;">What do you wish to know?</div>
                                        					@endif
														</div>

														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<!--<textarea class="form-control"  id="aboutshcool" name="aboutshcool" placeholder="Tell us how you heard about our School:*">{{ old('aboutshcool') }}</textarea>-->
															<input type="text" class="form-control" id="aboutshcool" name="aboutshcool" value="{{ old('aboutshcool') }}"  placeholder="Tell us how you heard about our School:*">
															<span id="aboutshcoolerr" style="color:red;display:none;">Tell us how you heard about our School?</span>
															@if ($errors->has('aboutshcool'))
                                        						<div class="error" style="color:red;">Tell us how you heard about our School?</div>
                                        					@endif
														</div>



													</div>

													<div class="col-md-12 col-sm-12 col-xs-12">

                                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                            <!--<div id="ReCaptacha1"></div>-->
                                                            <div class="g-recaptcha" data-sitekey="6Ldc_18UAAAAAKRHJrfSg1S8X3VWsZbgj0Bp7S0Y"></div>
                                                            @if ($errors->has('g-recaptcha-response'))
                                        							<div class="error" style="color:red;">Please Enter Captcha.</div>
                                        				    @endif
                                                        </div>

														<div class="form-group col-md-6 col-sm-6 col-xs-12">
														{{ csrf_field() }}
															 <button type="button" class="button pull-left" name="contactpagebtn" id="contactpagebtn"  onClick="SubmitConForm()"> Submit </button>
														</div>

													</div>

													<style>
													.mapdiv iframe{
														height:410px;
													}
													</style>
													<br> <br>
													<div class="col-xs-12 col-sm-12 col-md-12 tg-content mapdiv" style="height:410px;">
													<hr>
													<!--<div id="tg-officelocation" class="tg-officelocation"></div>-->
													<?php echo $Contact->map; ?>
													</div>
												</fieldset>
											</form>
										</div>


									</div>
								</div>
							</section>
						</div>

					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>
	</main>

<script type="text/javascript">
function SubmitConForm()
    {
         $('#contactpageform #contactpagebtn').attr('disabled','true');
         $('#contactpageform').submit();
    }

$(document).ready(function(){
	$('#job-submitX').click(function(){
	var name = $('#name').val();
	var dob = $('#dob').val();
	var address = $('#address').val();
	var nopskul = $('#nopskul').val();
	var grade = $('#grade').val();
	var gradetjoin = $('#gradetjoin').val();
	var year = $('#year').val();
	var nationality = $('#nationality').val();
	var whattoknow = $('#whattoknow').val();
	var aboutshcool = $('#aboutshcool').val();
	var nparents = $('#nparents').val();
	var designation = $('#designation').val();
	var organization = $('#organization').val();
	var occupation = $('#occupation').val();
	var offadd = $('#offadd').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	var restel = $('#restel').val();
	var offtel = $('#offtel').val();
	var mobile = $('#mobile').val();
	var email = $('#email').val();
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

	    if(name==""){
			$('#qnameerr').css('display','block');
			$('#name').focus();
			return false;
		}else{
			$('#qnameerr').css('display','none');
		}
		if(dob==""){
			$('#qdateerr').css('display','block');
			$('#dob').focus();
			return false;
		}else{
			$('#qdateerr').css('display','none');
		}
		if(address==""){
			$('#qraddresserr').css('display','block');
			$('#address').focus();
			return false;
		}else{
			$('#qraddresserr').css('display','none');
		}
		if(city==""){
			$('#cityerr').css('display','block');
			$('#city').focus();
			return false;
		}else{
			$('#cityerr').css('display','none');
		}
		if(state==""){
			$('#stateerr').css('display','block');
			$('#state').focus();
			return false;
		}else{
			$('#stateerr').css('display','none');
		}
		if(country==""){
			$('#qcountryerr').css('display','block');
			$('#country').focus();
			return false;
		}else{
			$('#qcountryerr').css('display','none');
		}
		if(pincode=="" || pincode.length<6){
			$('#pincodeerr').css('display','block');
			$('#pincode').focus();
			return false;
		}else{
			$('#pincodeerr').css('display','none');
		}
		if(nopskul==""){
			$('#qnskulerr').css('display','block');
			$('#nopskul').focus();
			return false;
		}else{
			$('#qnskulerr').css('display','none');
		}
		if(grade==""){
			$('#qgradeerr').css('display','block');
			$('#grade').focus();
			return false;
		}else{
			$('#qgradeerr').css('display','none');
		}
		if(gradetjoin==""){
			$('#gradetjoinerr').css('display','block');
			$('#gradetjoin').focus();
			return false;
		}else{
			$('#gradetjoinerr').css('display','none');
		}
		if(year==""){
			$('#qayearerr').css('display','block');
			$('#year').focus();
			return false;
		}else{
			$('#qayearerr').css('display','none');
		}
		if(nationality==""){
			$('#nationalityerr').css('display','block');
			$('#nationality').focus();
			return false;
		}else{
			$('#nationalityerr').css('display','none');
		}
		if(whattoknow==""){
			$('#whattoknowerr').css('display','block');
			$('#whattoknow').focus();
			return false;
		}else{
			$('#whattoknowerr').css('display','none');
		}
		if(aboutshcool==""){
			$('#aboutshcoolerr').css('display','block');
			$('#aboutshcool').focus();
			return false;
		}else{
			$('#aboutshcoolerr').css('display','none');
		}
		if(nparents==""){
			$('#nparentserr').css('display','block');
			$('#nparents').focus();
			return false;
		}else{
			$('#nparentserr').css('display','none');
		}
		if(designation==""){
			$('#designationerr').css('display','block');
			$('#designation').focus();
			return false;
		}else{
			$('#designationerr').css('display','none');
		}
		if(organization==""){
			$('#organizationerr').css('display','block');
			$('#organization').focus();
			return false;
		}else{
			$('#organizationerr').css('display','none');
		}
		if(occupation==""){
			$('#occupationerr').css('display','block');
			$('#occupation').focus();
			return false;
		}else{
			$('#occupationerr').css('display','none');
		}
		if(offadd==""){
			$('#offadderr').css('display','block');
			$('#offadd').focus();
			return false;
		}else{
			$('#offadderr').css('display','none');
		}
		if(restel=="" || restel.length<10 || restel.length >15){
			$('#restelerr').css('display','block');
			$('#restel').focus();
			return false;
		}else{
			$('#restelerr').css('display','none');
		}
		if(offtel=="" || offtel.length<10 || offtel.length >15){
			$('#offtelerr').css('display','block');
			$('#offtel').focus();
			return false;
		}else{
			$('#offtelerr').css('display','none');
		}
		if(mobile=="" || mobile.length<10 || mobile.length >15){
			$('#mobileerr').css('display','block');
			$('#mobile').focus();
			return false;
		}else{
			$('#mobileerr').css('display','none');
		}
		if(email=="" || !email_regex.test(email)){
			$('#emailerr').css('display','block');
			$('#email').focus();
			return false;
		}else{
			$('#emailerr').css('display','none');
		}
		if(captcha_code2=="" || captcha_code2 != rannumber){
			$('#captcha_code2err').css('display','block');
			$('#captcha_code2').focus();
			return false;
		}else{
			$('#captcha_code2err').css('display','none');
		}

	});
});
</script>

 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);



 $('.refreshbtn').click(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 });
});
</script>
@include('includes.index-footer')