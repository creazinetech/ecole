<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','1')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.url') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2>Apply for Admission</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
											<div class="tg-contactus tg-contactusvone">
											    
											    <div class="adm_status_msg"></div>
											    
                                                @if(Session::get('CaptchaVerificaion') !='')
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;">
                                                <div class="alert alert-danger">{{ Session::get('CaptchaVerificaion') }} </div>
                                                </div>
                                                @endif											    
												
												<br>
												<div class="alert alert-info col-md-12 col-sm-12 col-xs-12">Do read the  
												<strong><a href="/admission-procedure">Admission Procedure</a></strong> before you fill out this form.
												</div>
												<br>
												<h4 class="pull-left">Fill up the application form below. The mandatory fields are marked with an * </h4>
												<hr>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
													<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id="AdmissionForm"   enctype='multipart/form-data'  action="{{ url('proceed-to-payment') }}" >
													<div class="alert alert-info">Student's Detail</div>
													<fieldset>
														<div class="form-group">
															<input type="text" class="form-control"  name="name" id="name" value="{{ old('name') }}" placeholder="Student Name*">
															<span id="qnameerr" style="color:red;display:none;"> Please Enter name.</span>
															@if ($errors->has('name'))
                                        						<div class="error" style="color:red;">{{ $errors->first('name') }}</div>
                                        					@endif
														</div>
														<div class="form-group">
															<input type="text" class="datepicker form-control" name="dob" id="dob" value="{{ old('dob') }}" placeholder="Date Of Birth*">
															<span id="qdateerr" style="color:red;display:none;"> Please Enter Date Of Birth.</span>
															@if ($errors->has('dob'))
                                        						<div class="error" style="color:red;"> Please Enter Date Of Birth.</div>
                                        					@endif
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="address" id="address" value="{{ old('address') }}" placeholder="Residential Address*">
															<span id="qraddresserr" style="color:red;display:none;"> Please Enter Residential Address.</span>
															@if ($errors->has('address'))
                                        						<div class="error" style="color:red;"> Please Enter Residential Address.</div>
                                        					@endif
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="nopskul" id="nopskul" value="{{ old('nopskul') }}" placeholder="Name of Present School:*">
															<span id="qnskulerr" style="color:red;display:none;"> Please Enter Name of Present School.</span>
															@if ($errors->has('nopskul'))
                                        						<div class="error" style="color:red;">Please Enter Name of Present School.</div>
                                        					@endif
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="grade" id="grade" value="{{ old('grade') }}" placeholder="Present grade*">
															<span id="qgradeerr" style="color:red;display:none;"> Please Enter present grade.</span>
															@if ($errors->has('grade'))
                                        						<div class="error" style="color:red;">Please Enter present grade.</div>
                                        					@endif
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="gradetjoin" id="gradetjoin" value="{{ old('gradetjoin') }}" placeholder="Grade to join*">
															<span id="qjoinerr" style="color:red;display:none;"> Grade to join</span>
															@if ($errors->has('gradetjoin'))
                                        						<div class="error" style="color:red;"> Grade to join</div>
                                        					@endif
														</div>
														
														<div class="form-group">
														<!--<label for="sel1">Academic Year for which admission is sought</label>-->
														<select class="form-control" name="year" id="year">
													    <option value=""> Academic Year for which admission is sought</option>
														<?php 
														$start_year=date('Y');
														$end_year= date('Y')+1;
														$start_year1=$start_year+1;
														$end_year1=$end_year+1;
														for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
														<option value='<?php echo $i.'-'.$j; ?>'>
														    <?php echo $i.'-'.$j;?></option>
														<?php } ?>
														 </select>
														 <span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
															@if ($errors->has('year'))
                                        						<div class="error" style="color:red;">Please Enter Academic Year.</div>
                                        					@endif
														</div>
														
														<div class="form-group">
														  <input type="text" class="form-control" id="nationality" name="nationality" value="{{ old('nationality') }}" placeholder="Nationality*">
														  <span id="nationalityerr" style="color:red;display:none;">Please Enter Nationality.</span>
															@if ($errors->has('nationality'))
                                        						<div class="error" style="color:red;">Please Enter Nationality.</div>
                                        					@endif
														</div>
														
														
														<div class="alert alert-info">Parent Detail</div>
														
														<div class="form-group">
															<input type="text" class="form-control" name="nparents" id="nparents" value="{{ old('nparents') }}" placeholder="Parents Name*">
															<span id="qparenterr" style="color:red;display:none;"> Please Enter Parent Name.</span>
															@if ($errors->has('nparents'))
                                        						<div class="error" style="color:red;">Please Enter Parent Name.</div>
                                        					@endif
														</div>
														
														<div class="form-group">
															<input type="text" class="form-control" name="designation" id="designation" value="{{ old('designation') }}" placeholder="Designation*">
															<span id="qdesingnationerr" style="color:red;display:none;"> Please Enter Designation.</span>
															@if ($errors->has('designation'))
                                        						<div class="error" style="color:red;">Please Enter Designation.</div>
                                        					@endif
														</div>
														
														<div class="form-group">
															<input type="text" class="form-control" name="organization" id="organization" value="{{ old('organization') }}" placeholder="Name of the Organization*">
															<span id="qorganizationerr" style="color:red;display:none;"> Please Enter Name of the Organization.</span>
															@if ($errors->has('organization'))
                                        						<div class="error" style="color:red;">Please Enter Name of the Organization.</div>
                                        					@endif
														</div>
														
														<div class="form-group">
															<input type="text" class="form-control" name="occupation" id="occupation" value="{{ old('occupation') }}" placeholder="Occupation of the Working Parent*">
															<span id="qoccupationerr" style="color:red;display:none;"> Please Enter Occupation Name.</span>
															@if ($errors->has('occupation'))
                                        						<div class="error" style="color:red;">Please Enter Occupation Name.</div>
                                        					@endif
														</div>
														
														<div class="form-group">
															<input type="text" class="form-control" name="offadd" id="offadd" value="{{ old('offadd') }}" placeholder="Official Address:*">
															<span id="qoffadderr" style="color:red;display:none;"> Please Enter Official Address.</span>
															@if ($errors->has('offadd'))
                                        						<div class="error" style="color:red;">Please Enter Official Address.</div>
                                        					@endif
														</div>
														
														<div class="form-group">
															<input type="text" class="form-control" name="city" id="city" value="{{ old('city') }}" placeholder="City:*">
															<span id="qcityerr" style="color:red;display:none;"> Please Enter City.</span>
															@if ($errors->has('city'))
                                        						<div class="error" style="color:red;">Please Enter City.</div>
                                        					@endif
														</div>
														
														<div class="form-group">
														  <select class="form-control" id="state" name="state">
                                                                <option value=''>Select State</option>
                                                                <option value='Andhra Pradesh' @if (Input::old('state') == 'Andhra Pradesh') selected="selected" @endif>Andhra Pradesh</option>
                                                                <option value='Arunachal Pradesh' @if (Input::old('state') == 'Arunachal Pradesh') selected="selected" @endif >Arunachal Pradesh</option>
                                                                <option value='Assam' @if (Input::old('state') == 'Assam') selected="selected" @endif >Assam </option>
                                                                <option value='Bihar' @if (Input::old('state') == 'Bihar') selected="selected" @endif >Bihar</option>
                                                                <option value='Chhattisgarh' @if (Input::old('state') == 'Chhattisgarh') selected="selected" @endif >Chhattisgarh    </option>
                                                                <option value='Goa' @if (Input::old('state') == 'Goa') selected="selected" @endif >Goa</option>
                                                                <option value='Gujarat' @if (Input::old('state') == 'Gujarat') selected="selected" @endif >Gujarat</option>
                                                                <option value='Haryana' @if (Input::old('state') == 'Haryana') selected="selected" @endif >Haryana </option>
                                                                <option value='Himachal Pradesh' @if (Input::old('state') == 'Himachal Pradesh') selected="selected" @endif >Himachal Pradesh</option>
                                                                <option value='Jammu & Kashmir' @if (Input::old('state') == 'Jammu & Kashmir') selected="selected" @endif >Jammu & Kashmir</option>
                                                                <option value='Jharkhand' @if (Input::old('state') == 'Jharkhand') selected="selected" @endif >Jharkhand </option>
                                                                <option value='Karnataka' @if (Input::old('state') == 'Karnataka') selected="selected" @endif >Karnataka </option>
                                                                <option value='Kerla' @if (Input::old('state') == 'Kerla') selected="selected" @endif >Kerla </option>
                                                                <option value='Madhya Pradesh' @if (Input::old('state') == 'Madhya Pradesh') selected="selected" @endif >Madhya Pradesh</option>
                                                                <option value='Maharashtra' @if (Input::old('state') == 'Maharashtra') selected="selected" @endif >Maharashtra  </option>
                                                                <option value='Manipur' @if (Input::old('state') == 'Manipur') selected="selected" @endif >Manipur </option>
                                                                <option value='Meghalaya' @if (Input::old('state') == 'Meghalaya') selected="selected" @endif >Meghalaya</option>
                                                                <option value='Mizoram' @if (Input::old('state') == 'Mizoram') selected="selected" @endif > Mizoram</option>
                                                                <option value='Nagaland' @if (Input::old('state') == 'Nagaland') selected="selected" @endif >Nagaland</option>
                                                                <option value='Orissa' @if (Input::old('state') == 'Orissa') selected="selected" @endif >Orissa </option>
                                                                <option value='Punjab' @if (Input::old('state') == 'Punjab') selected="selected" @endif >Punjab </option>
                                                                <option value='Sikkim' @if (Input::old('state') == 'Sikkim') selected="selected" @endif >Sikkim </option>
                                                                <option value='Tamilnadu' @if (Input::old('state') == 'Tamilnadu') selected="selected" @endif >Tamilnadu</option>
                                                                <option value='Tripura' @if (Input::old('state') == 'Tripura') selected="selected" @endif >Tripura </option>
                                                                <option value='Uttar Pradesh' @if (Input::old('state') == 'Uttar Pradesh') selected="selected" @endif >Uttar Pradesh </option>
                                                                <option value='Uttaranchal' @if (Input::old('state') == 'Uttaranchal') selected="selected" @endif > Uttaranchal </option>
                                                                <option value='West Bengal' @if (Input::old('state') == 'West Bengal') selected="selected" @endif >West Bengal</option>
                                                                <option value='Outside India' @if (Input::old('state') == 'Outside India') selected="selected" @endif >Outside India</option>
														  </select>
														  <span id="qstaterr" style="color:red;display:none;"> Please Select State.</span>
															@if ($errors->has('state'))
                                        						<div class="error" style="color:red;">Please Select State.</div>
                                        					@endif
														  </div>															
													</div>
													
														<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														    
															<div class="alert alert-info">Address & Contact</div>
															
															<div class="form-group">
																<input type="text" class="form-control" name="country" id="country" value="{{ old('country') }}" placeholder="country:*">
																<span id="qcountryerr" style="color:red;display:none;"> Please Enter Country.</span>
															@if ($errors->has('country'))
                                        						<div class="error" style="color:red;">{{ $errors->first('country') }}</div>
                                        					@endif
															</div>
															
															<div class="form-group">
																<input type="text" max-length="6" class="form-control" name="pincode" id="pincode" value="{{ old('pincode') }}" placeholder="pincode:*" onkeypress="return isNumberKey(event)">
																<span id="qpincodeerr" style="color:red;display:none;"> Please Enter 6 digit Pincode.</span>
															@if ($errors->has('pincode'))
                                        						<div class="error" style="color:red;">{{ $errors->first('pincode') }}</div>
                                        					@endif
															</div>
															
															<div class="form-group">
															  <input type="tel" max-length="10" class="form-control" name="restel" id="restel" value="{{ old('restel') }}" placeholder="Residence Tel:*" onkeypress="return isNumberKey(event)">
															  <span id="qrtelnerr" style="color:red;display:none;"> Please Enter 10 Digit Residence Tel.</span>
															@if ($errors->has('restel'))
                                        						<div class="error" style="color:red;">Please Enter 10 Digit Residence Tel.</div>
                                        					@endif
															</div>
															
															<div class="form-group">
															  <input type="tel" max-length="10" class="form-control" name="offtel" id="offtel" value="{{ old('offtel') }}" placeholder="Office Tel:*" onkeypress="return isNumberKey(event)">
															  <span id="qotelnerr" style="color:red;display:none;"> Please Enter 10 digit Office Tel.</span>
															@if ($errors->has('offtel'))
                                        						<div class="error" style="color:red;">Please Enter 10 digit Office Tel.</div>
                                        					@endif
															</div>
															
															<div class="form-group">
															  <input type="text" max-length="10" class="form-control" name="mnfather" id="mnfather" value="{{ old('mnfather') }}" placeholder="Mobile Number Father:*" onkeypress="return isNumberKey(event)">
															  <span id="qmfathererr" style="color:red;display:none;"> Please Enter 10 Digit Mobile Number Father.</span>
															@if ($errors->has('mnfather'))
                                        						<div class="error" style="color:red;">Please Enter 10 Digit Mobile Number Father.</div>
                                        					@endif
															</div>
															
															<div class="form-group">
															  <input type="text" max-length="10" class="form-control" name="mnmother" id="mnmother" value="{{ old('mnmother') }}" placeholder="Mobile Number Mother:*" onkeypress="return isNumberKey(event)">
															   <span id="qmmothererr" style="color:red;display:none;"> Please Enter 10 digit Mobile Number Mother.</span>
															@if ($errors->has('mnmother'))
                                        						<div class="error" style="color:red;">Please Enter 10 digit Mobile Number Mother.</div>
                                        					@endif
															</div>
															
															<div class="form-group">
																<input type="email" class="form-control" name="emailf" id="emailf" value="{{ old('emailf') }}" placeholder="Email Address Father:*">
																 <span id="qfemailerr" style="color:red;display:none;"> Please Enter Email Address Father.</span>
															@if ($errors->has('emailf'))
                                        						<div class="error" style="color:red;">Please Enter Email Address Father.</div>
                                        					@endif
															</div>
															
															<div class="form-group">
																<input type="email" class="form-control" name="emailm" id="emailm" value="{{ old('emailm') }}" placeholder="Email Address Mother:*">
																 <span id="qmemailerr" style="color:red;display:none;"> Please Enter Email Address Mother.</span>
															@if ($errors->has('emailm'))
                                        						<div class="error" style="color:red;">Please Enter Email Address Mother.</div>
                                        					@endif
															</div>
															
															<div class="form-group">
																<label for="passport">Passsport Type :*</label>
																  <select class="form-control" id="passport" name="passport">
																	<option value="Indian Passport" @if (Input::old('passport') == 'Indian Passport') selected="selected" @endif >Indian Passport</option>
																	<option value="Non-Indian Passport" @if (Input::old('passport') == 'Non-Indian Passport') selected="selected" @endif >Non-Indian Passport</option>
																  </select>
																  <span id="qpassporterr" style="color:red;display:none;"> Please Enter Passport Type.</span>
															@if ($errors->has('passport'))
                                        						<div class="error" style="color:red;">Please Enter Passport Type.</div>
                                        					@endif
															</div>
															
															<div class="form-group">
															<textarea class="form-control" placeholder="How you heard about our School?*" name="ourskul" id="ourskul">{{ old('ourskul') }}</textarea>
															 <span id="qschoolerr" style="color:red;display:none;">How you heard about our School?</span>
															@if ($errors->has('ourskul'))
                                        						<div class="error" style="color:red;">How you heard about our School?</div>
                                        					@endif
															</div>
															
															
															<div class="form-group">
															<textarea class="form-control" placeholder="What do you wish know?" name="whattoknow" id="whattoknow">{{ old('whattoknow') }}</textarea>
															 <span id="qschoolerr" style="color:red;display:none;">What do you wish know?</span>
															@if ($errors->has('whattoknow'))
                                        						<div class="error" style="color:red;">What do you wish know?</div>
                                        					@endif
															</div>
															
															<div class="form-group col-md-6 col-sm-6 col-xs-7">
															    <img src="{!! \Config::get('app.url') !!}/images/cc-avenue.jpg" class="responsive">
															</div>
															
															
													<!--	<div class="form-group">
                                                            <div class="g-recaptcha" data-sitekey="6LdRDDwUAAAAAK_LeKgqYCiGJ-hPFNPJ6Ircafmo"></div>
                                                            @if ($errors->has('g-recaptcha-response'))
                                        					<div class="col-md-12 error" style="color:red;">Please verify captcha.</div>
                                        				    @endif
                                        				    <span id="g-recaptcha-responseerr" style="color:red;display:none;">Please verify captcha.</span>
                                                        </div>
                                                            <br> -->
                                                            
															<div class="col-10 fix text-center">
															<div class="success1"></div>
															<div class="error1"></div>
															{{ csrf_field() }}
															</div> 
															
															<div class="form-group">
                                                                <div class="col-md-12 col-sm-6 col-xs-12">
                                                                    <br>
															   <a class="button" name="asenquiry" id="asenquiry" >
															        Submit as enquiry <i class="fa fa-angle-double-right" aria-hidden="true"></i>
															    </a>
															    
															    <button type="submit" class="button" name="admission-btn" id="admission-btn" >
															        Proceed <i class="fa fa-angle-double-right" aria-hidden="true"></i>
															    </button>
															</div>
															</div>
														</div>
														</fieldset>	
														</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
	
<script type="text/javascript">

$(document).ready(function(){
    	
	var frm = $('#AdmissionForm');

    $('#asenquiry').click(function(e){
        e.preventDefault();
        
		var name = $('#name').val();
    	var dob = $('#dob').val();
    	var address = $('#address').val();
    	var nopskul = $('#nopskul').val();
    	var grade = $('#grade').val();
    	var gradetjoin = $('#gradetjoin').val();
    	var year = $('#year').val();
    	var nationality = $('#nationality').val();
    	var nparents = $('#nparents').val();
    	var designation = $('#designation').val();
    	var organization = $('#organization').val();
    	var occupation = $('#occupation').val();
    	var offadd = $('#offadd').val();
    	var city = $('#city').val();
    	var state = $('#state').val();
    	var country = $('#country').val();
    	var pincode = $('#pincode').val();
    	var restel = $('#restel').val();
    	var offtel = $('#offtel').val();
    	var mnfather = $('#mnfather').val();
    	var mnmother = $('#mnmother').val();
    	var emailf = $('#emailf').val();
    	var emailm = $('#emailm').val();
    	var passport = $('#passport').val();
    	var ourskul = $('#ourskul').val();
    //	var g-recaptcha-response = $('#g-recaptcha-response').val();
    	
    	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        
        if(name==""){
        	$('#qnameerr').css('display','block'); 
        	$('#name').focus(); 
        	return false;
        }else{
        	$('#qnameerr').css('display','none'); 
        }
        if(dob==""){
        	$('#qdateerr').css('display','block'); 
        	$('#dob').focus(); 
        	return false;
        }else{
        	$('#qdateerr').css('display','none'); 
        }
        if(address==""){
        	$('#qraddresserr').css('display','block'); 
        	$('#address').focus(); 
        	return false;
        }else{
        	$('#qraddresserr').css('display','none'); 
        }
        if(nopskul==""){
        	$('#qnskulerr').css('display','block'); 
        	$('#nopskul').focus(); 
        	return false;
        }else{
        	$('#qnskulerr').css('display','none'); 
        }
        if(grade==""){
        	$('#qgradeerr').css('display','block'); 
        	$('#grade').focus(); 
        	return false;
        }else{
        	$('#qgradeerr').css('display','none'); 
        }
        if(gradetjoin==""){
        	$('#qjoinerr').css('display','block'); 
        	$('#gradetjoin').focus(); 
        	return false;
        }else{
        	$('#qjoinerr').css('display','none'); 
        }
        if(year==""){
        	$('#qayearerr').css('display','block'); 
        	$('#year').focus(); 
        	return false;
        }else{
        	$('#qayearerr').css('display','none'); 
        }
        if(nationality==""){
        	$('#nationalityerr').css('display','block'); 
        	$('#nationality').focus(); 
        	return false;
        }else{
        	$('#nationalityerr').css('display','none'); 
        }        
        if(nparents==""){
        	$('#qparenterr').css('display','block'); 
        	$('#nparents').focus(); 
        	return false;
        }else{
        	$('#qparenterr').css('display','none'); 
        }
        if(designation==""){
        	$('#qdesingnationerr').css('display','block'); 
        	$('#designation').focus(); 
        	return false;
        }else{
        	$('#qdesingnationerr').css('display','none'); 
        }
        if(organization==""){
        	$('#qorganizationerr').css('display','block'); 
        	$('#organization').focus(); 
        	return false;
        }else{
        	$('#qorganizationerr').css('display','none'); 
        }
        if(occupation==""){
        	$('#qoccupationerr').css('display','block'); 
        	$('#occupation').focus(); 
        	return false;
        }else{
        	$('#qoccupationerr').css('display','none'); 
        }
        if(offadd==""){
        	$('#qoffadderr').css('display','block'); 
        	$('#offadd').focus(); 
        	return false;
        }else{
        	$('#qoffadderr').css('display','none'); 
        }
        if(city==""){
        	$('#qcityerr').css('display','block'); 
        	$('#city').focus(); 
        	return false;
        }else{
        	$('#qcityerr').css('display','none'); 
        }
        if(state==""){
        	$('#qstaterr').css('display','block'); 
        	$('#state').focus(); 
        	return false;
        }else{
        	$('#qstaterr').css('display','none'); 
        }
        if(country==""){
        	$('#qcountryerr').css('display','block'); 
        	$('#country').focus(); 
        	return false;
        }else{
        	$('#qcountryerr').css('display','none'); 
        }
        if(pincode=="" || pincode.length<6){
        	$('#qpincodeerr').css('display','block'); 
        	$('#pincode').focus(); 
        	return false;
        }else{
        	$('#qpincodeerr').css('display','none'); 
        }
        if(restel=="" || restel.length<10 || restel.length>15){
        	$('#qrtelnerr').css('display','block'); 
        	$('#restel').focus(); 
        	return false;
        }else{
        	$('#qrtelnerr').css('display','none'); 
        }
        if(offtel=="" || offtel.length<10 || offtel.length>15){
        	$('#qotelnerr').css('display','block'); 
        	$('#offtel').focus(); 
        	return false;
        }else{
        	$('#qotelnerr').css('display','none'); 
        }
        if(mnfather=="" || mnfather.length<10 || mnfather.length>15){
        	$('#qmfathererr').css('display','block'); 
        	$('#mnfather').focus(); 
        	return false;
        }else{
        	$('#qmfathererr').css('display','none'); 
        }
        if(mnmother=="" || mnmother.length<10 || mnmother.length>15){
        	$('#qmmothererr').css('display','block'); 
        	$('#mnmother').focus(); 
        	return false;
        }else{
        	$('#qmmothererr').css('display','none'); 
        }
        
        if(emailf=="" || !email_regex.test(emailf)){
        	$('#qfemailerr').css('display','block'); 
        	$('#emailf').focus(); 
        	return false;
        }else{
        	$('#qfemailerr').css('display','none'); 
        }
        if(emailm=="" || !email_regex.test(emailm)){
        	$('#qmemailerr').css('display','block'); 
        	$('#emailm').focus(); 
        	return false;
        }else{
        	$('#qmemailerr').css('display','none'); 
        }
        
        if(passport==""){
        	$('#qpassporterr').css('display','block'); 
        	$('#passport').focus(); 
        	return false;
        }else{
        	$('#qpassporterr').css('display','none'); 
        }
        if(ourskul==""){
        	$('#qschoolerr').css('display','block'); 
        	$('#ourskul').focus(); 
        	return false;
        }else{
        	$('#qschoolerr').css('display','none'); 
        }
    /*  if(g-recaptcha-response==""){
        	$('#g-recaptcha-responseerr').css('display','block'); 
        	$('#g-recaptcha-response').focus(); 
        	return false;
        }else{
        	$('#g-recaptcha-responseerr').css('display','none'); 
        }
    */    
        
            $.ajax({
    			type: 'get',
    			headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
    			url: 'admission/sendasenquiry',
               data: $("#AdmissionForm").serialize(), // serializes the form's elements.
               success: function(data)
               {
    			    $('.adm_status_msg').removeClass('alert alert-success');
    			    $('.adm_status_msg').removeClass('alert alert-danger');
    				if(data == 'success'){
    				    $(window).scrollTop($('.adm_status_msg').offset().top);
    				    $('.adm_status_msg').addClass('alert alert-success');
    					$('.adm_status_msg').html('<i class="fa fa-check-square-o"></i> Thank you, We will contact you soon.');
    				    $("#AdmissionForm").trigger('reset');
    				}
    				if(data == 'captcha-failed'){
    				    $(window).scrollTop($('.adm_status_msg').offset().top);
    				    $('.adm_status_msg').addClass('alert alert-danger');
    					$('.adm_status_msg').html('<i class="fa fa-ban"></i> Captcha verification failed.');
    				}   				
    				if(data == 'failed'){
    				    $(window).scrollTop($('.adm_status_msg').offset().top);
    				    $('.adm_status_msg').addClass('alert alert-danger');
    					$('.adm_status_msg').html('<i class="fa fa-ban"></i> Something went wrong, Please try again.');
    				}
               }
            });
        e.preventDefault();
    });
    
    
	$('#admission-btn').click(function(){
	var name = $('#name').val();
	var dob = $('#dob').val();
	var address = $('#address').val();
	var nopskul = $('#nopskul').val();
	var grade = $('#grade').val();
	var gradetjoin = $('#gradetjoin').val();
	var year = $('#year').val();
	var nationality = $('#nationality').val();
	var nparents = $('#nparents').val();
	var designation = $('#designation').val();
	var organization = $('#organization').val();
	var occupation = $('#occupation').val();
	var offadd = $('#offadd').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	var restel = $('#restel').val();
	var offtel = $('#offtel').val();
	var mnfather = $('#mnfather').val();
	var mnmother = $('#mnmother').val();
	var emailf = $('#emailf').val();
	var emailm = $('#emailm').val();
	var passport = $('#passport').val();
	var ourskul = $('#ourskul').val();
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

	    if(name==""){
			$('#qnameerr').css('display','block'); 
			$('#name').focus(); 
			return false;
		}else{
			$('#qnameerr').css('display','none'); 
		}
		if(dob==""){
			$('#qdateerr').css('display','block'); 
			$('#dob').focus(); 
			return false;
		}else{
			$('#qdateerr').css('display','none'); 
		}
		if(address==""){
			$('#qraddresserr').css('display','block'); 
			$('#address').focus(); 
			return false;
		}else{
			$('#qraddresserr').css('display','none'); 
		}
		if(nopskul==""){
			$('#qnskulerr').css('display','block'); 
			$('#nopskul').focus(); 
			return false;
		}else{
			$('#qnskulerr').css('display','none'); 
		}
		if(grade==""){
			$('#qgradeerr').css('display','block'); 
			$('#grade').focus(); 
			return false;
		}else{
			$('#qgradeerr').css('display','none'); 
		}
		if(gradetjoin==""){
			$('#qjoinerr').css('display','block'); 
			$('#gradetjoin').focus(); 
			return false;
		}else{
			$('#qjoinerr').css('display','none'); 
		}
		if(year==""){
			$('#qayearerr').css('display','block'); 
			$('#year').focus(); 
			return false;
		}else{
			$('#qayearerr').css('display','none'); 
		}
        if(nationality==""){
        	$('#nationalityerr').css('display','block'); 
        	$('#nationality').focus(); 
        	return false;
        }else{
        	$('#nationalityerr').css('display','none'); 
        }  
		if(nparents==""){
			$('#qparenterr').css('display','block'); 
			$('#nparents').focus(); 
			return false;
		}else{
			$('#qparenterr').css('display','none'); 
		}
		if(designation==""){
			$('#qdesingnationerr').css('display','block'); 
			$('#designation').focus(); 
			return false;
		}else{
			$('#qdesingnationerr').css('display','none'); 
		}
		if(organization==""){
			$('#qorganizationerr').css('display','block'); 
			$('#organization').focus(); 
			return false;
		}else{
			$('#qorganizationerr').css('display','none'); 
		}
		if(occupation==""){
			$('#qoccupationerr').css('display','block'); 
			$('#occupation').focus(); 
			return false;
		}else{
			$('#qoccupationerr').css('display','none'); 
		}
		if(offadd==""){
			$('#qoffadderr').css('display','block'); 
			$('#offadd').focus(); 
			return false;
		}else{
			$('#qoffadderr').css('display','none'); 
		}
		if(city==""){
			$('#qcityerr').css('display','block'); 
			$('#city').focus(); 
			return false;
		}else{
			$('#qcityerr').css('display','none'); 
		}
		if(state==""){
			$('#qstaterr').css('display','block'); 
			$('#state').focus(); 
			return false;
		}else{
			$('#qstaterr').css('display','none'); 
		}
		if(country==""){
			$('#qcountryerr').css('display','block'); 
			$('#country').focus(); 
			return false;
		}else{
			$('#qcountryerr').css('display','none'); 
		}
		if(pincode=="" || pincode.length<6){
			$('#qpincodeerr').css('display','block'); 
			$('#pincode').focus(); 
			return false;
		}else{
			$('#qpincodeerr').css('display','none'); 
		}
		if(restel=="" || restel.length<10 || restel.length>15){
			$('#qrtelnerr').css('display','block'); 
			$('#restel').focus(); 
			return false;
		}else{
			$('#qrtelnerr').css('display','none'); 
		}
		if(offtel=="" || offtel.length<10 || offtel.length>15){
			$('#qotelnerr').css('display','block'); 
			$('#offtel').focus(); 
			return false;
		}else{
			$('#qotelnerr').css('display','none'); 
		}
		if(mnfather=="" || mnfather.length<10 || mnfather.length>15){
			$('#qmfathererr').css('display','block'); 
			$('#mnfather').focus(); 
			return false;
		}else{
			$('#qmfathererr').css('display','none'); 
		}
		if(mnmother=="" || mnmother.length<10 || mnmother.length>15){
			$('#qmmothererr').css('display','block'); 
			$('#mnmother').focus(); 
			return false;
		}else{
			$('#qmmothererr').css('display','none'); 
		}
		
		if(emailf=="" || !email_regex.test(emailf)){
			$('#qfemailerr').css('display','block'); 
			$('#emailf').focus(); 
			return false;
		}else{
			$('#qfemailerr').css('display','none'); 
		}
		if(emailm=="" || !email_regex.test(emailm)){
			$('#qmemailerr').css('display','block'); 
			$('#emailm').focus(); 
			return false;
		}else{
			$('#qmemailerr').css('display','none'); 
		}
		
		if(passport==""){
			$('#qpassporterr').css('display','block'); 
			$('#passport').focus(); 
			return false;
		}else{
			$('#qpassporterr').css('display','none'); 
		}
		if(ourskul==""){
			$('#qschoolerr').css('display','block'); 
			$('#ourskul').focus(); 
			return false;
		}else{
			$('#qschoolerr').css('display','none'); 
		}
		

	});	
});	
</script> 
	
	
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')