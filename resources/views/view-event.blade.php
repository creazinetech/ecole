<?php 
	$Pages = DB::table('pages')->where('page_id','=','4')->get();
	foreach($Pages as $Page){ }
	foreach($Data as $Row){ }
?>	
@include('includes.index-header')

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	 
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/events"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder  tg-content-text">
											<h2><?php echo ucwords($Row->title); ?></h2>
										</div>
									<div class="descdiv tg-content tg-content-text">
										<div class="col-md-6">
										<p><strong>Date/Time :</strong></p>
										Date(s) - <?php echo date('d/m/Y',strtotime($Row->start_date));  ?><br>
										<?php echo $Row->timing;  ?>
										<br>
										<p><strong>Location :</strong></p>
										<?php echo $Row->location;  ?><br>
										<br>
										<?php if(isset($Row->brief_desc) && $Row->brief_desc !="") { 
										echo '<p>'.$Row->brief_desc.'</p>'; 
										} ?>
										
										</div>
										<div class="col-md-6">
										    <?php  echo $Row->location_address;   ?>
										</div>
										
										</div>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')