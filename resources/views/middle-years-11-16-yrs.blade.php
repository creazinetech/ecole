<?php
	$Pages = DB::table('pages')->where('page_id','=','10')->get();
	$Pages1 = DB::table('pages')->where('page_id','=','11')->get();
	$Gallery = DB::table('gallery')->where('page_id','=','10')->where('img_status','=','Active')->paginate(20);
	$Gallery1 = DB::table('gallery')->where('page_id','=','11')->where('img_status','=','Active')->paginate(20);
	
	foreach($Pages as $Page){ }
	foreach($Pages1 as $Page1){ }
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	<style>
	.tab-content > .tab-pane{background-color:#EDF9FF;}
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{background-color:#9FE6DE;}
	</style>
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/academics">Academics</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} 
                    //if(isset($Page1) && !empty($Page1->page_heading)){ echo ' &amp; '.$Page1->page_heading; }
                    ?>
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content ">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder">
											<h2><?php echo ucwords($Page->page_heading); ?> <?php //echo ucwords($Page1->page_heading); ?></h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											
										<div class="tabs-left" style="height:auto;">
										<ul class="nav nav-tabs">
										  <li class="active"> <a href="#middle" data-toggle="tab">Middle Years</a> </li>
										 <!-- <li> <a href="#igcse" data-toggle="tab">IGCSE</a> </li>-->
										</ul>
										<div class="tab-content">
										 
										  <div class="tab-pane active" id="middle">
										      <br>
											<div class="descdiv"  style="padding:15px;"><?php echo $Page->brief_desc; ?></div>
											
											<?php if(count($Gallery)>0){?>
                								<br>
                								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="background-color:#EDF9FF;">
                									<div class="tg-contactus tg-contactusvone">
                										<div class="tg-titleborder">
                											<h2>Life in the Middle School</h2>
                										</div>
                										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                										<?php foreach($Gallery as $GalImg){?>
                											<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3" >
                											    <img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox-group="middleyears" data-darkbox-description="<?php echo ucwords($GalImg->img_title); ?>" class="img-responsive thumbnail galleryimg">
                											</div>
                										<?php } ?>
                										<br>
                										</div>
                										<div class="ecole-text text-center"><?php echo $Gallery->render(); ?></div>
                									</div>
                								</div>
            								<?php } ?>
										  </div>
										  
										  <!-- <div class="tab-pane" id="igcse">
										      <br>
											<div class="descdiv"  style="padding:15px;"><?php echo $Page1->brief_desc; ?></div>
											
											<?php if(count($Gallery1)>0){?>
                								<br>
                								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="background-color:#EDF9FF;">
                									<div class="tg-contactus tg-contactusvone">
                										<div class="tg-titleborder">
                											<h2>Life at the IGCSE</h2>
                										</div>
                										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                										<?php foreach($Gallery1 as $GalImg1){?>
                											<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3" >
                												<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg1->img; ?>" data-darkbox="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg1->img; ?>" data-darkbox-group="igcse" data-darkbox-description="<?php echo ucwords($GalImg1->img_title); ?>" class="img-responsive thumbnail galleryimg"> 
                											</div>
                										<?php } ?>
                										<br>
                										</div>
                										<div class="ecole-text text-center"><?php echo $Gallery1->render(); ?></div>
                									</div>
                								</div>
            								<?php } ?>
										  </div> -->
										  
										  
										</div><!-- /tab-content -->
										</div><!-- /tabbable -->
										</div>
									</div>
								</div> 
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
                				<hr>
                					    <p>Related Links</p>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/playschool">Play School</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/early-yrndprimary-yr">Early Years & Primary Years</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/diploma-16-19-yrs">Diploma</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/college-counseling">College Counselling</a>
            					</div>
                			
								
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')