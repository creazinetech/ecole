<div class="container">


  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">

	<!-- Modal -->
	<div id="enquirypopup" class="modal fade in" role="dialog">
		<div class="modal-dialog ">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 tg-content-text">
		  <!-- Modal content-->
				<div class="modal-content  row" style="background-color:#86d3e2;">
					<div class="modal-header custom-modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="form_heading" style="color: #1B6D7F;">Quick Admission Enquiry</h4>
					</div>
					<div class="">
					        <div class="status_msg"></div><br>
							<div class="form-group col-sm-12">
								<input type="text" class="form-control" name="qnameX" id="qnameX" placeholder="Enter Name">
								<span id="qnameerrX" style="color:red;display:none;"> Please enter name.</span>
							</div>
							<div class="form-group col-sm-12">
								<input type="email" class="form-control" name="qemailX" id="qemailX" placeholder="Enter Email">
								<span id="qemailerrX" style="color:red;display:none;"> Please enter email id.</span>
							</div>

							<div class="form-group col-sm-12">
								<input type="text" class="form-control" name="qphoneX" id="qphoneX" placeholder="Enter Phone" onkeypress="return isNumberKey(event)">
								<span id="qphoneerrX" style="color:red;display:none;"> Please enter 10 digit phone number.</span>
							</div>

							<div class="form-group col-sm-12" style="display:none;">
								<textarea class="form-control" id="qmsgX" name="qmsgX" rows="4" placeholder="Enter Message"> </textarea>
								<span id="qmsgerrX" style="color:red;display:none;"> Please enter message.</span>
							</div>

							<div class="form-group  col-sm-12">
								<input type="hidden" id="rannumberX" name="rannumberX" class="captcha">
								<div class="captcha col-md-4" style="border: 2px solid green; width: 150px; line-height:1.4; height: 45px; text-align: center; vertical-align: middle;font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">
								</div>
								<div class="col-md-2" style="height:40px;margin-bottom:5px;">
								  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;" >
								  <i class="fa fa-refresh"></i>
								  </a>
								  <br><br>
								</div>
							  <input class="form-control" id="captcha_code2X" name="captcha_code2X"  placeholder="Enter Captcha" type="text" style="text-transform:none;margin-top:10px;">
								<span id="captcha_code2errX" style="color:red;display:none;"> Captcha missmatch.</span>
							</div>

							<div class="col-10 fix ">
								<div class="success1"></div>
								<div class="error1"></div>
								 {{ csrf_field() }}

								<div class="form-group col-sm-12">
									<button type="submit" class="btn pull-left sub_popenquiry" name="job-submit" id="job-submit" style="background-color:rgba(23, 110, 128, 1)">Send</button>

								</div>
							</div>

						<!--</form>-->
					</div>

				</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  tg-content-text">
			<img src="{!! \Config::get('app.url') !!}/images/popup_banner.jpg" style="width: 100%;">
			<p style="margin-top: 0px;padding: 2px;color: #fff;font-size: 15px!important;font-weight: bold;">by the The Times of India, School Survey 2016</p>
		</div>
			 <center>
			     <button type="button" class="btn-square-blue center-block btn-lg btn-info btnclose ecole-text" data-dismiss="modal" style="background-color:rgba(23, 110, 128, 1)">
			         Click here to visit website</button>
			     </center>
		</div>
	</div>
	<!-- Modal End -->




	<script>
		$('.sub_popenquiryXXX').on('click', function(){
			var qname = $('#qnameX').val();
			var qemail = $('#qemailX').val();
			var qphone = $('#qphoneX').val();
			var qmsg = $('#qmsgX').val();
			var captcha_code2 = $('#captcha_code2X').val();
			var rannumber = $('#rannumberX').val();
			var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
			if(qname==""){$('#qnameerr').show(); return false;}else{$('#qnameerr').hide();}
			if(qemail=="" || !email_regex.test(qemail)){$('#qemailerr').show(); return false;}else{$('#qemailerr').hide();}
			if(qphone==""  || qphone.length<10 || qphone.length>15){$('#qphoneerr').show(); return false;}else{$('#qphoneerr').hide();}
			if(qmsg==""){$('#qmsgerr').show(); return false;}else{$('#qmsgerr').hide();}
			if(captcha_code2=="" || captcha_code2 != rannumber){$('#captcha_code2err').show(); return false;}else{$('#captcha_code2err').hide();}
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'index/send_quick_enquiry',
				data: 'qname='+qname+'&qemail='+qemail+'&qphone='+qphone+'&qmsg='+qmsg+'&captcha_code2='+captcha_code2,
				success: function (data) {
					if(data == 'success'){
						$('.status_msg').html('<p style="color:green;"><i class="fa fa-check-square-o"></i> Thank you, We will contact you soon.</p>');
					}else{
						$('.status_msg').html('<p style="color:red;"><i class="fa fa-ban"></i> Something went wrong, Please try again.</p>');
					}
				}
			});
		});
	</script>
</div>

</div>

		<!--************************************
				Home Slider Start
		*************************************-->
		<div id="tg-content" class="tg-content">
		<div id="tg-addmissionslider" class="tg-addmissionslider owl-carousel tg-btnround">
			<?php
			$Sliders = DB::table('index_sliders')->where('is_status','=','Active')->get();
			foreach($Sliders as $Slider){?>
			<div class="item">
				<figure>
					<img src="{!! \Config::get('app.admin') !!}/images/slider-images/<?php echo $Slider->is_img; ?>" alt="image description">
					<figcaption class="tg-slidercontent">
						<div class="tg-slidercontentbox">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0 auto;width:100%;">
										<?php if(isset($Slider->is_title1) && $Slider->is_title1 !=""){ ?>
										<h2><div class="b1"><?php echo $Slider->is_title1; ?></div></h2>
										<?php } if(isset($Slider->is_title2) && $Slider->is_title2 !=""){ ?>
										<h3><div class="b2"><?php echo $Slider->is_title2; ?></div></h3>
										<?php } ?>

									</div>
								</div>
							</div>
						</div>
					</figcaption>
				</figure>
			</div>
			<?php } ?>

		</div>
		</div>

		<div class="tg-tickerbox">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

							<div class="item">
								<div class="tg-description" style="font-family:'Adamina',serif;">
								<marquee behavior="">
								<p>Ecole Mondiale World School is the first International School in Mumbai to be authorised to offer the three programs of PYP, MYP and DP.</p>
								</marquee>
								</div>
							</div>



					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Home Slider End
		*************************************-->
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);



 $('.refreshbtn').click(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 });
});
</script>