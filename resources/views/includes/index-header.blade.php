<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->

<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
 <?php
 use Illuminate\Pagination\Paginator;
 use Illuminate\Support\Facades\Route;
 $currentPath= Route::getFacadeRoot()->current()->uri();
 ?>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="google-site-verification" content="4xT7qckH8ifhl__FWHUMMRdwEfx-CfkdqSdRR8cPQjY" />
    <title> <?php if(@$currentPath == '/'){?> Best International IB Schools Juhu Mumbai | Top IB School Juhu Mumbai |<?php } ?><?php /* if($currentPath != 'index'){?><?php if(isset($Page) && !empty($Page->page_heading) && !isset($Page1)){ echo ucwords(trim($Page->page_heading).' | ');} ?><?php if(isset($Page) && !empty($Page->page_heading) && isset($Page1) && !empty($Page1->page_heading)){ echo ucwords($Page->page_heading); /*echo '&'.ucwords($Page1->page_heading).' | ';} */?><?php if(isset($Page) && !empty($Page->page_meta_title)){ echo ucwords($Page->page_meta_title).' | ';}?><?php /* if(!isset($Page1->page_heading) && !isset($Page->page_heading) && isset($PageTitle) && !empty($PageTitle)){ echo ucwords($PageTitle.'|');}?> <?php } */?>École Mondiale World School </title>
    <meta property="title" content="<?php if(isset($Page) && !empty($Page->page_meta_title)){ echo $Page->page_meta_title;}  if(isset($Page1) && !empty($Page1->page_meta_title)){ /*echo ' &amp; '.$Page1->page_meta_title;*/ } ?>" />
    <meta property="keywords" content="<?php if(isset($Page) && !empty($Page->page_meta_keywords)){ echo $Page->page_meta_keywords;} /* if(isset($Page1) && !empty($Page1->page_meta_keywords)){ echo ' &amp; '.$Page1->page_meta_keywords; } */if(@$currentPath == '/'){echo 'Best Schools In Mumbai, International School In Mumbai, International Schools In Mumbai, Ib Schools In Mumbai, Schools In Juhu, Best International School In Mumbai, Best Ib School In Mumbai, Ib Board Schools In Mumbai, Top International Schools In Mumbai, Top Ib Schools In Mumbai, International Board Schools In Mumbai, International School In Juhu, Schools In Juhu Mumbai, International School Mumbai, School In Juhu Mumbai, Best International School In Juhu, Top International School In Juhu, Top School In Juhu, Top International School Juhu, Best Ib School In Juhu, Best School In Mumbai';
        }  ?>" />
	<meta name="description" content="<?php if(isset($Page) && !empty($Page->page_meta_description)){ echo $Page->page_meta_description;}  if(isset($Page1) && !empty($Page1->page_meta_description)){ /*echo ' &amp; '.$Page1->page_meta_description;*/ }  if(@$currentPath == '/'){
         echo "Ecole Mondiale World School is the best International IB Board School in Juhu, Mumbai, to be authorised to offer the three programs of PYP, MYP and DP.";
          }   ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="{!! \Config::get('app.url') !!}/images/favicon.png">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">

	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/bootstrap.min.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/normalize.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/font-awesome.min.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/icomoon.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/animate.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/prettyPhoto.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/owl.carousel.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/owl.theme.default.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/transitions.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/color.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/responsive.css">
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/font-awesome/css/font-awesome.min.css">
	<!--<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/custom.css">-->
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/css/main.css">

	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>


	<script src="{!! \Config::get('app.url') !!}/plugins/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

	<script src="{!! \Config::get('app.url') !!}/plugins/osvaldas/main.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/osvaldas/doubletaptogo.js"></script>
    <!--Image popup view-->
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/darkbox/darkbox.css">
	<script src="{!! \Config::get('app.url') !!}/plugins/darkbox/darkbox.js"></script>

	<!--Datepicker CSS-->
	<link rel="stylesheet" href="{!! \Config::get('app.url') !!}/plugins/datepicker/datepicker3.css">

    <link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700|Adamina' rel='stylesheet' type='text/css'>

	<script>
	$(document).ready(function(){
	  //  $(".descdiv").html($(".descdiv").html().replace('src="/ecolemondiale/admin/public/plugins/kcfinder/upload/images/','src="http://ecolemondiale.org/demo/admin_ecole/public/plugins/kcfinder/upload/images/'));
    });
	</script>

	<!--------Google Re-Captcha---------->
        <script type="text/javascript">
          /*  var CaptchaCallback = function() {
                grecaptcha.render('ReCaptacha1', {'sitekey' : '6LfniTsUAAAAALQ4NwCrkJMsq-Lrr6_QaHqHw9jB'});
                grecaptcha.render('ReCaptacha2', {'sitekey' : '6LfniTsUAAAAALQ4NwCrkJMsq-Lrr6_QaHqHw9jB'});
            }; */
        </script>
        <script type="text/javascript">
        var CaptchaCallback = function() {
            $('.g-recaptcha').each(function(index, el) {
                grecaptcha.render(el, {'sitekey' : '6LfniTsUAAAAALQ4NwCrkJMsq-Lrr6_QaHqHw9jB'});
                });
            };
        </script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118985748-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118985748-1');
</script>
    <!--------Google Re-Captcha---------->


<style>
.sitemap-ul{
    margin-left:30px;
}
.sitemap-ul a{
    color:#0192DD;
}
.sitemap-ul li{
    font-size: 15px !important;
    text-align: justify !important;
    line-height: 34px !important;
    margin-bottom: 7px;
}

.xopac-logo {
    border: 1px solid #C78139;
    margin-top: 2%;
    overflow: hidden;
    width: 45%;
}
.xalignleft {
    float: left;
    padding: 56px 10px 7px 0px;
}
.xarrow-left {
    border-bottom: 39px solid transparent;
    border-right: 40px solid #c78139;
    border-top: 41px solid transparent;
    float: left;
    height: 0;
    width: 0;
}
.xglobe {
    background: none repeat scroll 0 0 #c78139;
    overflow: hidden;
    padding: 22px;
}
.xglobe a {
    color: #fff;
    font-size: 1.8em;
    font-weight: bold;
    font-family: 'Rokkitt',serif;
}

p{
    text-align: justify !important;line-height: 34px !important;font-size:18px !important;
}
#myBtn {
  display: none;
  position: fixed;
  bottom: 54px;
  right: 30px;
  z-index: 99;
  color:#2DB0F3;
  cursor: pointer;
  padding: 10px;
  border-radius:30px;
}

#myBtn:hover {
  background-color: #555;
}

.galleryimg{
    height:160px;
    width:100%;
}
table{
    font-family:'Adamina', serif;
}
li{
    font-family:'Adamina', serif;
}
.tg-content-text ul{
    margin-right:20px;
    margin-left:20px;
}
.header-heading-area h2{
    color: #fff;
    background-color:#153781;
    /*padding: 0 20px;*/
    line-height: 1.5;
    margin-bottom:-76px;
    font-size: 30px;
    font-weight:bold;
    position:relative;
    top: -75px;
    right: 25px;
    font-family:'Rokkitt',serif;
    opacity: 0.9;
}
.header-heading-area h2, .header-heading-area h3 {
    float: right;
    margin-top: 0;
    clear: right;
}

.tg-navigation ul li a{
    padding: 0 8px;
}

.blacktext{
    color:#474747;
}
.white-text{
    color:#ffffff;
}
.btn-square-blue{
    background-color:#11317B;
    color:#ffffff;
}
.tg-content-text .btn{
    border-radius:0px;
    background-color:#11317B;
    color:#ffffff;
    float:right;
}

.btn{
    border-radius:20px;
    background-color:#11317B;
    color:#ffffff;
}
.btn:hover {
  background: #008CE0;
  background-image: -webkit-linear-gradient(top, #008CE0, #3498db);
  background-image: -moz-linear-gradient(top, #008CE0, #3498db);
  background-image: -ms-linear-gradient(top, #008CE0, #3498db);
  background-image: -o-linear-gradient(top, #008CE0, #3498db);
  background-image: linear-gradient(to bottom, #008CE0, #3498db);
  text-decoration: none;
}

.button:hover {
  background: #008CE0;
  background-image: -webkit-linear-gradient(top, #008CE0, #3498db);
  background-image: -moz-linear-gradient(top, #008CE0, #3498db);
  background-image: -ms-linear-gradient(top, #008CE0, #3498db);
  background-image: -o-linear-gradient(top, #008CE0, #3498db);
  background-image: linear-gradient(to bottom, #008CE0, #3498db);
  text-decoration: none;
  color:#ffffff;
}
.big-button{

    height: 60px;
    width: auto;
    padding: 24px;
    font-size: 24px;

}
.button{
        background: #8D8D8D;
    	 text-align: center;
    	 padding: 9px 25px 11px;
    	 text-decoration: none;
    	 /*font-size: 1.3rem;*/
    	 font-size:13px;
    	 font-weight:bold;
    	 line-height: 1;
    	 cursor: pointer;
    	 outline: none;
    	 margin: 3px;
     	transition:all 0.35s ease-in-out;
    	-moz-transition:all 0.35s ease-in-out;
    	-webkit-transition:all 0.35s ease-in-out;
    	-o-transition:all 0.35s ease-in-out;
    	position: relative;
    	color: #FFFFFF;
    	box-shadow: 0 -3px rgba(0, 0, 0, 0.1) inset;
    	-moz-box-shadow: 0 -3px rgba(0, 0, 0, 0.1) inset;
    	-webkit-box-shadow: 0 -3px rgba(0, 0, 0, 0.1) inset;
    	-o-box-shadow: 0 -3px rgba(0, 0, 0, 0.1) inset;
    	display: inline-block;
    	border-radius: 60px;
    	border:none;
    	font-family: 'Adamina',serif;
    }


/*
#enquirypopup .modal-dialog {
    width: 400px;
    padding: 0px ;
    position: relative;
} */

#enquirypopup .modal-dialog {
    padding: 0px ;
    position: relative;
	    margin-top: 10%;
}

#enquirypopup .modal-dialog:before {

    height: 0px;
    width: 0px;
    border-left: 50px solid #12317b;
    border-left: 50px solid #12317b;
    border-right: 50px solid transparent;
    border-bottom: 50px solid transparent;
    position: absolute;
    top: 1px;
    left: -14px;
    z-index: 99;
}

.custom-modal-header {
    text-align: center;
    color: #12317b;
    text-transform: uppercase;
    letter-spacing: 2px;
    border-top: 4px solid;
}

#enquirypopup .modal-dialog .close {
    z-index: 99999999;
    color: white;
    text-shadow: 0px 0px 0px;
    font-weight: normal;
    top: 4px;
    right: 6px;
    position: absolute;
    opacity: 1;
}

.custom-modal-header .modal-title {
    /* font-weight: bold; */
    font-size: 18px;
}


#enquirypopup .modal-dialog1:after {
    content: '';
    height: 0px;
    width: 0px;
    /* border-right: 50px solid rgba(255, 0, 0, 0.98);
    border-right: 50px solid #12317b; */
    border-bottom: 50px solid transparent;
    position: absolute;
    top: -56px;
    right: 0px;
    z-index: 999999;
}


.form-group {
    margin-bottom: 15px !important;
}

.form-inline .form-control {
    display: inline-block;
    width: 100%;
    vertical-align: middle;
}

.modal-open .modal {
    overflow-x: hidden;
    overflow-y: auto;
    background-color: rgba(0, 0, 0, 0.6);
}
.subheading{
     color:#008CE0;font-size:30px;font-weight:bold;font-family:'Rokkitt', serif;line-height: 38px;
 }
.subheading1{
     font-weight:bold;font-family:'Adamina', serif;font-size:22px;
 }
 .subheading2{
     font-weight:bold;font-family:'rokkitt', serif;font-size:22px;
 }
.sidebar-newsletter #image {
    position: relative;
    left: 0;
    top: 0;
}
.sidebar-newsletter{
    margin-top:30px;
}
.sidebar-newsletter p{
    font-size: 12px;line-height: 9px;padding: 0px;font-weight: 0px;
}
.sidebar-newsletter span{
    font-size:24px;
}
.sidebar-newsletter #text {
    z-index: 100;
    position: relative;
    color: white;
    font-weight: bold;
    top: -60px;
    font-family:'rokkitt', serif;
}
.sidebar-newsletter #text:hover
{
    color:#e1e1d0;
}
.school-calendar #text{
    z-index: 100;
    position: relative;
    color: #ffffff;
    font-weight: bold;
    top: -54px;
    font-family: 'rokkitt', serif;
    right: -107px;
    font-size: 22px;
}
div.description{
	position:absolute; /* absolute position (so we can position it where we want)*/
	bottom:0px; /* position will be on bottom */
	left:0px;
	width:100%;
	/* styling bellow */
	background-color:black;
	color:white;
	opacity:0.6; /* transparency */
	filter:alpha(opacity=60); /* IE transparency */
}
p.description_content{
	padding:10px;
	margin:0px;
	font-size: 30px;
}


.school-calendars {
  position: relative;
  display: inline-block;
}

.school-calendars-text {
  position: absolute;
  height: 30%;
  text-align: center;
  width: 100%;
  margin: auto;
  top: 0;
  bottom: 0;
  right: 0;
  left: 20;
  font-size:16px;
  font-family:'rokkitt', serif;
  color:#ffffff;
}

.calendar-img {
  display: block;
  max-width: 100%;
  height:auto;
  margin: auto;
  padding: auto;
}
.school-calendars-data {
  margin-top: auto;
  margin-left: 100px;
}
.breadcrum-div{
  text-decoration:none;
  font-family:'Adamina', serif;
  font-size:12px;
}
.breadcrum-text{
    color:#474747;
}
.breadcrum-text:hover{
    color:#008CE0;
}
.tg-main > .container{
    margin:auto 0px;
    padding:auto 0px;
    width:100%;
}
.navbar-toggle{
    float:left;
    margin-left:15px;
}

@media only screen and (max-width : 360px) {
	.tg-sectionspace{
	  width:100%;
	}
	.tg-sidebar{
	  width:100%;
	}
}

@media only screen and (max-width : 330px) {
	.tg-haslayout{
	  width:100%;
	}
}

@media only screen and (max-width : 320px) {
	.tg-sectionspace{
	  width:100%;
	}
	.tg-sidebar{
	  width:100%;
	}

}

@media only screen and (max-width : 500px) {
	.tg-header{
		width:100%;
	}
}

@media only screen and (max-width :999px) {
	.tg-header{
		width:100%;
	}
}

@media screen and (max-width:768px) {

    .tg-addmissionslider.owl-carousel {
        display: none;
    }
    .tg-slidercontentbox{
        margin-top:30px;
    }
    .header-heading-area h2{
        font-size:18px;
        right:0px;
        top: -36px;
        width:100%;
    }
    .header-heading-area{
        margin: 36px auto;
        height:1px;
    }
    .school-calendars-text p{
        font-size:8px;
    }
    .galleryimg{
    height:95px;
    width:250%;
    }

}
@media screen and (max-width:360px) {
    .header-heading-area{
      /*  margin: 75px auto;
        margin-left: 7%;*/
        width: 100%;
    }
    .header-heading-area h2{
        font-size:20px;
        width: 100%;
    }
    .tg-copyright{
        text-align:center;
    }
    .tg-addnav ul li{
        text-align:center;
    }
    .tg-addnav ul {
    width: 100%;
    /* float: left; */
    line-height: 20px;
    margin-right: 20%;
    margin-left: 20%;
    }
    .school-calendars-text p{
        font-size:8px;
    }
     .tg-header{
        width:100%;
    }
    .galleryimg{
    height:85px;
    width:250%;
    }

}
@media screen and (max-width:240px) {
    .header-heading-area{
       /* margin: 75px auto;
        margin-left: 11%;*/
        width: 100%;
    }
    .header-heading-area h2{
        font-size:20px;
        width: 100%;
    }
    .school-calendars-text p{
        font-size:8px;
    }
    .tg-header{
        width:100%;
    }
    .galleryimg{
    height:85px;
    width:100%;
    }
}
.header-banner{
    width:100%;
}
</style>
</head>
<body class="tg-home tg-homethree">




	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper">
	<header id="tg-header" class="tg-header tg-haslayout hidden-lg">
		<div class="clearfix"></div>
			<div class="containerx">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">

					<style>
					    .navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:focus, .navbar-inverse .navbar-nav>.active>a:hover{
					        background-color:#0844b1;
					        color:#ffffff;
					        border-radius: 15px;
					    }
					    .navbar-inverse .navbar-nav>.open>a, .navbar-inverse .navbar-nav>.open>a:focus, .navbar-inverse .navbar-nav>.open>a:hover{
					        background-color:#0844b1;
					        color:#ffffff;
					        border-radius: 15px;
					    }
					    .navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form{
					        border:0px;
					    }
					    .dropdown>li>a{
					        border-radius:15px;
					    }
					    .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover{
					        color: #1422ce;
                            text-decoration: none;
                            background-color: #ffffff;
                            outline: 0;
					    }
					    @media (max-width: 767px){
                            .navbar-inverse .navbar-nav .open .dropdown-menu .divider {
                                background-color: #ffffff;
                                cursor:pointer;
                            }
					    }
					    @media (max-width: 767px){
					        .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a, .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:focus, .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:hover{
            					color: #fff;
                                background-color: rgba(8, 8, 8, 0.01);
					        }
					        .navbar-nav .open .dropdown-menu{
			                    background-color: #0b2e6d;
				                border-radius:15px;
					        }
					    }


					</style>
		    <nav class="navbar  navbar-inverse" style="background-color:rgba(34, 34, 34, 0);border:0px;">
                <div class="container-fluid">
                    <div class="tg-logoandnoticeboard">
							<strong class="tg-logoXXX">
							    <center>
							<a href="{!! \Config::get('app.url_base') !!}/index">
							<img src="{!! \Config::get('app.url') !!}/images/logow.png" alt="University of Education and knowledge" style="margin-bottom:10px;">
							</a>
								</center>
							</strong>

					</div>

                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle1 navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background-color:#11317B;">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                      <ul class="nav navbar-nav" style="background-color: #062994;border-radius:15px;color: #ffffff;margin:0 auto;width:100%;">

                        <li class="dropdown <?php if($currentPath == 'about-us'|| $currentPath == 'overview'|| $currentPath == 'what-sets-us-apart'|| $currentPath == 'message-from-the-principal'||  $currentPath == 'our-mission'){ echo 'active';} ?>">
                        <a class="dropdown-toggle" data-toggle="dropdown"  href="{!! \Config::get('app.url_base') !!}/about-us">About Us  <span class="caret"></span></a>
						 <ul class="dropdown-menu">
                            <li class="<?php if($currentPath == 'message-from-the-principal'){echo 'active';}?> ">
                                <a href="{!! \Config::get('app.url_base') !!}/message-from-the-principal">Message from the Principal</a>
                            </li>
                            <li class="divider"></li>
							<li class="<?php if($currentPath == 'overview'){echo 'active';}?>">
							    <a href="{!! \Config::get('app.url_base') !!}/overview">Overview</a>
						    </li>
							<li class="divider"></li>
							<li class="<?php if($currentPath == 'what-sets-us-apart'){echo 'active';}?>">
							    <a href="{!! \Config::get('app.url_base') !!}/what-sets-us-apart">What sets us apart</a>
							</li>
							<li class="divider"></li>
							<li class="<?php if($currentPath == 'our-mission'){echo 'active';}?>">
							    <a href="{!! \Config::get('app.url_base') !!}/our-mission">Mission, Vision & Aims</a>
							</li>
							<li class="divider"></li>
							<li>
							    <a href="http://www.rsicollege.org/" target="_blank">Russell Square International College</a>
							</li>
                          </ul>
                        </li>

                        <li class="divider"></li>

                        <li class="dropdown <?php if($currentPath == 'admissions'|| $currentPath == 'admission-procedure'|| $currentPath == 'schedule-a-visit'|| $currentPath == 'faq'||  $currentPath == 'fee-structure'){ echo 'active';} ?>">
                        <a class="dropdown-toggle" data-toggle="dropdown"  href="{!! \Config::get('app.url_base') !!}/admissions">
						 Admissions  <span class="caret"></span></a>
						 <ul class="dropdown-menu">
                            <li class="<?php if($currentPath == 'admission-procedure'){echo 'active';}?> ">
                                <a href="{!! \Config::get('app.url_base') !!}/admission-procedure">Admissions Procedure</a>
                            </li>
                            <li class="divider"></li>
							<li class="<?php if($currentPath == 'schedule-a-visit'){echo 'active';}?>">
							    <a href="{!! \Config::get('app.url_base') !!}/schedule-a-visit">Schedule A Visit</a>
						    </li>
							<li class="divider"></li>
							<li class="<?php if($currentPath == 'faq'){echo 'active';}?>">
							    <a href="{!! \Config::get('app.url_base') !!}/faq">FAQ</a>
							</li>
							<li class="divider"></li>
							<li class="<?php if($currentPath == 'school-calendar'){echo 'active';}?>">
							    <a href="{!! \Config::get('app.url_base') !!}/school-calendar">School Calendar</a>
							</li>
							<li class="divider"></li>
							<li class="<?php if($currentPath == 'fee-structure'){echo 'active';}?>">
							    <a href="{!! \Config::get('app.url_base') !!}/fee-structure">Fee Structure</a>
							</li>
                          </ul>
                        </li>

                        <li class="divider"></li>

                        <li class="dropdown <?php if($currentPath == 'academics'|| $currentPath == 'toddler'||  $currentPath == 'message-from-primary-head'|| $currentPath == 'play-school-juhu'|| $currentPath == 'ib-pyp-schools-mumbai'|| $currentPath == 'ib-myp-schools-mumbai'||  $currentPath == 'ib-diploma-school-mumbai'||  $currentPath == 'college-counseling'){ echo 'active';} ?>">
                          <a class="dropdown-toggle" data-toggle="dropdown"  href="{!! \Config::get('app.url_base') !!}/academics">
						 Academics  <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li  class="<?php if($currentPath == 'message-from-primary-head'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/message-from-primary-head">Message from the Head of Primary</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'toddler'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/toddler">Toddler</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'play-school-juhu'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/play-school-juhu">Play School</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'ib-pyp-schools-mumbai'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/ib-pyp-schools-mumbai">Early Years & Primary Years</a></li>
                            <li class="divider"></li>
                           <!-- <li  class="<?php if($currentPath == 'ib-myp-schools-mumbai'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/ib-myp-schools-mumbai">Middle Years & IGCSE</a></li>-->
                            <li  class="<?php if($currentPath == 'ib-myp-schools-mumbai'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/ib-myp-schools-mumbai">Middle Years</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'ib-diploma-school-mumbai'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/ib-diploma-school-mumbai">Diploma</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'college-counseling'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/college-counseling">College Counseling</a></li>
                          </ul>
                        </li>

                        <li class="divider"></li>

                        <li class="dropdown <?php if($currentPath == 'expeditions-camps' || $currentPath == 'campus-life'|| $currentPath == 'library'|| $currentPath == 'computer-centres'|| $currentPath == 'science-technology-labs'|| $currentPath == 'sports'||  $currentPath == 'music-dance-theatre'||  $currentPath == 'visual-arts'){ echo 'active';} ?>">
                          <a class="dropdown-toggle" data-toggle="dropdown"  href="{!! \Config::get('app.url_base') !!}/campus-life">
						 Campus Life  <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li  class="<?php if($currentPath == 'library'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/library">Libraries</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'computer-centres'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/computer-centres">Computer Centres</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'science-technology-labs'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/science-technology-labs">Science & Technology Labs</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'Sports & Recreation'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/sports">Sports & Recreation</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'music-dance-theatre'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/music-dance-theatre">Music, Dance & Theatre</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'visual-arts'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/visual-arts">Visual Arts</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'emun'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/emun">Ecole Model United Nations (EMUN)</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'expeditions-camps'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/expeditions-camps">CAS Programme</a></li>
                            <li class="divider"></li>
                            <li><a href="http://www.ecolemun.com/" target="_blank">EMUN</a></li>
							<li class="divider"></li>
							<li><a href="https://vimeo.com/88049971" target="_blank">Vimeo Ftloac</a></li>
							<li class="divider"></li>
							<li><a href="https://vimeo.com/78249675" target="_blank">Vimeo Cotw</a></li>
                          </ul>
                        </li>

                        <li class="divider"></li>

                        <li class="dropdown <?php if($currentPath == 'expeditions-camps' || $currentPath == 'career-opportunities'|| $currentPath == 'ceic-center'){ echo 'active';} ?>">
                          <a class="dropdown-toggle" data-toggle="dropdown"  href="{!! \Config::get('app.url_base') !!}/faculty-and-staff">
						 Faculty &amp; Career Openings <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li  class="<?php if($currentPath == 'toddler'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/career-opportunities">Career Opportunities</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'toddler'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/ceic-center">CEIC Center</a></li>
                          </ul>
                        </li>

                        <li class="divider"></li>

                        <li class="<?php if($currentPath == 'alumni'){ echo 'active';} ?>">
						    <a href="{!! \Config::get('app.url_base') !!}/alumni">Alumni</a>
						</li>

                        <li class="divider"></li>

                        <li class="dropdown <?php if($currentPath == 'news' || $currentPath == 'newsletter'|| $currentPath == 'events'|| $currentPath == 'youtube'){ echo 'active';} ?>">
                          <a class="dropdown-toggle" data-toggle="dropdown"  href="{!! \Config::get('app.url_base') !!}/news">
						 News <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li  class="<?php if($currentPath == 'news'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/news">News</a></li>
                            <?php /*?>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'newsletter'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/newsletter">Newsletter</a></li>
                            <?php */?>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'events'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/events">Events</a></li>
                            <li class="divider"></li>
                            <li  class="<?php if($currentPath == 'youtube'){echo 'active';}?>" ><a href="{!! \Config::get('app.url_base') !!}/youtube">Youtube</a></li>
                          </ul>
                        </li>

                        <li class="divider"></li>

                        <li class="<?php if($currentPath == 'contact-us'){ echo 'active';} ?>">
							<a href="{!! \Config::get('app.url_base') !!}/contact-us">Contact Us</a>
						</li>


                      </ul>
                      <!--<ul class="nav navbar-nav navbar-left">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                      </ul>-->
                    </div>
                </div>
            </nav>
			<!------------------------------------------------------------------------------------------------>


					</div>
				</div>
			</div>
		</header>
		<!--************************************
				Header End
		*************************************-->

		<header id="tg-header" class="tg-header tg-headervtwo tg-haslayout hidden-md">
			<div class="clearfix"></div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="tg-navigationarea">
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-3 lg3">
							<strong class="tg-logo">
							<a href="{!! \Config::get('app.url_base') !!}/index">
							<img src="{!! \Config::get('app.url') !!}/images/logow.png" alt="University of Education and knowledge" class="img-responsive"></a></strong>
						</div>

							<!--------Desktop menu-------------->
							<div class="col-xs-6 col-sm-12 col-md-5 col-lg-4">
							<div class="tg-navigationandsearch">
								<nav id="tg-nav" class="tg-nav">
									<div class="navbar-header">
										<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										</button>
									</div>
									<div id="tg-navigation" class="collapse navbar-collapse tg-navigation" style="width:471px;">
										<ul class="main-ul">
										<li style="margin:0px;" class="menu-item-has-children current-menu-item " >
											<a href="{!! \Config::get('app.url_base') !!}/about-us"  <?php if($currentPath == 'about-us'|| $currentPath == 'overview'|| $currentPath == 'what-sets-us-apart'|| $currentPath == 'message-from-the-principal'||  $currentPath == 'our-mission'){ echo 'style="color:#E2BC82;"';} ?> >
											    About Us</a>
											<ul class="sub-menu">
												<li><a href="{!! \Config::get('app.url_base') !!}/message-from-the-principal">Message from the Principal</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/overview">Overview</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/what-sets-us-apart">What sets us apart</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/our-mission">Mission, Vision & Aims</a></li>
												<li><a href="http://www.rsicollege.org/" target="_blank">Russell Square International College</a></li>
											</ul>

										</li>
										<li style="margin:0px;" class="menu-item-has-children">
											<a href="{!! \Config::get('app.url_base') !!}/admissions"  <?php if($currentPath == 'admissions'|| $currentPath == 'admission-procedure'|| $currentPath == 'schedule-a-visit'|| $currentPath == 'faq'|| $currentPath == 'school-calendar'||  $currentPath == 'fee-structure'||  $currentPath == 'apply-for-admission'){ echo 'style="color:#E2BC82;"';} ?>>Admissions</a>
											<ul class="sub-menu">
												<li><a href="{!! \Config::get('app.url_base') !!}/admission-procedure">Admissions Procedure</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/schedule-a-visit">Schedule a Visit</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/faq">FAQs</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/school-calendar">School Calendar</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/fee-structure">Fee Structure</a></li>
										    </ul>
										</li>
										<li style="margin:0px;" class="menu-item-has-children">
											<a href="{!! \Config::get('app.url_base') !!}/academics" <?php if($currentPath == 'message-from-primary-head'|| $currentPath == 'academics'|| $currentPath == 'toddler'|| $currentPath == 'play-school-juhu'|| $currentPath == 'ib-pyp-schools-mumbai'|| $currentPath == 'ib-myp-schools-mumbai'||  $currentPath == 'ib-diploma-school-mumbai'||  $currentPath == 'college-counseling'){ echo 'style="color:#E2BC82;"';} ?>>Academics</a>
											<ul class="sub-menu">
											    <li><a href="{!! \Config::get('app.url_base') !!}/message-from-primary-head">Message from the Head of Primary</a></li>
                                                <li><a href="{!! \Config::get('app.url_base') !!}/toddler">Toddler</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/play-school-juhu">Play School</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/ib-pyp-schools-mumbai">Early Years & Primary Years</a></li>
											<!--	<li><a href="{!! \Config::get('app.url_base') !!}/ib-myp-schools-mumbai">Middle Years & IGCSE</a></li>-->
    										    <li><a href="{!! \Config::get('app.url_base') !!}/ib-myp-schools-mumbai">Middle Years</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/ib-diploma-school-mumbai">Diploma</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/college-counseling">College Counseling</a></li>
											</ul>
										</li>
										<li style="margin:0px;" class="menu-item-has-children">
										<a href="{!! \Config::get('app.url_base') !!}/campus-life" <?php if($currentPath == 'expeditions-camps' || $currentPath == 'campus-life'|| $currentPath == 'library'|| $currentPath == 'computer-centres'|| $currentPath == 'science-technology-labs'|| $currentPath == 'sports'||  $currentPath == 'music-dance-theatre'||  $currentPath == 'visual-arts'){ echo 'style="color:#E2BC82;"';} ?>>Campus Life</a>
											<ul class="sub-menu">
												<li><a href="{!! \Config::get('app.url_base') !!}/library">Libraries</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/computer-centres">Computer Centres</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/science-technology-labs">Science & Technology Labs</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/sports">Sports & Recreation</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/music-dance-theatre">Music, Dance & Theatre</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/visual-arts">Visual Arts</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/emun">Ecole Model United Nations (EMUN)</a></li>
												<li><a href="{!! \Config::get('app.url_base') !!}/expeditions-camps">CAS Programme</a></li>
												<li><a href="http://www.ecolemun.com/" target="_blank">EMUN</a></li>
												<li><a href="https://vimeo.com/88049971" target="_blank">Vimeo Ftloac</a></li>
												<li><a href="https://vimeo.com/78249675" target="_blank">Vimeo Cotw</a></li>
									    	</ul>
										</li>
										</ul>
										</li>
									</ul>
									</div>
								</nav>




							</div>
						</div>

						<div class="col-xs-6 col-sm-12 col-md-5 col-lg-5 lg5">
							<div class="tg-navigationandsearch">
								<nav id="nav" role="navigation">
									<a href="#nav" title="Show navigation">Show navigation</a>
									<a href="#" title="Hide navigation">Hide navigation</a>
								  <ul class="clearfix">
									<li>
										<!--<div class="tg-searchbox">
											<a id="tg-btnsearch" class="tg-btnsearch" href="javascript:void(0);"><i class="icon-magnifier"></i></a>
											<form class="tg-formtheme tg-formsearch">
												<fieldset>
												<input type="search" name="search" class="form-control" placeholder="Start Your Search Here">
												</fieldset>
											</form>
										</div>-->
										<div class="tg-searchbox">
											<form action="{{ url('search') }}" method="post">
											<button type="submit" class="tg-btnsearch"><i class="icon-magnifier" style="height:42px;"></i></button>
											<div  class="tg-formtheme tg-formsearch" >
												<fieldset>
												<input type="text" name="search" id="search" class="form-control" placeholder="Search Here" required style="height:42px;">
												{{ csrf_field() }}
												</fieldset>
												</div>
											</form>
										</div>
									</li>

									<li class="menu-item-has-children">
										<a href="{!! \Config::get('app.url_base') !!}/faculty-and-staff" <?php if($currentPath == 'faculty-and-staff' || $currentPath == 'career-opportunities'|| $currentPath == 'ceic-center'){ echo 'style="color:#008CE0;"';} ?>>Faculty & Career Openings</a>
										<ul class="sub-menu">
											<li><a href="{!! \Config::get('app.url_base') !!}/career-opportunities">Career Opportunities</a></li>
											<li><a href="{!! \Config::get('app.url_base') !!}/ceic-center">CEIC Center</a></li>
										</ul>
									</li>
									<li> <a href="{!! \Config::get('app.url_base') !!}/alumni"  <?php if($currentPath == 'alumni'){ echo 'style="color:#008CE0;"';} ?>>Alumni</a></li>

									<li class="menu-item-has-children">
										<a href="{!! \Config::get('app.url_base') !!}/news" <?php if($currentPath == 'news' || $currentPath == 'newsletter'|| $currentPath == 'events'|| $currentPath == 'youtube'){ echo 'style="color:#008CE0;"';} ?> >News </a>
										<ul class="sub-menu">
											<li><a href="{!! \Config::get('app.url_base') !!}/news">Current News</a></li>
											<?php /*?><li><a href="{!! \Config::get('app.url_base') !!}/newsletter">School Newsletter</a></li><?php */?>
											<li><a href="{!! \Config::get('app.url_base') !!}/events">Events</a></li>
											<li><a href="{!! \Config::get('app.url_base') !!}/youtube">Youtube</a></li>
										</ul>
									</li>

									<li><a href="{!! \Config::get('app.url_base') !!}/contact-us" <?php if($currentPath == 'contact-us'){ echo 'style="color:#008CE0;"';} ?>>Contact Us</a></li>
								</ul>
								</nav>
							</div>

							<script>
								$( function()
								{
									$( '#nav li:has(ul)' ).doubleTapToGo();
								});
							</script>

							<script type="text/javascript">
								$(document).ready(function(){
									//$("#enquirypopup").modal('show');

								});
							</script>

						</div>
					</div>
				</div>
			</div>
		</header>
		<!--************************************
				Header End
		*************************************-->

