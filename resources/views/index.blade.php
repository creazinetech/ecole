@include('includes.index-header')
@include('includes.index-slider')

<main id="tg-main" class="tg-main tg-haslayout">
			<div class="container">
				<div class="row">
					<div id="tg-twocolumns" class="tg-twocolumns">

					   	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						<?php
							$Pages = DB::table('pages')->where('page_id','=','14')->get();
							foreach($Pages as $Page){ }
						?>
							<section class="tg-sectionspace tg-haslayout">
								<div class="tg-shortcode tg-welcomeandgreeting tg-welcomeandgreeting-v2">
									<figure>
									<img src="{!! \Config::get('app.url') !!}/images/pages/<?php echo $Page->page_img; ?>" alt="image description" style="height:170px;">
									</figure>
									<div class="tg-shortcodetextbox">
										<div class="tg-content-text">
											<p><?php echo $Page->short_desc; ?></p>
										</div>

										<div class="tg-btnpluslogo">
											<a class="tg-btn" href="{!! \Config::get('app.url_base') !!}/message-from-the-principal">read more</a>
										</div>
									</div>
								</div>
							</section>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3  tg-content-text">
						    <section class="tg-sectionspace tg-haslayout">
                			<img src="{!! \Config::get('app.url') !!}/images/popup_banner.jpg" style="width: 100%;">
                			<center style="margin-top: 0px;padding: 2px;font-size:12px!important;">By the The Times of India, School Survey 2017</center>
                			</section>
                		</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="font-size:16px;">
						    <?php
							$Testimonials = DB::table('testimonials')->where('t_status','=','Active')->where('t_soindex','=','1')->get();
							foreach($Testimonials as $Testimonial){ }
							?>

					    	<div class="tg-shortcode tg-welcomeandgreeting tg-welcomeandgreeting-v2 tg-content-text">
					    	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
						    <i class="fa fa-quote-left fa-2x" aria-hidden="true"></i>
						    </div>
						    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11" style="font-size:16px;">
                            <?php echo $Testimonial->t_content; ?>
                            </div>
                            <hr>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"> </div>
                            <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                            <strong>
                            <?php echo '- '.ucwords($Testimonial->tby); ?>
                            <small><?php echo ucwords($Testimonial->tdetails); ?></small>
                            </strong>
                            <br>
                            <small><a class="pull-right" style="color:black;font-weight:bold;" href="{!! \Config::get('app.url_base') !!}/testimonials">Read More</a></small>
                            </div>
					    	</div>

						</div>

						</div>

					<!--	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    <span>Quick Admission Enquiry</span>
						    <div class="">
					        <div class="status_msg"></div><br>
							<div class="form-group col-sm-12">
								<input class="form-control" name="qname" id="qname" placeholder="Enter Name" type="text">
								<span id="qnameerr" style="color:red;display:none;"> Please enter name.</span>
							</div>
							<div class="form-group col-sm-12">
								<input class="form-control" name="qemail" id="qemail" placeholder="Enter Email" type="email">
								<span id="qemailerr" style="color:red;display:none;"> Please enter email id.</span>
							</div>

							<div class="form-group col-sm-12">
								<input class="form-control" name="qphone" id="qphone" placeholder="Enter Phone" onkeypress="return isNumberKey(event)" type="text">
								<span id="qphoneerr" style="color:red;display:none;"> Please enter 10 digit phone number.</span>
							</div>

							<div class="form-group col-sm-12" style="display:none;">
								<textarea class="form-control" id="qmsg" name="qmsg" rows="4" placeholder="Enter Message"> </textarea>
								<span id="qmsgerr" style="color:red;display:none;"> Please enter message.</span>
							</div>

							<div class="form-group  col-sm-12">
								<input id="rannumber" name="rannumber" class="captcha" value="6TtrgR" type="hidden">
								<div class="captcha col-md-4" style="border: 2px solid green; width: 150px; line-height:1.4; height: 45px; text-align: center; vertical-align: middle;font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">6TtrgR</div>
								<div class="col-md-2" style="height:40px;margin-bottom:5px;">
								  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;">
								  <i class="fa fa-refresh"></i>
								  </a>
								  <br><br>
								</div>
							  <input class="form-control" id="captcha_code2" name="captcha_code2" placeholder="Enter Captcha" style="text-transform:none;margin-top:10px;" type="text">
								<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
							</div>

							<div class="col-10 fix ">
								<div class="success1"></div>
								<div class="error1"></div>
								 <input name="_token" value="rb7p3dAhL7qGI3c0vreNriprojbr84lzRXRqpBct" type="hidden">

								<div class="form-group col-sm-12">
									<button type="submit" class="btn pull-left sub_popenquiry" name="job-submit" id="job-submit" style="background-color:rgba(23, 110, 128, 1)">Send</button>

								</div>
							</div>


					</div>

					    </div>-->

				</div>
			</div>
		</div>

		</br></br>
		<div class="container tg-content-text" style="background-color:#008CE0;width:100%;">
		<div class="row" style="background-color:#008CE0;padding: 30px;">

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<h3 class="center-block subheading2 white-text">FACEBOOK</h3>
			<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FEcoleMondialeOfficial&tabs=timeline&width=400&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="400" height="480" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			</div>

    		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 white-text">
    			<h3 class="center-block subheading2 white-text">CURRENT NEWS</h3>
    			<div class="bg">
    			<div class="tg-widgetcontent bg">
    				<?php
    					$News = DB::table('news')->where('status','=','Active')->orderBy('id','desc')->limit(2)->get();
    					foreach($News as $New){
    				?>
    				<article class="tg-campus tg-campusleftthumb">
    					<figure class="tg-featuredimg">
    						<a href="{!! \Config::get('app.url') !!}/view-news/<?php echo $New->id;?>">
    							<img src="{!! \Config::get('app.admin') !!}/images/news/<?php echo $New->thumbnail;?>" style="height:70px;width:70px;padding-left:0px">
    						</a>
    					</figure>
    					<div class="tg-campuscontent">
    						<div class="tg-campustitle">
    							<p style="font-size: 14px !important;text-align: justify !important;line-height: 34px !important;"><?php echo $New->title;?></p>
    						</div>
    						<span class="tg-price" style="font-size: 18px !important;text-align: justify !important;line-height: 34px !important;">
    						    <?php echo $New->short_desc;?><br>
    						<a href="{!! \Config::get('app.url') !!}/view-news/<?php echo $New->id;?>" class="button"> Read More</a>
    						</span>
    					</div>
    				</article>
    				<?php } ?>
    			</div>
    			</div>
    		</div>

    		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    			<h3 class="center-block subheading2 white-text">TWITTER</h3>

    			<div class="twitter" style="background-color:#008CE0;">

    				<a class="twitter-timeline"  height="480" href="https://twitter.com/EcoleMondiale" data-chrome="nofooter noborders"> Tweets by @bhujbal111 </a>


    			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

    			</div>
    		</div>

		</div>
		</div>


		</div>

@include('includes.index-footer')