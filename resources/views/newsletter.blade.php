<?php
	$Pages = DB::table('pages')->where('page_id','=','42')->get();
	foreach($Pages as $Page){ }
	
?>	
@include('includes.index-header')

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content-text">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<?php if(isset($Page->brief_desc) && $Page->brief_desc !="") { ?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
											<?php echo $Page->brief_desc; ?>
										</div>
										<?php } ?>
										
										<div class="panel panel-default category-panel ">
                                           <div class="panel-footer">
                                                    <form action="{{ url('newsletter') }}" method="post">
                                                    <select id="filterby" name="filterby" class="input-sm">
                                                        <option value="pdfname" <?php if(isset($filterby) && $filterby=='pdfname'){ echo 'selected'; } ?>>Title</option>
                                                        <option value="created_at" <?php if(isset($filterby) && $filterby=='created_at'){ echo 'selected'; } ?>>Publish Date</option>
                                                    </select>
                                                    <select id="orderby" name="orderby" class="input-sm">
                                                        <option value="asc" <?php if(isset($orderby) && $orderby=='asc'){ echo 'selected'; } ?>>Asc</option>
                                                        <option value="desc" <?php if(isset($orderby) && $orderby=='desc'){ echo 'selected'; } ?>>Desc</option>
                                                    </select>
                                                    <div class="btn-group btn-group-sm">
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-info" style="margin-left:5px;">Filter &nbsp;</button>
                                                        <a class="btn btn-square" href="{!! \Config::get('app.url_base') !!}/newsletter" style="margin-left:5px;background-color:#1AC5A6;">Clear &nbsp;</a>
                                                    </div> 
                                                    </form>
                                           </div>
                                        </div>
										
										<?php 
										//$Newsletters = DB::table('newsletter')->where('status','=','Active')->orderBy('nlid','desc')->get();
										
										$Newsletters = DB::table('newsletter')->where('status','=','Active')->orderBy('nlid','desc')->paginate(10);
										if(!isset($filterby) && !isset($orderby) && count($Newsletters)>0){
											foreach($Newsletters as $row){
										?>
										<blockquote>
										<div class="col-md-12 tg-content-text">
											<div class="col-md-1 col-sm-2 col-xs-12">
												<center><img src="{!! \Config::get('app.url') !!}/images/pdf.png" class="img-responsive" style="padding:0px;"></center>
											</div>
											<div class="col-md-6 col-sm-10 col-xs-12">
												<b><?php echo ucwords($row->pdfname); ?></b>
											</div>
											<div class="col-md-5 col-sm-12 col-xs-12">
											<!--	<span><i class="fa fa-file"></i> 
												<?php echo round($row->pdfsize) . ' bytes';?>
												</span>
												&nbsp;&nbsp;
												<span class="spancount<?php echo $row->nlid ?>"><i class="fa fa-download"></i> <?php  echo $row->downloads; ?></span>-->
												<?php if( $row->stock==1){ ?>
												<a class="fillink blue downlink pull-right" data-id="<?php echo $row->nlid ?>" download="<?php echo $row->pdf; ?>" target="_blank" href="{!! \Config::get('app.admin') !!}/downloads/<?php echo $row->pdf; ?>"><i class="fa fa-download"></i> Download</a> 
											    <?php } ?>
											    <?php if( $row->stock==2){ ?>
											    <a href="{{URL::to('view-newsletter',array($row->nlid))}}" data-id="<?php echo $row->nlid ?>"  class="downlink pull-right"><i class="fa fa-file"></i> View</a>
											    <?php } ?>
											</div>
										</div>
									    <div class="clearfix"></div>
									    </blockquote>
										<?php
																					
										}
										echo '<div class="col-md-12 tg-content-text"><center>'.$Newsletters->render().'</center></div>';
										} 
										?>
										<?php 
										if(isset($filterby) && isset($orderby)){
										    $FilterNewsletters = DB::table('newsletter')->where('status','=','Active')->orderBy($filterby,$orderby)->get();
											foreach($FilterNewsletters as $row1){
										?>
										<blockquote>
										<div class="col-md-12 tg-content-text"> 
											<div class="col-md-1 col-sm-2 col-xs-12">
												<center><img src="{!! \Config::get('app.url') !!}/images/pdf.png" class="img-responsive" style="padding:0px;"></center>
											</div> 
											<div class="col-md-6 col-sm-10 col-xs-12">
												<b><a href="{{URL::to('view-newsletter',array($row1->nlid))}}"><?php echo ucwords($row1->pdfname); ?></a></b>
											</div>
											<div class="col-md-5 col-sm-12 col-xs-12">
											<!--	<span><i class="fa fa-file"></i> 
												<?php echo round($row1->pdfsize) . ' bytes'; ?>
												</span>
												&nbsp;&nbsp;
												<span class="spancount<?php echo $row1->nlid ?>"><i class="fa fa-download"></i> <?php  echo $row1->downloads; ?></span> -->
												
												<a class="fillink blue downlink pull-right" data-id="<?php echo $row1->nlid ?>" download="<?php echo $row1->pdf; ?>" target="_blank" href="{!! \Config::get('app.admin') !!}/downloads/<?php echo $row1->pdf; ?>"><i class="fa fa-download"></i> Download</a> 
											</div>
										</div>
									    <div class="clearfix"></div>
									    </blockquote>
										<?php
																					
										}
    									//	echo '<div class="col-md-12 tg-content-text"><center>'.$FilterNewsletters->render().'</center></div>';
										} 
										?>
										
									</div>
								</div>
							</section>
						</div>
						<script>
						$(document).ready(function(){
							$('.downlink').click(function(){
								var Nlid = $(this).attr('data-id');
								$.ajax({
									type: 'get',
									headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
									url: 'index/downloadcount',
									data: 'Nlid='+Nlid,
									success: function (data) {
										if(data >= 0){
											$('.spancount'+Nlid).html('<i class="fa fa-download"></i> '+data);
										}
									}
								});	
							});
						});
						</script>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')