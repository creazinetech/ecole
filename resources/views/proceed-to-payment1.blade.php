@include('includes.index-header')
<?php
	if(isset($Data)){
	}
?>

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">

	<img src="{!! \Config::get('app.url') !!}/images/default-header.jpg" class="img-responsive header-banner" > 
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    Admission Procedure
                </div>
            </h2>
        </div>
    </div>
	  
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2>Proceed Payment</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
											<div class="tg-contactus tg-contactusvone">
												
												<h5>The mandatory fields are marked with an * </h5>
												<hr>
												<div class="row">
												<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id=""   enctype='multipart/form-data'  action="{{ url('checkout1') }}" >
												<fieldset>
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="alert alert-info">Billing Details</div>
													    <input type="hidden" name="currency"  id="currency" value="INR"/>
													    <input type="hidden" name="tid" id="tid" readonly />
														<input type="hidden" name="redirect_url" value="{{ url('success') }}">
														<input type="hidden" name="cancel_url" value="{{ url('failure') }}"/> 
														<input type="hidden" name="name" id="name" value="<?php echo $Data['name'];?>">
														<input type="hidden" name="dob" id="dob" value="<?php echo $Data['dob'];?>">
														<input type="hidden" name="address" id="address" value="<?php echo $Data['address'];?>">
														<input type="hidden" name="nopskul" id="nopskul" value="<?php echo $Data['nopskul'];?>">
														<input type="hidden" name="grade" id="grade" value="<?php echo $Data['grade'];?>">
														<input type="hidden" name="gradetjoin" id="gradetjoin" value="<?php echo $Data['gradetjoin'];?>">
														<input type="hidden" name="year" id="year" value="<?php echo $Data['year'];?>">
														<input type="hidden" name="nparents" id="nparents" value="<?php echo $Data['nparents'];?>">
														<input type="hidden" name="designation" id="designation" value="<?php echo $Data['designation'];?>">
														<input type="hidden" name="organization" id="organization" value="<?php echo $Data['organization'];?>">
														<input type="hidden" name="occupation" id="occupation" value="<?php echo $Data['occupation'];?>">
														<input type="hidden" name="offadd" id="offadd" value="<?php echo $Data['offadd'];?>">
														<input type="hidden" name="city" id="city" value="<?php echo $Data['city'];?>">
														<input type="hidden" name="state" id="state" value="<?php echo $Data['state'];?>">
														<input type="hidden" name="country" id="country" value="<?php echo $Data['country'];?>">
														<input type="hidden" name="pincode" id="pincode" value="<?php echo $Data['pincode'];?>">
														<input type="hidden" name="restel" id="restel" value="<?php echo $Data['restel'];?>">
														<input type="hidden" name="mnfather" id="mnfather" value="<?php echo $Data['mnfather'];?>">
														<input type="hidden" name="mnmother" id="mnmother" value="<?php echo $Data['mnmother'];?>">
														<input type="hidden" name="emailf" id="emailf" value="<?php echo $Data['emailf'];?>">
														<input type="hidden" name="emailm" id="emailm" value="<?php echo $Data['emailm'];?>">
														<input type="hidden" name="offtel" id="offtel" value="<?php echo $Data['offtel'];?>">
														<input type="hidden" name="passport" id="passport" value="<?php echo $Data['passport'];?>">
														<input type="hidden" name="ourskul" id="ourskul" value="<?php echo $Data['ourskul'];?>">
													
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<label>Order Id:</label>
														<input type="text" class="form-control"  name="order_id" id="order_id" readonly value="<?php echo $Data['ordercode'];?>">
														</div>
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Bill Amount:</label>
															<input type="text" class="form-control" name="amount" id="amount" readonly value="<?php echo $Data['amount'];?>">
														</div>
														<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<label>Bill Name:</label>
															<input type="text" class="form-control" name="billing_name" id="billing_name" placeholder="Bill Name*"  value="<?php echo $Data['nparents'];?>">
															<span id="billing_nameerr" style="color:red;display:none;"> Please Enter Name On Bill.</span>
														</div>
														<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<label>Bill Address:</label>
															<textarea type="text" class="form-control" name="billing_address" id="billing_address" placeholder="Bill Address*"><?php echo $Data['address'];?></textarea>
															<span id="billing_addresserr" style="color:red;display:none;"> Please Enter Bill Address.</span>
														</div>
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Bill Country:</label>
															<input type="text" class="form-control" name="billing_country" id="billing_country" placeholder="Bill Country*"  value="<?php echo $Data['country'];?>">
															<span id="billing_countryerr" style="color:red;display:none;"> Please Enter Country Name.</span>
														</div>
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label for="sel1">Bill State:*</label>
															<select class="form-control" id="billing_state" name="billing_state">
																<option value=''>Bill State?</option>
																<option value='Andhra Pradesh' <?php if($Data['state'] == 'Andhra Pradesh'){echo 'selected';} ?> >Andhra Pradesh</option>
																<option value='Arunachal Pradesh' <?php if($Data['state'] == 'Arunachal Pradesh'){echo 'selected';} ?>>Arunachal Pradesh</option>
																<option value='Assam' <?php if($Data['state'] == 'Assam'){echo 'selected';} ?>>Assam </option>
																<option value='Bihar' <?php if($Data['state'] == 'Bihar'){echo 'selected';} ?>>Bihar</option>
																<option value='Chhattisgarh' <?php if($Data['state'] == 'Chhattisgarh'){echo 'selected';} ?> >Chhattisgarh    </option>
																<option value='Goa' <?php if($Data['state'] == 'Goa'){echo 'selected';} ?> >Goa</option>
																<option value='Gujarat' <?php if($Data['state'] == 'Gujarat'){echo 'selected';} ?> >Gujarat</option>
																<option value='Haryana' <?php if($Data['state'] == 'Andhra Pradesh'){echo 'selected';} ?> >Haryana </option>
																<option value='Himachal Pradesh' <?php if($Data['state'] == 'Haryana'){echo 'selected';} ?> >Himachal Pradesh</option>
																<option value='Jammu & Kashmir'<?php if($Data['state'] == 'Jammu & Kashmir'){echo 'selected';} ?> >Jammu & Kashmir</option>
																<option value='Jharkhand' <?php if($Data['state'] == 'Jharkhand'){echo 'selected';} ?>>Jharkhand </option>
																<option value='Karnataka' <?php if($Data['state'] == 'Karnataka'){echo 'selected';} ?>>Karnataka </option>
																<option value='Kerla' <?php if($Data['state'] == 'Kerla'){echo 'selected';} ?>>Kerla </option>
																<option value='Madhya Pradesh'<?php if($Data['state'] == 'Madhya Pradesh'){echo 'selected';} ?>>Madhya Pradesh</option>
																<option value='Maharashtra' <?php if($Data['state'] == 'Maharashtra'){echo 'selected';} ?>>Maharashtra  </option>
																<option value='Manipur' <?php if($Data['state'] == 'Manipur'){echo 'selected';} ?>>Manipur </option>
																<option value='Meghalaya' <?php if($Data['state'] == 'Meghalaya'){echo 'selected';} ?>>Meghalaya</option>
																<option value='Mizoram' <?php if($Data['state'] == 'Mizoram'){echo 'selected';} ?>> Mizoram</option>
																<option value='Nagaland' <?php if($Data['state'] == 'Nagaland'){echo 'selected';} ?>>Nagaland</option>
																<option value='Orissa' <?php if($Data['state'] == 'Orissa'){echo 'selected';} ?>>Orissa </option>
																<option value='Punjab'<?php if($Data['state'] == 'Punjab'){echo 'selected';} ?> >Punjab </option>
																<option value='Sikkim' <?php if($Data['state'] == 'Sikkim'){echo 'selected';} ?>>Sikkim </option>
																<option value='Tamilnadu' <?php if($Data['state'] == 'Tamilnadu'){echo 'selected';} ?>>Tamilnadu</option>
																<option value='Tripura' <?php if($Data['state'] == 'Tripura'){echo 'selected';} ?>>Tripura </option>
																<option value='UttarPradesh' <?php if($Data['state'] == 'UttarPradesh'){echo 'selected';} ?>>UttarPradesh </option>
																<option value='Uttaranchal' <?php if($Data['state'] == 'Uttaranchal'){echo 'selected';} ?>> Uttaranchal </option>
																<option value='WestBengal' <?php if($Data['state'] == 'WestBengal'){echo 'selected';} ?>>West Bengal</option>
																<option value='Outside India' <?php if($Data['state'] == 'Outside India'){echo 'selected';} ?>>Outside India</option>
															</select>
															<span id="billing_stateerr" style="color:red;display:none;"> Please Select State.</span>
														</div>	
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Bill City:</label>
															<input type="text" class="form-control" name="billing_city" id="billing_city" placeholder="City:*"  value="<?php echo $Data['city'];?>">
															<span id="billing_cityerr" style="color:red;display:none;"> Please Enter Bill City.</span>
														</div>
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Bill Pincode:</label>
															<input type="text" class="form-control" name="billing_zip" id="billing_zip" placeholder="pincode:*" onkeypress="return isNumberKey(event)" value="<?php echo $Data['pincode'];?>">
															<span id="billing_ziperr" style="color:red;display:none;"> Please Enter 6 Digit Pincode.</span>
														</div>
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Bill Telephone:</label>
															<input type="tel" class="form-control" name="billing_tel" id="billing_tel" placeholder="Bill Telephone:*" onkeypress="return isNumberKey(event)" value="<?php echo $Data['restel'];?>">
															<span id="billing_telerr" style="color:red;display:none;"> Please Enter 10 Digit Bill Telephone.</span>
														</div>
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Bill Email:</label>
															<input type="email" class="form-control" name="billing_email" id="billing_email" placeholder="Bill Email Address:*" value="<?php echo $Data['emailf'];?>">
															<span id="billing_emailerr" style="color:red;display:none;"> Please Enter Bill Email.</span>
														</div>
													</div>
													
													
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="alert alert-info">Shipping Details</div>
													
														<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<label>Shipping Name:</label>
															<input type="text" class="form-control" name="delivery_name" id="delivery_name" placeholder="Shipping Name*"  value="<?php echo $Data['nparents'];?>">
															<span id="delivery_nameerr" style="color:red;display:none;"> Please Enter Shipping Name.</span>
														</div>
														<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<label>Shipping Address:</label>
															<textarea type="text" class="form-control" name="delivery_address" id="delivery_address" placeholder="Shipping Address*"><?php echo $Data['address'];?></textarea>
															<span id="delivery_addresserr" style="color:red;display:none;"> Please Enter Shipping Address.</span>
														</div>
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Shipping Country:</label>
															<input type="text" class="form-control" name="delivery_country" id="delivery_country" placeholder="Shipping Country*" value="<?php echo $Data['country'];?>">
															<span id="delivery_countryerr" style="color:red;display:none;"> Please Enter Shipping Country.</span>
														</div>
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label for="sel1">Shipping State:*</label>
															<select class="form-control" id="delivery_state" name="delivery_state">
																<option value=''>Shipping State?</option>
																<option value='Andhra Pradesh' <?php if($Data['state'] == 'Andhra Pradesh'){echo 'selected';} ?> >Andhra Pradesh</option>
																<option value='Arunachal Pradesh' <?php if($Data['state'] == 'Arunachal Pradesh'){echo 'selected';} ?>>Arunachal Pradesh</option>
																<option value='Assam' <?php if($Data['state'] == 'Assam'){echo 'selected';} ?>>Assam </option>
																<option value='Bihar' <?php if($Data['state'] == 'Bihar'){echo 'selected';} ?>>Bihar</option>
																<option value='Chhattisgarh' <?php if($Data['state'] == 'Chhattisgarh'){echo 'selected';} ?> >Chhattisgarh    </option>
																<option value='Goa' <?php if($Data['state'] == 'Goa'){echo 'selected';} ?> >Goa</option>
																<option value='Gujarat' <?php if($Data['state'] == 'Gujarat'){echo 'selected';} ?> >Gujarat</option>
																<option value='Haryana' <?php if($Data['state'] == 'Andhra Pradesh'){echo 'selected';} ?> >Haryana </option>
																<option value='Himachal Pradesh' <?php if($Data['state'] == 'Haryana'){echo 'selected';} ?> >Himachal Pradesh</option>
																<option value='Jammu & Kashmir'<?php if($Data['state'] == 'Jammu & Kashmir'){echo 'selected';} ?> >Jammu & Kashmir</option>
																<option value='Jharkhand' <?php if($Data['state'] == 'Jharkhand'){echo 'selected';} ?>>Jharkhand </option>
																<option value='Karnataka' <?php if($Data['state'] == 'Karnataka'){echo 'selected';} ?>>Karnataka </option>
																<option value='Kerla' <?php if($Data['state'] == 'Kerla'){echo 'selected';} ?>>Kerla </option>
																<option value='Madhya Pradesh'<?php if($Data['state'] == 'Madhya Pradesh'){echo 'selected';} ?>>Madhya Pradesh</option>
																<option value='Maharashtra' <?php if($Data['state'] == 'Maharashtra'){echo 'selected';} ?>>Maharashtra  </option>
																<option value='Manipur' <?php if($Data['state'] == 'Manipur'){echo 'selected';} ?>>Manipur </option>
																<option value='Meghalaya' <?php if($Data['state'] == 'Meghalaya'){echo 'selected';} ?>>Meghalaya</option>
																<option value='Mizoram' <?php if($Data['state'] == 'Mizoram'){echo 'selected';} ?>> Mizoram</option>
																<option value='Nagaland' <?php if($Data['state'] == 'Nagaland'){echo 'selected';} ?>>Nagaland</option>
																<option value='Orissa' <?php if($Data['state'] == 'Orissa'){echo 'selected';} ?>>Orissa </option>
																<option value='Punjab'<?php if($Data['state'] == 'Punjab'){echo 'selected';} ?> >Punjab </option>
																<option value='Sikkim' <?php if($Data['state'] == 'Sikkim'){echo 'selected';} ?>>Sikkim </option>
																<option value='Tamilnadu' <?php if($Data['state'] == 'Tamilnadu'){echo 'selected';} ?>>Tamilnadu</option>
																<option value='Tripura' <?php if($Data['state'] == 'Tripura'){echo 'selected';} ?>>Tripura </option>
																<option value='UttarPradesh' <?php if($Data['state'] == 'UttarPradesh'){echo 'selected';} ?>>UttarPradesh </option>
																<option value='Uttaranchal' <?php if($Data['state'] == 'Uttaranchal'){echo 'selected';} ?>> Uttaranchal </option>
																<option value='WestBengal' <?php if($Data['state'] == 'WestBengal'){echo 'selected';} ?>>West Bengal</option>
																<option value='Outside India' <?php if($Data['state'] == 'Outside India'){echo 'selected';} ?>>Outside India</option>
															</select>
															<span id="delivery_stateerr" style="color:red;display:none;"> Please Select Shipping State.</span>
														</div>	
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Shipping City:</label>
															<input type="text" class="form-control" name="delivery_city" id="delivery_city" placeholder="City:*" value="<?php echo $Data['city'];?>">
															<span id="delivery_cityerr" style="color:red;display:none;"> Please Enter Shipping City.</span>
														</div>
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Shipping Pincode:</label>
															<input type="text" class="form-control" name="delivery_zip" id="shippincode" placeholder="pincode:*" onkeypress="return isNumberKey(event)"  value="<?php echo $Data['pincode'];?>">
															<span id="shippincodeerr" style="color:red;display:none;"> Please Enter 6 Digit Shipping Pincode.</span>
														</div>
														
														<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<label>Shipping Telephone:</label>
															<input type="tel" class="form-control" name="delivery_tel" id="delivery_tel" placeholder="Telephone:*" onkeypress="return isNumberKey(event)" value="<?php echo $Data['restel'];?>">
															<span id="delivery_telerr" style="color:red;display:none;"> Please Enter 10 Digit Shipping Telephone.</span>
														</div>
														<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<label>Delivery Notes:</label>
															<textarea class="form-control" name="deliverynotes" id="deliverynotes" placeholder="Delivery Notes:*"></textarea>
															<span id="billemailerr" style="color:red;display:none;"> Please Enter Delivery Note.</span>
														</div>
													</div>
													
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												    
														<div class="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
														    <img src="{!! \Config::get('app.url') !!}/images/cc-avenue.jpg" class="responsive">
														</div>
												    															
														<div class="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
															<input type="hidden" id="rannumber" name="rannumber" class="captcha">
															<div class="captcha col-md-4" style="border: 2px solid green; width: 150px; line-height:1.4; height: 45px; text-align: center; vertical-align: middle; font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">
															</div>
															<div class="col-md-2" style="height:40px;margin-bottom:5px;">
															  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;" >
															  <i class="fa fa-refresh"></i>
															  </a>
															  <br><br>
															</div>
														</div>
														<div class="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
														  <input class="form-control" id="captcha_code2" name="captcha_code2" placeholder="Enter Captcha" type="text"  style="text-transform:none;">
															<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
														</div>
														
														{{ csrf_field() }}
														<div class="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
														<button type="submit" class="button" name="job-submit" id="paynow" >Pay Now<i class="icon-right-arrow"></i></button>
														</div>
													</div>
													
												</fieldset>
												</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
	
<script type="text/javascript">

$(document).ready(function(){
	$('#paynowX').click(function(){
	var billing_name = $('#billing_name').val();
	var billing_address = $('#billing_address').val();
	var billing_country = $('#shipcity').val();
	var billing_state = $('#billing_state').val();
	var billing_city = $('#billing_city').val();
	var billing_zip = $('#billing_zip').val();
	var billing_telephone = $('#billing_telephone').val();
	var billing_email = $('#billing_email').val();
	
	var shipname = $('#shipname').val();
	var shipaddress = $('#shipaddress').val();
	var shipcountry = $('#shipcountry').val();
	var shipstate = $('#shipstate').val();
	var shipcity = $('#shipcity').val();
	var shippincode = $('#shippincode').val();
    var shiptelephone = $('#shiptelephone').val();
		
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		
	if(billing_name==""){
		$('#billing_nameerr').css('display','block'); 
		$('#billing_name').focus(); 
		return false;
	}else{
		$('#billing_nameerr').css('display','none'); 
	}
	if(billing_address==""){
		$('#billing_addresserr').css('display','block'); 
		$('#billing_address').focus(); 
		return false;
	}else{
		$('#billing_addresserr').css('display','none'); 
	}
	if(billing_country==""){
		$('#billing_countryerr').css('display','block'); 
		$('#billing_country').focus(); 
		return false;
	}else{
		$('#billing_countryerr').css('display','none'); 
	}
	if(billing_state==""){
		$('#billing_stateerr').css('display','block'); 
		$('#billing_state').focus(); 
		return false;
	}else{
		$('#billing_stateerr').css('display','none'); 
	}
	if(billing_city==""){
		$('#billing_cityerr').css('display','block'); 
		$('#billing_city').focus(); 
		return false;
	}else{
		$('#billing_cityerr').css('display','none'); 
	}
	if(billing_zip=="" || billing_zip.length<6 || billing_zip.length>6){
		$('#billing_ziperr').css('display','block'); 
		$('#billing_zip').focus(); 
		return false;
	}else{
		$('#billing_ziperr').css('display','none'); 
	}
	if(billing_telephone=="" || billing_telephone.length<10 || billing_telephone.length>15){
		$('#billing_telephoneerr').css('display','block'); 
		$('#billing_telephone').focus(); 
		return false;
	}else{
		$('#billing_telephoneerr').css('display','none'); 
	}
	if(billing_email=="" || !email_regex.test(billing_email)){
		$('#billing_emailerr').css('display','block'); 
		$('#billing_email').focus(); 
		return false;
	}else{
		$('#billing_emailerr').css('display','none'); 
	}
	
	if(shipname==""){
		$('#shipnameerr').css('display','block'); 
		$('#shipname').focus(); 
		return false;
	}else{
		$('#shipnameerr').css('display','none'); 
	}
	if(shipaddress==""){
		$('#shipaddresserr').css('display','block'); 
		$('#shipaddress').focus(); 
		return false;
	}else{
		$('#shipaddresserr').css('display','none'); 
	}
	if(shipcountry==""){
		$('#shipcountryerr').css('display','block'); 
		$('#shipcountry').focus(); 
		return false;
	}else{
		$('#shipcountryerr').css('display','none'); 
	}
	if(shipstate==""){
		$('#shipstateerr').css('display','block'); 
		$('#shipstate').focus(); 
		return false;
	}else{
		$('#shipstateerr').css('display','none'); 
	}
	if(shipcity==""){
		$('#shipcityerr').css('display','block'); 
		$('#shipcity').focus(); 
		return false;
	}else{
		$('#shipcityerr').css('display','none'); 
	}
	if(shippincode=="" || shippincode.length<6 || shippincode.length>6){
		$('#shippincodeerr').css('display','block'); 
		$('#shippincode').focus(); 
		return false;
	}else{
		$('#shippincodeerr').css('display','none'); 
	}
	if(shiptelephone=="" || shiptelephone.length<10 || shiptelephone.length>15){
		$('#shiptelephoneerr').css('display','block'); 
		$('#shiptelephone').focus(); 
		return false;
	}else{
		$('#shiptelephoneerr').css('display','none'); 
	}
	
	if(captcha_code2=="" || captcha_code2 != rannumber){
		$('#captcha_code2err').css('display','block'); 
		$('#captcha_code2').focus(); 
		return false;
	}else{
		$('#captcha_code2err').css('display','none'); 
	}

	});	
});	
</script> 
	
	
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 
<script>
	window.onload = function() {
		var d = new Date().getTime();
		document.getElementById("tid").value = d;
	};
</script>
@include('includes.index-footer')