<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','1')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')
	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.url') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2>Apply for Admission</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
											<div class="tg-contactus tg-contactusvone">
											    
											    <div class="adm_status_msg"></div>
											    
                                                @if(Session::get('CaptchaVerificaion') !='')
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;">
                                                <div class="alert alert-danger">{{ Session::get('CaptchaVerificaion') }} </div>
                                                </div>
                                                @endif											    
												
												<br>
												<div class="alert alert-info col-md-12 col-sm-12 col-xs-12">Do read the  
												<strong><a href="/admission-procedure">Admission Procedure</a></strong> before you fill out this form.
												</div>
												<br>
												<h4 class="pull-left">Fill up the application form below. The mandatory fields are marked with an * </h4>
												<hr>
												<div class="row">
												
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id="AdmissionForm"   enctype='multipart/form-data'  action="{{ url('proceed-to-payment') }}" >
													<div class="alert alert-info">Student's Detail</div>
													<fieldset>
													
														<div class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control"  name="name" id="name" value="{{ old('name') }}" placeholder="Student Name*">
															<span id="qnameerr" style="color:red;display:none;"> Please Enter name.</span>
															@if ($errors->has('name'))
                                        						<div class="error" style="color:red;">{{ $errors->first('name') }}</div>
                                        					@endif
														</div>
														<div class="form-group col-md-6 col-sm-6">
															<input type="text" class="datepicker form-control" name="dob" id="dob" value="{{ old('dob') }}" placeholder="Date Of Birth*">
															<span id="qdateerr" style="color:red;display:none;"> Please Enter Date Of Birth.</span>
															@if ($errors->has('dob'))
                                        						<div class="error" style="color:red;"> Please Enter Date Of Birth.</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
														  <input type="text" maxlength="10" class="form-control" id="mobile" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile No:*" onkeypress="return isNumberKey(event)">
														  <span id="mobileerr" style="color:red;display:none;">10 Digit Mobile Number?</span>
															@if ($errors->has('mobile'))
                                        						<div class="error" style="color:red;">10 Digit Mobile Number?</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="restel" name="restel" value="{{ old('restel') }}" placeholder="Residence Telephone*" onkeypress="return isNumberKey(event)">
														  <span id="restelerr" style="color:red;display:none;">10 Digit Residence Tel.?</span>
															@if ($errors->has('restel'))
                                        						<div class="error" style="color:red;">10 Digit Residence Tel.?</div>
                                        					@endif
														</div>
														
														<div class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="address" id="address" value="{{ old('address') }}" placeholder="Residential Address*">
															<span id="qraddresserr" style="color:red;display:none;"> Please Enter Residential Address.</span>
															@if ($errors->has('address'))
                                        						<div class="error" style="color:red;"> Please Enter Residential Address.</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}" placeholder="City:*">
															<span id="qcityerr" style="color:red;display:none;"> Please Enter City.</span>
															@if ($errors->has('city'))
                                        						<div class="error" style="color:red;">Please Enter City.</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<select class="form-control" id="state" name="state">
                                                                <option value=''>Select State</option>
                                                                <option value='Andhra Pradesh' @if (Input::old('state') == 'Andhra Pradesh') selected="selected" @endif>Andhra Pradesh</option>
                                                                <option value='Arunachal Pradesh' @if (Input::old('state') == 'Arunachal Pradesh') selected="selected" @endif >Arunachal Pradesh</option>
                                                                <option value='Assam' @if (Input::old('state') == 'Assam') selected="selected" @endif >Assam </option>
                                                                <option value='Bihar' @if (Input::old('state') == 'Bihar') selected="selected" @endif >Bihar</option>
                                                                <option value='Chhattisgarh' @if (Input::old('state') == 'Chhattisgarh') selected="selected" @endif >Chhattisgarh    </option>
                                                                <option value='Goa' @if (Input::old('state') == 'Goa') selected="selected" @endif >Goa</option>
                                                                <option value='Gujarat' @if (Input::old('state') == 'Gujarat') selected="selected" @endif >Gujarat</option>
                                                                <option value='Haryana' @if (Input::old('state') == 'Haryana') selected="selected" @endif >Haryana </option>
                                                                <option value='Himachal Pradesh' @if (Input::old('state') == 'Himachal Pradesh') selected="selected" @endif >Himachal Pradesh</option>
                                                                <option value='Jammu & Kashmir' @if (Input::old('state') == 'Jammu & Kashmir') selected="selected" @endif >Jammu & Kashmir</option>
                                                                <option value='Jharkhand' @if (Input::old('state') == 'Jharkhand') selected="selected" @endif >Jharkhand </option>
                                                                <option value='Karnataka' @if (Input::old('state') == 'Karnataka') selected="selected" @endif >Karnataka </option>
                                                                <option value='Kerla' @if (Input::old('state') == 'Kerla') selected="selected" @endif >Kerla </option>
                                                                <option value='Madhya Pradesh' @if (Input::old('state') == 'Madhya Pradesh') selected="selected" @endif >Madhya Pradesh</option>
                                                                <option value='Maharashtra' @if (Input::old('state') == 'Maharashtra') selected="selected" @endif >Maharashtra  </option>
                                                                <option value='Manipur' @if (Input::old('state') == 'Manipur') selected="selected" @endif >Manipur </option>
                                                                <option value='Meghalaya' @if (Input::old('state') == 'Meghalaya') selected="selected" @endif >Meghalaya</option>
                                                                <option value='Mizoram' @if (Input::old('state') == 'Mizoram') selected="selected" @endif > Mizoram</option>
                                                                <option value='Nagaland' @if (Input::old('state') == 'Nagaland') selected="selected" @endif >Nagaland</option>
                                                                <option value='Orissa' @if (Input::old('state') == 'Orissa') selected="selected" @endif >Orissa </option>
                                                                <option value='Punjab' @if (Input::old('state') == 'Punjab') selected="selected" @endif >Punjab </option>
                                                                <option value='Sikkim' @if (Input::old('state') == 'Sikkim') selected="selected" @endif >Sikkim </option>
                                                                <option value='Tamilnadu' @if (Input::old('state') == 'Tamilnadu') selected="selected" @endif >Tamilnadu</option>
                                                                <option value='Tripura' @if (Input::old('state') == 'Tripura') selected="selected" @endif >Tripura </option>
                                                                <option value='Uttar Pradesh' @if (Input::old('state') == 'Uttar Pradesh') selected="selected" @endif >Uttar Pradesh </option>
                                                                <option value='Uttaranchal' @if (Input::old('state') == 'Uttaranchal') selected="selected" @endif > Uttaranchal </option>
                                                                <option value='West Bengal' @if (Input::old('state') == 'West Bengal') selected="selected" @endif >West Bengal</option>
                                                                <option value='Outside India' @if (Input::old('state') == 'Outside India') selected="selected" @endif >Outside India</option>
														  </select>
														  <span id="qstateerr" style="color:red;display:none;"> Please Select State.</span>
															@if ($errors->has('state'))
                                        						<div class="error" style="color:red;">Please Select State.</div>
                                        					@endif
														</div>
														
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" name="country" id="country" value="{{ old('country') }}" placeholder="country:*">
															<span id="qcountryerr" style="color:red;display:none;"> Please Enter Country.</span>
															@if ($errors->has('country'))
                                        						<div class="error" style="color:red;">{{ $errors->first('country') }}</div>
                                        					@endif
														</div>
															
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" max-length="6" class="form-control" id="pincode" name="pincode" value="{{ old('pincode') }}" placeholder="Pincode:*" onkeypress="return isNumberKey(event)">
															<span id="pincodeerr" style="color:red;display:none;"> Please Enter 6 Digit Pincode.</span>
															@if ($errors->has('pincode'))
                                        						<div class="error" style="color:red;">{{ $errors->first('pincode') }}</div>
                                        					@endif
														</div>														
														
														<div class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="nopskul" id="nopskul" value="{{ old('nopskul') }}" placeholder="Name of Present School:*">
															<span id="qnskulerr" style="color:red;display:none;"> Please Enter Name of Present School.</span>
															@if ($errors->has('nopskul'))
                                        						<div class="error" style="color:red;">Please Enter Name of Present School.</div>
                                        					@endif
														</div>
														<div class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="grade" id="grade" value="{{ old('grade') }}" placeholder="Present grade*">
															<span id="qgradeerr" style="color:red;display:none;"> Please Enter present grade.</span>
															@if ($errors->has('grade'))
                                        						<div class="error" style="color:red;">Please Enter present grade.</div>
                                        					@endif
														</div>
														<div class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="gradetjoin" id="gradetjoin" value="{{ old('gradetjoin') }}" placeholder="Grade to join*">
															<span id="qjoinerr" style="color:red;display:none;"> Grade to join</span>
															@if ($errors->has('gradetjoin'))
                                        						<div class="error" style="color:red;"> Grade to join</div>
                                        					@endif
														</div>
														
														<div class="form-group col-md-6 col-sm-6">
														<!--<label for="sel1">Academic Year for which admission is sought</label>-->
														<select class="form-control" name="year" id="year">
													    <option value=""> Academic Year for which admission is sought</option>
														<?php 
														$start_year=date('Y');
														$end_year= date('Y')+1;
														$start_year1=$start_year+1;
														$end_year1=$end_year+1;
														for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
														<option value='<?php echo $i.'-'.$j; ?>'>
														    <?php echo $i.'-'.$j;?></option>
														<?php } ?>
														 </select>
														 <span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
															@if ($errors->has('year'))
                                        						<div class="error" style="color:red;">Please Enter Academic Year.</div>
                                        					@endif
														</div>
														
														<div class="form-group col-md-6 col-sm-6">
														  <input type="text" class="form-control" id="nationality" name="nationality" value="{{ old('nationality') }}" placeholder="Nationality*">
														  <span id="nationalityerr" style="color:red;display:none;">Please Enter Nationality.</span>
															@if ($errors->has('nationality'))
                                        						<div class="error" style="color:red;">Please Enter Nationality.</div>
                                        					@endif
														</div>
														
														</div>
													
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
													<div class="alert alert-info"><strong>Parent Detail</strong></div>
													
													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding:0px;">
													
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="fname"  name="fname" value="{{ old('fname') }}" placeholder="Father Name*">
															<span id="fnameerr" style="color:red;display:none;">Father Name Required.</span>
															@if ($errors->has('fname'))
                                        						<div class="error" style="color:red;">Father Name Required.</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="foccupation" name="foccupation" value="{{ old('foccupation') }}" placeholder="Father's Occupation*">
															<span id="foccupationerr" style="color:red;display:none;">Father's Occupation?</span>
															@if ($errors->has('foccupation'))
                                        						<div class="error" style="color:red;">Father's Occupation?</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="fdesignation" name="fdesignation" value="{{ old('fdesignation') }}" placeholder="Father's Designation*">
															<span id="fdesignationerr" style="color:red;display:none;">Father's Designation?</span>
															@if ($errors->has('fdesignation'))
                                        						<div class="error" style="color:red;">Father's Designation?</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="forganization" name="forganization" value="{{ old('forganization') }}" placeholder="Father's Organization*">
															<span id="forganizationerr" style="color:red;display:none;">Father's Organisation?</span>
															@if ($errors->has('forganization'))
                                        						<div class="error" style="color:red;">Father's Organisation?</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="fofftel" name="fofftel" value="{{ old('fofftel') }}" placeholder="Father's Office Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="fofftelerr" style="color:red;display:none;"> 10 Digits Office Telelphone.</span>
															@if ($errors->has('fofftel'))
                                        						<div class="error" style="color:red;">10 Digits Office Telelphone.</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="email" class="form-control" id="femail" name="femail" value="{{ old('femail') }}" placeholder="Father's Email*">
															<span id="femailerr" style="color:red;display:none;">Father's Email Id ?</span>
															@if ($errors->has('femail'))
                                        						<div class="error" style="color:red;">Father's Email Id ?</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-12 col-sm-12">
															<input type="text" class="form-control" id="foffadd" name="foffadd" value="{{ old('foffadd') }}" placeholder="Father's Official Address:*">
															<span id="foffadderr" style="color:red;display:none;">Official Address?</span>
															@if ($errors->has('foffadd'))
                                        						<div class="error" style="color:red;">Official Address?</div>
                                        					@endif
														</div>
														
													</div>
													
													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding:0px;">
													
														<div style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="mname"  name="mname" value="{{ old('mname') }}" placeholder="Mother Name*">
															<span id="mnameerr" style="color:red;display:none;">Mother Name?</span>
															@if ($errors->has('mname'))
                                        						<div class="error" style="color:red;">Mother Name?</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="moccupation" name="moccupation" value="{{ old('moccupation') }}" placeholder="Mother's Occupation*">
															<span id="moccupationerr" style="color:red;display:none;">Mother's Occupation?</span>
															@if ($errors->has('moccupation'))
                                        						<div class="error" style="color:red;">Mother's Occupation?</div>
                                        					@endif
														</div>
														
														<div style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="mdesignation" name="mdesignation" value="{{ old('mdesignation') }}" placeholder="Mother's Designation*">
															<span id="mdesignationerr" style="color:red;display:none;">Mother's Designation?</span>
															@if ($errors->has('mdesignation'))
                                        						<div class="error" style="color:red;">Mother's Designation?</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="morganization" name="morganization" value="{{ old('morganization') }}" placeholder="Mother's Organization*">
															<span id="morganizationerr" style="color:red;display:none;">Mother's Organisation?</span>
															@if ($errors->has('morganization'))
                                        						<div class="error" style="color:red;">Mother's Organisation?</div>
                                        					@endif
														</div>
														
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" maxlength="10" class="form-control" id="mofftel" name="mofftel" value="{{ old('mofftel') }}" placeholder="Mother's Office Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="mofftelerr" style="color:red;display:none;"> 10 Digits Office Telelphone.</span>
															@if ($errors->has('mofftel'))
                                        						<div class="error" style="color:red;">10 Digits Office Telelphone.</div>
                                        					@endif
														</div>
														
														<div class="form-group  col-md-6 col-sm-6">
															<input type="email" class="form-control" id="memail" name="memail" value="{{ old('memail') }}" placeholder="Mother's Email*">
															<span id="memailerr" style="color:red;display:none;">Mother's Email Id ?</span>
															@if ($errors->has('memail'))
                                        						<div class="error" style="color:red;">Mother's Email Id ?</div>
                                        					@endif
														</div>
														
														<div style="padding-left:0px;" class="form-group  col-md-12 col-sm-12">
															<input type="text" class="form-control" id="moffadd" name="moffadd" value="{{ old('moffadd') }}" placeholder="Mother's Official Address:*">
															<span id="moffadderr" style="color:red;display:none;">Official Address?</span>
															@if ($errors->has('moffadd'))
                                        						<div class="error" style="color:red;">Official Address?</div>
                                        					@endif
														</div>
														
														
													</div>
													
													</div>
													
													
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														    
															<div class="alert alert-info">Other Information</div>
															
															<div class="form-group col-md-4 col-xs-12">
																  <select class="form-control" id="passport" name="passport">
																	<option value="">Passsport Type?</option>
																	<option value="Indian Passport" @if (Input::old('passport') == 'Indian Passport') selected="selected" @endif >Indian Passport</option>
																	<option value="Non-Indian Passport" @if (Input::old('passport') == 'Non-Indian Passport') selected="selected" @endif >Non-Indian Passport</option>
																  </select>
																  <span id="passporterr" style="color:red;display:none;">Select Passport Type.</span>
															@if ($errors->has('passport'))
                                        						<div class="error" style="color:red;">Select Passport Type.</div>
                                        					@endif
															</div>
															
															<div class="form-group col-md-4 col-xs-12">
															<!--<textarea class="form-control" placeholder="How you heard about our School?*" name="ourskul" id="ourskul">{{ old('ourskul') }}</textarea>-->
															<input type="text" class="form-control" id="ourskul" name="ourskul" value="{{ old('ourskul') }}"  placeholder="How you heard about our School:*">
															 <span id="qschoolerr" style="color:red;display:none;">How you heard about our School?</span>
															@if ($errors->has('ourskul'))
                                        						<div class="error" style="color:red;">How you heard about our School?</div>
                                        					@endif
															</div>
															
															
															<div class="form-group col-md-4 col-xs-12">
															<!--<textarea class="form-control" placeholder="What do you wish know?" name="whattoknow" id="whattoknow">{{ old('whattoknow') }}</textarea>-->
															<input type="text" class="form-control" id="whattoknow" name="whattoknow" value="{{ old('whattoknow') }}" placeholder="What do you wish to know*">
															 <span id="whattoknowerr" style="color:red;display:none;">What do you wish know?</span>
															@if ($errors->has('whattoknow'))
                                        						<div class="error" style="color:red;">What do you wish know?</div>
                                        					@endif
															</div>
															
															<div class="form-group col-md-6 col-sm-6 col-xs-7">
															    <img src="{!! \Config::get('app.url') !!}/images/cc-avenue.jpg" class="responsive">
															</div>
															
															
													<!--	<div class="form-group">
                                                            <div class="g-recaptcha" data-sitekey="6LdRDDwUAAAAAK_LeKgqYCiGJ-hPFNPJ6Ircafmo"></div>
                                                            @if ($errors->has('g-recaptcha-response'))
                                        					<div class="col-md-12 error" style="color:red;">Please verify captcha.</div>
                                        				    @endif
                                        				    <span id="g-recaptcha-responseerr" style="color:red;display:none;">Please verify captcha.</span>
                                                        </div>
                                                            <br> -->
                                                            
															<div class="col-10 fix text-center">
															<div class="success1"></div>
															<div class="error1"></div>
															{{ csrf_field() }}
															</div> 
															
															<div class="form-group">
                                                                <div class="col-md-12 col-sm-6 col-xs-12">
                                                                    <br>
															   <a class="button" name="asenquiry" id="asenquiry" >
															        Submit as enquiry <i class="fa fa-angle-double-right" aria-hidden="true"></i>
															    </a>
															    
															    <button type="submit" class="button" name="admission-btn" id="admission-btn" >
															        Proceed to Pay <i class="fa fa-angle-double-right" aria-hidden="true"></i>
															    </button>
															</div>
															</div>
														</div>
														</fieldset>	
														</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
	
<script type="text/javascript">

$(document).ready(function(){
    	
	var frm = $('#AdmissionForm');

    $('#asenquiry').click(function(e){
        e.preventDefault();
      
		var name = $('#name').val();
    	var dob = $('#dob').val();
    	var address = $('#address').val();
    	var nopskul = $('#nopskul').val();
    	var grade = $('#grade').val();
    	var gradetjoin = $('#gradetjoin').val();
    	var year = $('#year').val();
    	var nationality = $('#nationality').val();
		
    	var fname = $('#fname').val();
    	var foccupation = $('#foccupation').val();
    	var fdesignation = $('#fdesignation').val();
    	var forganization = $('#forganization').val();
    	var foffadd = $('#foffadd').val();
    	var fofftel = $('#fofftel').val();
    	var femail = $('#femail').val();
    	var mobile = $('#mobile').val();
    	var restel = $('#restel').val();
		
    	var mname = $('#mname').val();
    	var moccupation = $('#moccupation').val();
    	var mdesignation = $('#mdesignation').val();
    	var morganization = $('#morganization').val();
    	var moffadd = $('#moffadd').val();
    	var mofftel = $('#mofftel').val();
    	var memail = $('#memail').val();
		
    	var city = $('#city').val();
    	var state = $('#state').val();
    	var country = $('#country').val();
    	var pincode = $('#pincode').val();
    	var passport = $('#passport').val();
    	var ourskul = $('#ourskul').val();
    	var whattoknow = $('#whattoknow').val();
    //	var g-recaptcha-response = $('#g-recaptcha-response').val();
    	
    	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        
        if(name==""){
        	$('#qnameerr').css('display','block'); 
        	$('#name').focus(); 
        	return false;
        }else{
        	$('#qnameerr').css('display','none'); 
        }
       if(dob==""){
        	$('#qdateerr').css('display','block'); 
        	$('#dob').focus(); 
        	return false;
        }else{
        	$('#qdateerr').css('display','none'); 
        }
		if(mobile=="" || mobile.length<10 || mobile.length>15){
			$('#mobileerr').css('display','block'); 
			$('#mobile').focus(); 
			return false;
		}else{
			$('#mobileerr').css('display','none'); 
		}
		if(restel=="" || restel.length<10 || restel.length>15){
			$('#restelerr').css('display','block'); 
			$('#restel').focus(); 
			return false;
		}else{
			$('#restelerr').css('display','none'); 
		}
         if(address==""){
        	$('#qraddresserr').css('display','block'); 
        	$('#address').focus(); 
        	return false;
        }else{
        	$('#qraddresserr').css('display','none'); 
        }
        if(city==""){
        	$('#qcityerr').css('display','block'); 
        	$('#city').focus(); 
        	return false;
        }else{
        	$('#qcityerr').css('display','none'); 
        }
        if(state==""){
        	$('#qstateerr').css('display','block'); 
        	$('#state').focus(); 
        	return false;
        }else{
        	$('#qstateerr').css('display','none'); 
        }
        if(country==""){
        	$('#qcountryerr').css('display','block'); 
        	$('#country').focus(); 
        	return false;
        }else{
        	$('#qcountryerr').css('display','none'); 
        }
        if(pincode=="" || pincode.length<6){
        	$('#pincodeerr').css('display','block'); 
        	$('#pincode').focus(); 
        	return false;
        }else{
        	$('#pincodeerr').css('display','none'); 
        }
       if(nopskul==""){
        	$('#qnskulerr').css('display','block'); 
        	$('#nopskul').focus(); 
        	return false;
        }else{
        	$('#qnskulerr').css('display','none'); 
        }
        if(grade==""){
        	$('#qgradeerr').css('display','block'); 
        	$('#grade').focus(); 
        	return false;
        }else{
        	$('#qgradeerr').css('display','none'); 
        }
        if(gradetjoin==""){
        	$('#qjoinerr').css('display','block'); 
        	$('#gradetjoin').focus(); 
        	return false;
        }else{
        	$('#qjoinerr').css('display','none'); 
        }
        if(year==""){
        	$('#qayearerr').css('display','block'); 
        	$('#year').focus(); 
        	return false;
        }else{
        	$('#qayearerr').css('display','none'); 
        }
        if(nationality==""){
        	$('#nationalityerr').css('display','block'); 
        	$('#nationality').focus(); 
        	return false;
        }else{
        	$('#nationalityerr').css('display','none'); 
        }
		if(fname==""){
			$('#fnameerr').css('display','block'); 
			$('#fname').focus(); 
			return false;
		}else{
			$('#fnameerr').css('display','none'); 
		}
		if(foccupation==""){
			$('#foccupationerr').css('display','block'); 
			$('#foccupation').focus(); 
			return false;
		}else{
			$('#foccupationerr').css('display','none'); 
		}
		if(fdesignation==""){
			$('#fdesignationerr').css('display','block'); 
			$('#fdesignation').focus(); 
			return false;
		}else{
			$('#fdesignationerr').css('display','none'); 
		}
		if(forganization==""){
			$('#forganizationerr').css('display','block'); 
			$('#forganization').focus(); 
			return false;
		}else{
			$('#forganizationerr').css('display','none'); 
		}
		if(fofftel=="" || fofftel.length<10 || fofftel.length>15){
			$('#fofftelerr').css('display','block'); 
			$('#fofftel').focus(); 
			return false;
		}else{
			$('#fofftelerr').css('display','none'); 
		}
		if(femail=="" || !email_regex.test(femail)){
			$('#femailerr').css('display','block'); 
			$('#femail').focus(); 
			return false;
		}else{
			$('#femailerr').css('display','none'); 
		}
		if(foffadd==""){
			$('#foffadderr').css('display','block'); 
			$('#foffadd').focus(); 
			return false;
		}else{
			$('#foffadderr').css('display','none'); 
		}
		if(mofftel!=""){
			if(mofftel.length<10 || mofftel.length>15){
			$('#mofftelerr').css('display','block'); 
			$('#mofftel').focus(); 
			}else{
				$('#mofftelerr').css('display','none'); 
			}
		}
		if(memail!="" && !email_regex.test(memail)){
        	$('#memailerr').css('display','block'); 
        	$('#memail').focus(); 
        	return false;
		}else{
        	$('#memailerr').css('display','none'); 
        }        
        if(passport=="" || passport=='undefined' || passport=='-'){
        	$('#passporterr').css('display','block'); 
        	$('#passport').focus(); 
        	return false;
        }else{
        	$('#passporterr').css('display','none'); 
        }
        if(ourskul==""){
        	$('#qschoolerr').css('display','block'); 
        	$('#ourskul').focus(); 
        	return false;
        }else{
        	$('#qschoolerr').css('display','none'); 
        }
		if(whattoknow==""){
        	$('#whattoknowerr').css('display','block'); 
        	$('#whattoknow').focus(); 
        	return false;
        }else{
        	$('#whattoknowerr').css('display','none'); 
        }
	
    /*  if(g-recaptcha-response==""){
        	$('#g-recaptcha-responseerr').css('display','block'); 
        	$('#g-recaptcha-response').focus(); 
        	return false;
        }else{
        	$('#g-recaptcha-responseerr').css('display','none'); 
        }
    */    
        
            $.ajax({
    			type: 'get',
    			headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
    			url: 'admission/sendasenquiry',
               data: $("#AdmissionForm").serialize(), // serializes the form's elements.
               success: function(data)
               {
    			    $('.adm_status_msg').removeClass('alert alert-success');
    			    $('.adm_status_msg').removeClass('alert alert-danger');
    				if(data == 'success'){
    				    $(window).scrollTop($('.adm_status_msg').offset().top);
    				    $('.adm_status_msg').addClass('alert alert-success');
    					$('.adm_status_msg').html('<i class="fa fa-check-square-o"></i> Thank you, We will contact you soon.');
    				    $("#AdmissionForm").trigger('reset');
    				}
    				if(data == 'captcha-failed'){
    				    $(window).scrollTop($('.adm_status_msg').offset().top);
    				    $('.adm_status_msg').addClass('alert alert-danger');
    					$('.adm_status_msg').html('<i class="fa fa-ban"></i> Captcha verification failed.');
    				}   				
    				if(data == 'failed'){
    				    $(window).scrollTop($('.adm_status_msg').offset().top);
    				    $('.adm_status_msg').addClass('alert alert-danger');
    					$('.adm_status_msg').html('<i class="fa fa-ban"></i> Something went wrong, Please try again.');
    				}
               }
            });
        e.preventDefault();
    });
    
    
	$('#admission-btnx').click(function(){
      
		var name = $('#name').val();
    	var dob = $('#dob').val();
    	var address = $('#address').val();
    	var nopskul = $('#nopskul').val();
    	var grade = $('#grade').val();
    	var gradetjoin = $('#gradetjoin').val();
    	var year = $('#year').val();
    	var nationality = $('#nationality').val();
		
    	var fname = $('#fname').val();
    	var foccupation = $('#foccupation').val();
    	var fdesignation = $('#fdesignation').val();
    	var forganization = $('#forganization').val();
    	var foffadd = $('#foffadd').val();
    	var fofftel = $('#fofftel').val();
    	var femail = $('#femail').val();
    	var mobile = $('#mobile').val();
    	var restel = $('#restel').val();
		
    	var mname = $('#mname').val();
    	var moccupation = $('#moccupation').val();
    	var mdesignation = $('#mdesignation').val();
    	var morganization = $('#morganization').val();
    	var moffadd = $('#moffadd').val();
    	var mofftel = $('#mofftel').val();
    	var memail = $('#memail').val();
		
    	var city = $('#city').val();
    	var state = $('#state').val();
    	var country = $('#country').val();
    	var pincode = $('#pincode').val();
    	var passport = $('#passport').val();
    	var ourskul = $('#ourskul').val();
    	var whattoknow = $('#whattoknow').val();
    //	var g-recaptcha-response = $('#g-recaptcha-response').val();
    	
    	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        
        if(name==""){
        	$('#qnameerr').css('display','block'); 
        	$('#name').focus(); 
        	return false;
        }else{
        	$('#qnameerr').css('display','none'); 
        }
       if(dob==""){
        	$('#qdateerr').css('display','block'); 
        	$('#dob').focus(); 
        	return false;
        }else{
        	$('#qdateerr').css('display','none'); 
        }
         if(address==""){
        	$('#qraddresserr').css('display','block'); 
        	$('#address').focus(); 
        	return false;
        }else{
        	$('#qraddresserr').css('display','none'); 
        }
		if(mobile=="" || mobile.length<10 || mobile.length>15){
			$('#mobileerr').css('display','block'); 
			$('#mobile').focus(); 
			return false;
		}else{
			$('#mobileerr').css('display','none'); 
		}
		if(restel=="" || restel.length<10 || restel.length>15){
			$('#restelerr').css('display','block'); 
			$('#restel').focus(); 
			return false;
		}else{
			$('#restelerr').css('display','none'); 
		}
        if(city==""){
        	$('#qcityerr').css('display','block'); 
        	$('#city').focus(); 
        	return false;
        }else{
        	$('#qcityerr').css('display','none'); 
        }
        if(state==""){
        	$('#qstateerr').css('display','block'); 
        	$('#state').focus(); 
        	return false;
        }else{
        	$('#qstateerr').css('display','none'); 
        }
        if(country==""){
        	$('#qcountryerr').css('display','block'); 
        	$('#country').focus(); 
        	return false;
        }else{
        	$('#qcountryerr').css('display','none'); 
        }
        if(pincode=="" || pincode.length<6){
        	$('#pincodeerr').css('display','block'); 
        	$('#pincode').focus(); 
        	return false;
        }else{
        	$('#pincodeerr').css('display','none'); 
        }
       if(nopskul==""){
        	$('#qnskulerr').css('display','block'); 
        	$('#nopskul').focus(); 
        	return false;
        }else{
        	$('#qnskulerr').css('display','none'); 
        }
        if(grade==""){
        	$('#qgradeerr').css('display','block'); 
        	$('#grade').focus(); 
        	return false;
        }else{
        	$('#qgradeerr').css('display','none'); 
        }
        if(gradetjoin==""){
        	$('#qjoinerr').css('display','block'); 
        	$('#gradetjoin').focus(); 
        	return false;
        }else{
        	$('#qjoinerr').css('display','none'); 
        }
        if(year==""){
        	$('#qayearerr').css('display','block'); 
        	$('#year').focus(); 
        	return false;
        }else{
        	$('#qayearerr').css('display','none'); 
        }
        if(nationality==""){
        	$('#nationalityerr').css('display','block'); 
        	$('#nationality').focus(); 
        	return false;
        }else{
        	$('#nationalityerr').css('display','none'); 
        }
		if(fname==""){
			$('#fnameerr').css('display','block'); 
			$('#fname').focus(); 
			return false;
		}else{
			$('#fnameerr').css('display','none'); 
		}
		if(foccupation==""){
			$('#foccupationerr').css('display','block'); 
			$('#foccupation').focus(); 
			return false;
		}else{
			$('#foccupationerr').css('display','none'); 
		}
		if(fdesignation==""){
			$('#fdesignationerr').css('display','block'); 
			$('#fdesignation').focus(); 
			return false;
		}else{
			$('#fdesignationerr').css('display','none'); 
		}
		if(forganization==""){
			$('#forganizationerr').css('display','block'); 
			$('#forganization').focus(); 
			return false;
		}else{
			$('#forganizationerr').css('display','none'); 
		}
		if(fofftel=="" || fofftel.length<10 || fofftel.length>15){
			$('#fofftelerr').css('display','block'); 
			$('#fofftel').focus(); 
			return false;
		}else{
			$('#fofftelerr').css('display','none'); 
		}
		if(femail=="" || !email_regex.test(femail)){
			$('#femailerr').css('display','block'); 
			$('#femail').focus(); 
			return false;
		}else{
			$('#femailerr').css('display','none'); 
		}
		if(foffadd==""){
			$('#foffadderr').css('display','block'); 
			$('#foffadd').focus(); 
			return false;
		}else{
			$('#foffadderr').css('display','none'); 
		}
		if(mofftel!=""){
			if(mofftel.length<10 || mofftel.length>15){
			$('#mofftelerr').css('display','block'); 
			$('#mofftel').focus(); 
			}else{
				$('#mofftelerr').css('display','none'); 
			}
		}
		if(memail!="" && !email_regex.test(memail)){
        	$('#memailerr').css('display','block'); 
        	$('#memail').focus(); 
        	return false;
		}else{
        	$('#memailerr').css('display','none'); 
        }        
        if(passport=="" || passport=='undefined' || passport=='-'){
        	$('#passporterr').css('display','block'); 
        	$('#passport').focus(); 
        	return false;
        }else{
        	$('#passporterr').css('display','none'); 
        }
        if(ourskul==""){
        	$('#qschoolerr').css('display','block'); 
        	$('#ourskul').focus(); 
        	return false;
        }else{
        	$('#qschoolerr').css('display','none'); 
        }
	/*	if(whattoknow==""){
        	$('#whattoknowerr').css('display','block'); 
        	$('#whattoknow').focus(); 
        	return false;
        }else{
        	$('#whattoknowerr').css('display','none'); 
        }
	
      if(g-recaptcha-response==""){
        	$('#g-recaptcha-responseerr').css('display','block'); 
        	$('#g-recaptcha-response').focus(); 
        	return false;
        }else{
        	$('#g-recaptcha-responseerr').css('display','none'); 
        }
    */    
	});	
});	
</script> 
	
	
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')