<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','4')->get();
	 
	foreach($Pages as $Page){ }
	
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<?php if(isset($Page->brief_desc) && $Page->brief_desc !="") { ?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
											<?php echo $Page->brief_desc; ?>
										</div>
										<?php } ?>
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="padding:0px;">
										
										<div class="panel panel-default category-panel ">
                                           <div class="panel-footer">
											<form action="{{ url('events') }}" method="post">
											
											<input type="text" class="input-sm" id="title" name="title" placeholder="Search" value="<?php if(isset($title)){ echo $title;} ?>">
											
											<input type="text" class="input-sm" id="location" name="location" placeholder="Location" value="<?php if(isset($location)){ echo $location;} ?>">
											
											<input type="text" class="input-sm datepicker" id="date1" name="date1" placeholder="Date" value="<?php if(isset($Date1)){ echo $Date1;} ?>">
											
											<input type="text" class="input-sm datepicker" id="date2" name="date2" placeholder="Date" value="<?php if(isset($Date2)){ echo $Date2;} ?>">
											
											
											
											<div class="btn-group">
												{{ csrf_field() }}
												<a class="btn btn-square" href="{!! \Config::get('app.url_base') !!}/events" style="margin-left:5px;background-color:#1AC5A6;">
												<i class="fa fa-close fa-1x"></i></a>
												<button type="submit" class="btn btn-info" style="margin-left:5px;"><i class="fa fa-search fa-1x"></i></button>
											</div> 
											</form>
                                           </div>
                                        </div>
										
										
										<?php 
										if(isset($EventData)){
										if(count($EventData)>0){
											foreach($EventData as $News){
										?>
										<div class="col-md-6">
										<table cellspacing="0" cellpadding="0">
										<tbody><tr><td>
										<div class="col-md-3" style="background-color:#5ebeeb;text-transform:uppercase;text-align:center;color:#fff; font-size:16px;">
										<strong style="color:#fff;"><?php echo date('M',strtotime($News->start_date)); ?></strong> 
										<br><?php echo date('d',strtotime($News->start_date)); ?>
										</div>
										<div class="col-md-9" style="text-align:left;">
										<a href="{{URL::to('view-event',array($News->id))}}" style="color:#c19f69;"><?php echo ucwords($News->title); ?></a> <br>
										<?php echo strtoupper($News->timing); ?>
										</div>
										</td></tr></tbody></table>
										</div>
										
										<?php 
										}
										}else{
											echo 'No events Found.';
										}
										}else{
										?>
										
										<?php 
										
										$NewsList = DB::table('events')->where('start_date','>=',date('Y-m-d'))->where('status','=','Active')->orderBy('id','desc')->get();
										if(count($NewsList)>0){
											foreach($NewsList as $News){
										?>
										<div class="col-md-6">
										<table cellspacing="0" cellpadding="0">
										<tbody><tr><td>
										<div class="col-md-3" style="background-color:#5ebeeb;text-transform:uppercase;text-align:center;color:#fff; font-size:16px;">
										<strong style="color:#fff;"><?php echo date('M',strtotime($News->start_date)); ?></strong> 
										<br><?php echo date('d',strtotime($News->start_date)); ?>
										</div>
										<div class="col-md-9" style="text-align:left;">
										<a href="{{URL::to('view-event',array($News->id))}}" style="color:#c19f69;"><?php echo ucwords($News->title); ?></a> <br>
										<?php echo strtoupper($News->timing); ?>
										</div>
										</td></tr></tbody></table>
										</div>
										
										<?php 
										}
										}else{
											echo 'No events Found.';
										}
										}
										?>
										
										
										
										</div>	
										
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')