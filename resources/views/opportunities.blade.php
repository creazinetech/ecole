<?php 
    $Pages = DB::table('jobs')->where('url','=','Opportunities-2')->orWhere('url','=','Opportunities')->get();
	foreach($Pages as $Page){ }
	$PageTitle="Career Opportunities";
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->title); ?>"> 
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    Career Opportunities<?php //echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    </div>
	  
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/faculty-and-staff">Faculty & Career Opening</a>
	             / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/career-opportunities">Career Opportunities</a>
                / <a  class="breadcrum-text">Opportunities</a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 tg-content-text">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->title); ?></h2>
										</div>
										<?php if(isset($Page->brief_desc) && $Page->brief_desc!=""){ ?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<?php echo $Page->brief_desc; ?>
											<br>
											<?php
											$JobTitles = DB::table('job_titles')->where('status','=','Active')->orderBy('title', 'asc')->get();
											if(count($JobTitles)>0){
											    echo '<ul>';
											    foreach($JobTitles as $JobTitle){
											        echo '<li>'.ucwords($JobTitle->title).'</li>';
											    }
											    echo '</ul><br>';
											}
											?>
										</div>
										<?php } ?>
										<br><br><br>
											<a href="{!! \Config::get('app.url_base') !!}/apply-for-job" class="button big-button"  style="font-size:19px;padding:21px;">
											Apply Now</a>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')