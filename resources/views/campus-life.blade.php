<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','16')->get();
	$Gallery = DB::table('gallery')->where('page_id','=','16')->where('img_status','=','Active')->get();
	foreach($Pages as $Page){ }
?>	

@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
											<?php echo $Page->brief_desc; ?>
											<hr>
										</div>
										
										<a href="{!! \Config::get('app.url_base') !!}/library">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','28')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<a href="{!! \Config::get('app.url_base') !!}/computer-centres">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','27')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<a href="{!! \Config::get('app.url_base') !!}/science-technology-labs">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','29')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<a href="{!! \Config::get('app.url_base') !!}/sports">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','30')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<a href="{!! \Config::get('app.url_base') !!}/music-dance-theatre">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','31')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<a href="{!! \Config::get('app.url_base') !!}/visual-arts">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','33')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<a href="{!! \Config::get('app.url_base') !!}/emun">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','35')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<a href="{!! \Config::get('app.url_base') !!}/expeditions-camps">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<?php 
										$pages1 = DB::table('pages')
											->leftJoin('gallery','gallery.page_id','=','pages.page_id')
											->where('pages.page_id','=','36')->orderBy(DB::raw('RAND()'))->take(1)->get();
										foreach($pages1 as $page1){}
										if(isset($page1->img) && $page1->img !=""){
										?>
										<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $page1->img; ?>" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } else{?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive thumbnail" style="height:250px;width:100%;">
										<?php } ?>
										<center><h3 class="ecole-text"><?php echo $page1->page_heading;  ?></h3><center>
										</div>
										</a>
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<hr>
											<div class="xopac-logo">
											    <img src="{!! \Config::get('app.url') !!}/images/opac.jpg" class="xalignleft" alt="LOGO1">
											    <div class="xarrow-left"></div>
											    <div class="xglobe">
											        <span><a href="https://ecole-rsic.follettdestiny.com/" target="_blank">Visit Website</a></span>
										        </div>
                                            </div>

										<!--	<div class="xopac-logo">
											    <img src="{!! \Config::get('app.url') !!}/images/primarylibrary.png" class="xalignleft" alt="LOGO1">
											    <div class="xarrow-left"></div>
											    <div class="xglobe">
											        <span><a href="https://sites.google.com/a/ecolemondiale.org/primary-library/ " target="_blank">Visit Website</a></span>
										        </div>
                                            </div>-->
                                            
										</div>

									
									</div>
								</div>
								
								
								
								
								
								
								<?php if(count($Gallery)>0){?>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder">
											<h2>Gallery</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php foreach($Gallery as $GalImg){?>
											<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3" >
												<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox-group="toddler" data-darkbox-description="<?php echo ucwords($GalImg->img_title); ?>" class="img-responsive thumbnail">
											</div>
										<?php } ?>
										<br>
										</div> 
									</div>
									
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                					    <hr>
                					    <p><strong>Related Links</strong></p>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/playschool">Play School</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/early-yrndprimary-yr">Early Years & Primary Years</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/diploma-16-19-yrs">Diploma</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/college-counseling">College Counselling</a>
                					</div>
					
								</div>
								<?php } ?>
							</section>
						</div>
						
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')