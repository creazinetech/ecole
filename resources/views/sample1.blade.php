@include('includes.index-header')
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="tg-contactus tg-contactusvone">
								<div class="tg-titleborder">
									<h2>Message from the Principal</h2>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							  <p class="container-fluid text-justify">Keep in touch with former friends, classmates, and your alma mater. Fill out the form below and we'll be happy to get you connected. The details you provide will only be used internally at École Mondiale World School. Trust us to keep your information confidential.</p>

								<p>Note: * indicates that the fields are mandatory</p></div>
								  <div class="">
								  </div>
							</div>
							<hr>
						   </div>
							
							
							</section>
										  
							
						</div>
					</div>
		  
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						<aside id="tg-sidebar" class="tg-sidebar">
							<div class="tg-widget">
							  <div class="tg-widgetcontent tg-sidebar">
								<h4>Why Us</h4>
								<img src="images/why-us-sidebar.jpg" alt=""/> </br>
								<button type="button" class="btn btn-default btn-primary tg-btn btn-block">Apply Online</button>
							 <button type="button" class="btn btn-default btn-primary tg-btn btn-block btn-group-lg">Enquire Now</button>
							 <br><br>

							 <button type="button" class="btn btn-primary btn-block icon-file-pdf"> Download our NEWSLETTER</button>
							 <br><br>
							 
							  <blockquote class="bg icon-quotes-left"> We have/had three children at Ecole Mondiale for several years through the primary, middle and high school years and all three are extremely happy being at Ecole, which is a common theme at Ecole; the kids are happy. Ecole has and continues to provide our children with an environment where they can figure out their varied interests while pursuing an academically rigorous IB program. Ecole teachers truly distinguish themselves: they encourage kids to experiment, express themselves in and out of the classroom and the teachers take genuine interest in each and every child. For any motivated student, Ecole is a great place to learn, make lifelong friendships, and pursue and excel in extra-curricular interests be it in art, music, theatre, sports or community service. For students who want this balance throughout their school years, Ecole Mondiale is the best school in Mumbai.
							   
							   Shujaat and Zeenath Khan Sana Khan, Class of 2013, Pomona College, Class of 2017, Saad Khan, Class of 2015, Saif Khan, G5</blockquote>
							 
							</div>	
							</div>
						</aside>
					</div>
			  
	  </div>
		</div>
	</main>
		
		
@include('includes.index-footer')