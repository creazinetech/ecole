<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','17')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder">
											<h2><?php echo ucwords($Page->page_heading);?></h2>
										</div>
										<?php if(isset($Page->brief_desc) && $Page->brief_desc!=""){?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="padding:0px;">
											<?php echo $Page->brief_desc;?>
										</div>
										<?php } ?>
									</div>
									
									<div class="clearfix"></div>
									<div class="flash-message  tg-content-text">
									@foreach (['danger', 'warning', 'success', 'info'] as $msg)
									  @if(Session::has('alert-' . $msg))

									  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
									  @endif
									@endforeach
									</div>
									<hr>
									
									<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id="" enctype='multipart/form-data'  action="{{ url('addalumni') }}" >
									
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tg-content-text">
										<div class="alert alert-info">Personal Details</div>
										<hr>	
											<div class="form-group">
												<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name:*" value="{{ old('firstname') }}">
												<span id="firstnameerr" style="color:red;display:none;"> Enter First Name.</span>
												@if ($errors->has('firstname'))
                            						<div class="error" style="color:red;">{{ $errors->first('firstname') }}</div>
                            					@endif
											</div>
											<div class="form-group">
												<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name:*" value="{{ old('lastname') }}">
												<span id="lastnameerr" style="color:red;display:none;"> Enter Last Name.</span>
												@if ($errors->has('lastname'))
                            						<div class="error" style="color:red;">{{ $errors->first('lastname') }}</div>
                            					@endif
											</div>
											<div class="form-group">
												<select class="form-control" name="graduate_yr" id="graduate_yr">
													<option value="">Graduate Year?</option>
													<?php 
													$start_year=date('Y')-20;
													$end_year= date('Y');
													$start_year1=$start_year+1;
													$end_year1=$end_year+1;
													for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
													<option value='<?php echo $i.'-'.$j; ?>'><?php echo $i.'-'.$j;?></option>
													<?php } ?>
												</select>
												<span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
												@if ($errors->has('graduate_yr'))
                        						<div class="error" style="color:red;">Please Enter Academic Year.</div>
                        					@endif
											</div>
											
											<div class="alert alert-info">How May We Reach You</div>
											<hr>
											<div class="form-group"> 
										   <textarea class="form-control" name="home_address" id="home_address" placeholder="Home Address:*">{{ old('home_address') }}</textarea>
										   <span id="home_addresserr" style="color:red;display:none;"> Please Enter Home Address.</span>
											@if ($errors->has('home_address'))
                        						<div class="error" style="color:red;">{{ $errors->first('home_address') }}</div>
                        					@endif
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="city" name="city" placeholder="City:" value="{{ old('city') }}">
										   <span id="cityerr" style="color:red;display:none;"> Please Enter City.</span>
											@if ($errors->has('city'))
                        						<div class="error" style="color:red;">{{ $errors->first('city') }}</div>
                        					@endif
										   </div>
											<div class="form-group">
												<select class="form-control" id="state" name="state">
													<option value=''>Select State</option>
                                                    <option value='Andhra Pradesh' @if (Input::old('state') == 'Andhra Pradesh') selected="selected" @endif>Andhra Pradesh</option>
                                                    <option value='Arunachal Pradesh' @if (Input::old('state') == 'Arunachal Pradesh') selected="selected" @endif >Arunachal Pradesh</option>
                                                    <option value='Assam' @if (Input::old('state') == 'Assam') selected="selected" @endif >Assam </option>
                                                    <option value='Bihar' @if (Input::old('state') == 'Bihar') selected="selected" @endif >Bihar</option>
                                                    <option value='Chhattisgarh' @if (Input::old('state') == 'Chhattisgarh') selected="selected" @endif >Chhattisgarh    </option>
                                                    <option value='Goa' @if (Input::old('state') == 'Goa') selected="selected" @endif >Goa</option>
                                                    <option value='Gujarat' @if (Input::old('state') == 'Gujarat') selected="selected" @endif >Gujarat</option>
                                                    <option value='Haryana' @if (Input::old('state') == 'Haryana') selected="selected" @endif >Haryana </option>
                                                    <option value='Himachal Pradesh' @if (Input::old('state') == 'Himachal Pradesh') selected="selected" @endif >Himachal Pradesh</option>
                                                    <option value='Jammu & Kashmir' @if (Input::old('state') == 'Jammu & Kashmir') selected="selected" @endif >Jammu & Kashmir</option>
                                                    <option value='Jharkhand' @if (Input::old('state') == 'Jharkhand') selected="selected" @endif >Jharkhand </option>
                                                    <option value='Karnataka' @if (Input::old('state') == 'Karnataka') selected="selected" @endif >Karnataka </option>
                                                    <option value='Kerla' @if (Input::old('state') == 'Kerla') selected="selected" @endif >Kerla </option>
                                                    <option value='Madhya Pradesh' @if (Input::old('state') == 'Madhya Pradesh') selected="selected" @endif >Madhya Pradesh</option>
                                                    <option value='Maharashtra' @if (Input::old('state') == 'Maharashtra') selected="selected" @endif >Maharashtra  </option>
                                                    <option value='Manipur' @if (Input::old('state') == 'Manipur') selected="selected" @endif >Manipur </option>
                                                    <option value='Meghalaya' @if (Input::old('state') == 'Meghalaya') selected="selected" @endif >Meghalaya</option>
                                                    <option value='Mizoram' @if (Input::old('state') == 'Mizoram') selected="selected" @endif > Mizoram</option>
                                                    <option value='Nagaland' @if (Input::old('state') == 'Nagaland') selected="selected" @endif >Nagaland</option>
                                                    <option value='Orissa' @if (Input::old('state') == 'Orissa') selected="selected" @endif >Orissa </option>
                                                    <option value='Punjab' @if (Input::old('state') == 'Punjab') selected="selected" @endif >Punjab </option>
                                                    <option value='Sikkim' @if (Input::old('state') == 'Sikkim') selected="selected" @endif >Sikkim </option>
                                                    <option value='Tamilnadu' @if (Input::old('state') == 'Tamilnadu') selected="selected" @endif >Tamilnadu</option>
                                                    <option value='Tripura' @if (Input::old('state') == 'Tripura') selected="selected" @endif >Tripura </option>
                                                    <option value='Uttar Pradesh' @if (Input::old('state') == 'Uttar Pradesh') selected="selected" @endif >Uttar Pradesh </option>
                                                    <option value='Uttaranchal' @if (Input::old('state') == 'Uttaranchal') selected="selected" @endif > Uttaranchal </option>
                                                    <option value='West Bengal' @if (Input::old('state') == 'West Bengal') selected="selected" @endif >West Bengal</option>
                                                    <option value='Outside India' @if (Input::old('state') == 'Outside India') selected="selected" @endif >Outside India</option>
											  </select>
												<span id="stateerr" style="color:red;display:none;"> Please Select State.</span>
										    	@if ($errors->has('state'))
                        						    <div class="error" style="color:red;">{{ $errors->first('state') }}</div>
                        					    @endif
											</div>
														<div class="form-group"> 
										   <input type="text" class="form-control" name="zip"  id="zip" placeholder="Zipcode:" value="{{ old('zip') }}">
										   <span id="ziperr" style="color:red;display:none;"> Please Enter Zipcode.</span>
										    	@if ($errors->has('zip'))
                        						    <div class="error" style="color:red;">Please Enter Zipcode.</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="contact1" name="contact1" value="{{ old('contact1') }}" onkeypress="return isNumberKey(event)" placeholder="Contact No(Home):">
										   <span id="contact1err" style="color:red;display:none;"> Please Enter Contact No.</span>
										    	@if ($errors->has('contact1'))
                        						    <div class="error" style="color:red;">Enter Contact Number</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="contact2" name="contact2" value="{{ old('contact2') }}" onkeypress="return isNumberKey(event)" placeholder="Mobile No:*">
										   <span id="contact2err" style="color:red;display:none;"> Please Enter Mobile No.</span>
										    	@if ($errors->has('contact2'))
                        						    <div class="error" style="color:red;">Enter Mobile Number</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="emailid" name="emailid" value="{{ old('emailid') }}" placeholder="Personal Email ID:*">
										   <span id="emailiderr" style="color:red;display:none;"> Please Enter Email Id.</span>
										    	@if ($errors->has('emailid'))
                        						    <div class="error" style="color:red;">Enter Email Id</div>
                        					    @endif
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="website" value="{{ old('website') }}" name="website" placeholder="Personal Website:">
										   <span id="websiteerr" style="color:red;display:none;"> Please Enter Personal Website.</span>
										    	@if ($errors->has('website'))
                        						    <div class="error" style="color:red;">Enter Personal Website.</div>
                        					    @endif
										   </div>  
												
											<div class="alert alert-info">Other Details</div>	
											<hr>
											<div class="form-group">
												<select class="form-control" id="study_type" name="study_type">
													<option value="">Are You Studying?</option>
													<option value='Full Time' selected='selected' @if (Input::old('study_type') == 'Full Time') selected="selected" @endif>Full Time</option>
													<option value='Part Time' @if (Input::old('study_type') == 'Part Time') selected="selected" @endif>Part Time</option>
												</select>
												<span id="study_typeerr" style="color:red;display:none;"> Please Select Are You Studying?.</span>
										    	@if ($errors->has('study_type'))
                        						    <div class="error" style="color:red;">Select study type.</div>
                        					    @endif
											</div>
											
											<div class="form-group"> 
										   <input type="text" class="form-control" id="university" name="university" value="{{ old('university') }}" placeholder="Current University Name">
										   <span id="universityerr" style="color:red;display:none;"> Please Enter University Name.</span>
										    	@if ($errors->has('university'))
                        						    <div class="error" style="color:red;">Current University Name.</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="programme" name="programme" value="{{ old('programme') }}" placeholder="Current Programme: eg BA (Business), MA(Music)">
										   <span id="programmeerr" style="color:red;display:none;"> Please Enter Current Programme.</span>
										    	@if ($errors->has('programme'))
                        						    <div class="error" style="color:red;">Please Enter Current Programme.</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="last_university" name="last_university" value="{{ old('last_university') }}" placeholder="Last University Name(previous, if any):">
										   <span id="last_universityerr" style="color:red;display:none;"> Please Enter Last University Name.</span>
										    	@if ($errors->has('last_university'))
                        						    <div class="error" style="color:red;">Please Enter University Name.</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="last_programme" name="last_programme" value="{{ old('last_programme') }}" placeholder="Last Programme: eg BA (Business), MA(Music)">
										   <span id="last_programmeerr" style="color:red;display:none;"> Please Enter Last Programme.</span>
										    	@if ($errors->has('last_programme'))
                        						    <div class="error" style="color:red;">Please Enter Last Programme.</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group">
										   <textarea class="form-control" name="last_address" id="last_address" placeholder="Last Address:*">{{ old('last_address') }}</textarea>
										   <span id="last_addresserr" style="color:red;display:none;"> Please Enter Last Address.</span>
										    	@if ($errors->has('last_address'))
                        						    <div class="error" style="color:red;">Please Enter Last Address.</div>
                        					    @endif
										   </div>
										   
										   <div class="form-group">
												<label for="sel1">Are you willing to serve as an alumni ambassador for prospective applicants and current students:</label>
												<select class="form-control" id="alumni_ambassador" name="alumni_ambassador">
													<option value='no' selected='selected' @if (Input::old('alumni_ambassador') == 'no') selected="selected" @endif>No</option>
													<option value='yes' @if (Input::old('alumni_ambassador') == 'yes') selected="selected" @endif>Yes</option>
												</select>
												<span id="alumni_ambassadorerr" style="color:red;display:none;">Would you like to be alumni ambassador?</span>
										    	@if ($errors->has('alumni_ambassador'))
                        						    <div class="error" style="color:red;">Would you like to be alumni ambassador?</div>
                        					    @endif
												
											</div>
											
											 <p>If yes,please note that we will release your preferred contact information to prospective applicants and current students.</p>
											 
											 <p>Optional: Update your alumni file by submitting the following : </p>
											 <br>
												1.Curriculum vitae <br>
												2.Photo 	<br> 
												Send mail at  <a href="mailto:alumni@ecolemondiale.org">alumni@ecolemondiale.org</a><br> 
												Hard copies will not be returned.
										</div>
												
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tg-content-text">								 
											<div class="alert alert-info">To Fill if Applicable</div>
											<hr>
											<div class="form-group"> 
												<input type="text" class="form-control" id="occupation" name="occupation" placeholder="Occupation:" value="{{ old('occupation') }}">
												<span id="occupationerr" style="color:red;display:none;"> Please Enter Occupation.</span>
										    	@if ($errors->has('occupation'))
                        						    <div class="error" style="color:red;">Please Enter Occupation.</div>
                        					    @endif
											</div>
											<div class="form-group"> 
												<input type="text" class="form-control" id="organisation" name="organisation" value="{{ old('organisation') }}" placeholder="Name of The Organization/Employer:">
												<span id="organisationerr" style="color:red;display:none;"> Please Enter Organisation.</span>
										    	@if ($errors->has('organisation'))
                        						    <div class="error" style="color:red;">Please Enter Organisation.</div>
                        					    @endif
											</div>
											
											<div class="form-group"> 
												<input type="text" class="form-control" id="designation" name="designation" value="{{ old('designation') }}" placeholder="Designation:">
												<span id="designationerr" style="color:red;display:none;"> Please Enter Designation.</span>
										    	@if ($errors->has('designation'))
                        						    <div class="error" style="color:red;">Please Enter Designation.</div>
                        					    @endif
											</div>
											
											<div class="form-group">
											<textarea class="form-control" id="off_address" name="off_address" placeholder="Address (Business/Office):">{{ old('off_address') }}</textarea>
											<span id="off_addresserr" style="color:red;display:none;"> Please Enter Address (Business/Office).</span>
										    	@if ($errors->has('off_address'))
                        						    <div class="error" style="color:red;">Please Enter Address (Business/Office).</div>
                        					    @endif
											</div>
											
											<div class="form-group"> 
												<input type="text" class="form-control" name="off_contact" name="off_contact" value="{{ old('off_contact') }}" placeholder="Office ContactNo:">
												<span id="off_contacterr" style="color:red;display:none;"> Please Enter Office Contact No.</span>
										    	@if ($errors->has('off_contact'))
                        						    <div class="error" style="color:red;"> Please Enter Office Contact No.</div>
                        					    @endif
											</div>
											
											<div class="form-group"> 
												<input type="text" class="form-control" id="off_fax" name="off_fax" value="{{ old('off_fax') }}" placeholder="Office Fax No:">
												<span id="off_faxerr" style="color:red;display:none;"> Please Enter Office Fax No.</span>
										    	@if ($errors->has('off_fax'))
                        						    <div class="error" style="color:red;">Please Enter Office Fax No.</div>
                        					    @endif
											</div>
											
											<div class="form-group"> 
												<input type="text" class="form-control" id="off_email" name="off_email" value="{{ old('off_email') }}" placeholder="Email Id:">
												<span id="off_emailerr" style="color:red;display:none;"> Please Enter Office Email id.</span>
										    	@if ($errors->has('off_email'))
                        						    <div class="error" style="color:red;">Please Enter Office Email id.</div>
                        					    @endif
											</div>
											
											<div class="form-group"> 
												<input type="text" class="form-control" id="off_website" name="off_website" value="{{ old('off_website') }}" placeholder="Office Website:">
												<span id="off_websiteerr" style="color:red;display:none;"> Please Enter Office Website.</span>
										    	@if ($errors->has('off_website'))
                        						    <div class="error" style="color:red;">Please Enter Office Website.</div>
                        					    @endif
											</div>
											
											<div class="alert alert-info">Alumni Survey</div>
											<hr>
											<div class="form-group">
												<select class="form-control" id="work_type" name="work_type">
													<option value=''>Are You Working?</option>
													<option value='Full Time' @if (Input::old('work_type') == 'Part Time') selected="selected" @endif>Full Time</option>
													<option value='Part Time' @if (Input::old('work_type') == 'Part Time') selected="selected" @endif>Part Time</option>
												</select>
												<span id="work_typeerr" style="color:red;display:none;">Are You Working?</span>
										    	@if ($errors->has('work_type'))
                        						    <div class="error" style="color:red;">Are You Working?</div>
                        					    @endif
											</div>
											<div class="form-group">
												<select class="form-control" id="gender" name="gender">
													<option value=''> Gender?</option>
													<option value='male' selected='selected' @if (Input::old('gender') == 'male') selected="selected" @endif>male</option>
													<option value='female' @if (Input::old('gender') == 'female') selected="selected" @endif> Female</option>
												</select>
												<span id="gendererr" style="color:red;display:none;"> Please Select Gender.</span>
										    	@if ($errors->has('gender'))
                        						    <div class="error" style="color:red;">Please Select Gender.</div>
                        					    @endif
											</div>
											<div class="form-group"> 
												<input type="text" class="datepicker form-control" id="dob" name="dob" value="{{ old('dob') }}" placeholder="Date of Birth:">
												<span id="doberr" style="color:red;display:none;"> Please Enter dob.</span>
										    	@if ($errors->has('dob'))
                        						    <div class="error" style="color:red;"> Please Enter dob.</div>
                        					    @endif
											</div>
											
											<div class="alert alert-info">Alumni News</div>
											<hr>
											<p>Keep EMWS and your classmates informed about you and your achievements!</p>
									 
											<div class="form-group"> 
												<textarea class="form-control" name="personal" id="personal" placeholder="Personal:">{{ old('personal') }}</textarea>
												<span id="personalerr" style="color:red;display:none;"> Please Enter Personal information.</span>
										    	@if ($errors->has('personal'))
                        						    <div class="error" style="color:red;">Please Enter Personal Information.</div>
                        					    @endif
											</div>
											<div class="form-group"> 
												<textarea class="form-control" name="professional" id="professional" placeholder="Professional:">{{ old('professional') }}</textarea>
												<span id="professionalerr" style="color:red;display:none;"> Please Enter Professional Information.</span>
												@if ($errors->has('personal'))
                        						    <div class="error" style="color:red;">Please Enter Professional Information.</div>
                        					    @endif
											</div>
											<div class="form-group"> 
												<textarea class="form-control" name="hobbies" id="hobbies" placeholder="Hobbies:">{{ old('hobbies') }}</textarea>
												<span id="hobbieserr" style="color:red;display:none;"> Please Enter Hobbies.</span>
												@if ($errors->has('personal'))
                        						    <div class="error" style="color:red;">Please Enter Hobbies.</div>
                        					    @endif
											</div>
											<div class="form-group"> 
												<textarea class="form-control" name="vacation" id="vacation" placeholder="Vacation:">{{ old('vacation') }}</textarea>
												<span id="vacationerr" style="color:red;display:none;"> Please Enter Vacation Information.</span>
												@if ($errors->has('vacation'))
                        						    <div class="error" style="color:red;">Please Enter Vacation Information.</div>
                        					    @endif
											</div>
											
											<!--<div class="form-group">
												<input type="hidden" id="rannumber" name="rannumber" class="captcha">
												<div class="captcha col-md-4" style="border: 2px solid green; width: 150px; line-height:1.4; height: 45px; text-align: center; vertical-align: middle; font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">
												</div>
												<div class="col-md-2" style="height:40px;margin-bottom:5px;">
												  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;" >
												  <i class="fa fa-refresh"></i>
												  </a>
												  <br><br>
												</div>
											</div>
											<div class="form-group">
												<input class="form-control" id="captcha_code2" name="captcha_code2" placeholder="Enter Captcha" type="text" style="text-transform:none;">
												<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
											</div> -->
											
											<div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                <!--<div id="ReCaptacha1"></div>-->
                                                <div class="g-recaptcha" data-sitekey="6LdRDDwUAAAAAK_LeKgqYCiGJ-hPFNPJ6Ircafmo"></div>
                                                @if ($errors->has('g-recaptcha-response'))
                            							<div class="error" style="color:red;">Please Enter Captcha.</div>
                            				    @endif 
                                            </div>		
											
											<div class="form-group">
												{{ csrf_field() }}
												<button class="button" id="alumni-submit" onclick="">Submit</button>
											</div>		
											
											
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 tg-content-text">
											
											
										</div>
											
										
										
										<br>	  
									</form>
								
								
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
	
<script type="text/javascript">

$(document).ready(function(){
    $('#alumni-submitx').click(function(){
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var graduate_yr = $('#graduate_yr').val();
    var home_address = $('#home_address').val();
    var city = $('#city').val();
    var state = $('#state').val();
    var zip = $('#zip').val();
    var contact1 = $('#contact1').val();
    var contact2 = $('#contact2').val();
    var emailid = $('#emailid').val();
    var website = $('#website').val();
    var study_type = $('#study_type').val();
    var university = $('#university').val();
    var programme = $('#programme').val();
    var last_university = $('#last_university').val();
    var last_programme = $('#last_programme').val();
    var last_address = $('#last_address').val();
    var alumni_ambassador = $('#alumni_ambassador').val();
    var occupation = $('#occupation').val();
    var organisation = $('#organisation').val();
    var designation = $('#designation').val();
    var off_address = $('#off_address').val();
    var off_contact = $('#off_contact').val();
    var off_fax = $('#off_fax').val();
    var off_email = $('#off_email').val();
    var off_website = $('#off_website').val();
    var work_type = $('#work_type').val();
    var gender = $('#gender').val();
    var dob = $('#dob').val();
    var personal = $('#personal').val();
    var professional = $('#professional').val();
    var hobbies = $('#hobbies').val();
    var vacation = $('#vacation').val();
    var captcha_code2 = $('#captcha_code2').val();
    var rannumber = $('#rannumber').val();
    var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

        if(firstname==""){
            $('#firstnameerr').css('display','block'); 
            $('#firstname').focus(); 
            return false;
        }else{
            $('#firstnameerr').css('display','none'); 
        }
		if(lastname==""){
            $('#lastnameerr').css('display','block'); 
            $('#lastname').focus(); 
            return false;
        }else{
            $('#lastnameerr').css('display','none'); 
        }
		if(graduate_yr==""){
            $('#qayearerr').css('display','block'); 
            $('#graduate_yr').focus(); 
            return false;
        }else{
            $('#qayearerr').css('display','none'); 
        }
        if(home_address==""){
            $('#home_addresserr').css('display','block'); 
            $('#home_address').focus(); 
            return false;
        }else{
            $('#home_addresserr').css('display','none'); 
        }
		if(city==""){
            $('#cityerr').css('display','block'); 
            $('#city').focus(); 
            return false;
        }else{
            $('#cityerr').css('display','none'); 
        }
		if(state==""){
            $('#stateerr').css('display','block'); 
            $('#state').focus(); 
            return false;
        }else{
            $('#stateerr').css('display','none'); 
        }
        if(zip==""){
            $('#ziperr').css('display','block'); 
            $('#zip').focus(); 
            return false;
        }else{
            $('#ziperr').css('display','none'); 
        }
        if(contact1==""){
            $('#contact1err').css('display','block'); 
            $('#contact1').focus(); 
            return false;
        }else{
            $('#contact1err').css('display','none'); 
        }
        if(contact2==""){
            $('#contact2err').css('display','block'); 
            $('#contact2').focus(); 
            return false;
        }else{
            $('#contact2err').css('display','none'); 
        }
		if(emailid=="" || !email_regex.test(emailid)){
            $('#emailiderr').css('display','block'); 
            $('#emailid').focus(); 
            return false;
        }else{
            $('#emailiderr').css('display','none'); 
        }
		if(website==""){
            $('#websiteerr').css('display','block'); 
            $('#website').focus(); 
            return false;
        }else{
            $('#websiteerr').css('display','none'); 
        }
		if(study_type==""){
            $('#study_typeerr').css('display','block'); 
            $('#study_type').focus(); 
            return false;
        }else{
            $('#study_typeerr').css('display','none'); 
        }
		if(university==""){
            $('#universityerr').css('display','block'); 
            $('#university').focus(); 
            return false;
        }else{
            $('#universityerr').css('display','none'); 
        }
		if(programme==""){
            $('#programmeerr').css('display','block'); 
            $('#programme').focus(); 
            return false;
        }else{
            $('#programmeerr').css('display','none'); 
        }
		if(last_university==""){
            $('#last_universityerr').css('display','block'); 
            $('#last_university').focus(); 
            return false;
        }else{
            $('#last_universityerr').css('display','none'); 
        }
		if(last_programme==""){
            $('#last_programmeerr').css('display','block'); 
            $('#last_programme').focus(); 
            return false;
        }else{
            $('#last_programmeerr').css('display','none'); 
        }
		if(last_address==""){
            $('#last_addresserr').css('display','block'); 
            $('#last_address').focus(); 
            return false;
        }else{
            $('#last_addresserr').css('display','none'); 
        }
		if(alumni_ambassador==""){
            $('#alumni_ambassadorerr').css('display','block'); 
            $('#alumni_ambassador').focus(); 
            return false;
        }else{
            $('#alumni_ambassadorerr').css('display','none'); 
        }
		if(occupation==""){
            $('#occupationerr').css('display','block'); 
            $('#occupation').focus(); 
            return false;
        }else{
            $('#occupationerr').css('display','none'); 
        }
		if(organisation==""){
            $('#organisationerr').css('display','block'); 
            $('#organisation').focus(); 
            return false;
        }else{
            $('#organisationerr').css('display','none'); 
        }
		if(designation==""){
            $('#designationerr').css('display','block'); 
            $('#designation').focus(); 
            return false;
        }else{
            $('#designationerr').css('display','none'); 
        }
		if(off_address==""){
            $('#off_addresserr').css('display','block'); 
            $('#off_address').focus(); 
            return false;
        }else{
            $('#off_addresserr').css('display','none'); 
        }
		if(off_contact==""){
            $('#off_contacterr').css('display','block'); 
            $('#off_contact').focus(); 
            return false;
        }else{
            $('#off_contacterr').css('display','none'); 
        }
		if(off_fax==""){
            $('#off_faxerr').css('display','block'); 
            $('#off_fax').focus(); 
            return false;
        }else{
            $('#off_faxerr').css('display','none'); 
        }
		if(off_email=="" || !email_regex.test(off_email)){
            $('#off_emailerr').css('display','block'); 
            $('#off_email').focus(); 
            return false;
        }else{
            $('#off_emailerr').css('display','none'); 
        }
		if(off_website==""){
            $('#off_websiteerr').css('display','block'); 
            $('#off_website').focus(); 
            return false;
        }else{
            $('#off_websiteerr').css('display','none'); 
        }
		if(work_type==""){
            $('#work_typeerr').css('display','block'); 
            $('#work_type').focus(); 
            return false;
        }else{
            $('#work_typeerr').css('display','none'); 
        }
		if(gender==""){
            $('#gendererr').css('display','block'); 
            $('#gender').focus(); 
            return false;
        }else{
            $('#gendererr').css('display','none'); 
        }
		if(dob==""){
            $('#doberr').css('display','block'); 
            $('#dob').focus(); 
            return false;
        }else{
            $('#doberr').css('display','none'); 
        }
	/*	if(personal==""){
            $('#personalerr').css('display','block'); 
            $('#personal').focus(); 
            return false;
        }else{
            $('#personalerr').css('display','none'); 
        }
		if(professional==""){
            $('#professionalerr').css('display','block'); 
            $('#professional').focus(); 
            return false;
        }else{
            $('#professionalerr').css('display','none'); 
        }
		if(hobbies==""){
            $('#hobbieserr').css('display','block'); 
            $('#hobbies').focus(); 
            return false;
        }else{
            $('#hobbieserr').css('display','none'); 
        }
		if(vacation==""){
            $('#vacationerr').css('display','block'); 
            $('#vacation').focus(); 
            return false;
        }else{
            $('#vacationerr').css('display','none'); 
        }
        */

    });    
});    
</script> 

 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')