@include('includes.index-header')
<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','54')->get();
	foreach($Pages as $Page){ }
?>	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.url') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.url') !!}/images/default-header.jpg" class="img-responsive" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	  </div>
	<div class="tg-tickerbox">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="item">
							<div class="tg-description">
								<p>Ecole Mondiale World School is the first International School in Mumbai to be authorised to offer the three programs of PYP, MYP and DP.</p>
							</div>
						</div>
						
						
				
				</div>
			</div>
		</div>
	</div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<?php echo $Page->brief_desc; ?>
										</div>
									</div>
								</div>
								<br><br>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										     <p><strong>What's Next</strong></p>
									    	    <a class="btn btn-primary btn-sm" href="{!! \Config::get('app.url_base') !!}/apply-for-admission">Apply For Admission</a>
									    	    <a class="btn btn-primary btn-sm" href="{!! \Config::get('app.url_base') !!}/contact-us">Enquiry Now</a>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										    <hr>
										    <p><strong>Related Links</strong></p>
									    	<a class="btn btn-primary btn-sm" href="{!! \Config::get('app.url_base') !!}/toddler">Toddler</a>
									    	<a class="btn btn-primary btn-sm" href="{!! \Config::get('app.url_base') !!}/school-calendar">School Calendar</a>
									    	<a class="btn btn-primary btn-sm" href="#">School Calendar</a>
									    	<a class="btn btn-primary btn-sm" href="{!! \Config::get('app.url_base') !!}/faq">FAQ</a>
										</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')