<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','53')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	  
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										
										
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
										<?php
										$pages1 = DB::table('pages')->where('pages.page_id','=','56')->get();
										foreach($pages1 as $page1){}
										?>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($page1->page_img) && $page1->page_img !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $page1->page_img; ?>" class="img-responsive" style="height: auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
										<a href="{!! \Config::get('app.url_base') !!}/message-from-primary-head" class="subheading"><?php echo ucwords($page1->page_heading); ?></a><br>
											<?php echo $page1->short_desc; ?>
											<br>
											<a href="{!! \Config::get('app.url_base') !!}/message-from-primary-head" class="button">
											Read More</a>
										</div>
										</div>
										
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
										<?php
										$pages1 = DB::table('pages')->where('pages.page_id','=','7')->get();
										foreach($pages1 as $page1){}
										?>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($page1->page_img) && $page1->page_img !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $page1->page_img; ?>" class="img-responsive" style="height: auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
										<a href="{!! \Config::get('app.url_base') !!}/playschool" class="subheading"><?php echo ucwords($page1->page_heading); ?></a><br>
											<?php echo $page1->short_desc; ?>
											<br>
											<a href="{!! \Config::get('app.url_base') !!}/playschool" class="button">
											Read More</a>
										</div>
										</div>	
										
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
										<?php
										$pages1 = DB::table('pages')->where('pages.page_id','=','8')->get();
										foreach($pages1 as $page1){}
										$pages2 = DB::table('pages')->where('pages.page_id','=','9')->get();
										foreach($pages2 as $page2){}
										?>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($page1->page_img) && $page1->page_img !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $page1->page_img; ?>" class="img-responsive" style="height: auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
										<a href="{!! \Config::get('app.url_base') !!}/early-yrndprimary-yr" class="subheading"><?php echo ucwords($page1->page_heading.' & '.$page2->page_heading); ?></a>
										<br>
										<?php echo $page1->short_desc; ?>
											<br>
											<a href="{!! \Config::get('app.url_base') !!}/early-yrndprimary-yr" class="button">
											Read More</a>
										</div>
										</div>
										
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
										<?php
										$pages1 = DB::table('pages')->where('pages.page_id','=','10')->get();
										foreach($pages1 as $page1){}
										$pages2 = DB::table('pages')->where('pages.page_id','=','11')->get();
										foreach($pages2 as $page2){}
										?>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($page1->page_img) && $page1->page_img !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $page1->page_img; ?>" class="img-responsive" style="height: auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
										<a href="{!! \Config::get('app.url_base') !!}/middle-years-11-16-yrs"  class="subheading"><?php echo ucwords($page1->page_heading); ?></a>
										<br>	<?php echo $page1->short_desc; ?>
											<br>
											<a href="{!! \Config::get('app.url_base') !!}/middle-years-11-16-yrs" class="button">
											Read More</a>
										</div>
										</div>
										
										
										
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
										<?php
										$pages1 = DB::table('pages')->where('pages.page_id','=','12')->get();
										foreach($pages1 as $page1){}
										?>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($page1->page_img) && $page1->page_img !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $page1->page_img; ?>" class="img-responsive" style="height: auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
										<a href="{!! \Config::get('app.url_base') !!}/diploma-16-19-yrs" class="subheading"><?php echo ucwords($page1->page_heading); ?></a>
											<?php echo $page1->short_desc; ?>
											<br>
											<a href="{!! \Config::get('app.url_base') !!}/diploma-16-19-yrs" class="button">
											Read More</a>
										</div>
										</div>	
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
										<?php
										$pages1 = DB::table('pages')->where('pages.page_id','=','13')->get();
										foreach($pages1 as $page1){}
										?>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($page1->page_img) && $page1->page_img !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $page1->page_img; ?>" class="img-responsive" style="height: auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
										<a href="{!! \Config::get('app.url_base') !!}/college-counseling" class="subheading"><?php echo ucwords($page1->page_heading); ?></a>
										<br>
											<?php echo $page1->short_desc; ?>
											<br>
											<a href="{!! \Config::get('app.url_base') !!}/college-counseling" class="button">
											Read More</a>
										</div>
										</div>	
										
										
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')