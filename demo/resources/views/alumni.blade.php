<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','17')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder">
											<h2><?php echo ucwords($Page->page_heading);?></h2>
										</div>
										<?php if(isset($Page->brief_desc) && $Page->brief_desc!=""){?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="padding:0px;">
											<?php echo $Page->brief_desc;?>
										</div>
										<?php } ?>
									</div>
									
									<div class="clearfix"></div>
									<div class="flash-message  tg-content-text">
									@foreach (['danger', 'warning', 'success', 'info'] as $msg)
									  @if(Session::has('alert-' . $msg))

									  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
									  @endif
									@endforeach
									</div>
									<hr>
									
									<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id="" enctype='multipart/form-data'  action="{{ url('addalumni') }}" >
									
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tg-content-text">
										<div class="alert alert-info">Personal Details</div>
										<hr>	
											<div class="form-group">
												<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name:*">
												<span id="firstnameerr" style="color:red;display:none;"> Enter First Name.</span>
											</div>
											<div class="form-group">
												<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name:*">
												<span id="lastnameerr" style="color:red;display:none;"> Enter Last Name.</span>
											</div>
											<div class="form-group">
												<select class="form-control" name="graduate_yr" id="graduate_yr">
													<option value="">Graduate Year?</option>
													<?php 
													$start_year=date('Y')-20;
													$end_year= date('Y');
													$start_year1=$start_year+1;
													$end_year1=$end_year+1;
													for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
													<option value='<?php echo $i.'-'.$j; ?>'><?php echo $i.'-'.$j;?></option>
													<?php } ?>
												</select>
												<span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
											</div>
											<div class="alert alert-info">How May We Reach You</div>
											<hr>
											<div class="form-group"> 
										   <textarea class="form-control" name="home_address" id="home_address" placeholder="Home Address:*"></textarea>
										   <span id="home_addresserr" style="color:red;display:none;"> Please Enter Home Address.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="city" name="city" placeholder="City:">
										   <span id="cityerr" style="color:red;display:none;"> Please Enter City.</span>
										   </div>
											<div class="form-group">
												<select class="form-control" id="state" name="state">
													<option value='' >State?</option>
													<option value='Andhra Pradesh'>Andhra Pradesh  </option>
													<option value='Arunachal Pradesh' >Arunachal Pradesh</option>
													<option value='Assam'>Assam </option>
													<option value='Bihar' >Bihar</option>
													<option value='Chhattisgarh' >Chhattisgarh</option>
													<option value='Goa' >Goa</option>
													<option value='Gujarat' >Gujarat</option>
													<option value='Haryana'>Haryana </option>
													<option value='Himachal Pradesh' >Himachal Pradesh</option>
													<option value='Jammu &amp; Kashmir' >Jammu &amp; Kashmir</option>
													<option value='Jharkhand'>Jharkhand </option>
													<option value='Karnataka'>Karnataka </option>
													<option value='Kerla'>Kerla </option>
													<option value='Madhya Pradesh'>Madhya Pradesh</option>
													<option value='Maharashtra'>Maharashtra  </option>
													<option value='Manipur'>Manipur </option>
													<option value='Meghalaya'>Meghalaya</option>
													<option value=' Mizoram'> Mizoram</option>
													<option value='Nagaland'>Nagaland</option>
													<option value='Orissa'>Orissa </option>
													<option value='Punjab'>Punjab </option>
													<option value='Sikkim'>Sikkim </option>
													<option value='Tamilnadu' >Tamilnadu</option>
													<option value='Tripura'>Tripura </option>
													<option value='Uttar Pradesh'>Uttar Pradesh </option>
													<option value=' Uttaranchal'> Uttaranchal </option>
													<option value='West Bengal' >West Bengal</option>
													<option value='Outside India' >Outside India</option>
												</select>
												<span id="stateerr" style="color:red;display:none;"> Please Select State.</span>
											</div>
														<div class="form-group"> 
										   <input type="text" class="form-control" name="zip"  id="zip" placeholder="Zip:">
										   <span id="ziperr" style="color:red;display:none;"> Please Enter Zip.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="contact1" name="contact1" onkeypress="return isNumberKey(event)" placeholder="Contact No(Home):">
										   <span id="contact1err" style="color:red;display:none;"> Please Enter Contact No.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="contact2" name="contact2" onkeypress="return isNumberKey(event)" placeholder="Mobile No:*">
										   <span id="contact2err" style="color:red;display:none;"> Please Enter Mobile No.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="emailid" name="emailid" placeholder="Personal Email ID:*">
										   <span id="emailiderr" style="color:red;display:none;"> Please Enter Email Id.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="website" name="website" placeholder="Personal Website Add :">
										   <span id="websiteerr" style="color:red;display:none;"> Please Enter Website.</span>
										   </div>  
												
											<div class="alert alert-info">Current Details</div>	
											<hr>
											<div class="form-group">
												<select class="form-control" id="sel1">
													<option value="">Are You Studying?</option>
													<option value='Full Time' selected='selected'>Full Time</option>
													<option value='Part Time' >Part Time</option>
												</select>
												<span id="sel1err" style="color:red;display:none;"> Please Select Are You Studying?.</span>
											</div>
											<div class="form-group"> 
										   <input type="text" class="form-control" id="university" name="university" placeholder="University Name:">
										   <span id="universityerr" style="color:red;display:none;"> Please Enter University Name.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="programme" name="programme" placeholder="Programme: eg BA (Business), MA(Music)">
										   <span id="programmeerr" style="color:red;display:none;"> Please Enter Programme.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="last_university" name="last_university" placeholder="University Name(previous, if any):">
										   <span id="last_universityerr" style="color:red;display:none;"> Please Enter University Name.</span>
										   </div>
										   <div class="form-group"> 
										   <input type="text" class="form-control" id="last_programme" name="last_programme" placeholder="Programme: eg BA (Business), MA(Music)">
										   <span id="last_programmeerr" style="color:red;display:none;"> Please Enter Programme.</span>
										   </div>
										   <div class="form-group">
										   <textarea class="form-control" name="last_address" id="last_address" placeholder="Address:*"></textarea>
										   <span id="last_addresserr" style="color:red;display:none;"> Please Enter Address.</span>
										   </div>
										   
										   <div class="form-group">
												<label for="sel1">Are you willing to serve as an alumni ambassador for prospective applicants and current students:</label>
												<select class="form-control" id="alumni_ambassador" name="alumni_ambassador">
													<option value='no' selected='selected'>No</option>
													<option value='yes'>Yes</option>
												</select>
												<span id="alumni_ambassadorerr" style="color:red;display:none;"> Please Select alumni ambassador.</span>
											</div>
											 <p>If yes,please note that we will release your preferred contact information to prospective applicants and current students.</p>
											 
											 <p>Optional: Update your alumni file by submitting the following : </p>
											 <br>
												1.Curriculum vitae <br>
												2.Photo 	<br> 
												Send mail at  <a href="mailto:alumni@ecolemondiale.org">alumni@ecolemondiale.org</a><br> 
												Hard copies will not be returned.
										</div>
												
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tg-content-text">								 
											<div class="alert alert-info">To Fill if Applicable</div>
											<hr>
											<div class="form-group"> 
												<input type="text" class="form-control" id="occupation" name="occupation" placeholder="Occupation:">
												<span id="occupationerr" style="color:red;display:none;"> Please Enter Occupation.</span>
											</div>
											<div class="form-group"> 
												<input type="text" class="form-control" id="organisation" name="organisation" placeholder="Name of The Organization/Employer:">
												<span id="organisationerr" style="color:red;display:none;"> Please Enter Organisation.</span>
											</div>
											<div class="form-group"> 
												<input type="text" class="form-control" id="designation" name="designation" placeholder="Designation:">
												<span id="designationerr" style="color:red;display:none;"> Please Enter Designation.</span>
											</div>
											<div class="form-group">
											<textarea class="form-control" id="off_address" name="off_address" placeholder="Address (Business/Office):"></textarea>
											<span id="off_addresserr" style="color:red;display:none;"> Please Enter Address (Business/Office).</span>
											</div>
											<div class="form-group"> 
												<input type="text" class="form-control" name="off_contact" name="off_contact" placeholder="Office ContactNo:">
												<span id="off_contacterr" style="color:red;display:none;"> Please Enter Office Contact No.</span>
											</div>
											<div class="form-group"> 
												<input type="text" class="form-control" id="off_fax" name="off_fax" placeholder="Office Fax No:">
												<span id="off_faxerr" style="color:red;display:none;"> Please Enter Office Fax No.</span>
											</div>
											<div class="form-group"> 
												<input type="text" class="form-control" id="off_email" name="off_email" placeholder="Email Id:">
												<span id="off_emailerr" style="color:red;display:none;"> Please Enter Office Email No.</span>
											</div>
											<div class="form-group"> 
												<input type="text" class="form-control" id="off_website" name="off_website" placeholder="WebSite:">
												<span id="off_websiteerr" style="color:red;display:none;"> Please Enter Office Website.</span>
											</div>
											<div class="alert alert-info">Alumni Survey</div>
											<hr>
											<div class="form-group">
												<select class="form-control" id="work_type" name="work_type">
													<option value=''>Are You Working?</option>
													<option value='Full Time'>Full Time</option>
													<option value='Part Time' >Part Time</option>
												</select>
												<span id="work_typeerr" style="color:red;display:none;"> Please Select Are You Working?</span>
											</div>
											<div class="form-group">
												<select class="form-control" id="gender" name="gender">
													<option value=''> Gender?</option>
													<option value='male' selected='selected'>male</option>
													<option value='female'> Female</option>
												</select>
												<span id="gendererr" style="color:red;display:none;"> Please Select Gender.</span>
											</div>
											<div class="form-group"> 
												<input type="text" class="datepicker form-control" id="dob" name="dob" placeholder="Date of Birth:">
												<span id="doberr" style="color:red;display:none;"> Please Enter dob.</span>
											</div>
											
											<div class="alert alert-info">Alumni News</div>
											<hr>
											<p>Keep EMWS and your classmates informed about you and your achievements!</p>
									 
											<div class="form-group"> 
												<textarea class="form-control" name="personal" id="personal" placeholder="Personal:"></textarea>
												<span id="personalerr" style="color:red;display:none;"> Please Enter Personal.</span>
											</div>
											<div class="form-group"> 
												<textarea class="form-control" name="professional" id="professional" placeholder="Professional:"></textarea>
												<span id="professionalerr" style="color:red;display:none;"> Please Enter Professional.</span>
											</div>
											<div class="form-group"> 
												<textarea class="form-control" name="hobbies" id="hobbies" placeholder="Hobbies:"></textarea>
												<span id="hobbieserr" style="color:red;display:none;"> Please Enter Hobbies.</span>
											</div>
											<div class="form-group"> 
												<textarea class="form-control" name="vacation" id="vacation" placeholder="Vacation:"></textarea>
												<span id="vacationerr" style="color:red;display:none;"> Please Enter Vacation.</span>
											</div>
											
											<div class="form-group">
												<input type="hidden" id="rannumber" name="rannumber" class="captcha">
												<div class="captcha col-md-4" style="border: 2px solid green; width: 150px; line-height:1.4; height: 45px; text-align: center; vertical-align: middle; font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">
												</div>
												<div class="col-md-2" style="height:40px;margin-bottom:5px;">
												  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;" >
												  <i class="fa fa-refresh"></i>
												  </a>
												  <br><br>
												</div>
											</div>
											<div class="form-group">
												<input class="form-control" id="captcha_code2" name="captcha_code2" placeholder="Enter Captcha" type="text" style="text-transform:none;">
												<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
											</div>		
											
											<div class="form-group">
												{{ csrf_field() }}
												<button class="button" id="alumni-submit" onclick="">Submit</button>
											</div>		
											
											
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 tg-content-text">
											
											
										</div>
											
										
										
										<br>	  
									</form>
								
								
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
	
<script type="text/javascript">

$(document).ready(function(){
    $('#alumni-submit').click(function(){
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var graduate_yr = $('#graduate_yr').val();
    var home_address = $('#home_address').val();
    var city = $('#city').val();
    var state = $('#state').val();
    var zip = $('#zip').val();
    var contact1 = $('#contact1').val();
    var contact2 = $('#contact2').val();
    var emailid = $('#emailid').val();
    var website = $('#website').val();
    var sel1 = $('#sel1').val();
    var university = $('#university').val();
    var programme = $('#programme').val();
    var last_university = $('#last_university').val();
    var last_programme = $('#last_programme').val();
    var last_address = $('#last_address').val();
    var alumni_ambassador = $('#alumni_ambassador').val();
    var occupation = $('#occupation').val();
    var organisation = $('#organisation').val();
    var designation = $('#designation').val();
    var off_address = $('#off_address').val();
    var off_contact = $('#off_contact').val();
    var off_fax = $('#off_fax').val();
    var off_email = $('#off_email').val();
    var off_website = $('#off_website').val();
    var work_type = $('#work_type').val();
    var gender = $('#gender').val();
    var dob = $('#dob').val();
    var personal = $('#personal').val();
    var professional = $('#professional').val();
    var hobbies = $('#hobbies').val();
    var vacation = $('#vacation').val();
    var captcha_code2 = $('#captcha_code2').val();
    var rannumber = $('#rannumber').val();
    var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

        if(firstname==""){
            $('#firstnameerr').css('display','block'); 
            $('#firstname').focus(); 
            return false;
        }else{
            $('#firstnameerr').css('display','none'); 
        }
		if(lastname==""){
            $('#lastnameerr').css('display','block'); 
            $('#lastname').focus(); 
            return false;
        }else{
            $('#lastnameerr').css('display','none'); 
        }
		if(graduate_yr==""){
            $('#qayearerr').css('display','block'); 
            $('#graduate_yr').focus(); 
            return false;
        }else{
            $('#qayearerr').css('display','none'); 
        }
        if(home_address==""){
            $('#home_addresserr').css('display','block'); 
            $('#home_address').focus(); 
            return false;
        }else{
            $('#home_addresserr').css('display','none'); 
        }
		if(city==""){
            $('#cityerr').css('display','block'); 
            $('#city').focus(); 
            return false;
        }else{
            $('#cityerr').css('display','none'); 
        }
		if(state==""){
            $('#stateerr').css('display','block'); 
            $('#state').focus(); 
            return false;
        }else{
            $('#stateerr').css('display','none'); 
        }
        if(zip==""){
            $('#ziperr').css('display','block'); 
            $('#zip').focus(); 
            return false;
        }else{
            $('#ziperr').css('display','none'); 
        }
        if(contact1==""){
            $('#contact1err').css('display','block'); 
            $('#contact1').focus(); 
            return false;
        }else{
            $('#contact1err').css('display','none'); 
        }
        if(contact2==""){
            $('#contact2err').css('display','block'); 
            $('#contact2').focus(); 
            return false;
        }else{
            $('#contact2err').css('display','none'); 
        }
		if(emailid=="" || !email_regex.test(emailid)){
            $('#emailiderr').css('display','block'); 
            $('#emailid').focus(); 
            return false;
        }else{
            $('#emailiderr').css('display','none'); 
        }
		if(website==""){
            $('#websiteerr').css('display','block'); 
            $('#website').focus(); 
            return false;
        }else{
            $('#websiteerr').css('display','none'); 
        }
		if(sel1==""){
            $('#sel1err').css('display','block'); 
            $('#sel1').focus(); 
            return false;
        }else{
            $('#sel1err').css('display','none'); 
        }
		if(university==""){
            $('#universityerr').css('display','block'); 
            $('#university').focus(); 
            return false;
        }else{
            $('#universityerr').css('display','none'); 
        }
		if(programme==""){
            $('#programmeerr').css('display','block'); 
            $('#programme').focus(); 
            return false;
        }else{
            $('#programmeerr').css('display','none'); 
        }
		if(last_university==""){
            $('#last_universityerr').css('display','block'); 
            $('#last_university').focus(); 
            return false;
        }else{
            $('#last_universityerr').css('display','none'); 
        }
		if(last_programme==""){
            $('#last_programmeerr').css('display','block'); 
            $('#last_programme').focus(); 
            return false;
        }else{
            $('#last_programmeerr').css('display','none'); 
        }
		if(last_address==""){
            $('#last_addresserr').css('display','block'); 
            $('#last_address').focus(); 
            return false;
        }else{
            $('#last_addresserr').css('display','none'); 
        }
		if(alumni_ambassador==""){
            $('#alumni_ambassadorerr').css('display','block'); 
            $('#alumni_ambassador').focus(); 
            return false;
        }else{
            $('#alumni_ambassadorerr').css('display','none'); 
        }
		if(occupation==""){
            $('#occupationerr').css('display','block'); 
            $('#occupation').focus(); 
            return false;
        }else{
            $('#occupationerr').css('display','none'); 
        }
		if(organisation==""){
            $('#organisationerr').css('display','block'); 
            $('#organisation').focus(); 
            return false;
        }else{
            $('#organisationerr').css('display','none'); 
        }
		if(designation==""){
            $('#designationerr').css('display','block'); 
            $('#designation').focus(); 
            return false;
        }else{
            $('#designationerr').css('display','none'); 
        }
		if(off_address==""){
            $('#off_addresserr').css('display','block'); 
            $('#off_address').focus(); 
            return false;
        }else{
            $('#off_addresserr').css('display','none'); 
        }
		if(off_contact==""){
            $('#off_contacterr').css('display','block'); 
            $('#off_contact').focus(); 
            return false;
        }else{
            $('#off_contacterr').css('display','none'); 
        }
		if(off_fax==""){
            $('#off_faxerr').css('display','block'); 
            $('#off_fax').focus(); 
            return false;
        }else{
            $('#off_faxerr').css('display','none'); 
        }
		if(off_email=="" || !email_regex.test(off_email)){
            $('#off_emailerr').css('display','block'); 
            $('#off_email').focus(); 
            return false;
        }else{
            $('#off_emailerr').css('display','none'); 
        }
		if(off_website==""){
            $('#off_websiteerr').css('display','block'); 
            $('#off_website').focus(); 
            return false;
        }else{
            $('#off_websiteerr').css('display','none'); 
        }
		if(work_type==""){
            $('#work_typeerr').css('display','block'); 
            $('#work_type').focus(); 
            return false;
        }else{
            $('#work_typeerr').css('display','none'); 
        }
		if(gender==""){
            $('#gendererr').css('display','block'); 
            $('#gender').focus(); 
            return false;
        }else{
            $('#gendererr').css('display','none'); 
        }
		if(dob==""){
            $('#doberr').css('display','block'); 
            $('#dob').focus(); 
            return false;
        }else{
            $('#doberr').css('display','none'); 
        }
		if(personal==""){
            $('#personalerr').css('display','block'); 
            $('#personal').focus(); 
            return false;
        }else{
            $('#personalerr').css('display','none'); 
        }
		if(professional==""){
            $('#professionalerr').css('display','block'); 
            $('#professional').focus(); 
            return false;
        }else{
            $('#professionalerr').css('display','none'); 
        }
		if(hobbies==""){
            $('#hobbieserr').css('display','block'); 
            $('#hobbies').focus(); 
            return false;
        }else{
            $('#hobbieserr').css('display','none'); 
        }
		if(vacation==""){
            $('#vacationerr').css('display','block'); 
            $('#vacation').focus(); 
            return false;
        }else{
            $('#vacationerr').css('display','none'); 
        }
        if(captcha_code2=="" || captcha_code2 != rannumber){
            $('#captcha_code2err').css('display','block'); 
            $('#captcha_code2').focus(); 
            return false;
        }else{
            $('#captcha_code2err').css('display','none'); 
        }

    });    
});    
</script> 

 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')