<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','40')->get();
	foreach($Pages as $Page){ }
	$Pages1 = DB::table('pages')->where('page_id','=','37')->get();
	foreach($Pages1 as $Page1){ }
	$Pages2 = DB::table('pages')->where('page_id','=','38')->get();
	foreach($Pages2 as $Page2){ }
	$Pages3 = DB::table('pages')->where('page_id','=','39')->get();
	foreach($Pages3 as $Page3){ }
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	  
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo ucwords($Page->page_heading);} ?>
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content-text">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<?php if(isset($Page->brief_desc) && $Page->brief_desc !="") { ?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
											<?php echo $Page->brief_desc; ?>
										</div>
										<?php } ?>
										
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;padding:0px;">
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($Page1->page_img) && $Page1->page_img !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page1->page_img; ?>" class="img-responsive" alt="<?php echo ucwords($Page->page_heading); ?>" style="height: auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" alt="<?php echo ucwords($Page->page_heading); ?>" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 tg-content-text">
										<a href="{!! \Config::get('app.url_base') !!}/overview-profile-training-growth" class="subheading"><?php echo ucwords($Page1->page_heading).','.ucwords($Page2->page_heading); ?></a></br>
											<?php echo $Page1->short_desc; ?>
											<a href="{!! \Config::get('app.url_base') !!}/overview-profile-training-growth" class="button">
											Read More</a>
										</div>
										</div>
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;padding:0px;">
											<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
											<?php if(isset($Page3->page_img) && $Page3->page_img !=""){ ?>
											<img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page3->page_img; ?>" class="img-responsive" alt="<?php echo ucwords($Page->page_heading); ?>" style="height: auto;width:100%;"> 
											<?php }else{ ?>
											<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" alt="<?php echo ucwords($Page->page_heading); ?>" style="height: auto;"> 
											<?php } ?>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 tg-content-text">
											<a href="{!! \Config::get('app.url_base') !!}/training-development-growth"  class="subheading"><?php echo ucwords($Page3->page_heading); ?></a><br>
												<?php echo $Page3->short_desc; ?>
												<a href="{!! \Config::get('app.url_base') !!}/training-development-growth" class="button">
												Read More</a>
											</div>
										</div>
										
										
										
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')