<?php 
	$Pages = DB::table('pages')->where('page_id','=','42')->get();
	foreach($Pages as $Page){ }
	foreach($Data as $row){ }
?>	
@include('includes.index-header')

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    
    </div>
	  
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/newsletter"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone tg-content">
										<div class="tg-titleborder  tg-content-text">
											<h2><?php echo ucwords($row->pdfname); ?></h2>
										</div>
										<div class="descdiv  tg-content">
										<style>
										    .table td{
										        text-align:left;
										      }
										</style>
										<table class="table table-bordered table-stripped table-responsive">
										<tbody>
										<tr><td>Download</td><td class="spancount"><?php  echo $row->downloads; ?></td></tr>
										<tr><td>Stock</td><td>
										<?php if($row->stock== 0 ||$row->stock == NULL ||$row->stock=='') {echo '∞';}else{echo $row->stock;}?>
										
										</td></tr>
										<tr><td>File Size</td><td><?php  echo round($row->pdfsize) . ' bytes'; ?></td></tr>
										<tr><td>Create Date</td>
										<td><?php echo date('M d, Y',strtotime($row->created_at)); ?></td>
										</tr>
										<tr><td colspan="2">
										<a class="fillink blue downlink" data-id="<?php echo $row->nlid ?>" download="<?php echo $row->pdf; ?>" target="_blank" href="{!! \Config::get('app.admin') !!}/downloads/<?php echo $row->pdf; ?>"><i class="fa fa-download"></i> Download</a> 
										</td></tr>
										</tbody></table>
										
										</div>
									</div>
								</div>
							</section>
						</div>
						<script>
						$(document).ready(function(){
							$('.downlink').click(function(){
								var Nlid = $(this).attr('data-id');
								$.ajax({
									type: 'get',
									headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
									url: '../index/downloadcount',
									data: 'Nlid='+Nlid,
									success: function (data) {
										if(data >= 0){
											$('.spancount').html(data);
										}
									}
								});	
							});
						});
						</script>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')