@include('included.login-header')

  <body class="login">
    <div>
	 
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
			<form id="sign_in" method="POST" action="{!! \Config::get('app.url_base') !!}/auth/register">
			<h1>Register</h1>
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Your Name" required autofocus>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" required>
					<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
				</div>
				<div class="form-group">
					<div class="checkbox icheck pull-left">
					<label>
					<input type="checkbox"> I agree to the <a href="#">terms</a>
					</label>
					</div>
				</div>
		
				<div class="form-group">
					<button type="submit" class="btn btn-success btn-block btn-flat">Register</button>
				</div>
				<p> <a href="/auth/login" class="pull-left">Sign In ?</a></p>

				<div class="clearfix"></div>
@include('included.login-footer')