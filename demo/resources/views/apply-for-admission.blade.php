<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','1')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.url') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.url') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2>Apply for Admission</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
											<div class="tg-contactus tg-contactusvone">
												
												<br>
												<div class="alert alert-info col-md-12 col-sm-12 col-xs-12">Do read the  
												<strong><a href="/admission-procedure">Admission Procedure</a></strong> before you fill out this form.
												</div>
												<br>
												<h4 class="pull-left">Fill up the application form below. The mandatory fields are marked with an * </h4>
												<hr>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
													<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id=""   enctype='multipart/form-data'  action="{{ url('proceed-to-payment') }}" >
													<div class="alert alert-info">Student's Detail</div>
													<fieldset>
														<div class="form-group">
															<input type="text" class="form-control"  name="name" id="name" placeholder="Student Name*">
															<span id="qnameerr" style="color:red;display:none;"> Please Enter name.</span>
														</div>
														<div class="form-group">
															<input type="text" class="datepicker form-control" name="dob" id="dob" placeholder="Date Of Birth*">
															<span id="qdateerr" style="color:red;display:none;"> Please Select Date Of Birth.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="address" id="address" placeholder="Residential Address*">
															<span id="qraddresserr" style="color:red;display:none;"> Please Enter Residential Address.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="nopskul" id="nopskul" placeholder="Name of Present School:*">
															<span id="qnskulerr" style="color:red;display:none;"> Please Enter Name of Present School.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="grade" id="grade" placeholder="Present grade*">
															<span id="qgradeerr" style="color:red;display:none;"> Please Enter present grade.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="gradetjoin" id="gradetjoin" placeholder="Grade to join*">
															<span id="qjoinerr" style="color:red;display:none;"> Grade to join</span>
														</div>
														
														<div class="form-group">
														<label for="sel1">Academic Year for which admission is sought</label>
														<select class="form-control" name="year" id="year">
														<?php 
														$start_year=date('Y');
														$end_year= date('Y')+1;
														$start_year1=$start_year+1;
														$end_year1=$end_year+1;
														for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
														<option value='<?php echo $i.'-'.$j; ?>'><?php echo $i.'-'.$j;?></option>
														<?php } ?>
														 </select>
														 <span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
														</div>
														<div class="alert alert-info">Parent Detail</div>
														<div class="form-group">
															<input type="text" class="form-control" name="nparents" id="nparents" placeholder="Parents Name*">
															<span id="qparenterr" style="color:red;display:none;"> Please Enter Parent Name.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="designation" id="designation" placeholder="Designation*">
															<span id="qdesingnationerr" style="color:red;display:none;"> Please Enter Designation.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="organization" id="organization" placeholder="Name of the Organization*">
															<span id="qorganizationerr" style="color:red;display:none;"> Please Enter Name of the Organization.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="occupation" id="occupation" placeholder="Occupation of the Working Parent*">
															<span id="qoccupationerr" style="color:red;display:none;"> Please Enter Name Occupation of the Working Parent.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="offadd" id="offadd" placeholder="Official Address:*">
															<span id="qoffadderr" style="color:red;display:none;"> Please Enter Official Address.</span>
														</div>
														<div class="form-group">
															<input type="text" class="form-control" name="city" id="city" placeholder="City:*">
															<span id="qcityerr" style="color:red;display:none;"> Please Enter City.</span>
														</div>
														<div class="form-group">
														  <select class="form-control" id="state" name="state">
															  <option value=''>Select State</option>
															  <option value='Andhra Pradesh'>Andhra Pradesh</option>
															  <option value='Arunachal Pradesh'>Arunachal Pradesh</option>
															  <option value='Assam'>Assam </option>
															  <option value='Bihar'>Bihar</option>
															  <option value='Chhattisgarh'>Chhattisgarh    </option>
															  <option value='Goa'>Goa</option>
															  <option value='Gujarat'>Gujarat</option>
															  <option value='Haryana'>Haryana </option>
															  <option value='Himachal Pradesh'>Himachal Pradesh</option>
															  <option value='Jammu & Kashmir'>Jammu & Kashmir</option>
															  <option value='Jharkhand'>Jharkhand </option>
															  <option value='Karnataka'>Karnataka </option>
															  <option value='Kerla'>Kerla </option>
															  <option value='Madhya Pradesh'>Madhya Pradesh</option>
															  <option value='Maharashtra'>Maharashtra  </option>
															  <option value='Manipur'>Manipur </option>
															  <option value='Meghalaya'>Meghalaya</option>
															  <option value='Mizoram'> Mizoram</option>
															  <option value='Nagaland'>Nagaland</option>
															  <option value='Orissa'>Orissa </option>
															  <option value='Punjab'>Punjab </option>
															  <option value='Sikkim'>Sikkim </option>
															  <option value='Tamilnadu'>Tamilnadu</option>
															  <option value='Tripura'>Tripura </option>
															  <option value='Uttar Pradesh'>Uttar Pradesh </option>
															  <option value='Uttaranchal'> Uttaranchal </option>
															  <option value='West Bengal'>West Bengal</option>
															  <option value='Outside India'>Outside India</option>
														  </select>
														  <span id="qstaterr" style="color:red;display:none;"> Please Select State.</span>
														  </div>															
													</div>
														<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
															<div class="alert alert-info">Address & Contact</div>
															<div class="form-group">
																<input type="text" class="form-control" name="country" id="country" placeholder="country:*">
																<span id="qcountryerr" style="color:red;display:none;"> Please Enter Country.</span>
															</div>
															<div class="form-group">
																<input type="text" max-length="6" class="form-control" name="pincode" id="pincode" placeholder="pincode:*" onkeypress="return isNumberKey(event)">
																<span id="qpincodeerr" style="color:red;display:none;"> Please Enter 6 digit Pincode.</span>
															</div>
															
															<div class="form-group">
															  <input type="tel" max-length="10" class="form-control" name="restel" id="restel" placeholder="Residence Tel:*" onkeypress="return isNumberKey(event)">
															  <span id="qrtelnerr" style="color:red;display:none;"> Please Enter 10 Digit Residence Tel.</span>
															</div>
															<div class="form-group">
															  <input type="tel" max-length="10" class="form-control" name="offtel" name="offtel" placeholder="Office Tel:*" onkeypress="return isNumberKey(event)">
															  <span id="qotelnerr" style="color:red;display:none;"> Please Enter 10 digit Office Tel.</span>
															</div>
															<div class="form-group">
															  <input type="text" max-length="10" class="form-control" name="mnfather" id="mnfather" placeholder="Mobile Number Father:*" onkeypress="return isNumberKey(event)">
															  <span id="qmfathererr" style="color:red;display:none;"> Please Enter 10 Digit Mobile Number Father.</span>
															</div>
															<div class="form-group">
															  <input type="text" max-length="10" class="form-control" name="mnmother" id="mnmother" placeholder="Mobile Number Mother:*" onkeypress="return isNumberKey(event)">
															   <span id="qmmothererr" style="color:red;display:none;"> Please Enter 10 digit Mobile Number Mother.</span>
															</div>
															<div class="form-group">
																<input type="email" class="form-control" name="emailf" id="emailf" placeholder="Email Address Father:*">
																 <span id="qfemailerr" style="color:red;display:none;"> Please Enter Email Address Father.</span>
															</div>
															<div class="form-group">
																<input type="email" class="form-control" name="emailm" id="emailm" placeholder="Email Address Mother:*">
																 <span id="qmemailerr" style="color:red;display:none;"> Please Enter Email Address Mother.</span>
															</div>
															<div class="form-group">
																<label for="passport">Passsport Type :*</label>
																  <select class="form-control" id="passport" name="passport">
																	<option value="Indian Passport">Indian Passport</option>
																	<option value="Non-Indian Passport">Non-Indian Passport</option>
																  </select>
																  <span id="qpassporterr" style="color:red;display:none;"> Please Enter Passport Type.</span>
															</div>
															<div class="form-group">
															<textarea class="form-control" placeholder="How you heard about our School?*" name="ourskul" id="ourskul"></textarea>
															 <span id="qschoolerr" style="color:red;display:none;"> Tell us how you heard about our School.</span>
															</div>
															
															<div class="form-group">
																<input type="hidden" id="rannumber" name="rannumber" class="captcha">
																<div class="captcha col-md-4" style="border: 2px solid green; width: 150px; line-height:1.4; height: 45px; text-align: center; vertical-align: middle; font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">
																</div>
																<div class="col-md-2" style="height:40px;margin-bottom:5px;">
																  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;" >
																  <i class="fa fa-refresh"></i>
																  </a>
																</div>
															</div>
															
															<div class="form-group">
															    <input class="form-control" id="captcha_code2" name="captcha_code2"  placeholder="Enter Captcha" type="text" style="text-transform:none;">
																<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
															</div>
															
															<div class="form-group">
															    <img src="{!! \Config::get('app.url') !!}/images/cc-avenue.jpg" class="responsive">
															</div>
															
															<div class="col-10 fix text-center">
															<div class="success1"></div>
															<div class="error1"></div>
															{{ csrf_field() }}
															</div>
															<div class="form-group">
															<button type="submit" class="button" name="job-submit" id="admission-btn" >Procced to pay <i class="icon-right-arrow"></i></button>
															</div>
														</div>
														</fieldset>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
	
<script type="text/javascript">

$(document).ready(function(){
	$('#admission-btn').click(function(){
	var name = $('#name').val();
	var dob = $('#dob').val();
	var address = $('#address').val();
	var nopskul = $('#nopskul').val();
	var grade = $('#grade').val();
	var gradetjoin = $('#gradetjoin').val();
	var year = $('#year').val();
	var nparents = $('#nparents').val();
	var designation = $('#designation').val();
	var organization = $('#organization').val();
	var occupation = $('#occupation').val();
	var offadd = $('#offadd').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	var restel = $('#restel').val();
	var offtel = $('#offtel').val();
	var mnfather = $('#mnfather').val();
	var mnmother = $('#mnmother').val();
	var emailf = $('#emailf').val();
	var emailm = $('#emailm').val();
	var passport = $('#passport').val();
	var ourskul = $('#ourskul').val();
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

	    if(name==""){
			$('#qnameerr').css('display','block'); 
			$('#name').focus(); 
			return false;
		}else{
			$('#qnameerr').css('display','none'); 
		}
		if(dob==""){
			$('#qdateerr').css('display','block'); 
			$('#dob').focus(); 
			return false;
		}else{
			$('#qdateerr').css('display','none'); 
		}
		if(address==""){
			$('#qraddresserr').css('display','block'); 
			$('#address').focus(); 
			return false;
		}else{
			$('#qraddresserr').css('display','none'); 
		}
		if(nopskul==""){
			$('#qnskulerr').css('display','block'); 
			$('#nopskul').focus(); 
			return false;
		}else{
			$('#qnskulerr').css('display','none'); 
		}
		if(grade==""){
			$('#qgradeerr').css('display','block'); 
			$('#grade').focus(); 
			return false;
		}else{
			$('#qgradeerr').css('display','none'); 
		}
		if(gradetjoin==""){
			$('#qjoinerr').css('display','block'); 
			$('#gradetjoin').focus(); 
			return false;
		}else{
			$('#qjoinerr').css('display','none'); 
		}
		if(year==""){
			$('#qayearerr').css('display','block'); 
			$('#year').focus(); 
			return false;
		}else{
			$('#qayearerr').css('display','none'); 
		}
		if(nparents==""){
			$('#qparenterr').css('display','block'); 
			$('#nparents').focus(); 
			return false;
		}else{
			$('#qparenterr').css('display','none'); 
		}
		if(designation==""){
			$('#qdesingnationerr').css('display','block'); 
			$('#designation').focus(); 
			return false;
		}else{
			$('#qdesingnationerr').css('display','none'); 
		}
		if(organization==""){
			$('#qorganizationerr').css('display','block'); 
			$('#organization').focus(); 
			return false;
		}else{
			$('#qorganizationerr').css('display','none'); 
		}
		if(occupation==""){
			$('#qoccupationerr').css('display','block'); 
			$('#occupation').focus(); 
			return false;
		}else{
			$('#qoccupationerr').css('display','none'); 
		}
		if(offadd==""){
			$('#qoffadderr').css('display','block'); 
			$('#offadd').focus(); 
			return false;
		}else{
			$('#qoffadderr').css('display','none'); 
		}
		if(city==""){
			$('#qcityerr').css('display','block'); 
			$('#city').focus(); 
			return false;
		}else{
			$('#qcityerr').css('display','none'); 
		}
		if(state==""){
			$('#qstaterr').css('display','block'); 
			$('#state').focus(); 
			return false;
		}else{
			$('#qstaterr').css('display','none'); 
		}
		if(country==""){
			$('#qcountryerr').css('display','block'); 
			$('#country').focus(); 
			return false;
		}else{
			$('#qcountryerr').css('display','none'); 
		}
		if(pincode=="" || pincode.length<6){
			$('#qpincodeerr').css('display','block'); 
			$('#pincode').focus(); 
			return false;
		}else{
			$('#qpincodeerr').css('display','none'); 
		}
		if(restel=="" || restel.length<10 || restel.length>15){
			$('#qrtelnerr').css('display','block'); 
			$('#restel').focus(); 
			return false;
		}else{
			$('#qrtelnerr').css('display','none'); 
		}
		if(offtel=="" || offtel.length<10 || offtel.length>15){
			$('#qotelnerr').css('display','block'); 
			$('#offtel').focus(); 
			return false;
		}else{
			$('#qotelnerr').css('display','none'); 
		}
		if(mnfather=="" || mnfather.length<10 || mnfather.length>15){
			$('#qmfathererr').css('display','block'); 
			$('#mnfather').focus(); 
			return false;
		}else{
			$('#qmfathererr').css('display','none'); 
		}
		if(mnmother=="" || mnmother.length<10 || mnmother.length>15){
			$('#qmmothererr').css('display','block'); 
			$('#mnmother').focus(); 
			return false;
		}else{
			$('#qmmothererr').css('display','none'); 
		}
		
		if(emailf=="" || !email_regex.test(emailf)){
			$('#qfemailerr').css('display','block'); 
			$('#emailf').focus(); 
			return false;
		}else{
			$('#qfemailerr').css('display','none'); 
		}
		if(emailm=="" || !email_regex.test(emailm)){
			$('#qmemailerr').css('display','block'); 
			$('#emailm').focus(); 
			return false;
		}else{
			$('#qmemailerr').css('display','none'); 
		}
		
		if(passport==""){
			$('#qpassporterr').css('display','block'); 
			$('#passport').focus(); 
			return false;
		}else{
			$('#qpassporterr').css('display','none'); 
		}
		if(ourskul==""){
			$('#qschoolerr').css('display','block'); 
			$('#ourskul').focus(); 
			return false;
		}else{
			$('#qschoolerr').css('display','none'); 
		}
		
		if(captcha_code2=="" || captcha_code2 != rannumber){
			$('#captcha_code2err').css('display','block'); 
			$('#captcha_code2').focus(); 
			return false;
		}else{
			$('#captcha_code2err').css('display','none'); 
		}

	});	
});	
</script> 
	
	
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')