<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','39')->get();
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
        <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/faculty-and-staff">Faculty & Career Opening</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo ucwords($Page->page_heading);} ?> 
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
		
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<div id="tg-content" class="tg-content">
			<section class="tg-sectionspace tg-haslayout">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin-bottom: 10px;">
				<div class="tg-contactus tg-contactusvone">
					<div class="tg-titleborder tg-content">
						<h2>Career Opening</h2>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text" style="margin-bottom:30px;padding:0px;">
							<?php echo $Page->brief_desc; ?>
					</div>
					
				</div>
			 
				<div class="tg-titleborder tg-content">
				   <h2>Apply Now</h2>
				  <!--<h3 class="progress-bar-striped">Step 1 of 6</h3>
				  <div id="Progressbar1">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:16%;height: 20px;border-radius:5px;"> 16% </div>
				
				  </div>-->
				</div>
				</div>
				
				<hr>
				
				<p  class="tg-content-text">Fill up the application form below. The mandatory fields are marked with an *</p>
				
				<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id=""   enctype='multipart/form-data'  action="{{ url('applyjob') }}" >
				<div class="alert alert-info">Personal Details</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="padding:0px;">
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
					  <select class="form-control" name="position" id="position">
						<option value='' >Position applying for?</option>
						<option value='Secretary' >Secretary</option>
						<option value='Receptionist' >Receptionist</option>
						<option value='Lab Assistant' >Lab Assistant</option>
						<option value='Executive Assistant' >Executive Assistant</option>
						<option value='Company Secretary' >Company Secretary</option>
						<option value='Admissions officer' >Admissions officer</option>
						<option value='Asst. College Counselor' >Asst. College Counselor</option>
						<option value='Director of Creative Arts' >Director of Creative Arts</option>
						<option value='Director of Sports Activities' >Director of Sports Activities</option>
						<option value='College Counsellor' >College Counsellor</option>
						<option value='Teacher for English /ESL- MYP & IB Diploma' >Teacher for English /ESL- MYP & IB Diploma</option>
						<option value='Teacher for Maths- MYP & IB Diploma' >Teacher for Maths- MYP & IB Diploma</option>
						<option value='Teacher for General Science - MYP & IB Diploma' >Teacher for General Science - MYP & IB Diploma</option>
						<option value='Teacher for Humanities - MYP & IB Diploma' >Teacher for Humanities - MYP & IB Diploma</option>
						<option value='Teacher for Physics - MYP & IB Diploma' >Teacher for Physics - MYP & IB Diploma</option>
						<option value='Teacher for Physical Education- MYP & DP' >Teacher for Physical Education- MYP & DP</option>
						<option value='Teacher for Geography - MYP & IB Diploma' >Teacher for Geography - MYP & IB Diploma</option>
						<option value='Teacher for Chemistry - MYP & IB Diploma' >Teacher for Chemistry - MYP & IB Diploma</option>
						<option value='Teacher for History - MYP & IB Diploma' >Teacher for History - MYP & IB Diploma</option>
						<option value='Teacher for Biology - MYP & IB Diploma' >Teacher for Biology - MYP & IB Diploma</option>
						<option value='Teacher for Psychology - MYP & IB Diploma' >Teacher for Psychology - MYP & IB Diploma</option>
						<option value='Teacher for Physics & Chemistry- MYP & IB Diploma' >Teacher for Physics & Chemistry- MYP & IB Diploma</option>
						<option value='Teacher for Environmental System and Society - MYP & IB Diploma' >Teacher for Environmental System and Society - MYP & IB Diploma</option>
						<option value='Teacher for Economics - MYP & IB Diploma' >Teacher for Economics - MYP & IB Diploma</option>
						<option value='Teacher for Computer Science - MYP & IB Diploma' >Teacher for Computer Science - MYP & IB Diploma</option>
						<option value='Teacher for Business Management -   IB Diploma' >Teacher for Business Management -   IB Diploma</option>
						<option value='Teacher for Design Technology - MYP & IB Diploma' >Teacher for Design Technology - MYP & IB Diploma</option>
						<option value='Teacher for Visual Art - MYP & IB Diploma' >Teacher for Visual Art - MYP & IB Diploma</option>
						<option value='Teacher for Hindi - MYP & IB Diploma' >Teacher for Hindi - MYP & IB Diploma</option>
						<option value='Teacher for Music - MYP & IB Diploma' >Teacher for Music - MYP & IB Diploma</option>
						<option value='Teacher for Theatre Arts- MYP & IB Diploma' >Teacher for Theatre Arts- MYP & IB Diploma</option>
						<option value='Teacher for French - MYP & IB Diploma' >Teacher for French - MYP & IB Diploma</option>
						<option value='Teacher for Drama - MYP & IB Diploma' >Teacher for Drama - MYP & IB Diploma</option>
						<option value='Teacher for Spanish - MYP & IB Diploma' >Teacher for Spanish - MYP & IB Diploma</option>
						<option value='Teacher for I. T.  - MYP & IB Diploma' >Teacher for I. T.  - MYP & IB Diploma</option>
						<option value='Teacher for ESL -   MYP & IB Diploma' >Teacher for ESL -   MYP & IB Diploma</option>
						<option value='Teacher for Physical Education - PYP' >Teacher for Physical Education - PYP</option>
						<option value='Teacher for Visual Arts -  PYP' >Teacher for Visual Arts -  PYP</option>
						<option value='Head Librarian - PYP' >Head Librarian - PYP</option>
						<option value='Librarian – MYP & IB Diploma' >Librarian – MYP & IB Diploma</option>
						<option value='Asst. Librarian' >Asst. Librarian</option>
						<option value='Special Needs Educator – MYP & IB Diploma' >Special Needs Educator – MYP & IB Diploma</option>
						<option value='Teacher for Dance – PYP & MYP' >Teacher for Dance – PYP & MYP</option>
						<option value='Homeroom Teacher for Primary Years (Grade 1 to 5)' >Homeroom Teacher for Primary Years (Grade 1 to 5)</option>
						<option value='Homeroom Teacher for Early Years' >Homeroom Teacher for Early Years</option>
						<option value='Co- Teacher for Primary Years (Grade 1 to 5)' >Co- Teacher for Primary Years (Grade 1 to 5)</option>
						<option value='Co- Teacher for Early Years' >Co- Teacher for Early Years</option>
						<option value='Maintenance Manager' >Maintenance Manager</option>
						<option value='Finance Manager' >Finance Manager</option>
						<option value='Finance Officer' >Finance Officer</option>
						<option value='Administration Officer' >Administration Officer</option>
						<option value='PYP Coordinator' >PYP Coordinator</option>
						<option value='Manager (Accounts)' >Manager (Accounts)</option>
						</select>
						<span id="positionerr" style="color:red;display:none;"> Please Select Position applying for?</span>
					</div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name*">
						<span id="nameerr" style="color:red;display:none;"> Please Enter Name.</span>
					</div>
					
				   <div class="form-group col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="gender" id="gender">
							<option value="">Gender?</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
						<span id="gendererr" style="color:red;display:none;"> Please Enter Gender</span>
					</div>
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="nationality" id="nationality" placeholder="Nationality*">
						<span id="nationalityerr" style="color:red;display:none;"> Please Enter Nationality</span>
					</div>
					
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
						<label>DD-MM-YYYY</label>
						<input type="text" class="datepicker form-control" id="dob" name="dob" placeholder="Date of Birth*">
						<span id="doberr" style="color:red;display:none;"> Please Enter Date of Birth</span>
					</div>
					
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
						<label>Upload Photograph (Accepted file types: jpg, gif, png, jpeg, bmp.)</label>
						<input type="file" name="photo" id="photo" class="form-control" placeholder="Upload Photograph">
						<span id="photoerr" style="color:red;display:none;"> Please Upload Photograph.</span>
					</div>		
					
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<select class="form-control" name="marital_status" id="marital_status">
							<option value="">Marital Status?</option>
							<option value="Married">Married</option>
							<option value="Unmarried">Unmarried</option>
							<option value="Other">Other</option>
						</select>
						<span id="marital_statuserr" style="color:red;display:none;"> Please Enter Marital Status.</span>
				   </div>

				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<textarea type="text" class="form-control" name="children" id="children" placeholder="Children's Name - Age"></textarea>
						<span id="childrenerr" style="color:red;display:none;"> Please Enter Children's Names - Age.</span>
				   </div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="cadd" id="cadd" placeholder="Current Address*">
						<span id="cadderr" style="color:red;display:none;"> Please Enter Current Address.</span>
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" max-length="6" class="form-control" name="pincode" id="pincode" placeholder="Zipcode" onkeypress="return isNumberKey(event)">
						<span id="pincodeerr" style="color:red;display:none;"> Please Enter 6 Digit Zipcode.</span>
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" max-length="10" class="form-control" name="landline" id="landline" placeholder="Landline no.*" onkeypress="return isNumberKey(event)">
						<span id="landlineerr" style="color:red;display:none;"> Please Enter Landline no.</span>
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" max-length="10" class="form-control" name="mob_no" id="mob_no" placeholder="Mobile no." onkeypress="return isNumberKey(event)">
						<span id="mob_noerr" style="color:red;display:none;"> Please Enter 10 Digit Mobile no.</span>
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="email_id" id="email_id" placeholder="Email id*">
						<span id="email_iderr" style="color:red;display:none;"> Please Enter Email Id.</span>
				   </div>
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="padd" id="padd" placeholder="Permanent Address (Write ‘same’ if the same as current address)*">
						<span id="padderr" style="color:red;display:none;"> Please Enter Permanent Address.</span>
				   </div>
							
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				   <div class="alert alert-info">Passport Details</div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="passport_no" id="passport_no" placeholder="Passport no.*">
						<span id="passport_noerr" style="color:red;display:none;"> Please Enter Passport no.</span>
				   </div>	
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="issue_date" id="issue_date" placeholder="Passport issue date (dd-mm-yyyy)*">
						<span id="issue_dateerr" style="color:red;display:none;"> Please Enter Passport issue date.</span>
				   </div>	
				   	
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="issue_place" id="issue_place" placeholder="Issue Place*">
						<span id="issue_placeerr" style="color:red;display:none;"> Please Enter Issue Place.</span>
					</div>	
				   	
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" name="expiry_date" id="expiry_date" placeholder="Expiry Date (dd-mm-yyyy)*">
						<span id="expiry_dateerr" style="color:red;display:none;"> Please Enter Expiry Date.</span>
					</div>					   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				   <div class="alert alert-info">Educational Details</div>
				<table class="table table-responsive table-hover table-bordered input_fields_wrap">
				<tr style="font-weight:200;font-size:12px;">
				<th>University</th>
				<th>Board</th>
				<th>Start Date</th>
				<th>Complition Date</th>
				<th>Grade</th>
				<th>Percentage</th>
				<th></th>
				</tr>
				<tr>
				<td><input type="text" class="form-control" name="university[]" id="university" placeholder="University*"></td>
				<td><input type="text" class="form-control" name="board[]" id="board" placeholder="Board*"></td>
				<td><input type="text" class="form-control" name="start_date[]" id="start_date" placeholder="Start Date*"></td>
				<td><input type="text" class="form-control" name="completion_date[]" id="completion_date" placeholder="Complition Date*"></td>
				<td><input type="text" class="form-control" name="grade[]" id="grade" placeholder="Grade*"></td>
				<td><input type="text" class="form-control" name="percentage[]" id="percentage" placeholder="Percentage*"></td>
				<td><span class="btn btn-primary btn-sm green" id="add_attributes" name="add_attributes"><i class="fa fa-plus"></i></span></td>
				</tr>
				</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
					<div class="alert alert-info">List the professional development courses attended by you, if any</div>
					<table class="table table-responsive table-hover table-bordered pctbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Attended</th>
					<th>Course</th>
					<th>Start Date</th>
					<th>Complition Date</th>
					<th>Location</th>
					<th></th>
					</tr>
					<tr>
					<td><input type="text" class="form-control" name="pattended[]" id="pattended" placeholder="Attended*"></td>
					<td><input type="text" class="form-control" name="pcourse[]" id="pcourse" placeholder="Course*"></td>
					<td><input type="text" class="form-control" name="pstart_date[]" id="pstart_date" placeholder="Start Date*"></td>
					<td><input type="text" class="form-control" name="pcompletion_date[]" id="pcompletion_date" placeholder="Completion Date*"></td>
					<td><input type="text" class="form-control" name="plocation[]" id="plocation" placeholder="Location*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addpc" name="addpc"><i class="fa fa-plus"></i></span></td>
					</tr>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
					<div class="alert alert-info">List the professional development courses conducted by you, if any</div> 
					<table class="table table-responsive table-hover table-bordered pctbl1">
					<tr style="font-weight:200;font-size:12px;">
					<th>Conducted</th>
					<th>Course</th>
					<th>Start Date</th>
					<th>Complition Date</th>
					<th>Location</th>
					<th></th>
					</tr>
					<tr>
					<td><input type="text" class="form-control" name="pattended1[]" id="pattended1" placeholder="Conducted*"></td>
					<td><input type="text" class="form-control" name="pcourse1[]" id="pcourse1" placeholder="Course*"></td>
					<td><input type="text" class="form-control" name="pstart_date1[]" id="pstart_date1" placeholder="Start Date*"></td>
					<td><input type="text" class="form-control" name="pcompletion_date1[]" id="pcompletion_date1" placeholder="Completion Date*"></td>
					<td><input type="text" class="form-control" name="plocation1[]" id="plocation1" placeholder="Location*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addpc1" name="addpc1"><i class="fa fa-plus"></i></span></td>
					</tr>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
					<div class="alert alert-info">Publications/Research Papers/ Documents produced (in the last 5 years)</div>
					<table class="table table-responsive table-hover table-bordered prtbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Title</th>
					<th>Published in</th>
					<th>Date of Publishing</th>
					<th>Presented at</th>
					<th>Date of Presentation</th>
					<th></th>
					</tr>
					<tr>
					<td><input type="text" class="form-control" name="prtitle[]" id="prtitle" placeholder="Title*"></td>
					<td><input type="text" class="form-control" name="publish_in[]" id="publish_in" placeholder="Published In*"></td>
					<td><input type="text" class="form-control" name="publish_date[]" id="publish_date" placeholder="Publish Date*"></td>
					<td><input type="text" class="form-control" name="presented_at[]" id="presented_at" placeholder="Presented At*"></td>
					<td><input type="text" class="form-control" name="presentation_date[]" id="presentation_date" placeholder="Presentation Date*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addpr" name="addpr"><i class="fa fa-plus"></i></span></td>
					</tr>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				   <div class="alert alert-info">Other Details</div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="internation_school_exp" name="internation_school_exp" placeholder="Experience in International Schools*">
				   </div>	
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
				   <label>Experience In</label>
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="IB"> IB
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="IGCSE"> IGCSE
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="A Level"> A Level
						<input type="checkbox" class="" name="experience_in[]" id="experience_in" value="None"> None
				   </div>	
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="experience_year" name="experience_year" placeholder="Experience in Years*">
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="current_employer" name="current_employer" placeholder="Current Employer*">
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="designation" name="designation" placeholder="Designation*">
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="joindt_curr_emp" name="joindt_curr_emp" placeholder="Joining Date of Current Job*">
					</div>				   
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
					<div class="alert alert-info">Employment Details</div>
					<table class="table table-responsive table-hover table-bordered ehtbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Employer Name</th>
					<th>Designation</th>
					<th>Period</th>
					<th>Subjects</th>
					<th>Curriculum</th>
					<th>Responsibility</th>
					<th>Leaving Reason</th>
					<th></th>
					</tr>
					<tr>
					<td><input type="text" class="form-control" name="eh_name[]" id="eh_name" placeholder="Employer Name*"></td>
					<td><input type="text" class="form-control" name="eh_designation[]" id="eh_designation" placeholder="Designation*"></td>
					<td><input type="text" class="form-control" name="period[]" id="period" placeholder="Period*"></td>
					<td><input type="text" class="form-control" name="subjects[]" id="subjects" placeholder="Subjects*"></td>
					<td><input type="text" class="form-control" name="curriculum[]" id="curriculum" placeholder="Curriculum*"></td>
					<td><input type="text" class="form-control" name="responsibility[]" id="responsibility" placeholder="Responsibility*"></td>
					<td><input type="text" class="form-control" name="leaving_reason[]" id="leaving_reason" placeholder="Leaving Reason*"></td>
					<td><span class="btn btn-primary btn-sm green" id="addeh" name="addeh"><i class="fa fa-plus"></i></span></td>
					</tr>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				   <div class="alert alert-info">Skills & Interests</div>
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="it_skills" name="it_skills" placeholder="Skills">
						<span id="it_skillserr" style="color:red;display:none;"> Please Enter Skills In You're Good</span>
				   </div>	
				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="hobbies" name="hobbies" placeholder="Hobbies">
						<span id="hobbieserr" style="color:red;display:none;"> Please Enter Hobbies.</span>
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="current_sal" name="current_sal" placeholder="Current Salary*">
						<span id="current_salerr" style="color:red;display:none;"> Please Enter Current Salary.</span>
					</div>				   
					<div class="form-group col-md-6 col-sm-6 col-xs-12"> 
						<input type="text" class="form-control" id="expected_sal" name="expected_sal" placeholder="Expected Salary*">
						<span id="expected_salerr" style="color:red;display:none;"> Please Enter Expected Salary.</span>
					</div>	

				   <div class="form-group col-md-12 col-sm-6 col-xs-12"> 
						<textarea type="text" class="form-control" id="language_speak" name="language_speak" placeholder="Language Skills*"></textarea>
						<span id="language_speakerr" style="color:red;display:none;"> Please Enter Language Known?</span>
				   </div>	
				   					
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text" style="overflow-x:auto">
					<div class="alert alert-info">Confidential Referees Contact Details</div>
					<table class="table table-responsive table-hover table-bordered ertbl">
					<tr style="font-weight:200;font-size:12px;">
					<th>Reference Name</th>
					<th>Designation</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Address</th>
					<th></th>
					</tr>
					<tr>
					<td><input type="text" class="form-control" name="ername[]" id="ername" placeholder="Reference Name*"></td>
					<td><input type="text" class="form-control" name="erdesignation[]" id="erdesignation" placeholder="Designation*"></td>
					<td><input type="text" class="form-control" name="erphone[]" id="erphone" placeholder="Phone No*"></td>
					<td><input type="text" class="form-control" name="eremail[]" id="eremail" placeholder="Email*"></td>
					<td><input type="text" class="form-control" name="eraddress[]" id="eraddress" placeholder="Address*"></td>
					<td><span class="btn btn-primary btn-sm green" id="adder" name="adder"><i class="fa fa-plus"></i></span></td>
					</tr>
					</table>			   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				
				   <div class="form-group col-md-6 col-sm-6 col-xs-12" style="padding:0px;"> 
						<textarea type="text" class="form-control" id="edu_statement" name="edu_statement" placeholder="Personal Statement of Education*"></textarea>
						<span id="edu_statementerr" style="color:red;display:none;"> Please Enter Personal Statement of Education.</span>
				   </div>	
				   
				   <div class="form-group col-md-6 col-sm-6 col-xs-12"  style="padding:0px;"> 
						<textarea type="text" class="form-control" id="other_info" name="other_info" placeholder="Any other information, if required*"></textarea>
				   </div>

					<div class="form-group col-md-6 col-sm-6 col-xs-7">
						<input type="hidden" id="rannumber" name="rannumber" class="captcha">
						<div class="captcha col-md-6 col-sm-4 col-xs-6" style="border: 2px solid green;line-height:1.4; height: 45px; text-align: center; vertical-align: middle; font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">
						</div>
						<div class="col-md-1">
						  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;" >
						  <i class="fa fa-refresh"></i>
						  </a>
						  <br><br>
						</div>
					  
					</div>	
					
					 <div class="form-group col-md-6 col-sm-6 col-xs-5"  style="padding:0px;"> 
						<input class="form-control" id="captcha_code2" name="captcha_code2" placeholder="Enter Captcha" type="text" style="text-transform:none;">
						<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
				   </div>
				   
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tg-content-text">
				{{ csrf_field() }}
				<button  type="submit" class="button" id="job-submit1">Submit</button>
				<button  type="reset" class="button">Clear</button>
				</div>
				
				</form>
			</section>
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    <hr>
			    <p><strong>Related Links</strong></p>
			    <a class="button" href="{!! \Config::get('app.url_base') !!}/overview-profile-training-growth/" style="margin:5px;">Overview & Training , Development & Growth</a>
		    	<a class="button" href="{!! \Config::get('app.url_base') !!}/recognition-promotion/" style="margin:5px;">Recognition &amp; Promotion</a>
                <a class="button" href="{!! \Config::get('app.url_base') !!}/opportunities/" style="margin:5px;">Opportunities</a>
                <a class="button" href="{!! \Config::get('app.url_base') !!}/in-service-training/" style="margin:5px;">In Service Training</a>
                <a class="button" href="{!! \Config::get('app.url_base') !!}/teacher-profile/" style="margin:5px;">Teacher Profile</a>
            </div>
                                
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			@include('includes.index-sidebar')
		</div>	
	</main>
	
<script type="text/javascript">

$(document).ready(function(){
	$('#job-submit1').click(function(){
	var position = $('#position').val();
	var name = $('#name').val();
	var gender = $('#gender').val();
	var nationality = $('#nationality').val();
	var dob = $('#dob').val();
	var photo = $('#photo').val();
	var marital_status = $('#marital_status').val();
	var children = $('#childrenXXX').val();
	var cadd = $('#cadd').val();
	var pincode = $('#pincode').val();
	var landline = $('#landline').val();
	var mob_no = $('#mob_no').val();
	var email_id = $('#email_id').val();
	var padd = $('#padd').val();
	var passport_no = $('#passport_no').val();
	var issue_date = $('#issue_date').val();
	var issue_place = $('#issue_place').val();
	var expiry_date = $('#expiry_date').val();
	var internation_school_exp = $('#internation_school_exp').val();
	var experience_in = $('#experience_in').val();
	var experience_year = $('#experience_year').val();
	var current_employer = $('#current_employer').val();
	var designation = $('#designation').val();
	var joindt_curr_emp = $('#joindt_curr_emp').val();
	var it_skills = $('#it_skills').val();
	var hobbies = $('#hobbies').val();
	var current_sal = $('#current_sal').val();
	var expected_sal = $('#expected_sal').val();
	var language_speak = $('#language_speak').val();
	var edu_statement = $('#edu_statementXXX').val();
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
	
	    if(position==""){
			$('#positionerr').css('display','block'); 
			$('#position').focus(); 
			return false;
		}else{
			$('#positionerr').css('display','none'); 
		}
		if(name==""){
			$('#nameerr').css('display','block'); 
			$('#name').focus(); 
			return false;
		}else{
			$('#nameerr').css('display','none'); 
		}
		if(gender==""){
			$('#gendererr').css('display','block'); 
			$('#gender').focus(); 
			return false;
		}else{
			$('#gendererr').css('display','none'); 
		}
		if(nationality==""){
			$('#nationalityerr').css('display','block'); 
			$('#nationality').focus(); 
			return false;
		}else{
			$('#nationalityerr').css('display','none'); 
		}
		if(dob==""){
			$('#doberr').css('display','block'); 
			$('#dob').focus(); 
			return false;
		}else{
			$('#doberr').css('display','none'); 
		}
		if(photo==""){
			$('#photoerr').css('display','block'); 
			$('#photo').focus(); 
			return false;
		}else{
			$('#photoerr').css('display','none'); 
		}
		if(marital_status==""){
			$('#marital_statuserr').css('display','block'); 
			$('#marital_status').focus(); 
			return false;
		}else{
			$('#marital_statuserr').css('display','none'); 
		}
		if(children==""){
			$('#childrenerr').css('display','block'); 
			$('#children').focus(); 
			return false;
		}else{
			$('#childrenerr').css('display','none'); 
		}
		if(cadd==""){
			$('#cadderr').css('display','block'); 
			$('#cadd').focus(); 
			return false;
		}else{
			$('#cadderr').css('display','none'); 
		}
	/*	if(pincode=="" || pincode.length<6){
			$('#pincodeerr').css('display','block'); 
			$('#pincode').focus(); 
			return false;
		}else{
			$('#pincodeerr').css('display','none'); 
		} */
		if(landline=="" || landline.length<10 || landline.length>15){
			$('#landlineerr').css('display','block'); 
			$('#landline').focus(); 
			return false;
		}else{
			$('#landlineerr').css('display','none'); 
		}
	/*	if(mob_no=="" || mob_no.length<10 || mob_no.length>15){
			$('#mob_noerr').css('display','block'); 
			$('#mob_no').focus(); 
			return false;
		}else{
			$('#mob_noerr').css('display','none'); 
		} */
		
		if(email_id=="" || !email_regex.test(email_id)){
			$('#email_iderr').css('display','block'); 
			$('#email_id').focus(); 
			return false;
		}else{
			$('#email_iderr').css('display','none'); 
		}
		
		if(padd==""){
			$('#padderr').css('display','block'); 
			$('#padd').focus(); 
			return false;
		}else{
			$('#padderr').css('display','none'); 
		}
		if(passport_no==""){
			$('#passport_noerr').css('display','block'); 
			$('#passport_no').focus(); 
			return false;
		}else{
			$('#passport_noerr').css('display','none'); 
		}
		if(issue_date==""){
			$('#issue_dateerr').css('display','block'); 
			$('#issue_date').focus(); 
			return false;
		}else{
			$('#issue_dateerr').css('display','none'); 
		}
		if(issue_place==""){
			$('#issue_placeerr').css('display','block'); 
			$('#issue_place').focus(); 
			return false;
		}else{
			$('#issue_placeerr').css('display','none'); 
		}
		if(expiry_date==""){
			$('#expiry_dateerr').css('display','block'); 
			$('#expiry_date').focus(); 
			return false;
		}else{
			$('#expiry_dateerr').css('display','none'); 
		}
		if(internation_school_exp==""){
			$('#internation_school_experr').css('display','block'); 
			$('#internation_school_exp').focus(); 
			return false;
		}else{
			$('#internation_school_experr').css('display','none'); 
		}
		if(experience_in==""){
			$('#experience_inerr').css('display','block'); 
			$('#experience_in').focus(); 
			return false;
		}else{
			$('#experience_inerr').css('display','none'); 
		}
		if(experience_year==""){
			$('#experience_yearerr').css('display','block'); 
			$('#experience_year').focus(); 
			return false;
		}else{
			$('#experience_yearerr').css('display','none'); 
		}
		if(current_employer==""){
			$('#current_employererr').css('display','block'); 
			$('#current_employer').focus(); 
			return false;
		}else{
			$('#current_employererr').css('display','none'); 
		}
		if(designation==""){
			$('#designationerr').css('display','block'); 
			$('#designation').focus(); 
			return false;
		}else{
			$('#designationerr').css('display','none'); 
		}
		if(joindt_curr_emp==""){
			$('#joindt_curr_emperr').css('display','block'); 
			$('#joindt_curr_emp').focus(); 
			return false;
		}else{
			$('#joindt_curr_emperr').css('display','none'); 
		}
	/*	if(it_skills==""){
			$('#it_skillserr').css('display','block'); 
			$('#it_skills').focus(); 
			return false;
		}else{
			$('#it_skillserr').css('display','none'); 
		}
		if(hobbies==""){
			$('#hobbieserr').css('display','block'); 
			$('#hobbies').focus(); 
			return false;
		}else{
			$('#hobbieserr').css('display','none'); 
		}
		*/
		if(current_sal==""){
			$('#current_salerr').css('display','block'); 
			$('#current_sal').focus(); 
			return false;
		}else{
			$('#current_salerr').css('display','none'); 
		}
		if(expected_sal==""){
			$('#expected_salerr').css('display','block'); 
			$('#expected_sal').focus(); 
			return false;
		}else{
			$('#expected_salerr').css('display','none'); 
		} 
		if(language_speak==""){
			$('#language_speakerr').css('display','block'); 
			$('#language_speak').focus(); 
			return false;
		}else{
			$('#language_speakerr').css('display','none'); 
		}
		if(edu_statement==""){
			$('#edu_statementerr').css('display','block'); 
			$('#edu_statement').focus(); 
			return false;
		}else{
			$('#edu_statementerr').css('display','none'); 
		}
		
		if(captcha_code2=="" || captcha_code2 != rannumber){
			$('#captcha_code2err').css('display','block'); 
			$('#captcha_code2').focus(); 
			return false;
		}else{
			$('#captcha_code2err').css('display','none'); 
		}

	});	
});	
</script> 

<script type="text/javascript">
//$(document).ready(function() {
	
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 0; //initlal text box count
    $("#add_attributes").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<tr class="'+x+'"><td><input type="text" class="form-control" name="university[]" id="university'+x+'" placeholder="University*"></td><td><input type="text" class="form-control" name="board[]" id="board'+x+'" placeholder="Board*"></td><td><input type="text" class="form-control" name="start_date[]" id="start_date'+x+'" placeholder="Start Date*"></td><td><input type="text" class="form-control" name="completion_date[]" id="completion_date'+x+'" placeholder="Complition Date*"></td><td><input type="text" class="form-control" name="grade[]" id="grade'+x+'" placeholder="Grade*"></td><td><input type="text" class="form-control" name="percentage[]" id="percentage'+x+'" placeholder="Percentage*"></td><td><a data-id="'+x+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	var x1 = 0;
    $("#addpc").click(function(e){ //on add input button click
        e.preventDefault();
        if(x1 < max_fields){ //max input box allowed
            x1++; //text box increment
            $('.pctbl').append('<tr class="'+x1+'"><td><input type="text" class="form-control" name="pattended[]" id="pattended'+x1+'" placeholder="Attended*"></td><td><input type="text" class="form-control" name="pcourse[]" id="pcourse'+x1+'" placeholder="Course*"></td><td><input type="text" class="form-control" name="pstart_date[]" id="pstart_date'+x1+'" placeholder="Start Date*"></td><td><input type="text" class="form-control" name="pcompletion_date[]" id="pcompletion_date'+x1+'" placeholder="Complition Date*"></td><td><input type="text" class="form-control" name="plocation[]" id="plocation'+x1+'" placeholder="Location*"></td><td><a data-id="'+x1+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.pctbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
    
    
	var x11 = 0;
    $("#addpc1").click(function(e){ //on add input button click
        e.preventDefault();
        if(x11 < max_fields){ //max input box allowed
            x11++; //text box increment
            $('.pctbl1').append('<tr class="'+x11+'"><td><input type="text" class="form-control" name="pattended1[]" id="pattended1'+x11+'" placeholder="Conducted*"></td><td><input type="text" class="form-control" name="pcourse1[]" id="pcourse1'+x11+'" placeholder="Course*"></td><td><input type="text" class="form-control" name="pstart_date1[]" id="pstart_date1'+x11+'" placeholder="Start Date*"></td><td><input type="text" class="form-control" name="pcompletion_date1[]" id="pcompletion_date1'+x11+'" placeholder="Complition Date*"></td><td><input type="text" class="form-control" name="plocation1[]" id="plocation1'+x11+'" placeholder="Location*"></td><td><a data-id="'+x11+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
	
    $('.pctbl1').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
    
	
	var x2 = 0;
    $("#addpr").click(function(e){ //on add input button click
        e.preventDefault();
        if(x2 < max_fields){ //max input box allowed
            x2++; //text box increment
            $('.prtbl').append('<tr class="'+x2+'"><td><input type="text" class="form-control" name="prtitle[]" id="prtitle'+x2+'" placeholder="Title*"></td><td><input type="text" class="form-control" name="publish_in[]" id="publish_in'+x2+'" placeholder="Published In*"></td><td><input type="text" class="form-control" name="publish_date[]" id="publish_date'+x2+'" placeholder="Publish Date*"></td><td><input type="text" class="form-control" name="presented_at[]" id="presented_at'+x2+'" placeholder="Presented At*"></td><td><input type="text" class="form-control" name="presentation_date[]" id="presentation_date'+x2+'" placeholder="Presentation Date*"></td><td><a data-id="'+x2+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.prtbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	
	var x3 = 0;
    $("#addeh").click(function(e){ //on add input button click
        e.preventDefault();
        if(x3 < max_fields){ //max input box allowed
            x3++; //text box increment
            $('.ehtbl').append('<tr class="'+x3+'"><td><input type="text" class="form-control" name="eh_name[]" id="eh_name'+x3+'" placeholder="Employer Name*"></td><td><input type="text" class="form-control" name="eh_designation[]" id="eh_designation'+x3+'" placeholder="Designation*"></td><td><input type="text" class="form-control" name="period[]" id="period'+x3+'" placeholder="Period*"></td><td><input type="text" class="form-control" name="subjects[]" id="subjects'+x3+'" placeholder="Subjects*"></td><td><input type="text" class="form-control" name="curriculum[]" id="curriculum'+x3+'" placeholder="Curriculum*"></td><td><input type="text" class="form-control" name="responsibility[]" id="responsibility'+x3+'" placeholder="Responsibility*"></td><td><input type="text" class="form-control" name="leaving_reason[]" id="leaving_reason'+x3+'" placeholder="Leaving Reason*"></td><td><a data-id="'+x3+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.ehtbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	
	var x4 = 0;
    $("#adder").click(function(e){ //on add input button click
        e.preventDefault();
        if(x4 < max_fields){ //max input box allowed
            x4++; //text box increment
            $('.ertbl').append('<tr class="'+x4+'"><td><input type="text" class="form-control" name="ername[]" id="ername'+x4+'" placeholder="Reference Name*"></td><td><input type="text" class="form-control" name="erdesignation[]" id="erdesignation'+x4+'" placeholder="Designation*"></td><td><input type="text" class="form-control" name="erphone[]" id="erphone'+x4+'" placeholder="Phone No*"></td><td><input type="text" class="form-control" name="eremail[]" id="eremail'+x4+'" placeholder="Email*"></td><td><input type="text" class="form-control" name="eraddress[]" id="eraddress'+x4+'" placeholder="Address*"></td><td><a data-id="'+x4+'" href="javascript:void(0)" type="close" class="remove_field btn btn-danger btn-sm"><i class="fa fa-close"></i></a></td></tr>'); //add input box
		}
    });
   
    $('.ertbl').on("click",".remove_field", function(e){ //user click on remove text
	var ClassNAme= $(this).attr('data-id');
         $(this).closest('tr').remove();
    });
	
	
//});
</script>	
	
<script type="text/javascript" src="js/custom.js"></script>	
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')