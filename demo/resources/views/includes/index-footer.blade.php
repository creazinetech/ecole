	<?php 
	$ContactData = DB::table('contactus')->get();
	foreach($ContactData as $Contactus){ }?>	
		<footer id="tg-footer" class="tg-footer tg-haslayout">	 
			<div class="tg-footermiddlebar">
				<div class="">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
							<div class="tg-widget tg-widgetcompanyinfo">
								<div class="tg-widgetcontent">
								<div class="tg-widgettitle">
									<h3 class="center-block subheading2">Contact Us</h3>
								</div>
									<ul class="tg-infolist">
										<?php if(isset($Contactus->c_address1) && $Contactus->c_address1!=""){?>
										<li  class="tg-content">
											<i class="icon-location"></i> <address> <?php echo $Contactus->c_address1;?> </address>
										</li>
										<?php } ?>
										<?php if(isset($Contactus->c_address2) && $Contactus->c_address2!=""){?>
										<li>
											<i class=""></i> <address><?php echo $Contactus->c_address2;?></address>
										</li>
										<?php } ?>
										<?php if(isset($Contactus->c_contact1) && $Contactus->c_contact1!=""){?>
										<li>
											<i class="icon-phone-handset"></i>
											<span><?php echo $Contactus->c_contact1;?></span>
										</li>
										<?php } ?>
										<?php if(isset($Contactus->c_contact2) && $Contactus->c_contact2!=""){?>
										<li>
											<i class="icon-phone-handset"></i>
											<span><?php echo $Contactus->c_contact2;?></span>
										</li>
										<?php } ?>
										<?php if(isset($Contactus->faxno) && $Contactus->faxno!=""){?>
										<li>
											<i class="icon-printer"></i> <span><?php echo $Contactus->faxno;?></span>
										</li>
										<?php } ?>
										<?php if(isset($Contactus->c_email) && $Contactus->c_email!=""){?>
										<li>
											<a href="mailto:<?php echo $Contactus->c_email;?>">
												<i class="icon-mail"></i>
												<span><?php echo $Contactus->c_email;?></span>
											</a>
										</li>
										<?php } ?>
									</ul>
									
								<div class="tg-infolist">
                                    <a href="https://www.facebook.com/EcoleMondialeOfficial" title="Facebook" target="blank"><i class="fa fa-facebook fa-2x"></i></a> &nbsp;
                                    <a href="https://twitter.com/EcoleMondiale" target="_blank" title="Youtube" rel="nofollow"><i class="fa fa-twitter fa-2x"></i></a> &nbsp;             
                                    <a href="https://plus.google.com/117037178384570861333/about" title="Google+" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a>&nbsp;
                                    <a href="http://www.pinterest.com/ecolemondiale/" title="Pinterest" target="_new"><i class="fa fa-pinterest fa-2x"></i></a>&nbsp;
                                    <a href="https://www.youtube.com/user/EcoleMondialeschool" title="Youtube" target="_blank"><i class="fa fa-youtube fa-2x"></i></a>
                                    </div>
									
								</div>
							</div>
						</div>
						
						<!--<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
							<div class="tg-widget">
								<div class="tg-widgettitle">
									<h3 class="center-block subheading2">Downloads</h3>
								</div>
								<div class="tg-widgetcontent">
									<ul>
											<li>
												<a href="javascript:void(0);">
													<i class="fa fa-paperclip"></i>
													<span>Prospectus</span>
												</a>
											</li>
											<li>
												<a href="javascript:void(0);">
													<i class="fa fa-map-signs"></i>
													<span>Admission Guide</span>
												</a>
											</li>
											<li>
												<a href="javascript:void(0);">
													<i class="fa fa-list-ul"></i>
													<span>Merit List 2017</span>
												</a>
											</li>
											<li>
												<a href="javascript:void(0);">
													<i class="fa fa-calendar-check-o "></i>
													<span>Examination Date Sheet</span>
												</a>
											</li>
											<li>
												<a href="javascript:void(0);">
													<i class="fa fa-bookmark-o"></i>
													<span>Examination Guide 2017</span>
												</a>
											</li>
											<li>
												<a href="{!! \Config::get('app.url_base') !!}/testimonials">
													<i class="fa fa-pencil"></i>
													<span>Testimonials</span>
												</a>
											</li>
										</ul>
								</div>
							</div>
						</div>-->
						
						
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
							<div class="tg-widget tg-widgetflickrgallery">
								<div class="tg-widgettitle">
									<h3 class="center-block subheading2">Gallery</h3>
								</div>
								<div class="tg-widgetcontent">
									<ul>
									<?php 
									$GalleryImgs = DB::table('gallery')->orderBy(DB::raw('RAND()'))->take(12)->get();
									$i=1;
									foreach($GalleryImgs as $GalleryImg){
									?>	
										<li>
											<figure>
												<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalleryImg->img; ?>" data-darkbox="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalleryImg->img; ?>" data-darkbox-group="footer" data-darkbox-description="<?php echo $GalleryImg->img_title; ?>" style="height:70px;width:100%;">
											</figure>
										</li>
									<?php $i++; } ?>	
									</ul>
								</div>
							</div>
						</div>
						
					
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
							<div class="tg-widget tg-widgetcoursecategories">
								<div class="tg-widgettitle">
									<h3 class="center-block subheading2">Other Links</h3>
								</div>
								<div class="tg-widgetcontent">
									<ul>
										<li><a class="" href="https://ecole.managebac.com/login" target="_blank">ManageBac Login</a></li>
										<li><a class="" href="https://ecole-rsic.follettdestiny.com/" target="_blank">Destiny Library Login</a></li>
										<li><a class="" href="https://sites.google.com/a/ecolemondiale.org/primary-library/" target="_blank">Primary Library</a></li>
										
									</ul>
								</div>
							</div>
						</div>
						
						
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						    <div class="tg-widget tg-widgetcompanyinfo">
								<div class="tg-widgetcontent">
								<div class="tg-widgettitle">
									<h3 class="center-block subheading2">Quick Inquiry</h3>
								</div>
								    <div class="form-group col-sm-12">
								    <div class="status_msg"></div>
								    </div> 
        							<div class="form-group col-sm-12">
        								<input class="form-control" name="qname" id="qname" placeholder="Enter Name" type="text">
        								<span id="qnameerr" style="color:red;display:none;"> Please enter name.</span>
        							</div>
        							<div class="form-group col-sm-12">
        								<input class="form-control" name="qemail" id="qemail" placeholder="Enter Email" type="email">
        								<span id="qemailerr" style="color:red;display:none;"> Please enter valid email id.</span>
        							</div>
        							
        							<div class="form-group col-sm-12">
        								<input class="form-control" name="qphone" id="qphone" placeholder="Enter Phone" maxlength="12" onkeypress="return isNumberKey(event)" type="text">
        								<span id="qphoneerr" style="color:red;display:none;"> Please enter 10 digit phone number.</span>
        							</div>
        							
        							<div class="form-group col-sm-12" style="display:none;">
        								<textarea class="form-control" id="qmsg" name="qmsg" rows="4" placeholder="Enter Message"> </textarea>
        								<span id="qmsgerr" style="color:red;display:none;"> Please enter message.</span>
        							</div>
        							
        							<div class="form-group  col-sm-12">
        								<input id="rannumber" name="rannumber" class="captcha" value="6TtrgR" type="hidden">
        								<div class="captcha col-md-4" style="border: 2px solid green; width: 150px; line-height:1.4; height: 45px; text-align: center; vertical-align: middle;font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic; background-image: url('images/captchaimg1.png');">6TtrgR</div>
        								<div class="col-md-2" style="height:40px;margin-bottom:5px;">
        								  <a class="pull-left refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:16px;">
        								  <i class="fa fa-refresh"></i>
        								  </a>
        								  <br><br>
        								</div>
        							  <input class="form-control" id="captcha_code2" name="captcha_code2" placeholder="Enter Captcha" style="text-transform:none;margin-top:10px;" type="text">
        								<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
        							</div>
        							
        							<div class="col-10 fix ">
        								<div class="success1"></div>
        								<div class="error1"></div>
        								 <input name="_token" value="rb7p3dAhL7qGI3c0vreNriprojbr84lzRXRqpBct" type="hidden">
        								 
        								<div class="form-group col-sm-12">
        									<button type="submit" class="pull-left button" name="job-submit" id="quickenquiry">Send</button>
        			
        								</div>
        							</div>
							    </div>
							</div>
						</div>
						
						
						<div class="clearfix"></div>
				        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						    <hr>
                            <div class="footer-logo">
			                    <div class="mobile-layout"> <!-- .mobile-layout start -->
			                    <?php 
							        $Fsliders = DB::table('footer_sliders')->where('fs_status','=','Active')->orderBy('fs_order','asc')->get();
							        if(count($Fsliders)>0){
							        	foreach($Fsliders as $Fslider){
							        ?>
									<a target="_blank" href="<?php echo strtolower($Fslider->fs_title1); ?>">
									    <img src="{!! \Config::get('app.admin') !!}/images/footerslider-images/<?php echo $Fslider->fs_img; ?>" style="margin-right:2px;"></a>
									<?php }} ?>
									</div> <!-- .mobile-layout end -->
		                        </div>							
							
						</div>
						
						
						</div>
						</div>
						
			</div>
			<div class="tg-footerbar" style="background:#11317b;">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ecole-text">
							<span class="tg-copyright">Copyright © 1995-<?php echo date('Y');?>, Ecole Mondiale World School , All rights reserved. | Designed & Developed by <a href="http://www.matrixbricks.com/" target="new" style="color:#ffffff;">Matrix Bricks Infotech</a></span>
							<nav class="tg-addnav">
								<ul>
									<li><a href="{!! \Config::get('app.url_base') !!}/privacy-policy">Privacy Policy</a></li>
									<li><a href="{!! \Config::get('app.url_base') !!}/sitemap">Sitemap</a></li>
									<li><a href="{!! \Config::get('app.url_base') !!}/contact-us">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
	
	
<a onclick="topFunction()" id="myBtn" title="Go to top" style="background-color:#ffffff; border:4px solid #2DB0F3;font-weight:bold;">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</a>


<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    $('html,body').animate({ scrollTop: 0 }, 'slow');
   // document.documentElement.scrollTop = 0;
}
</script>		
		
<script>
$(document).ready(function(){
	$('#quickenquiry').click( function(){
		var qname = $('#qname').val();
		var qemail = $('#qemail').val();
		var qphone = $('#qphone').val();
		var qmsg = $('#qmsgX').val();
		var captcha_code2 = $('#captcha_code2').val();
		var rannumber = $('#rannumber').val();
		var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		
		if(qname==""){$('#qnameerr').show(); return false;}else{$('#qnameerr').hide();}
		if(qemail=="" || !email_regex.test(qemail)){$('#qemailerr').show(); return false;}else{$('#qemailerr').hide();}
		if(qphone==""  || qphone.length<10 || qphone.length>15){$('#qphoneerr').show(); return false;}else{$('#qphoneerr').hide();}
		if(qmsg==""){$('#qmsgerr').show(); return false;}else{$('#qmsgerr').hide();}
		if(captcha_code2=="" || captcha_code2 != rannumber){$('#captcha_code2err').show(); return false;}else{$('#captcha_code2err').hide();}
		$.ajax({
			type: 'get',
			headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
			url: 'index/send_quick_enquiry',
			data: 'qname='+qname+'&qemail='+qemail+'&qphone='+qphone+'&qmsg='+qmsg+'&captcha_code2='+captcha_code2,
			success: function (data) {
			    $('.status_msg').removeClass('alert alert-success');
			    $('.status_msg').removeClass('alert alert-danger');
				if(data == 'success'){
				    $('.status_msg').addClass('alert alert-success');
					$('.status_msg').html('<i class="fa fa-check-square-o"></i> Thank you, We will contact you soon.');
				}else{
				    $('.status_msg').addClass('alert alert-danger');
					$('.status_msg').html('<i class="fa fa-ban"></i> Something went wrong, Please try again.');
				}
			}
		});
	});
});
</script>
	
		
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<script src="{!! \Config::get('app.url') !!}/plugins/js/vendor/jquery-library.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/vendor/bootstrap.min.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/mapclustering/data.json"></script>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&amp;language=en"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/mapclustering/markerclusterer.min.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/mapclustering/infobox.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/mapclustering/map.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/owl.carousel.min.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/isotope.pkgd.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/prettyPhoto.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/countdown.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/collapse.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/moment.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/gmap3.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/js/main.js"></script>
	<!--Datepicker JS-->
	<script src="{!! \Config::get('app.url') !!}/plugins/datepicker/bootstrap-datepicker.js"></script> 
</body>
<div id="fb-root"></div>

<script type="text/javascript">
$(document).ready(function(){	
/// ONLY YEAR DATEPICKER
	$('.datepicker').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy"
	});
});
</script> 

   <script language="Javascript">
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;
 
          return true;
       }
       //-->
    </script>

	
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Mirrored from mydesignhoard.com/HTML/html/education/index3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Jun 2017 10:08:46 GMT -->
</html>