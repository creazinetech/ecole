							
							<style>
							    .sidebar-btn{
							        height:45px;
							        padding:7px;
							        vertical-align: middle;
							        font-family:'Rokkitt',serif;
							        font-size: 22px;
							        text-transform: uppercase;
							        margin-bottom:4px;
							        color:#ffffff;
							    }
							    .orbtn{
                                    display: inline-block;
                                    width:30px;
                                    height:30px;
                                    position:absolute;
                                    left:156px;
                                    top:460px
							    }
							    
							   @media screen and (max-width:1200px) {
                                
                                    .orbtn {
                                        display: none;
                                    }
                                }
                                @media screen and (max-width:760px) {
                                
                                    .tg-dropdowarrow {
                                        display: block;
                                    }
                                }
                                
							</style>
							<aside id="tg-sidebar" class="tg-sidebar">
								<div class="tg-widget">
								  <div class="tg-widgetcontent tg-sidebar">
									<h3 class="blacktext ecole-text">Why Us</h3>
									<center>
									<a href="{!! \Config::get('app.url_base') !!}/about-us">
									<img src="{!! \Config::get('app.url') !!}/images/why-us-sidebar.jpg" alt=""/>
									</a>
									<center>
									</br>
								 <a type="button" class="btn btn-block btn-group-lg btn-primary sidebar-btn" href="{!! \Config::get('app.url_base') !!}/apply-for-admission" >
									Apply Online
								 </a>
								<center><img class="orbtn" src="{!! \Config::get('app.url') !!}/images/orbtn.png" /></center>
								 <a type="button" class="btn btn-block btn-group-lg btn-primary sidebar-btn" href="{!! \Config::get('app.url_base') !!}/contact-us">
									Enquire Now
								 </a>
								 
                                <a href="{!! \Config::get('app.url_base') !!}/newsletter">
								<div class="sidebar-newsletter">
								    <img  id="image" src="{!! \Config::get('app.url') !!}/images/sidebar-newsletter.png"> 
								    <center>
								        <span id="text">
								            <small>Download our</small><br>
								            <span>NEWSLETTER</span>
							            </span>
						            </center>
							    </div>
							    </a>
								
								<br>
							
							    	<?php 
        							$Testimonials = DB::table('testimonials')->where('t_status','=','Active')->where('onsidebar','=','1')->get();
        							if(count($Testimonials) !=0){
        							foreach($Testimonials as $Testimonial){ }
        							?>
        							<div class="col-md-12">
        							<center>
                                    <div class="well col-md-12" style="padding:0px;font-family:'Adamina', serif;font-size: 13px !important;text-align:left;"> 
                                    
                                    <div class="col-md-12 " style="line-height: 25px !important;color:black;margin:1px;padding:2px;">
                                        <i class="fa fa-quote-left fa-3x" style="color:#8D8D8D;margin-top:-15px;" aria-hidden="true"></i>
									<?php echo $Testimonial->t_content; ?>
									<br>
									<span style="font-size:16px;"><?php echo $Testimonial->tby; ?></span>
									<?php echo '&nbsp;'.$Testimonial->tdetails; ?>
									</div> 
								  </div>
								  </center>
								  </div>
								  <?php } ?>
								 
							  </div>	
								</div>
					  </aside>