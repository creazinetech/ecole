<?php
	$Pages = DB::table('pages')->where('page_id','=','50')->get();
	foreach($Pages as $Page){ }
?>	
@include('includes.index-header')


	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	 
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    
    </div>
	  
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
    										<?php 
    										$Testimonials = DB::table('testimonials')->where('t_status','=','Active')->orderBy('t_id','desc')->get();
    										if(count($Testimonials)>0){
    											foreach($Testimonials as $Testimonial){
    										?>
    										<div class="alert alert-info tg-content-text">
    										<i class="fa fa-quote-left fa-1x" aria-hidden="true"></i>
    										<?php echo $Testimonial->t_content ?>
    										<hr>
                                            <p>
                                            <?php echo ucwords($Testimonial->tby); ?>
                                            <small><?php echo ucwords($Testimonial->tdetails); ?></small>
                                            </p>
    										</div>
    										<?php  } }?>
										</div>
										
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')