 @include('included.header')
 @include('included.super-admin-sidebar')

<div class="right_col" role="main">
  <div class="">
	<div class="clearfix"></div>
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_content" style="height:420px;">
			<center>
				<img src="{!! \Config::get('app.url') !!}/images/deny.png" class="img-responsive avatar-view">
				<h3>You do not have permission to access this page!</h3>
				<br>
				<a href="/home" class="btn btn-primary btn-md">Go Back</a>
			</center>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
 @include('included.footer')