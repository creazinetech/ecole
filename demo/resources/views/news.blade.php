<?php
	$Pages = DB::table('pages')->where('page_id','=','41')->get();
	foreach($Pages as $Page){ }
	
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text"><?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?></a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content-text">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<?php if(isset($Page->brief_desc) && $Page->brief_desc !="") { ?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
											<?php echo $Page->brief_desc; ?>
										</div>
										<?php } ?>
										
										
										<?php 
										$NewsList = DB::table('news')
									//	->where('created_at','>=',date('Y-m-01').' 00:00:00')
									//	->where('created_at','<=',date('Y-m-t').' 23:59:59')
										->where('status','=','Active')
										->orderBy('id','desc')
									//	->get();
									->paginate(10);
										if(count($NewsList)>0){
											foreach($NewsList as $News){
										?>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;padding:0px;">
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<?php if(isset($News->thumbnail) && $News->thumbnail !=""){ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/news/<?php echo $News->thumbnail; ?>" class="img-responsive" alt="<?php echo ucwords($News->title); ?>" style="height:auto;width:100%;"> 
										<?php }else{ ?>
										<img src="{!! \Config::get('app.admin') !!}/images/default-landing-thumb.jpg" class="img-responsive" alt="<?php echo ucwords($News->title); ?>" style="height: auto;"> 
										<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 tg-content-text">
										<a href="{{URL::to('view-news',array($News->id))}}"  class="subheading"><?php echo ucwords($News->title); ?> </a>
										<br>
											<?php echo $News->short_desc; ?>
											<br>
											<a href="{{URL::to('view-news',array($News->id))}}" class="button"> Read More</a>
											
										</div>
										</div>
										<?php   
										} 
										echo '<div class="col-md-12 tg-content-text"><center>'.$NewsList->render().'</center></div>';
										} 
										?>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')