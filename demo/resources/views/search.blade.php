@include('includes.index-header')
<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
?>	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<img src="{!! \Config::get('app.url') !!}/images/default-header.jpg" class="img-responsive header-banner" alt=""> 
		    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    Search Result
                </div>
            </h2>
        </div>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2>Search Results</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
										
											<?php 
											
											if(isset($NewsList) && count($NewsList)>0){
												foreach($NewsList as $News){
											?>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;padding:0px;">
											<a href="{{URL::to('view-news',array($News->id))}}"  class="subheading"><?php echo ucwords($News->title); ?> </a>
											<br>
												<?php echo $News->short_desc; ?>
												<br>
												<a href="{{URL::to('view-news',array($News->id))}}" class="button"> Read More</a>
											</div>
											<?php } } ?>
											
										
											<?php 
											if(isset($PagesList) && count($PagesList)>0){
												foreach($PagesList as $Pages){
											?>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;padding:0px;">
												<a href="{{$Pages->pgurl}}"  class="subheading"><?php echo ucwords($Pages->page_heading); ?> </a>
												<?php if($Pages->short_desc !=""){?>
												<br>
												<?php echo $Pages->short_desc; ?>
												<?php } ?>
												<br>
												<a href="{{$Pages->pgurl}}" class="button"> Read More</a>
											</div>
											<?php } } ?>
											
										</div>
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')