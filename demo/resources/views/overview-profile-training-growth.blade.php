<?php
	$Pages = DB::table('pages')->where('page_id','=','37')->get();
	$Pages1 = DB::table('pages')->where('page_id','=','38')->get();
	$Gallery = DB::table('gallery')->where('page_id','=','37')->get();
	$Gallery1 = DB::table('gallery')->where('page_id','=','38')->get();
	
	foreach($Pages as $Page){ }
	foreach($Pages1 as $Page1){ }

?>	
@include('includes.index-header')
	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading).', '.ucwords($Page->page_heading); ?>"> 
	<?php }else if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/<?php echo $Page1->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading).', '.ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading).', '.ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	<style>
	.tab-content > .tab-pane{background-color:#EDF9FF;}
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{background-color:#9FE6DE;}
	</style>	
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/faculty-and-staff">Faculty & Career Opening</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo ucwords($Page->page_heading);} ?>
                    <?php if(isset($Page1) && !empty($Page1->page_heading)){ echo ', '.ucwords($Page1->page_heading);} ?>
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9   tg-content-text">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder  tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?> , <?php echo ucwords($Page1->page_heading); ?></h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										
										<style>
										.datatab{height:auto !important;}
										</style>
										<div role="tabpanel">
											<ul class="nav nav-tabs" role="tablist">
												<li class="active"> <a href="#page1" data-toggle="tab">
												    <strong class="blacktext ecole-text"><?php echo ucwords($Page->page_heading); ?></a></strong></li>
												<li> <a href="#page2" data-toggle="tab">
												    <strong class="blacktext ecole-text"><?php echo ucwords($Page1->page_heading); ?></a></strong></li>
												</li>
											</ul>
											<div class="tab-content">
											    <div role="tabpanel" class="tab-pane datatab active" id="page1" style="background-color:#edf9ff;border:1px solid #dddddd;padding:8px">
												<br>
												   <div class="descdiv"><?php echo $Page->brief_desc; ?></div>								   
												</div>
												 <div role="tabpanel" class="tab-pane datatab" id="page2" style="background-color:#edf9ff;border:1px solid #dddddd;padding:8px">
												<br>
												   <div class="descdiv"><?php echo $Page1->brief_desc; ?></div>								   
												</div>
											</div>
									    </div>
										
										
										
										</div>
										
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    			    <hr>
                    			    <p><strong>Related Links</strong></p>
                    			    <a class="button" href="{!! \Config::get('app.url_base') !!}/recognition-promotion/" style="margin:5px;">Recognition &amp; Promotion</a>
                                    <a class="button" href="{!! \Config::get('app.url_base') !!}/opportunities/" style="margin:5px;">Opportunities</a>
                                    <a class="button" href="{!! \Config::get('app.url_base') !!}/in-service-training/" style="margin:5px;">In Service Training</a>
                                    <a class="button" href="{!! \Config::get('app.url_base') !!}/teacher-profile/" style="margin:5px;">Teacher Profile</a>
                                    <a class="button" href="{!! \Config::get('app.url_base') !!}/training-development-growth/" style="margin:5px;">Career Opening</a>
                                </div>
								
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
	<script>
	var tabsFn = (function() {
  
  function init() {
    setHeight();
  }
  
  function setHeight() {
    var $tabPane = $('.tab-pane'),
        tabsHeight = $('.nav-tabs').height();
    
    $tabPane.css({
      height: tabsHeight
    });
  }
    
  $(init);
})();
	</script>
@include('includes.index-footer')