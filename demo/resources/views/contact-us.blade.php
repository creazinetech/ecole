<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','44')->get();
	foreach($Pages as $Page){ }
	$ContactUs = DB::table('contactus')->get();
	foreach($ContactUs as $Contact){ }
	
	
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	  
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo ucwords($Page->page_heading);} ?>
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content-text">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<div class="row">
										
											<div class="clearfix"></div>
											<div class="flash-message tg-content">
											@foreach (['danger', 'warning', 'success', 'info'] as $msg)
											  @if(Session::has('alert-' . $msg))

											  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
											  @endif
											@endforeach
											</div>
											
											<form name="" method="post"  class="contact-form form2 tg-formtheme tg-formcontactus" id=""   enctype='multipart/form-data'  action="{{ url('addcontactusenquiry') }}" >
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
												<div class="alert alert-info"><strong>Student's Detail</strong></div>
												<fieldset>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" id="name" name="name" placeholder="Student Name*">
															<span id="qnameerr" style="color:red;display:none;"> Please Enter name.</span>
														</div>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="datepicker form-control" id="dob" name="dob" placeholder="Date Of Birth* DD-MM-YYYY">
															<span id="qdateerr" style="color:red;display:none;"> Please Select Date Of Birth.</span>
														</div>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" id="address" name="address" placeholder="Residential Address*">
															<span id="qraddresserr" style="color:red;display:none;"> Please Enter Residential Address.</span>
														</div>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="nopskul" id="nopskul" placeholder="Name of Present School:*">
															<span id="qnskulerr" style="color:red;display:none;"> Please Enter Name of Present School.</span>
														</div>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" id="grade" name="grade" placeholder="Present grade*">
															<span id="qgradeerr" style="color:red;display:none;"> Please Enter present grade.</span>
														</div>
														<div  style="padding:0px;" class="form-group col-md-6 col-sm-6">
															<input type="text" class="form-control" name="gradetjoin" name="gradetjoin" placeholder="Grade to join*">
															<span id="gradetjoinerr" style="color:red;display:none;">Please Enter Grade To Join</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<select class="form-control" id="year" name="year">
															<option value=""> Academic Year for which admission is sought</option>
																<?php 
																$start_year=date('Y');
																$end_year= date('Y')+1;
																$start_year1=$start_year+1;
																$end_year1=$end_year+1;
																for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
																<option value='<?php echo $i.'-'.$j; ?>'><?php echo $i.'-'.$j;?></option>
																<?php } ?>
															</select>
															<span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" class="form-control" id="whattoknow" name="whattoknow" placeholder="What do you wish to know*">
														  <span id="whattoknowerr" style="color:red;display:none;">What do you wish to know.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-12 col-sm-12">
															<textarea class="form-control"  id="aboutshcool" name="aboutshcool" placeholder="Tell us how you heard about our School:*"></textarea>
															<span id="aboutshcoolerr" style="color:red;display:none;">Tell us how you heard about our School?</span>
														</div>
													</div>
													<div  style="padding:0px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content">
														<div class="alert alert-info"><strong>Parent Detail</strong></div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" name="nparents"  name="nparents" placeholder="Parents Name*">
															<span id="nparentserr" style="color:red;display:none;"> Please Enter Parent Name.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="designation" name="designation" placeholder="Designation*">
															<span id="designationerr" style="color:red;display:none;"> Please Enter Parent Name.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="organization" name="organization" placeholder="Name of the Organization*">
															<span id="organizationerr" style="color:red;display:none;"> Please Enter Organisation Name.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="occupation" name="occupation" placeholder="Occupation of the Working Parent*">
															<span id="occupationerr" style="color:red;display:none;"> Please Enter Occupation Name.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="offadd" name="offadd" placeholder="Official Address:*">
															<span id="offadderr" style="color:red;display:none;"> Please Enter Official Address.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" class="form-control" id="city" name="city" placeholder="City:*">
															<span id="cityerr" style="color:red;display:none;"> Please Enter City Name.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<select class="form-control" id="state" name="state">
															  <option value='' >Select State</option>
															  <option value='Andhra Pradesh'>Andhra Pradesh</option>
															  <option value='Arunachal Pradesh'>Arunachal Pradesh</option>
															  <option value='Assam' >Assam </option>
															  <option value='Bihar' >Bihar</option>
															  <option value='Chhattisgarh' >Chhattisgarh    </option>
															  <option value='Goa' >Goa</option>
															  <option value='Gujarat' >Gujarat</option>
															  <option value='Haryana' >Haryana </option>
															  <option value='Himachal Pradesh' >Himachal Pradesh</option>
															  <option value='Jammu & Kashmir' >Jammu & Kashmir</option>
															  <option value='Jharkhand' >Jharkhand </option>
															  <option value='Karnataka' >Karnataka </option>
															  <option value='Kerla' >Kerla </option>
															  <option value='Madhya Pradesh' >Madhya Pradesh</option>
															  <option value='Maharashtra' >Maharashtra  </option>
															  <option value='Manipur' >Manipur </option>
															  <option value='Meghalaya' >Meghalaya</option>
															  <option value=' Mizoram' > Mizoram</option>
															  <option value='Nagaland' >Nagaland</option>
															  <option value='Orissa' >Orissa </option>
															  <option value='Punjab' >Punjab </option>
															  <option value='Sikkim' >Sikkim </option>
															  <option value='Tamilnadu' >Tamilnadu</option>
															  <option value='Tripura' >Tripura </option>
															  <option value='Uttar Pradesh' >Uttar Pradesh </option>
															  <option value=' Uttaranchal' > Uttaranchal </option>
															  <option value='West Bengal' >West Bengal</option>
															  <option value='Outside India' >Outside India</option>
														  </select>
														  <span id="stateerr" style="color:red;display:none;"> Please Select State.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="text" max-length="6" class="form-control" id="pincode" name="pincode" placeholder="Pincode:*" onkeypress="return isNumberKey(event)">
															<span id="pincodeerr" style="color:red;display:none;"> Please Enter 6 Digit Pincode.</span>
														</div>
														
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="restel" name="restel" placeholder="Residence Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="restelerr" style="color:red;display:none;"> Please Enter 10 Digit Residence Telelphone.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="offtel" name="offtel" placeholder="Office Tel:*" onkeypress="return isNumberKey(event)">
														  <span id="offtelerr" style="color:red;display:none;"> Please Enter 10 Digit Office Telelphone.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
														  <input type="text" max-length="10" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number:*" onkeypress="return isNumberKey(event)">
														  <span id="mobileerr" style="color:red;display:none;"> Please Enter 10 Digit Mobile Number.</span>
														</div>
														<div  style="padding:0px;" class="form-group  col-md-6 col-sm-6">
															<input type="email" class="form-control" id="email" name="email" placeholder="Email* (Your email address will not be published.)">
															<span id="emailerr" style="color:red;display:none;"> Please Enter Email Id.</span>
														</div>
														
														<div  style="padding:0px;" class="form-group  col-md-6  col-sm-6">
															<div class="form-group col-md-6 col-sm-6" style="padding:0px;">
															<input type="hidden" id="rannumber" name="rannumber" class="captcha">
															<div class="bg-primary captcha col-md-11 col-sm-6 col-xs-12" style="border: 2px solid green; width:129px; line-height:1.4;text-align: center; vertical-align: middle; font-size: 26px; font-weight: bold;font-family: tahoma;font-style: italic;">
															</div>
															<div class="col-md-1">
															  <a class="refreshbtn" style="margin: 16px 0px 0px 4px; color:#0c4da2;font-weight:bolder;font-size:22px;" >
															  <i class="fa fa-refresh"></i>
															  </a>
															</div>
															</div>
															<div class="form-group  col-md-6  col-sm-6"  style="padding:0px;">
															<input class="form-control" id="captcha_code2" name="captcha_code2"  placeholder="Enter Captcha" type="text" style="text-transform:none;">
															<span id="captcha_code2err" style="color:red;display:none;"> Captcha missmatch.</span>
														</div>
															
														</div>
														
														<div class="form-group col-md-6 col-sm-3">
														{{ csrf_field() }}
															 <button type="submit" class="button pull-left" name="job-submit" id="job-submit" > Submit </button>
														</div>
														
														
													</div>	
													<style>
													iframe{
														height:410px;
													}
													</style>
													<br> <br>
													<div class="col-xs-12 col-sm-12 col-md-12 tg-content" style="height:410px;">
													<hr>
													<!--<div id="tg-officelocation" class="tg-officelocation"></div>-->
													<?php echo $Contact->map; ?>
													</div>
												</fieldset>
											</form>
										</div>

							
									</div>
								</div>
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>

<script type="text/javascript">

$(document).ready(function(){
	$('#job-submit').click(function(){
	var name = $('#name').val();
	var dob = $('#dob').val();
	var address = $('#address').val();
	var nopskul = $('#nopskul').val();
	var grade = $('#grade').val();
	var gradetjoin = $('#gradetjoin').val();
	var year = $('#year').val();
	var whattoknow = $('#whattoknow').val();
	var aboutshcool = $('#aboutshcool').val();
	var nparents = $('#nparents').val();
	var designation = $('#designation').val();
	var organization = $('#organization').val();
	var occupation = $('#occupation').val();
	var offadd = $('#offadd').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	var restel = $('#restel').val();
	var offtel = $('#offtel').val();
	var mobile = $('#mobile').val();
	var email = $('#email').val();
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
	
	    if(name==""){
			$('#qnameerr').css('display','block'); 
			$('#name').focus(); 
			return false;
		}else{
			$('#qnameerr').css('display','none'); 
		}
		if(dob==""){
			$('#qdateerr').css('display','block'); 
			$('#dob').focus(); 
			return false;
		}else{
			$('#qdateerr').css('display','none'); 
		}
		if(address==""){
			$('#qraddresserr').css('display','block'); 
			$('#address').focus(); 
			return false;
		}else{
			$('#qraddresserr').css('display','none'); 
		}
		if(nopskul==""){
			$('#qnskulerr').css('display','block'); 
			$('#nopskul').focus(); 
			return false;
		}else{
			$('#qnskulerr').css('display','none'); 
		}
		if(grade==""){
			$('#qgradeerr').css('display','block'); 
			$('#grade').focus(); 
			return false;
		}else{
			$('#qgradeerr').css('display','none'); 
		}
		if(gradetjoin==""){
			$('#gradetjoinerr').css('display','block'); 
			$('#gradetjoin').focus(); 
			return false;
		}else{
			$('#gradetjoinerr').css('display','none'); 
		}
		if(year==""){
			$('#qayearerr').css('display','block'); 
			$('#year').focus(); 
			return false;
		}else{
			$('#qayearerr').css('display','none'); 
		}
		if(whattoknow==""){
			$('#whattoknowerr').css('display','block'); 
			$('#whattoknow').focus(); 
			return false;
		}else{
			$('#whattoknowerr').css('display','none'); 
		}
		if(aboutshcool==""){
			$('#aboutshcoolerr').css('display','block'); 
			$('#aboutshcool').focus(); 
			return false;
		}else{
			$('#aboutshcoolerr').css('display','none'); 
		}
		if(nparents==""){
			$('#nparentserr').css('display','block'); 
			$('#nparents').focus(); 
			return false;
		}else{
			$('#nparentserr').css('display','none'); 
		}
		if(designation==""){
			$('#designationerr').css('display','block'); 
			$('#designation').focus(); 
			return false;
		}else{
			$('#designationerr').css('display','none'); 
		}
		if(organization==""){
			$('#organizationerr').css('display','block'); 
			$('#organization').focus(); 
			return false;
		}else{
			$('#organizationerr').css('display','none'); 
		}
		if(occupation==""){
			$('#occupationerr').css('display','block'); 
			$('#occupation').focus(); 
			return false;
		}else{
			$('#occupationerr').css('display','none'); 
		}
		if(offadd==""){
			$('#offadderr').css('display','block'); 
			$('#offadd').focus(); 
			return false;
		}else{
			$('#offadderr').css('display','none'); 
		}
		if(city==""){
			$('#cityerr').css('display','block'); 
			$('#city').focus(); 
			return false;
		}else{
			$('#cityerr').css('display','none'); 
		}
		if(state==""){
			$('#stateerr').css('display','block'); 
			$('#state').focus(); 
			return false;
		}else{
			$('#stateerr').css('display','none'); 
		}
		if(pincode=="" || pincode.length<6){
			$('#pincodeerr').css('display','block'); 
			$('#pincode').focus(); 
			return false;
		}else{
			$('#pincodeerr').css('display','none'); 
		}
		if(restel=="" || restel.length<10 || restel.length >15){
			$('#restelerr').css('display','block'); 
			$('#restel').focus(); 
			return false;
		}else{
			$('#restelerr').css('display','none'); 
		}
		if(offtel=="" || offtel.length<10 || offtel.length >15){
			$('#offtelerr').css('display','block'); 
			$('#offtel').focus(); 
			return false;
		}else{
			$('#offtelerr').css('display','none'); 
		}
		if(mobile=="" || mobile.length<10 || mobile.length >15){
			$('#mobileerr').css('display','block'); 
			$('#mobile').focus(); 
			return false;
		}else{
			$('#mobileerr').css('display','none'); 
		}
		if(email=="" || !email_regex.test(email)){
			$('#emailerr').css('display','block'); 
			$('#email').focus(); 
			return false;
		}else{
			$('#emailerr').css('display','none'); 
		}
		if(captcha_code2=="" || captcha_code2 != rannumber){
			$('#captcha_code2err').css('display','block'); 
			$('#captcha_code2').focus(); 
			return false;
		}else{
			$('#captcha_code2err').css('display','none'); 
		}

	});	
});	
</script> 	
	
 <script>
$(document).ready(function(){
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);

 

 $('.refreshbtn').click(function(){ 
 var len='6';
    charSet = 'ABCDEFGHIJKLMNOPQRSTUV0123456789WXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
 $('.captcha').val(randomString);
 $('.captcha').html(randomString);
 
 });
});
</script> 	
@include('includes.index-footer')