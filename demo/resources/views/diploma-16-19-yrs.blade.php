<?php
	use Illuminate\Support\Facades\Route;
	$currentPath= Route::getFacadeRoot()->current()->uri();
	$Pages = DB::table('pages')->where('page_id','=','12')->get();
	$Gallery = DB::table('gallery')->where('page_id','=','12')->where('img_status','=','Active')->paginate(20);
	foreach($Pages as $Page){ }
?>
@include('includes.index-header')
	

	<!--************************************
			Home Slider Start
	*************************************-->
	<div id="tg-content" class="tg-content">
	<?php if(isset($Page->page_banner) && $Page->page_banner !=""){ ?>
	  <img src="{!! \Config::get('app.admin') !!}/images/pages/<?php echo $Page->page_banner; ?>" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php }else{ ?>
	<img src="{!! \Config::get('app.admin') !!}/images/default-header.jpg" class="img-responsive header-banner" alt="<?php echo ucwords($Page->page_heading); ?>"> 
	<?php } ?>
	<?php if(isset($Page->banner_title) && $Page->banner_title !=""){ ?>
	    <div class="header-heading-area">
    	    <h2 id="header-heading1">
        	    <div class="col-md-12">
                    <?php echo ucwords($Page->banner_title); ?>
                </div>
            </h2>
        </div>
    <?php } ?>
    </div>
	<!--************************************
			Home Slider End
	*************************************-->
	
	<main id="tg-main" class="tg-main tg-haslayout">
		<div class="container">
		    
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrum-div">
                <a class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/">Home</a>
	            / <a  class="breadcrum-text" href="{!! \Config::get('app.url_base') !!}/academics">Academics</a>
                / <a  class="breadcrum-text">
                    <?php if(isset($Page) && !empty($Page->page_heading)){ echo $Page->page_heading;} ?>
                    </a>
                <hr>
	        </div>
	        
			<div class="row">
				<div id="tg-twocolumns" class="tg-twocolumns">
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
						<div id="tg-content" class="tg-content">
							<section class="tg-sectionspace tg-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder tg-content">
											<h2><?php echo ucwords($Page->page_heading); ?></h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
											<?php echo $Page->brief_desc; ?>
										</div>
									</div>
								</div>
								<?php if(count($Gallery)>0){?>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
									<div class="tg-contactus tg-contactusvone">
										<div class="tg-titleborder">
											<h2>Life in the Diploma Level</h2>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php foreach($Gallery as $GalImg){?>
											<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3" >
												<img src="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox="{!! \Config::get('app.admin') !!}/images/gallery/<?php echo $GalImg->img; ?>" data-darkbox-group="diploma" data-darkbox-description="<?php echo ucwords($GalImg->img_title); ?>" class="img-responsive thumbnail galleryimg">
											</div>
										<?php } ?>
										</div>
										<div class="ecole-text text-center"><?php echo $Gallery->render(); ?></div>
									</div>
								</div>
								<?php } ?>
								
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-content-text">
                					    <hr>
                					    <p><strong>Related Links</strong></p>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/playschool">Play School</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/early-yrndprimary-yr">Early Years & Primary Years</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/middle-years-11-16-yrs">Middle Years</a>
                				    	<a class="button" href="{!! \Config::get('app.url_base') !!}/college-counseling">College Counselling</a>
            					</div>
                				
								
								
							</section>
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@include('includes.index-sidebar')
					</div>	
	</main>
@include('includes.index-footer')