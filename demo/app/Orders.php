<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['orderid','ordercode','orderstatus','admissionid','amount','billname','billaddress','billcountry','billstate','billcity','billpincode','billtelephone','billemail','shipname','shipaddress','shipcountry','shipstate','shipcity','shippincode','shiptelephone','deliverynotes','admission_date'];

}
