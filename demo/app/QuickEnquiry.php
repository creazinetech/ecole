<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class QuickEnquiry extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quick_enquiry';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['qe_id','qe_name','qe_mobile','qe_email','qe_message'];

}
