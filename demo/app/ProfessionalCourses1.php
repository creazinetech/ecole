<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ProfessionalCourses1 extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'professional_courses1';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pid1','jaid','pattended1','pcourse1','pstart_date1','pcompletion_date1','plocation1'];

}
