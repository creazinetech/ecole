<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ProfessionalCourses extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'professional_courses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pid','jaid','pattended','pcourse','pstart_date','pcompletion_date','plocation'];

}
