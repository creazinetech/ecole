<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

	class MastersProvider extends ServiceProvider
	{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];
	
	public function boot()
    {
        //
    }
	public function register()
    {
        //
    }

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
	}
	
