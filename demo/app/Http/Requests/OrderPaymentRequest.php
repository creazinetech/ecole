<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class OrderPaymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'pay_id' => '',
            'order_code' => 'required',
            'amount' => 'required',
            'paid_amt' => 'required',
            'bal_amt' => 'required',
            'po_status' => 'required',
            'order_dt' => 'required',
            'company_id' => 'required',
            'dist_email' => 'required',
            'cancel_dt' => 'required',
            'rejected_dt' => 'required',
            'pay_status' => 'required',
            'due_date' => '',
            'created_at' => '',
            'updated_at' => '',
        ];
    }
}
