<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class ProductsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'prod_id' => '',
            'prod_name' => 'required',
            'prod_desc' => 'required',
            'prod_features' => 'required',
            'prod_price' => 'required',
            'cat_id' => '',
            'company_id' => 'required',
            'bunch_piece' => '',
            'bunch_price' => '',
            'prod_img' => '',
            'prod_imges' => '',
            'created_at' => '',
            'updated_at' => '',
            'prod_brand' => 'required',
            'prod_specs' => 'required',
            'prod_for' => 'required',
            'prod_code' => 'required',
        ];
    }
}
