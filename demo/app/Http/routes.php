<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('sample', function () {
	return view('sample');
});

Route::get('/', 'IndexController@index');
Route::get('index', 'IndexController@index');
Route::get('index/send_quick_enquiry', 'IndexController@sendquickenquiry');
Route::get('message-from-the-principal', function () {
	return view('message-from-the-principal'); 
});
Route::get('our-mission', function () {
	return view('our-mission'); 
});
Route::get('overview', function () {
	return view('overview'); 
});

Route::get('admission-procedure', function () {
	return view('admission-procedure'); 
});
Route::get('apply-for-admission', function () {
 return view('apply-for-admission'); 
});
Route::get('schedule-a-visit', function () {
	return view('schedule-a-visit'); 
});
Route::get('faq', function () {
	return view('faq'); 
});
Route::get('school-calendar', function () {
	return view('school-calendar'); 
});
Route::get('fee-structure', function () {
	return view('fee-structure'); 
});
Route::get('toddler', function () {
	return view('toddler'); 
});
Route::get('college-counseling', function () {
	return view('college-counseling'); 
});
Route::get('middle-years-11-16-yrs', function () {
	return view('middle-years-11-16-yrs'); 
});
Route::get('library', function () {
	return view('library'); 
});

//Route::post('addadmissions','AdmissionController@addadmissions')->name('addadmissions');
Route::post('proceed-to-payment','AdmissionController@addadmissions')->name('proceed-to-payment');
Route::post('checkout','AdmissionController@checkout')->name('checkout');
Route::post('success','AdmissionController@success')->name('success');
Route::post('failure','AdmissionController@success')->name('failure');

Route::get('overview-profile-training-growth', function () {
	return view('overview-profile-training-growth'); 
});

Route::get('apply-for-job', function () {
	return view('apply-for-job'); 
});

Route::get('training-development-growth', function () {
	return view('apply-for-job'); 
});

Route::get('faculty-and-staff', function () {
	return view('faculty-and-staff'); 
});

Route::get('alumni', function () {
	return view('alumni'); 
});
Route::post('addalumni', 'IndexController@addalumni');

Route::get('news', function () {
	return view('news'); 
});
Route::get('view-news/{id}', 'IndexController@viewnews');

Route::get('newsletter', function () {
	return view('newsletter'); 
});
Route::get('view-newsletter/{id}', 'IndexController@viewnewsletter');
Route::get('index/downloadcount', 'IndexController@downloadcount');

Route::get('events', function () {
	return view('events'); 
});
Route::get('view-event/{id}', 'IndexController@viewevent');

Route::get('youtube', function () {
	return view('youtube'); 
});
Route::get('view-youtube-video/{id}', 'IndexController@viewyoutube');

Route::get('contact-us', function () {
	return view('contact-us'); 
});
Route::post('addcontactusenquiry','IndexController@addcontactusenquiry')->name('addcontactusenquiry');

Route::get('ceic-center', function () {
	return view('ceic-center'); 
});

Route::get('career-opportunities', function () {
	return view('career-opportunities'); 
});

Route::get('recognition-promotion', function () {
	return view('recognition-promotion'); 
});

Route::get('opportunities-2', function () {
	return view('opportunities'); 
});

Route::get('opportunities', function () {
	return view('opportunities'); 
});

Route::get('in-service-training', function () {
	return view('in-service-training'); 
});

Route::get('teacher-profile', function () {
	return view('teacher-profile'); 
});

Route::post('applyjob','IndexController@applyjob')->name('applyjob');

Route::get('privacy-policy', function () {
	return view('privacy-policy'); 
});

Route::get('sitemap', function () {
	return view('sitemap'); 
});
Route::get('thankyou', function () {
	return view('thankyou'); 
});
Route::get('testimonials', function () {
	return view('testimonials'); 
});
Route::get('campus-life', function () {
	return view('campus-life'); 
});
Route::get('about-us', function () {
	return view('about-us'); 
});

Route::get('what-sets-us-apart', function () {
	return view('what-sets-us-apart'); 
});

Route::get('admissions', function () {
	return view('admissions'); 
});
Route::get('academics', function () {
	return view('academics'); 
});

Route::get('search', function () {
	return view('search'); 
});

Route::post('search','IndexController@searchcontent')->name('search');

Route::post('newsletter','IndexController@newsletter')->name('newsletter');
Route::post('events','IndexController@events')->name('events');

////////////////////////////////////Snehal/////////////////////////////////////
Route::get('playschool', function () {
 return view('playschool'); 
});
Route::get('early-yrndprimary-yr', function () {
 return view('early-yrndprimary-yr'); 
});
Route::get('diploma-16-19-yrs', function () {
 return view('diploma-16-19-yrs'); 
});
Route::get('computer-centres', function () {
 return view('computer-centres'); 
});
Route::get('science-technology-labs', function () {
 return view('science-technology-labs'); 
});
Route::get('sports', function () {
 return view('sports'); 
});
Route::get('music-dance-theatre', function () {
 return view('music-dance-theatre'); 
});
Route::get('visual-arts', function () {
 return view('visual-arts'); 
});
Route::get('emun', function () {
 return view('emun'); 
});
Route::get('expeditions-camps', function () {
 return view('expeditions-camps'); 
});
