<?php

namespace App\Http\Controllers;
use Auth;
use App\IndexSliders; //iNCLUDE MODAL
use App\QuickEnquiry; //iNCLUDE MODAL
use App\SchoolCalendar; //iNCLUDE MODAL
use App\Alumni; //iNCLUDE MODAL
use App\News; //iNCLUDE MODAL
use App\Newsletter; //iNCLUDE MODAL
use App\ContactEnquiry; //iNCLUDE MODAL
use App\JobApplications; //iNCLUDE MODAL
use App\EducationDetails; //iNCLUDE MODAL
use App\ProfessionalCourses; //iNCLUDE MODAL
use App\ProfessionalCourses1; //iNCLUDE MODAL
use App\PaperResearch; //iNCLUDE MODAL
use App\EmploymentHistory; //iNCLUDE MODAL
use App\EmploymentReference; //iNCLUDE MODAL
use App\Pages; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class IndexController extends Controller
{
	
    public function index(){
			$Sliders = DB::table('index_sliders')->where('is_status','=','Active')->get();
			return view('index',compact('Sliders'));
    }
	
    public function messagefromprincipal(){
			return view('message-from-the-principal');
    }
	
    public function schoolcalendar(){
			return view('school-calendar');
    }
	
	public function sendquickenquiry(Request $request){ 
		if ($request->ajax()) {
			$QuickEnquiry = new QuickEnquiry;
			$QuickEnquiry->qe_name = $request->input('qname');
			$QuickEnquiry->qe_email = strtolower($request->input('qemail'));
			$QuickEnquiry->qe_mobile = $request->input('qphone');
			//$QuickEnquiry->qe_message = $request->input('qmsg');
			$ID= $QuickEnquiry->save();
			
			if($ID ==1){
				//////////////MAIL START///////////////
    				$to = strtolower($request->input('qemail'));
                    $subject = "Ecole Mondiale Enquiry";
                    $message = "<b>Dear ".$request->input('qname').", </b><br>";
                    $message.= "<p>we have got your enquiry details, we will contact you soon..!</p>";
                    $message.= "<br><br><small>Thank you for ordering, Have a good day.!</small>";
                    $header = "From:noreply@ecolmondiale.org \r\n";
                    $header.= "MIME-Version: 1.0\r\n";
                    $header.= "Content-type: text/html\r\n";
                    mail ($to,$subject,$message,$header);
				//////////////MAIL END////////////////
				//////////////MAIL START///////////////
    				$to1 = "sohail@matrixbricks.com,enquiry@ecolemondiale.org";
                    $subject1 = "Ecole Mondiale Enquiry";
                    $message1 = "Hi,<br>";
                    $message1.= "<p>New enquiry details are as follows:</p>";
                    $message1.= "Name: ".$request->input('qname')."<br>Email: ".strtolower($request->input('qemail'))."<br>";
                    $message1.= "Phone: ".$request->input('qphone')."<br><br>";
                   // $message1.= "Phone: ".$request->input('qphone')."<br> Message: ".$request->input('qmsg')."<br><br>";
                    $header1 = "From:noreply@ecolmondiale.org \r\n";
                    $header1.= "MIME-Version: 1.0\r\n";
                    $header1.= "Content-type: text/html\r\n";
                    mail ($to1,$subject1,$message1,$header1);
					//////////////MAIL END////////////////
			echo 'success'; die;
			}else{
				
				echo 'failed'; die;
			}
		}
		else{
			echo 'failed';
			die;
		}	
	}
	
	public function addalumni(Request $request){
		
				$Alumni = new Alumni;
				$Alumni->firstname = $request->input('firstname');
				$Alumni->lastname = $request->input('lastname');
				$Alumni->graduate_yr = $request->input('graduate_yr');
				$Alumni->city = $request->input('city');
				$Alumni->state = $request->input('state');
				$Alumni->zip = $request->input('zip');
				$Alumni->contact1 = $request->input('contact1');
				$Alumni->contact2 = $request->input('contact2');
				$Alumni->home_address = $request->input('home_address');
				$Alumni->emailid = strtolower($request->input('emailid'));
				$Alumni->website = strtolower($request->input('website'));
				$Alumni->university = $request->input('university');
				$Alumni->programme = $request->input('programme');
				$Alumni->last_university = $request->input('last_university');
				$Alumni->last_programme = $request->input('last_programme');
				$Alumni->last_address = $request->input('last_address');
				$Alumni->occupation = $request->input('occupation');
				$Alumni->organisation = $request->input('organisation');
				$Alumni->designation = $request->input('designation');
				$Alumni->off_address = $request->input('off_address');
				$Alumni->off_contact = $request->input('off_contact');
				$Alumni->off_fax = $request->input('off_fax');
				$Alumni->off_email = strtolower($request->input('off_email'));
				$Alumni->off_website = strtolower($request->input('off_website'));
				$Alumni->work_type = $request->input('work_type');
				$Alumni->gender = $request->input('gender');
				$Alumni->dob = $request->input('dob');
				$Alumni->alumni_ambassador = $request->input('alumni_ambassador');
				$Alumni->personal = $request->input('personal');
				$Alumni->professional = $request->input('professional');
				$Alumni->hobbies = $request->input('hobbies');
				$Alumni->vacation = $request->input('vacation');
				$ID= $Alumni->save();
				
				$request->session()->flash('alert-success', 'Alumni enquiry sent successfully!');
				return redirect('alumni');
	}
	
	
	public function viewnews($id){
		$Data = DB::table('news')->where('id', '=', $id)->get();
		return view('view-news',compact('Data'));
			
	}	
	
	public function viewnewsletter($id){
		$Data = DB::table('newsletter')->where('nlid', '=', $id)->get();
		return view('view-newsletter',compact('Data'));
			
	}

	public function viewevent($id){
		$Data = DB::table('events')->where('id', '=', $id)->get();
		return view('view-event',compact('Data'));
			
	}		

	public function viewyoutube($id){
		$Data = DB::table('youtube_video')->where('ycatid', '=', $id)->where('ystatus', '=', 'Active')->get();
		return view('view-youtube-video',compact('Data'));
			
	}		

	public function downloadcount(Request $request){ 
		if ($request->ajax()) {
			$request->input('Nlid');
			$Data = DB::table('newsletter')->where('nlid', '=',$request->input('Nlid'))->get();
			foreach($Data as $row){}
			$row->downloads;
			
			$Newsletter = new Newsletter;
			$Data = array(
			'downloads' => $row->downloads+1
			);		
			$Newsletter->where('nlid',$request->input('Nlid'))->update($Data);
			
			$Data = DB::table('newsletter')->where('nlid', '=',$request->input('Nlid'))->get();
			foreach($Data as $row){}
			echo $row->downloads; die;
			
		}
	}
	
	public function addcontactusenquiry(Request $request){
	
		$CEObj = new ContactEnquiry;
		$CEObj->name = ucwords($request->input('name'));
		$CEObj->dob = date('Y-m-d',strtotime($request->input('dob')));
		$CEObj->address = $request->input('address');
		$CEObj->nopskul = ucwords($request->input('nopskul'));
		$CEObj->grade = ucwords($request->input('grade'));
		$CEObj->gradetjoin = ucwords($request->input('gradetjoin'));
		$CEObj->year = $request->input('year');
		$CEObj->nparents = ucwords($request->input('nparents'));
		$CEObj->designation = ucwords($request->input('designation'));
		$CEObj->organization = ucwords($request->input('organization'));
		$CEObj->occupation = ucwords($request->input('occupation'));
		$CEObj->offadd = ucwords($request->input('offadd'));
		$CEObj->city = ucwords($request->input('city'));
		$CEObj->state = ucwords($request->input('state'));
		$CEObj->country = ucwords($request->input('country'));
		$CEObj->pincode = $request->input('pincode');
		$CEObj->restel = $request->input('restel');
		$CEObj->offtel = $request->input('offtel');
		$CEObj->mobile = $request->input('mobile');
		$CEObj->email = strtolower($request->input('email'));
		$CEObj->whattoknow = $request->input('whattoknow');
		$CEObj->aboutschool = $request->input('aboutshcool');
		$ID= $CEObj->save();
		
		$request->session()->flash('alert-success', 'Your enquiry sent successfully, We will contact you soon!');
		return redirect('contact-us');
	}	
	
	
	public function applyjob(Request $request){
		$JobObj = new JobApplications;
		$JobObj->name = $request->input('name');
		$JobObj->dob = $request->input('dob');
		$JobObj->position = $request->input('position');
		if($request->hasFile('photo')) {
			$file = Input::file('photo');
			$timestamp = date('Ymdhi');
			$AppPic = $timestamp. '-' .$file->getClientOriginalName();
			$file->move(public_path().'/images/job-applications/',$AppPic);
			$JobObj->photo =$AppPic;
		}
		$JobObj->gender = $request->input('gender');
		$JobObj->nationality = $request->input('nationality');
		$JobObj->marital_status = $request->input('marital_status');
		$JobObj->children = $request->input('children');
		$JobObj->cadd = $request->input('cadd');
		$JobObj->padd = strtolower($request->input('padd'));
		$JobObj->pincode = strtolower($request->input('pincode'));
		$JobObj->mob_no = $request->input('mob_no');
		$JobObj->landline = $request->input('landline');
		$JobObj->email_id = strtolower($request->input('email_id'));
		$JobObj->passport_no = $request->input('passport_no');
		$JobObj->issue_date = $request->input('issue_date');
		$JobObj->issue_place = $request->input('issue_place');
		$JobObj->expiry_date = $request->input('expiry_date');
		$ExperienceIn="";
		if(!empty($request->input('experience_in'))){
    		foreach($request->input('experience_in') as $ExpIn){
    			if(isset($ExpIn) && !empty($ExpIn)){
    				$ExperienceIn.= $ExpIn.',';
    			}
    		}
		}
		$JobObj->experience_in = $ExperienceIn;
		$JobObj->experience_year = $request->input('experience_year');
		$JobObj->current_employer = $request->input('current_employer');
		$JobObj->designation = $request->input('designation');
		$JobObj->internation_school_exp = $request->input('internation_school_exp');
		$JobObj->joindt_curr_emp = $request->input('joindt_curr_emp');
		$JobObj->it_skills = $request->input('it_skills');
		$JobObj->language_speak = $request->input('language_speak');
		$JobObj->hobbies = $request->input('hobbies');
		$JobObj->current_sal = $request->input('current_sal');
		$JobObj->expected_sal = $request->input('expected_sal');
		$JobObj->newjoin_dt = $request->input('newjoin_dt');
		$JobObj->edu_statement = $request->input('edu_statement');
		$JobObj->other_info = $request->input('other_info');
		$JobObj->save();
		$JAID = $JobObj->id;
		
		
		////Educational Details
		$Educount = count($request->input('university'));
		for($i=0;$i<$Educount;$i++){
			if($request->input('university')[$i] !=""){
				$EduObj = new EducationDetails;
				$EduObj->jaid=$JAID;
				$EduObj->university = $request->input('university')[$i];
				$EduObj->board = $request->input('board')[$i];
				$EduObj->start_date = $request->input('start_date')[$i];
				$EduObj->completion_date = $request->input('completion_date')[$i];
				$EduObj->percentage = $request->input('percentage')[$i];
				$EduObj->grade = $request->input('grade')[$i];
				$EduObj->save();
			}
		}
		
		////Paper Reasearch
		$PRcount = count($request->input('prtitle'));
		for($i=0;$i<$PRcount;$i++){
			if($request->input('prtitle')[$i] !=""){
				$PRObj = new PaperResearch;
				$PRObj->jaid=$JAID;
				$PRObj->prtitle = $request->input('prtitle')[$i];
				$PRObj->publish_in = $request->input('publish_in')[$i];
				$PRObj->publish_date = $request->input('publish_date')[$i];
				$PRObj->presented_at = $request->input('presented_at')[$i];
				$PRObj->presentation_date = $request->input('presentation_date')[$i];
				$PRObj->save();
			}
		}
		
		////Professional Courses
		$PCcount = count($request->input('pattended'));
		for($i=0;$i<$PCcount;$i++){
			if($request->input('pattended')[$i] !=""){
				$PCObj = new ProfessionalCourses;
				$PCObj->jaid=$JAID;
				$PCObj->pattended = $request->input('pattended')[$i];
				$PCObj->pcourse = $request->input('pcourse')[$i];
				$PCObj->pstart_date = $request->input('pstart_date')[$i];
				$PCObj->pcompletion_date = $request->input('pcompletion_date')[$i];
				$PCObj->plocation = $request->input('plocation')[$i];
				$PCObj->save();
			}
		}
		
		////Professional Courses1
		$PCcount1 = count($request->input('pattended1'));
		for($i=0;$i<$PCcount1;$i++){
			if($request->input('pattended1')[$i] !=""){
				$PCObj1 = new ProfessionalCourses1;
				$PCObj1->jaid=$JAID;
				$PCObj1->pattended1 = $request->input('pattended1')[$i];
				$PCObj1->pcourse1 = $request->input('pcourse1')[$i];
				$PCObj1->pstart_date1 = $request->input('pstart_date1')[$i];
				$PCObj1->pcompletion_date1 = $request->input('pcompletion_date1')[$i];
				$PCObj1->plocation1 = $request->input('plocation1')[$i];
				$PCObj1->save();
			}
		}
		
		////Employement Details
		$EHcount = count($request->input('eh_name'));
		for($i=0;$i<$EHcount;$i++){
			if($request->input('eh_name')[$i] !=""){
				$EHObj = new EmploymentHistory;
				$EHObj->jaid=$JAID;
				$EHObj->eh_name = $request->input('eh_name')[$i];
				$EHObj->eh_designation = $request->input('eh_designation')[$i];
				$EHObj->period = $request->input('period')[$i];
				$EHObj->subjects = $request->input('subjects')[$i];
				$EHObj->curriculum = $request->input('curriculum')[$i];
				$EHObj->responsibility = $request->input('responsibility')[$i];
				$EHObj->leaving_reason = $request->input('leaving_reason')[$i];
				$EHObj->save();
			}
		}
		
		////Supervisor Reference
		$ERcount = count($request->input('ername'));
		for($i=0;$i<$ERcount;$i++){
			if($request->input('ername')[$i] !=""){
				$ERObj = new EmploymentReference;
				$ERObj->jaid=$JAID;
				$ERObj->ername = $request->input('ername')[$i];
				$ERObj->erdesignation = $request->input('erdesignation')[$i];
				$ERObj->erphone = $request->input('erphone')[$i];
				$ERObj->eremail = $request->input('eremail')[$i];
				$ERObj->eraddress = $request->input('eraddress')[$i];
				$ERObj->save();
			}
		}
		
		$request->session()->flash('alert-success', 'Thank You for applying job, we will contact you soon');
		return redirect('thankyou');
		
	}	
	
	public function searchcontent(Request $request){
		$request->input('search');
		$NewsList = DB::table('news')->where('title', 'like','%'.$request->input('search').'%')->get();
		$PagesList = DB::table('pages')->where('page_meta_keywords', 'like','%'.$request->input('search').'%')->get();
		return view('search',compact('NewsList','PagesList'));
	}	
	
	public function newsletter(Request $request){
		if(!empty($request->input('filterby'))){
		    $filterby = $request->input('filterby');
		}else{
		    $filterby = 'nlid';
		}
		if(!empty($request->input('orderby'))){
		    $orderby = $request->input('orderby');
		}else{
		    $orderby = 'desc';
		}
		
		//$FilterNewsletters = DB::table('newsletter')->where('status','=','Active')->orderBy($filterby,$orderby)->paginate(1);
		return view('newsletter',compact('orderby','filterby'));
	}
	
	public function events(Request $request){
		
		$title = $request->input('title');
		$location = $request->input('location');
		if(!empty($request->input('date1'))){
		$Date1 = $request->input('date1');
		$date1 = date('Y-m-d',strtotime($Date1));
		}
		if(!empty($request->input('date2'))){
		$Date2 = $request->input('date2');
		$date2 = date('Y-m-d',strtotime($Date2));
		}
		
		
			if(!empty($title)){
				$EventData = DB::table('events')->where('title','like','%'.$title.'%')->orderBy('id','desc')->get();
			}
			if(!empty($location)){
				$EventData = DB::table('events')->where('location','like','%'.$location.'%')->orderBy('id','desc')->get();
			}
			if(!empty($request->input('date1'))){
				$EventData = DB::table('events')
					->where('start_date','>=',$date1)
					->orderBy('id','desc')->get();
			}
			if(!empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')->get();
			}
			if(!empty($title) && !empty($location)&& !empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && !empty($location)&& empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && !empty($location)&& !empty($request->input('date1'))&& empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->orderBy('id','desc')
					->get();
			}
			if(empty($title) && !empty($location)&& !empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(empty($title) && !empty($location)&& empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('location','like','%'.$location.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(empty($title) && !empty($location)&& !empty($request->input('date1'))&& empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('location','like','%'.$location.'%')
					->where('start_date','>=',$date1)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& !empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('start_date','>=',$date1)
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& empty($request->input('date1'))&& !empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& !empty($request->input('date1'))&& empty($request->input('date2'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('start_date','<=',$date2)
					->orderBy('id','desc')
					->get();
			}
			if(!empty($title) && empty($location)&& empty($request->input('date1'))){
				$EventData = DB::table('events')
					->where('title','like','%'.$title.'%')
					->where('location','like','%'.$location.'%')
					->orderBy('id','desc')
					->get();
			}
			
		return view('events',compact('EventData','title','location','Date1','Date2'));
	}
	
}
