<?php

namespace App\Http\Controllers;
use Auth;
use App\Admissions; //iNCLUDE MODAL
use App\Orders; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class AdmissionController extends Controller
{
	
    public function index(){
			$Sliders = DB::table('index_sliders')->where('is_status','=','Active')->get();
			return view('index',compact('Sliders'));
    }
	
    public function messagefromprincipal(){
			return view('message-from-the-principal');
    }
	
    public function schoolcalendar(){
			return view('school-calendar');
    }
	

	public function addadmissions(Request $request){
		$Admissions = new Admissions;
		$Admissions->name =stripslashes($request->input('name'));
		$Admissions->dob =stripslashes($request->input('dob'));
		$Admissions->address =stripslashes($request->input('address'));
		$Admissions->nopskul =stripslashes($request->input('nopskul'));
		$Admissions->grade =stripslashes($request->input('grade'));
		$Admissions->gradetjoin =stripslashes($request->input('gradetjoin'));
		$Admissions->year =stripslashes($request->input('year'));
		$Admissions->nparents =stripslashes($request->input('nparents'));
		$Admissions->designation =stripslashes($request->input('designation'));
		$Admissions->organization =stripslashes($request->input('organization'));
		$Admissions->occupation =stripslashes($request->input('occupation'));
		$Admissions->offadd =stripslashes($request->input('offadd'));
		$Admissions->city =stripslashes($request->input('city'));
		$Admissions->state =stripslashes($request->input('state'));
		$Admissions->country =stripslashes($request->input('country'));
		$Admissions->pincode =stripslashes($request->input('pincode'));
		$Admissions->restel =stripslashes($request->input('restel'));
		$Admissions->offtel =stripslashes($request->input('offtel'));
		$Admissions->mnfather =stripslashes($request->input('mnfather'));
		$Admissions->mnmother =stripslashes($request->input('mnmother'));
		$Admissions->emailf =stripslashes($request->input('emailf'));
		$Admissions->emailm =stripslashes($request->input('emailm'));
		$Admissions->passport =stripslashes($request->input('passport'));
		$Admissions->ourskul =stripslashes($request->input('ourskul'));
		// $Admissions->save();
		
		$Orders = DB::table('orders')->where('ordercode','!=','')->orderBy('ordercode','desc')->take(1)->get(); 
		//echo count($Orders);die;
		if(count($Orders) == 0){
			$NewOrderCode =1;
		}else{
		    //$xplode = explode('-',$Orders[0]->ordercode);
			//$OldOrderCode = $xplode[1]+1;
			//$NewOrderCode = "ecolem-".$OldOrderCode;
			$NewOrderCode = $Orders[0]->ordercode;
		}
		if($request->input('passport') == 'Indian Passport'){ $Amount=10000;}
		if($request->input('passport') == 'Non-Indian Passport'){ $Amount=16000;}
		
		$Data = array(
			'ordercode' => $NewOrderCode,
			'amount' => $Amount,
			'name' => $request->input('name'),
			'dob' => $request->input('dob'),
			'address' => $request->input('address'),
			'nopskul' => $request->input('nopskul'),
			'grade' => $request->input('grade'),
			'gradetjoin' => $request->input('gradetjoin'),
			'year' => $request->input('year'),
			'nparents' => $request->input('nparents'),
			'designation' => $request->input('designation'),
			'organization' => $request->input('organization'),
			'occupation' => $request->input('occupation'),
			'offadd' => $request->input('offadd'),
			'city' => $request->input('city'),
			'state' => $request->input('state'),
			'country' => $request->input('country'),
			'pincode' => $request->input('pincode'),
			'restel' => $request->input('restel'),
			'mnfather' => $request->input('mnfather'),
			'mnmother' => $request->input('mnmother'),
			'emailf' => strtolower($request->input('emailf')),
			'emailm' => strtolower($request->input('emailm')),
			'passport' => $request->input('passport'),
			'ourskul' => $request->input('ourskul'),
			'offtel' => $request->input('offtel'),
			);
		return view('proceed-to-payment',compact('Data'));	
	}
	
	
function encrypt($plainText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
	  	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad = $this->pkcs5_pad($plainText, $blockSize);
	  	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
		{
	      $encryptedText = mcrypt_generic($openMode, $plainPad);
  	      mcrypt_generic_deinit($openMode);
		      			
		} 
		return bin2hex($encryptedText);
	}

	function decrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
	 	mcrypt_generic_deinit($openMode);
		return $decryptedText;
		
	}
	//*********** Padding Function *********************

	 function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

	//********** Hexadecimal to Binary function for php 4.0 version ********

	function hextobin($hexString) 
   	{ 
        	$length = strlen($hexString); 
        	$binString="";   
        	$count=0; 
        	while($count<$length) 
        	{       
        	    $subString =substr($hexString,$count,2);           
        	    $packedString = pack("H*",$subString); 
        	    if ($count==0)
		    {
				$binString=$packedString;
		    } 
        	    
		    else 
		    {
				$binString.=$packedString;
		    } 
        	    
		    $count+=2; 
        	} 
  	        return $binString;
    }
    
    
	public function checkout(Request $request){
		
		$Admissions = new Admissions;
		$Admissions->name =stripslashes($request->input('name'));
		$Admissions->dob =stripslashes($request->input('dob'));
		$Admissions->address =stripslashes($request->input('address'));
		$Admissions->nopskul =stripslashes($request->input('nopskul'));
		$Admissions->grade =stripslashes($request->input('grade'));
		$Admissions->gradetjoin =stripslashes($request->input('gradetjoin'));
		$Admissions->year =stripslashes($request->input('year'));
		$Admissions->nparents =stripslashes($request->input('nparents'));
		$Admissions->designation =stripslashes($request->input('designation'));
		$Admissions->organization =stripslashes($request->input('organization'));
		$Admissions->occupation =stripslashes($request->input('occupation'));
		$Admissions->offadd =stripslashes($request->input('offadd'));
		$Admissions->city =stripslashes($request->input('city'));
		$Admissions->state =stripslashes($request->input('state'));
		$Admissions->country =stripslashes($request->input('country'));
		$Admissions->pincode =stripslashes($request->input('pincode'));
		$Admissions->restel =stripslashes($request->input('restel'));
		$Admissions->offtel =stripslashes($request->input('offtel'));
		$Admissions->mnfather =stripslashes($request->input('mnfather'));
		$Admissions->mnmother =stripslashes($request->input('mnmother'));
		$Admissions->emailf =strtolower(stripslashes($request->input('emailf')));
		$Admissions->emailm =strtolower(stripslashes($request->input('emailm')));
		$Admissions->passport =stripslashes($request->input('passport'));
		$Admissions->ourskul =stripslashes($request->input('ourskul'));
		$Admissions->save();
		$Admid = $Admissions->id;
		
		$Orders = new Orders;
		$Orders->admissionid =$Admid;
		$Orders->ordercode =stripslashes($request->input('order_id'));
		$Orders->amount =stripslashes($request->input('amount'));
		$Orders->billname =stripslashes($request->input('billing_name'));
		$Orders->billaddress =stripslashes($request->input('billing_address'));
		$Orders->billcountry =stripslashes($request->input('billing_country'));
		$Orders->billstate =stripslashes($request->input('billing_state'));
		$Orders->billcity =stripslashes($request->input('billing_city'));
		$Orders->billpincode =stripslashes($request->input('billing_zip'));
		$Orders->billtelephone =stripslashes($request->input('billing_tel'));
		$Orders->billemail =strtolower(stripslashes($request->input('billing_email')));
		$Orders->shipname =stripslashes($request->input('delivery_name'));
		$Orders->shipaddress =stripslashes($request->input('delivery_address'));
		$Orders->shipcountry =stripslashes($request->input('delivery_country'));
		$Orders->shipstate =stripslashes($request->input('delivery_state'));
		$Orders->shipcity =stripslashes($request->input('delivery_city'));
		$Orders->shippincode =stripslashes($request->input('delivery_zip'));
		$Orders->shiptelephone =stripslashes($request->input('shiptelephone'));
		$Orders->deliverynotes =stripslashes($request->input('delivery_tel'));
		$Orders->admission_date =date('Y-m-d',strtotime(stripslashes($request->input('admission_date'))));
		
		$Orders->save();
		$Orderid = $Orders->id;
		
		$working_key='392DDCC8A10D81185E3F03EC15ABDF01';//Shared by CCAVENUES
		$access_code='6J2BHM5KCK1HU8JN';//Shared by CCAVENUES
		$merchant_id='11278';//Shared by CCAVENUES
		
		//$checksum=$this->getchecksum($merchant_id,$request->input('amount'),stripslashes($request->input('ordercode')),stripslashes($request->input('Redirect_Url')),$working_key); // Method to generate checksum
	
	//	$merchant_data= 'Merchant_Id='.$merchant_id.'&Amount='.$request->input('amount').'&Order_Id='.stripslashes($request->input('ordercode')).'&Redirect_Url='.stripslashes($request->input('Redirect_Url')).'&billing_cust_name='.stripslashes($request->input('billname')).'&billing_cust_address='.stripslashes($request->input('billaddress')).'&billing_cust_country='.stripslashes($request->input('billcountry')).'&billing_cust_state='.stripslashes($request->input('billstate')).'&billing_cust_city='.stripslashes($request->input('billcity')).'&billing_zip_code='.stripslashes($request->input('billpincode')).'&billing_cust_tel='.stripslashes($request->input('billtelephone')).'&billing_cust_email='.strtolower(stripslashes($request->input('billemail'))).'&delivery_cust_name='.stripslashes($request->input('shipname')).'&delivery_cust_address='.stripslashes($request->input('shipaddress')).'&delivery_cust_country='.stripslashes($request->input('shipcountry')).'&delivery_cust_state='.stripslashes($request->input('shipstate')).'&delivery_cust_city='.stripslashes($request->input('shipcity')).'&delivery_zip_code='.stripslashes($request->input('shippincode')).'&delivery_cust_tel='.stripslashes($request->input('shiptelephone')).'&billing_cust_notes='.stripslashes($request->input('deliverynotes'));
        $merchant_data='';
        foreach ($_POST as $key => $value){
		$merchant_data.=$key.'='.urlencode($value).'&';
    	}
	
	    //echo $merchant_data;die;
		$encrypted_data=$this->encrypt($merchant_data,$working_key); // Method for encrypting the data.
		
		$Token =$request->input('_token');
		return view('checkout',compact('encrypted_data','access_code','Token'));	
	}
	
	public function success(Request $request){
	    //var_dump($_POST);die;
	    $workingKey='392DDCC8A10D81185E3F03EC15ABDF01';		//Working Key should be provided here.
    	$encResponse=$request->input('encResponse');			//This is the response sent by the CCAvenue Server
    	$rcvdString=$this->decrypt($encResponse,$workingKey);		//AES Decryption used as per the specified working key.
    	$AuthDesc="";
    	$MerchantId="";
    	$OrderId="";
    	$Amount=0;
    	$Checksum=0;
    	$veriChecksum=false;
    	$decryptValues=explode('&', $rcvdString);
    	$dataSize=sizeof($decryptValues);
    	
        for($i = 0; $i < $dataSize; $i++) 
	    {
    		$information=explode('=',$decryptValues[$i]);
    		if($i==0)	$MerchantId=$information[1];	
    		if($i==1)	$OrderId=$information[1];
    		if($i==2)	$Amount=$information[1];	
    		if($i==3)	$AuthDesc=$information[1];
    		if($i==4)	$Checksum=$information[1];	
    	}   
    	$rcvdString=$MerchantId.'|'.$OrderId.'|'.$Amount.'|'.$AuthDesc.'|'.$workingKey;
    	$veriChecksum=$this->verifyChecksum($this->genchecksum($rcvdString), $Checksum);
	
	if($veriChecksum==TRUE && $AuthDesc==="Y")
    	{
    		//echo "<br>Thank you. Your credit card has been charged and your transaction is successful.";
    		
        //	mysqli_query($con,"update member_transaction set transaction_status='Paid' where `ordercode`='$OrderId'");
        	
        	$Orders = new Orders;
    			$Data = array(
    			'orderstatus' => 'Paid',
    			);
    		$Orders->where('ordercode',$OrderId)->update($Data);
    		
        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.ordercode', '=',$OrderId)
    	            ->get();
        	foreach($Data as $rowp){}
        	
        	$subject = "Apply For Admission - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billing_email;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            
            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Apply For Admission - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' ><tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr><tr><td>PAYMENT STATUS</td><td>:</td><td>PAID</td></tr><tr><td>TOTAL AMOUNT</td><td>:</td><td>".$Amount."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billing_address."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billcontact."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shipincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shipcontact."</td></tr></table></body>
            </html>";
            
            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
    	
    		//Here you need to put in the routines for a successful 
    		//transaction such as sending an email to customer,
    		//setting database status, informing logistics etc etc
    		$request->session()->flash('alert-success', 'Thank you. Your credit card has been charged and your transaction is successful.');
		    return redirect('thankyou');
    	}	
    	
        else if($veriChecksum==TRUE && $AuthDesc==="B")
    	{
    	//	echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
        		
    		
        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.admissionid', '=',$OrderId)
    	            ->get();
        	foreach($Data as $rowp){}
        	
        	$subject = "Apply For Admission - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billemail;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            
            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Apply For Admission - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' ><tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr><tr><td>PAYMENT STATUS</td><td>:</td><td>PAID</td></tr><tr><td>TOTAL AMOUNT</td><td>:</td><td>".$Amount."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billaddress."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billcontact."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shipincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shipcontact."</td></tr></table></body>
            </html>";
        
            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
        	//mysqli_query($con,"update member_transaction set transaction_status='Paid' where `member_id`='$OrderId'");
    		//Here you need to put in the routines/e-mail for a  "Batch Processing" order
    		//This is only if payment for this transaction has been made by an American Express Card
    		//since American Express authorisation status is available only after 5-6 hours by mail from ccavenue and at the "View Pending Orders"
    		$request->session()->flash('alert-success', 'Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail');
		    return redirect('thankyou');
    	}	
    	else if($veriChecksum==TRUE && $AuthDesc==="N")
    	{
    	//	echo "<br>";
        		
    		
        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.ordercode', '=',$OrderId)
    	            ->get();
        	foreach($Data as $rowp){}
        	
        	$subject = "Apply For Admission - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billemail;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            
            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Apply For Admission - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' ><tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr><tr><td>PAYMENT STATUS</td><td>:</td><td>PAID</td></tr><tr><td>TOTAL AMOUNT</td><td>:</td><td>".$Amount."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billaddress."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billcontact."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shipincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shipcontact."</td></tr></table></body>
            </html>";
        
            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
        	//mysqli_query($con,"update member_transaction set transaction_status='Paid' where `member_id`='$OrderId'");
    		//Here you need to put in the routines/e-mail for a  "Batch Processing" order
    		//This is only if payment for this transaction has been made by an American Express Card
    		//since American Express authorisation status is available only after 5-6 hours by mail from ccavenue and at the "View Pending Orders"
			$request->session()->flash('alert-success', 'Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail');
		    return redirect('thankyou');
    	}
    	else
    	{
    	//	echo "<br>Security Error. Illegal access detected";
        		
    		
        	$Data = DB::table('orders')
        	        ->leftJoin('admissions','admissions.id','=','orders.admissionid')
        	        ->where('orders.ordercode', '=',$OrderId)
    	            ->get();
        	foreach($Data as $rowp){}
        	
        	$subject = "Apply For Admission - Ecole Mondiale";
    		$to =  "sohail@matrixbricks.com";
            $to1 =  "enquiry@ecolemondiale.org";
            $to2 =  $rowp->billemail;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            
            // More headers
            $headers .= 'From: Ecole Mondiale<enquiry@ecolemondiale.org>' . "\r\n";
        	$message = "<html>
            <head>
            <title>Apply For Admission - Ecole Mondiale</title>
            </head>
            <body><p>Details : </p><table width='700px' style='font-family:&#39;latolight&#39;;font-size:16px;line-height:20px;' ><tr><td>ORDER ID</td><td>:</td><td>".$OrderId."</td></tr><tr><td>PAYMENT STATUS</td><td>:</td><td>PAID</td></tr><tr><td>TOTAL AMOUNT</td><td>:</td><td>".$Amount."</td></tr>
            <tr><td>BILLING NAME </td><td>:</td><td>".$rowp->billname."</td></tr>
            <tr><td>BILLING ADDRESS </td><td>:</td><td>".$rowp->billaddress."</td></tr>
            <tr><td>BILLING COUNTRY </td><td>:</td><td>".$rowp->billcountry."</td></tr>
            <tr><td>BILLING STATE </td><td>:</td><td>".$rowp->billstate."</td></tr>
            <tr><td>BILLING CITY </td><td>:</td><td>".$rowp->billcity."</td></tr>
            <tr><td>BILLING PINCODE  </td><td>:</td><td>".$rowp->billpincode."</td></tr>
            <tr><td>BILLING CONTACT</td><td>:</td><td>".$rowp->billcontact."</td></tr>
            <tr><td>BILLING EMAIL </td><td>:</td><td>".$rowp->billemail."</td></tr>
            <tr><td>SHIPPING NAME </td><td>:</td><td>".$rowp->shipname."</td></tr>
            <tr><td>SHIPPING ADDRESS </td><td>:</td><td>".$rowp->shipaddress."</td></tr>
            <tr><td>SHIPPING COUNTRY </td><td>:</td><td>".$rowp->shipcountry."</td></tr>
            <tr><td>SHIPPING STATE </td><td>:</td><td>".$rowp->shipstate."</td></tr>
            <tr><td>SHIPPING CITY </td><td>:</td><td>".$rowp->shipcity."</td></tr>
            <tr><td>SHIPPING PINCODE   </td><td>:</td><td>".$rowp->shipincode."</td></tr>
            <tr><td>SHIPPING CONTACT </td><td>:</td><td>".$rowp->shipcontact."</td></tr></table></body>
            </html>";
        
            mail($to,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to1,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
            mail($to2,$subject,$message,$headers,'-f enquiry@ecolemondiale.org');
        	//mysqli_query($con,"update member_transaction set transaction_status='Paid' where `member_id`='$OrderId'");
    		//Here you need to put in the routines/e-mail for a  "Batch Processing" order
    		//This is only if payment for this transaction has been made by an American Express Card
    		//since American Express authorisation status is available only after 5-6 hours by mail from ccavenue and at the "View Pending Orders"
    		$request->session()->flash('alert-danger', 'Security Error. Illegal access detected');
		    return redirect('thankyou');
    	}
	}
	
	
	
}
