<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\SocialMedia; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class SocialLinkController extends Controller
{
	
    public function socialmedia(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" && Auth::user()->role_id =="2")
		{
			$SocialMedia = DB::table('social_media')->get();
			return view('social-media',compact('SocialMedia'));
		}else{
			return redirect('auth/logout');
		}
    }

	public function updatesocial(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$SocialMedia = new SocialMedia;
				$Data = array(
				'social_link' => $request->input('Value'),
				);
				$SocialMedia->where('social_id',$request->input('Sid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function socialstatus(Request $request){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$SocialMedia = new SocialMedia;
				$Data = array(
				'social_status' => $request->input('status'),
				);
				$SocialMedia->where('social_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
}
