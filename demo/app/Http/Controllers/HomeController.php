<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class HomeController extends Controller
{
	
    public function home(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" && Auth::user()->role_id =="2")
		{
			return view('home');
		}else{
			return redirect('auth/logout');
		}
    }

 
    public function profile(){
		if(isset(Auth::user()->email) && Auth::user()->email !="" && Auth::user()->role_id =="2" && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
			{
				$UserData = DB::table('users')->where('email',Auth::user()->email)->get();
				$Compdata = DB::table('company')->where('company_email',Auth::user()->email)->get();
				return view('profile',compact('Compdata','UserData'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}	
	}
	
	
	public function updateprofile(Request $request){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','7')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Company = new Company;
				$Data = array(
				'company_name' => $request->input('company_name'),
				'company_website' => $request->input('company_website'),
				'company_email' => $request->input('company_email'),
				'company_status' => $request->input('company_status'),
				'company_contact1' => $request->input('company_contact1'),
				'company_contact2' => $request->input('company_contact2'),
				'company_address' => $request->input('company_address'),
				'company_type' => $request->input('company_type'),
				);
				
				if($request->hasFile('company_logo')) {
					$file = Input::file('company_logo');
					$timestamp = date('Ymdhi');
					$logoname = $request->input('company_name').'_'.$timestamp. '-' .$file->getClientOriginalName();
					$Data['company_logo'] = stripslashes($logoname);
					$file->move(public_path().'/images/company_logo/',$logoname);
				}else{
					$Compdata = DB::table('company')->where('company_id',$request->input('company_id'))->get();
					foreach($Compdata as $Cdata){}
					if($Cdata->company_logo !=""){
						$Data['company_logo'] = $Cdata->company_logo;
					}else{
						$Data['company_logo'] ='nologo.png';
					}
				}
				$Company->where('company_id',$request->input('company_id'))->update($Data);
				$request->session()->flash('alert-success', 'Profile updated successfully!');
				return redirect('home');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

	
    public function resetpassword()
    {
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active" ){
			return view('resetpassword');
		}
	}
    public function updatepassword(Request $request)
    {
		if(isset(Auth::user()->email) && Auth::user()->email !="" && Auth::user()->role_id =="2" && Auth::user()->status =="Active" )
		{
			$UserData = DB::table('users')->where('email',Auth::user()->email)->get();
			foreach($UserData as $row){}
			if(count($UserData)>0)
			{
				if($request->input('new_password') != $request->input('password_confirmation'))
				{
				$request->session()->flash('alert-danger', 'Password missmatched, Please try again later!');
				return redirect('resetpassword');
				}
				if(Auth::user()->email== $request->input('email'))
				{
					$User = new User;
					if($request->input('new_password') !=''){
						$Data = array(
						'password' => bcrypt($request->input('new_password')),
						'name' =>$request->input('name'),
						);					
					}else{
					$Data = array(
					'name' => $request->input('name'),
					);
					}
					$User->where('email',Auth::user()->email)->update($Data);
					$request->session()->flash('alert-success', 'Password updated successfully!');
					$updated="Updated";
					return view('resetpassword',compact('updated'));
				}
				else
				{
					$request->session()->flash('alert-danger', 'Old password is wrong, Please try again later!');
					return redirect('resetpassword');
				}
			}
			else
			{
				$request->session()->flash('alert-danger', 'Something went wrong, Please try again later!');
				return redirect('resetpassword');
			}
		}else{
			return redirect('auth/logout');
		}
    }	
	
}
