<?php

namespace App\Http\Controllers;
use Auth;
use App\Alumni; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class AlumniController extends Controller
{
	
	
    public function alumni(){
		if(Auth::user()->role_id =="2" && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','6')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('alumni')->orderBy('aid','asc')->get();
				return view('alumni',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function alumnidelete($id){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','6')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				DB::table('alumni')->where('aid', '=', $id)->delete();
				session()->flash('alert-success', 'Alumni enquiry has been deleted!');
				return redirect('alumni');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function alumniview($id){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','6')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				$Data = DB::table('alumni')->where('alumni.aid', '=', $aid)->get();
				return view('alumni-view',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function alumniedit($id){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','6')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$aid = Crypt::decrypt($id);
				$Data = DB::table('alumni')->where('alumni.aid', '=', $aid)->get();
				return view('alumni-edit',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function alumniupdate(Request $request){
		if(Auth::user()->role_id == '2' && Auth::user()->status =="Active" ){
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','6')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Alumni = new Alumni;
				$Data = array(
				'firstname' => $request->input('firstname'),
				'lastname' => $request->input('lastname'),
				'home_address' => $request->input('home_address'),
				'graduate_yr' => $request->input('graduate_yr'),
				'city' => $request->input('city'),
				'state' => $request->input('state'),
				'zip' => $request->input('zip'),
				'contact1' => $request->input('contact1'),
				'contact2' => $request->input('contact2'),
				'emailid' => $request->input('emailid'),
				'website' => $request->input('website'),
				'university' => $request->input('university'),
				'programme' => $request->input('programme'),
				'last_university' => $request->input('last_university'),
				'last_programme' => $request->input('last_programme'),
				'last_address' => $request->input('last_address'),
				'occupation' => $request->input('occupation'),
				'organisation' => $request->input('organisation'),
				'designation' => $request->input('designation'),
				'off_address' => $request->input('off_address'),
				'off_contact' => $request->input('off_contact'),
				'off_fax' => $request->input('off_fax'),
				'off_email' => $request->input('off_email'),
				'off_website' => $request->input('off_website'),
				'work_type' => $request->input('work_type'),
				'gender' => $request->input('gender'),
				'dob' => $request->input('dob'),
				'alumni_ambassador' => $request->input('alumni_ambassador'),
				'personal' => $request->input('personal'),
				'professional' => $request->input('professional'),
				'hobbies' => $request->input('hobbies'),
				'vacation' => $request->input('vacation'),
				);
				
				$Alumni->where('aid',$request->input('aid'))->update($Data);				
				$request->session()->flash('alert-success', 'Alumni enquiry updated successfully!');
				return redirect('alumni');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
}
