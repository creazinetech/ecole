<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	if(isset(Auth::user()->email) && Auth::user()->email !="" && Auth::user()->role_id =="2"){return view('home');}
	else{return view('auth/login'); }
});

Route::get('/user/{id}', function ($id) {
    $user = App\User::find($id);
    return "The user with ID ".$id." is ".$user->email;
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('logout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//Home...
Route::get('home','HomeController@home');


// Distributors routes...
Route::get('social-media','SocialLinkController@socialmedia')->name('social-media');
Route::get('social-media/updatesocial', 'SocialLinkController@updatesocial')->name('updatesocial');
Route::get('social-media/socialstatus', 'SocialLinkController@socialstatus')->name('socialstatus');

//Index Sliders...
Route::get('index-sliders','SlidersController@indexsliders')->name('index-sliders');
Route::get('index-sliders/isstatus', 'SlidersController@isstatus')->name('isstatus');
Route::get('index-sliders/isorder', 'SlidersController@isorder')->name('isorder');
Route::post('addissliders', 'SlidersController@addissliders')->name('addissliders');
Route::get('index-sliders/isdelete/{id}', 'SlidersController@isdelete');

//Quick Enquiries...
Route::get('quick-enquiry','QuickEnquiryController@quickenquiry')->name('quick-enquiry');
Route::get('quick-enquiry/qedelete/{id}', 'QuickEnquiryController@qedelete');

//Manage Pages...
Route::get('pages','PagesController@pages')->name('pages');
Route::get('editpage/{id}', 'PagesController@editpage');
Route::post('updatepage', 'PagesController@updatepage')->name('updatepage');

//Admissions Routes...
Route::get('admissions', 'AdmissionsController@index')->name('admissions');
Route::get('admissions/delete/{id}', 'AdmissionsController@admissiondelete');
Route::get('admissions/view/{id}', 'AdmissionsController@view');
Route::get('admissions/edit/{id}', 'AdmissionsController@edit');
Route::get('admissions/downloadPDF/{id}/{type}','AdmissionsController@downloadPDF');
Route::get('admissions/viewPDF/{id}/{type}','AdmissionsController@viewPDF');
Route::post('admissions/updateadmissions', 'AdmissionsController@updateadmissions')->name('updateadmissions');

//Alumni Routes...
Route::get('alumni', 'AlumniController@alumni')->name('alumni');
Route::get('alumni/alumni-delete/{id}', 'AlumniController@alumnidelete');
Route::get('alumni/alumni-view/{id}', 'AlumniController@alumniview');
Route::get('alumni/alumni-edit/{id}', 'AlumniController@alumniedit');
Route::post('alumni/alumni-update', 'AlumniController@alumniupdate')->name('alumniupdate');
Route::get('alumni/alumni-pdf-print/{id}', 'AlumniController@downloadPDF');
Route::get('alumni/alumni-pdf-view/{id}', 'AlumniController@viewPDF');

//Enquiries Routes...
Route::get('enquiry', 'EnquiryController@index')->name('enquiry');
Route::get('enquiry/delete/{id}', 'EnquiryController@enquirydelete');
Route::get('enquiry/view/{id}', 'EnquiryController@view');
Route::get('enquiry/enquiry-pdf/{id}','EnquiryController@downloadPDF');
Route::get('enquiry/edit/{id}', 'EnquiryController@edit');
Route::post('enquiry/enquiryupdate', 'EnquiryController@enquiryupdate')->name('enquiryupdate');


//Youtube Category Routes
Route::get('youtube/category', 'YoutubeController@category');
Route::post('youtube/updateycategory', 'YoutubeController@updateycategory')->name('updateycategory');
Route::get('youtube/categorystatus', 'YoutubeController@categorystatus')->name('categorystatus');
Route::post('youtube/addyoutubecat', 'YoutubeController@addyoutubecat')->name('addyoutubecat');
Route::get('youtube/ycategorystatus', 'YoutubeController@ycategorystatus')->name('ycategorystatus');
Route::get('youtube/deleteycategory/{id}', 'YoutubeController@deleteycategory');
Route::get('youtube/editcategory/{id}', 'YoutubeController@editcategory')->name('editcategory');

//Youtube Video Routes...
Route::get('youtube/videos', 'YoutubeController@videos');
Route::post('youtube/addyoutubevideo', 'YoutubeController@addyoutubevideo')->name('addyoutubevideo');
Route::get('youtube/yvideostatus', 'YoutubeController@yvideostatus')->name('yvideostatus');
Route::get('youtube/deletevideos/{id}', 'YoutubeController@deletevideos');


//News Letters routes..
Route::get('newsletter', 'NewslettersController@index')->name('newsletter');
Route::get('newsletter/index', 'NewslettersController@index')->name('newsletter');
Route::post('newsletter/addnewsletter', 'NewslettersController@addnewsletter')->name('addnewsletter');
Route::get('newsletter/deletenewsletter/{id}', 'NewslettersController@deletenewsletter');
Route::get('newsletter/editnewsletter/{id}', 'NewslettersController@editnewsletter')->name('editnewsletter');
Route::post('newsletter/updatenewsletter', 'NewslettersController@updatenewsletter')->name('updatenewsletter');
Route::get('newsletter/newsletterstatus', 'NewslettersController@newsletterstatus')->name('newsletterstatus');

//News Letters routess...
Route::get('school-calendar', 'SchoolCalendarController@index')->name('school-calendar');
Route::get('school-calendar/index', 'SchoolCalendarController@index')->name('school-calendar');
Route::post('school-calendar/addcalendar', 'SchoolCalendarController@addcalendar')->name('addcalendar');
Route::get('school-calendar/deletecalendar/{id}', 'SchoolCalendarController@deletecalendar');
Route::get('school-calendar/editcalendar/{id}', 'SchoolCalendarController@editcalendar')->name('editcalendar');
Route::post('school-calendar/updatecalendar', 'SchoolCalendarController@updatecalendar')->name('updatecalendar');
Route::get('school-calendar/calendarstatus', 'SchoolCalendarController@calendarstatus')->name('calendarstatus');

//News routes...
Route::get('news', 'NewsController@index')->name('news');
Route::get('news/index', 'NewsController@index')->name('news');
Route::post('news/addnews', 'NewsController@addnews')->name('addnews');
Route::get('news/deletenews/{id}', 'NewsController@deletenews');
Route::get('news/editnews/{id}', 'NewsController@editnews')->name('editnews');
Route::post('news/updatenews', 'NewsController@updatenews')->name('updatenews');
Route::get('news/newsstatus', 'NewsController@newsstatus')->name('newsstatus');

//Events routes...
Route::get('event', 'EventsController@index')->name('event');
Route::get('event/index', 'EventsController@index')->name('event');
Route::get('event/addevent', 'EventsController@addevent')->name('addevent');
Route::post('event/saveevent', 'EventsController@saveevent')->name('saveevent');
Route::get('event/deleteevent/{id}', 'EventsController@deleteevent');
Route::get('event/editevent/{id}', 'EventsController@editevent')->name('editevent');
Route::post('event/updateevent', 'EventsController@updateevent')->name('updateevent');
Route::get('event/eventstatus', 'EventsController@eventstatus')->name('eventstatus');

//Events routes...
Route::get('gallery', 'GalleryController@index')->name('gallery');
Route::get('gallery/index', 'GalleryController@index')->name('gallery');
Route::post('gallery/addgallery', 'GalleryController@addgallery')->name('addgallery');
Route::get('gallery/gallerystatus', 'GalleryController@gallerystatus')->name('gallerystatus');
Route::get('gallery/deletegalleryimg', 'GalleryController@deletegalleryimg')->name('deletegalleryimg');
Route::get('gallery/deleteallgalleryimg', 'GalleryController@deleteallgalleryimg')->name('deleteallgalleryimg');

//Jobs routes...
Route::get('jobs', 'JobsController@index')->name('jobs');
Route::get('jobs/index', 'JobsController@index')->name('jobs');
Route::post('jobs/addjob', 'JobsController@addjob')->name('addjob');
Route::get('jobs/deletejob/{id}', 'JobsController@deletejob');
Route::get('jobs/editjob/{id}', 'JobsController@editjob')->name('editjob');
Route::post('jobs/updatejob', 'JobsController@updatejob')->name('updatejob');
Route::get('jobs/jobstatus', 'JobsController@jobstatus')->name('jobstatus');

//Job Applications Routes...
Route::get('job-applications', 'JobApplicationsController@index')->name('job-applications');
Route::get('job-applications/delete/{id}', 'JobApplicationsController@delete');
Route::get('job-applications/view/{id}', 'JobApplicationsController@view');
Route::get('job-applications/edit/{id}', 'JobApplicationsController@edit');
Route::post('job-applications/updatejobapplication', 'JobApplicationsController@updatejobapplication')->name('updatejobapplication');
Route::get('job-applications/pdf-print/{id}', 'JobApplicationsController@downloadPDF');
Route::get('job-applications/pdf-view/{id}', 'JobApplicationsController@viewPDF');


//Profile routes...
Route::get('profile', 'HomeController@profile')->name('profile');
Route::post('updateprofile', 'HomeController@updateprofile')->name('updateprofile');
Route::post('updatepassword', 'HomeController@updatepassword')->name('updatepassword');
Route::get('resetpassword', 'HomeController@resetpassword')->name('resetpassword');

// Users routes...
Route::get('users', 'UsersController@index')->name('users');
Route::get('users', 'UsersController@index');
Route::get('users/add', 'UsersController@add');
Route::post('users/adduser', 'UsersController@adduser')->name('adduser');
Route::get('users/view/{id}', 'UsersController@view')->name('viewuser');
Route::get('users/edit/{id}', 'UsersController@edituser');
Route::get('users/delete/{id}', 'UsersController@deleteuser');
Route::post('users/updateuser', 'UsersController@updateuser')->name('updateuser');
Route::get('users/userstatus', 'UsersController@userstatus')->name('userstatus');

//Roles routes...
Route::get('roles', 'RolesController@index')->name('roles');
Route::get('roles/add', function () { return view('roles/add'); });
Route::post('roles/saverole', 'RolesController@saverole')->name('saverole');
Route::get('roles/rolestatus', 'RolesController@rolestatus')->name('rolestatus');
Route::get('roles/delete/{id}', 'RolesController@deleterole');
Route::get('roles/edit/{id}', 'RolesController@editrole');
Route::post('roles/updaterole', 'RolesController@updaterole')->name('updaterole');

//Permissions routes...
Route::get('permissions', 'PermissionsController@index')->name('roles');
Route::get('permissions/edit-permissions/{id}', 'PermissionsController@editpermissions');
Route::post('permissions/addpermissions', 'PermissionsController@addpermissions')->name('addpermissions');

////////////////////////////////////Snehal/////////////////////////////////////
//Contact Us
Route::get('contact-us','ContactUsController@contactus')->name('contact-us');
Route::post('updatecontactus', 'ContactUsController@updatecontactus')->name('updatecontactus');
Route::get('contact-us/cedit/{id}', 'ContactUsController@editcontactus');
//Footer Sliders
Route::get('footer-sliders','FslidersController@footersliders')->name('footer-sliders');
Route::get('footer-sliders/fsstatus', 'FslidersController@fsstatus')->name('fsstatus');
Route::get('footer-sliders/fsorder', 'FslidersController@fsorder')->name('fsorder');
Route::post('addfssliders', 'FslidersController@addfssliders')->name('addfssliders');
Route::get('footer-sliders/fsdelete/{id}', 'FslidersController@fsdelete');

//Testimonials 
Route::get('testimonials','TestimonialsController@testimonials')->name('testimonials');
Route::get('testimonials/tstatus', 'TestimonialsController@tstatus')->name('tstatus');
Route::post('addtestimonials', 'TestimonialsController@addtestimonials')->name('addtestimonials');
Route::get('testimonials/showindex', 'TestimonialsController@showindex')->name('showindex');
Route::get('testimonials/tdelete/{id}', 'TestimonialsController@tdelete');
Route::get('testimonials/showsidebar', 'TestimonialsController@showsidebar')->name('showsidebar');

//Universities 
Route::get('universities','UniversitiesController@index')->name('universities');
Route::get('universities','UniversitiesController@index');
Route::get('universities/add', 'UniversitiesController@add');
Route::post('adduniversities', 'UniversitiesController@adduniversities')->name('adduniversities');
Route::get('universities/view/{id}', 'UniversitiesController@view')->name('viewuniversities');
Route::get('universities/edit/{id}', 'UniversitiesController@edituniversities');
Route::get('universities/ustatus', 'UniversitiesController@ustatus')->name('ustatus');
Route::get('universities/udelete/{id}', 'UniversitiesController@udelete');
Route::post('universities/updateuniversities', 'UniversitiesController@updateuniversities')->name('updateuniversities');