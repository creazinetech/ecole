<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\Universities; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class UniversitiesController extends Controller
{
	
	
    public function index(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','19')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Universities = DB::table('universities')->orderby('country_name','asc')->get();
				return view('universities/index',compact('Universities'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
   

    public function add(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','19')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				return view('universities.add');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function adduniversities(Request $request){
		
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','19')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
			    var_dump($_POST);die;
				$Universities = new Universities;
				$Universities->country_status ='Active';
				$Universities->country_content =stripslashes($request->input('country_content'));
				$Universities->country_name =stripslashes($request->input('country_name'));
				$Universities->country_status =stripslashes($request->input('country_status'));
				$Universities->save();
				$request->session()->flash('alert-success', 'Universities uploaded successfully!');
				return redirect('universities');
			}else{
				return view('unauthorised');
			}
		}else{
				return redirect('auth/logout');
		}
	}
	
	
	
	public function udelete($id){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','19')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$country_id = Crypt::decrypt($id);
				DB::table('universities')->where('country_id', '=', $country_id)->delete();
				session()->flash('alert-success', 'Universities has been deleted!');
				return redirect('universities');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function edituniversities($id){
		
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','19')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$userid = Crypt::decrypt($id);
				$UserData = DB::table('universities')
				    ->where('country_id', '=', $userid)
					->get();
				return view('universities.edit',compact('UserData'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	

	
	public function updateuniversities(Request $request){
	    if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','19')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{			
        		$Universities = new Universities;
        		$Data = array(
        		'country_name' => $request->input('country_name'),
        		'country_content' => $request->input('country_content'),
        		'country_status' => $request->input('country_status'),
        		);		
        		$Universities->where('country_id',$request->input('country_id'))->update($Data);
        		$request->session()->flash('alert-success', 'Universities details updated successfully!');
        		return redirect('universities');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
		
	}

	public function ustatus(Request $request){
		
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Universities = new Universities;
				$Data = array(
				'country_status' => $request->input('status'),
				);
				$Universities->where('country_id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
}
