<?php

namespace App\Http\Controllers;
use Auth;
use App\SchoolCalendar; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class SchoolCalendarController extends Controller
{
	
	
    public function index(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','12')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('school_calendar')->orderBy('id','desc')->get();
				return view('school-calendar.index',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function addcalendar(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','12')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$SchoolCalendar = new SchoolCalendar;
				if($request->hasFile('pdf')) {
					$file = Input::file('pdf');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('pdf')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					$pdf = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/downloads/',$pdf);
					$SchoolCalendar->pdf =$pdf;
				}
				$SchoolCalendar->status ='Active';
				$SchoolCalendar->pdfname =stripslashes($request->input('pdfname'));
				$SchoolCalendar->save();
				$request->session()->flash('alert-success', 'Calendar added successfully!');
				return redirect('school-calendar');
			}else{
				return view('unauthorised');
			}
		}
	}	
	
	
	public function calendarstatus(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			if ($request->ajax()) {
				$SchoolCalendar = new SchoolCalendar;
				$Data = array(
				'status' => $request->input('status'),
				);
				$SchoolCalendar->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deletecalendar($id){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','12')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				$Data = DB::table('school_calendar')->where('id', '=', $id)->get();
				
				if(!empty($Data[0]->pdf) || $Data[0]->pdf !="" || $Data[0]->pdf !=NULL){
					if (file_exists(public_path().'/downloads/'.$Data[0]->pdf)) {
						unlink(public_path().'/downloads/'.$Data[0]->pdf);
					}
				}
				DB::table('school_calendar')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'Calendar has been deleted!');
				return redirect('school-calendar');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}	
	
	public function editcalendar($id){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','12')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$page_id = Crypt::decrypt($id);
				$Data = DB::table('school_calendar')
					->where('id', '=', $page_id)
					->get();
				return view('school-calendar/editcalendar',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{ 
			return redirect('auth/logout');
		}
	}		
	
	public function updatecalendar(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','12')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$NLData = DB::table('school_calendar')->where('id', '=',$request->input('id'))->get();
				
				$SchoolCalendar = new SchoolCalendar;
				$Data = array(
				'pdfname' => $request->input('pdfname'),
				'status' => $request->input('status'),
				);				
				
				if($request->hasFile('pdf') == 1) {
					
					if(!empty($NLData[0]->pdf) || $NLData[0]->pdf !="" || $NLData[0]->pdf !=NULL){
						if (file_exists(public_path().'/downloads/'.$NLData[0]->pdf)) {
						    unlink(public_path().'/downloads/'.$NLData[0]->pdf);
						}
					}
					$file = Input::file('pdf');
					$timestamp = date('s');
					$rand = rand(4,1000);
					$extension = $request->file('pdf')->getClientOriginalExtension();
					$onlyName = explode('.'.$extension,$file->getClientOriginalName());
					// $pdf = 'Ecole-'.$rand.$timestamp. '-' .$file->getClientOriginalName();
					$pdf = $onlyName[0].'(Ecole-'.$rand.$timestamp.').'.$extension;
					$file->move(public_path().'/downloads/',$pdf);
					$Data['pdf']=$pdf;
				}else{
					$Data['pdf']= $NLData[0]->pdf;
				}
				
				
				$SchoolCalendar->where('id',$request->input('id'))->update($Data);
				
				$request->session()->flash('alert-success', 'Calendar updated successfully!');
				return redirect('school-calendar');
			}else{
				return view('unauthorised');
			}
		}
	}

}
