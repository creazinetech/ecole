<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\Roles; //iNCLUDE MODAL
use App\Rolesctrl; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class UsersController extends Controller
{
	
	
    public function index(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','20')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Users = DB::table('users')
					->leftJoin('roles','roles.role_id','=','users.role_id')
					->where('users.added_by','<>','0')
					->orderBy('users.name', 'asc')
					->get();
					
				return view('users.index',compact('Users'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

    public function add(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','20')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('roles')->where('panel_id','2')->where('role_status','Active')->orderBy('role_name','asc')->get();
				return view('users.add',compact('Data'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }
	
	public function adduser(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','20')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$User = new User;
				$User->name = $request->input('name');
				$User->email = $request->input('email');
				$User->password = bcrypt($request->input('password'));
				$User->added_by = Auth::user()->id;
				$User->status = $request->input('status');
				$User->role_id = $request->input('role_id');
				$User->save();
				$request->session()->flash('alert-success', 'User added successfully!');
				return redirect('users');
			}else{
				return view('unauthorised');
			}
		}else{
				return redirect('auth/logout');
		}
	}
	
	public function deleteuser($id){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','20')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
			{
				$id = Crypt::decrypt($id);
				DB::table('users')->where('id', '=', $id)->delete();
				session()->flash('alert-success', 'User has been deleted!');
				return redirect('users');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function edituser($id){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','20')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$userid = Crypt::decrypt($id);
				$UserData = DB::table('users')
					->where('users.id', '=', $userid)
					->get();
				
				$userid = Crypt::decrypt($id);
				$UserData = DB::table('users')->where('id', '=', $userid)->get();
				$Data = DB::table('roles')->where('panel_id','2')->where('role_status','Active')->orderBy('role_name','asc')->get();	
				return view('users.edit',compact('UserData','Data'));
				
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function updateuser(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','20')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				if($request->input('password') == $request->input('confirm_password'))
				{
					$User = new User;
					$Data = array(
					'name' => $request->input('name'),
					'email' => $request->input('email'),
					'status' => $request->input('status'),
					'role_id' =>$request->input('role_id'),
					'password' => bcrypt($request->input('password')),
					);		
					$User->where('id',$request->input('id'))->update($Data);
					$request->session()->flash('alert-success', 'User details updated successfully!');
					return redirect('users');
				}else{
					$request->session()->flash('alert-danger', 'Password missmatched, Please try again!');
					return redirect('users');
				}
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

	public function userstatus(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Users = new User;
				$Data = array(
				'status' => $request->input('status'),
				);
				$Users->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}
		}
		else{
			echo 'failed';
			die;
		}
		
	}

	
}

?>