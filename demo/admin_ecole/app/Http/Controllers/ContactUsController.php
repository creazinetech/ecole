<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\ContactUs; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class ContactUsController extends Controller
{
	
    public function contactus(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','2')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$ContactUs = DB::table('contactus')->get();
				return view('contactus',compact('ContactUs'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	// public function editcontactus(Request $request){
		// if(isset(Auth::user()->email) &&  Auth::user()->status =="Active")
		// {
			// $RoleArray = [];
			// $roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','2')->get();
			// foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			// if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			// {
				// $ContactUs = new ContactUs;
				// $ContactUs->t_status ='Active';
				// $ContactUs->c_address1 =stripslashes($request->input('c_address1'));
				// $ContactUs->c_address2 =stripslashes($request->input('c_address2'));
				// $ContactUs->c_contact1 =stripslashes($request->input('c_contact1'));
				// $ContactUs->c_contact2 =stripslashes($request->input('c_contact2'));
				// $ContactUs->c_email =stripslashes($request->input('c_email'));
				// $ContactUs->map =stripslashes($request->input('map'));
				// $ContactUs->faxno =stripslashes($request->input('faxno'));
				// $ContactUs->save();
				// $request->session()->flash('alert-success', 'ContactUs uploaded successfully!');
				// return redirect('contactus');
			// }else{
				// return view('unauthorised');
			// }
		// }
	// }
	public function editcontactus($id){
		if(isset(Auth::user()->email) && Auth::user()->status == 'Active'){
			$c_id = Crypt::decrypt($id);
			$Compdata = ContactUs::where('c_id',$c_id)->first();
			return view('contact-us/cedit',compact('Compdata'));
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function updatecontactus(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status == 'Active'){
			
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','3')->get();
			
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){
				
				 
				 ///////////////////
				 $User = new ContactUs;
					$Data = array(
					'c_address1' => $request->input('c_address1'),
					'c_address2' => $request->input('c_address2'),
					'c_contact1' => $request->input('c_contact1'),
					'c_contact2' => $request->input('c_contact2'),
					'c_email' => $request->input('c_email'),
					'map' => $request->input('map'),
					'faxno' => $request->input('faxno')
					);		
					$User->where('c_id',$request->input('c_id'))->update($Data);
					

			$request->session()->flash('alert-success', 'ContactUs updated successfully!');
			return redirect('contact-us');
		}else{
			return redirect('auth/logout');
		}
		}else{
			return redirect('auth/logout');
		}
	}
	
	
	
	
	

	
	
	
}
