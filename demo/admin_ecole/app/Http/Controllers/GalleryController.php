<?php

namespace App\Http\Controllers;
use Auth;
use App\User; //iNCLUDE MODAL
use App\Gallery; //iNCLUDE MODAL
use App\Pages; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Crypt; //For Encrption-Decryption

class GalleryController extends Controller
{
	
    public function index(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','13')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Data = DB::table('gallery')->leftJoin('pages','pages.page_id','=','gallery.page_id')->orderby('pages.page_title')->get();
				$Pages = DB::table('pages')->orderby('page_title')->get();
				return view('gallery.index',compact('Data','Pages'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function addgallery(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','13')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('add',$RoleArray) || Auth::user()->added_by== 0)
			{
				$ID="";
				if($request->hasFile('img')) {
					foreach(Input::file("img") as $file) {
					$timestamp = date('Yhis');
					$ext = $file->guessClientExtension();
					$newname = 'gallery_'.rand(4,'99999').date('is').'.'.$ext;
					//$ProdImages = $timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/gallery/',$newname);
					$TableObj = new Gallery;
					$TableObj->img =$newname;
					$TableObj->img_status ='Active';
					$TableObj->page_id =$request->input('page_id');
					$TableObj->img_title =stripslashes($request->input('img_title'));
					$TableObj->save();
					}
				}
				$request->session()->flash('alert-success', 'Images uploaded successfully!');
				return redirect('gallery');
			}else{
				return view('unauthorised');
			}
		}
	}
	
	public function gallerystatus(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$Gallery = new Gallery;
				$Data = array(
				'img_status' => $request->input('status'),
				);
				$Gallery->where('id',$request->input('cid'))->update($Data);
				echo 'success';
				die;
			}
			else{
				echo 'failed';
				die;
			}	
		}else{
			echo 'failed';
			die;
		}
	}
	
	public function deletegalleryimg(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$id = $request->input('cid');
				$Data = DB::table('gallery')->where('id', '=', $id)->get();
				if(!empty($Data[0]->img) || $Data[0]->img !="" || $Data[0]->img !=NULL){
				    if (file_exists(public_path().'/images/gallery/'.$Data[0]->img)) {
					    unlink(public_path().'/images/gallery/'.$Data[0]->img);
				    }
				}
				DB::table('gallery')->where('id', '=', $id)->delete();
				
				$Data1 = DB::table('gallery')->leftJoin('pages','pages.page_id','=','gallery.page_id')->orderby('pages.page_title')->get();
				$GData='';
				$path= $request->input('path');
				foreach($Data1 as $row){
				$GData.='<div class="col-md-55">
								<div class="thumbnail">
								  <div class="image view view-first">
									<img style="width: 100%; height:100%; display: block;" src="images/gallery/'.$row->img.'" alt="image" />
									<div class="mask">
									  <p>'.$row->img_title.'</p>
									  <div class="tools tools-bottom">
										<select id="img_status" name="img_status" class="input-xs img_status" style="color:black;font-size:12px;" data-id="'.$row->id.'">
										<option value="Active"';
										if($row->img_status=='Active'){ $GData.='selected';}
										$GData.='>Active</option>
										<option value="Inactive"';
										if($row->img_status=='Inactive'){ $GData.='selected';}
										$GData.='>Inactive</option>
										</select>
										<a class="delete_img" data-id="'.$row->id.'" style="color:red;"><i class="fa fa-times"></i></a>
									  </div>
									</div>
								  </div>
								  <div class="caption">
									<p class="text-center">'.$row->page_title.'</p>
								  </div>
								</div>
							</div>';
				}
				
				echo $GData;
				die;
			}
			else
			{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}
	
	
	public function deleteallgalleryimg(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active")
		{
			if ($request->ajax()) {
				$id = $request->input('cid');
				$Data = DB::table('gallery')->where('page_id', '=', $id)->get();
				foreach($Data as $OData){
					if(!empty($OData->img) || $OData->img !="" || $OData->img !=NULL){
				        if (file_exists(public_path().'/images/gallery/'.$OData->img)) {
							unlink(public_path().'/images/gallery/'.$OData->img);
				        }
					}
				}
				DB::table('gallery')->where('page_id', '=', $id)->delete();
				
				$Data1 = DB::table('gallery')->leftJoin('pages','pages.page_id','=','gallery.page_id')->orderby('pages.page_title')->get();
				$GData='';
				$path= $request->input('path');
				foreach($Data1 as $row){
				$GData.='<div class="col-md-55">
								<div class="thumbnail">
								  <div class="image view view-first">
									<img style="width: 100%; height:100%; display: block;" src="images/gallery/'.$row->img.'" alt="image" />
									<div class="mask">
									  <p>'.$row->img_title.'</p>
									  <div class="tools tools-bottom">
										<select id="img_status" name="img_status" class="input-xs img_status" style="color:black;font-size:12px;" data-id="'.$row->id.'">
										<option value="Active"';
										if($row->img_status=='Active'){ $GData.='selected';}
										$GData.='>Active</option>
										<option value="Inactive"';
										if($row->img_status=='Inactive'){ $GData.='selected';}
										$GData.='>Inactive</option>
										</select>
										<a class="delete_img" data-id="'.$row->id.'" style="color:red;"><i class="fa fa-times"></i></a>
									  </div>
									</div>
								  </div>
								  <div class="caption">
									<p class="text-center">'.$row->page_title.'</p>
								  </div>
								</div>
							</div>';
				}
				
				echo $GData;
				die;
			}
			else
			{
				echo 'failed';
				die;
			}
		}else{
			echo 'failed';
			die;
		}
	}
	
	
}
