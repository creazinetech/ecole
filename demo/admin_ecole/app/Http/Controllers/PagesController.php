<?php

namespace App\Http\Controllers;
use Auth;
use App\Pages; //iNCLUDE MODAL
use DB; // USE Database Model
use Validator; //INCLUDE VALIDATOR
use Illuminate\Http\Request; //INCLUDE HTTP REQUESTER
use Illuminate\Http\Response; //INCLUDE HTTP RESPONSE
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Crypt; //For Encrption-Decryption

class PagesController extends Controller
{
	
	
    public function pages(){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','5')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Pages = DB::table('pages')->orderBy('page_title','asc')->get();
				return view('pages',compact('Pages'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
    }

	public function editpage($id){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','5')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$page_id = Crypt::decrypt($id);
				$PageData = DB::table('pages')
					->where('page_id', '=', $page_id)
					->get();
				return view('editpage',compact('PageData'));
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}
	
	public function updatepage(Request $request){
		if(isset(Auth::user()->email) && Auth::user()->status =="Active" )
		{
			$RoleArray = [];
			$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','5')->get();
			foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
			if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
			{
				$Pages = new Pages;
				$Data = array(
				'page_heading' => $request->input('page_heading'),
				'page_meta_title' => $request->input('page_meta_title'),
				'page_meta_description' => $request->input('page_meta_description'),
				'page_meta_keywords' => $request->input('page_meta_keywords'),
				'short_desc' => $request->input('short_desc'),
				'brief_desc' => $request->input('brief_desc'),
				'banner_title' => $request->input('banner_title'),
				);	
				
				if($request->input('page_id')== 25){
				    $Data['brief_desc2']=$request->input('brief_desc2');
				    $Data['brief_desc3']=$request->input('brief_desc3');
				    $Data['brief_desc4']=$request->input('brief_desc4');
				}
				
				$PageData = DB::table('pages')->where('page_id', '=',$request->input('page_id'))->get();
				foreach($PageData as $PageDetails){}
				
				if($request->hasFile('page_img')) {
					if(isset($PageDetails->page_img) && $PageDetails->page_img !=""){
					    if (file_exists(public_path().'/images/pages/'.$PageDetails->page_img)) {
					     unlink(public_path().'/images/pages/'.$PageDetails->page_img);
					    }
					}
					$file = Input::file('page_img');
					$timestamp = date('Ymdhi');
					$page_img = $timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/pages/',$page_img);
					$Data['page_img']=$page_img;
				}
				if($request->hasFile('page_banner')) {
					if(isset($PageDetails->page_banner) && $PageDetails->page_banner !=""){
					    if (file_exists(public_path().'/images/pages/'.$PageDetails->page_banner)) {
					        unlink(public_path().'/images/pages/'.$PageDetails->page_banner);
					    }
					}
					$file = Input::file('page_banner');
					$timestamp = date('Ymdhi');
					$page_banner = $timestamp. '-' .$file->getClientOriginalName();
					$file->move(public_path().'/images/pages/',$page_banner);
					$Data['page_banner']=$page_banner;
				}
				$Pages->where('page_id',$request->input('page_id'))->update($Data);
				$request->session()->flash('alert-success', 'Pages details updated successfully!');
				return redirect('pages');
			}else{
				return view('unauthorised');
			}
		}else{
			return redirect('auth/logout');
		}
	}

}
