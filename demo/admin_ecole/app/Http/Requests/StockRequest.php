<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class StockRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'stock_id' => '',
            'prod_id' => 'required',
            'total_qty' => 'required',
            'bunch_piece' => 'required',
            'sold_qty' => '',
            'min_stock' => '',
            'created_at' => '',
            'updated_at' => '',
        ];
    }
}
