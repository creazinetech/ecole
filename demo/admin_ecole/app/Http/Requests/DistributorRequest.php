<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class DistributorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
    {
        return [
            'dist_id' => '',
            'dist_name' => 'required',
            'dist_email' => 'required',
            'dist_contact1' => 'required',
            'dist_contact2' => '',
            'dist_address' => 'required',
            'dist_country' => 'required',
            'dist_state' => 'required',
            'dist_city' => 'required',
            'dist_pincode' => 'required',
            'dist_type' => 'required',
            'vat_tin' => '',
            'added_by' => '',
            'company_id' => '',
            'gst' => '',
            'cst' => '',
            'trans_name' => '',
            'trans_con' => '',
            'trans_mark' => '',
        ];
    }
}
