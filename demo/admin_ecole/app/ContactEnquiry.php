<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ContactEnquiry extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_enquiry';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name','dob','address','nopskul','grade','gradetjoin','year','nparents','designation','organization','occupation','offadd','city','state','country','pincode','restel','offtel','mobile','email','whattoknow','aboutshcool'];

}
