<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Disttypes extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dist_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['dist_type','dt_status','company_id','discount'];

}
