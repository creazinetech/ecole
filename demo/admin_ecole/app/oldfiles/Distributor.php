<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'distributor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['dist_name','dist_email','dist_contact1','dist_contact2','dist_address','dist_country','dist_state','dist_city','dist_pincode','dist_type','added_by','cat_id','company_id','gst','cst','trans_name','trans_con','trans_mark','vat_tin','sm_id','dist_type2','con_person','designation','bank_name','branch','acc_no'];

}
