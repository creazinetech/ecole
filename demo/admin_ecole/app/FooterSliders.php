<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class FooterSliders extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'footer_sliders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fs_id','fs_img','fs_status','fs_order','fs_title1'];

}
