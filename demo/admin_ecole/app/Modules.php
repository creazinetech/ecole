<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'controller';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['panel_id','ctrl_name'];

}
