<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'social_media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['social_id','social_title','social_status','social_link'];

}
