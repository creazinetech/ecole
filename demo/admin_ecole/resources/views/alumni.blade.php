 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','6')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result">
			  </p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Alumni List</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">				
					<table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header datatable-keytable nowrap" cellspacing="0" width="100%">
                      <thead>
                          <th width="3%">Sr No</th>
                          <th>First Name</th>
                          <th>Middle Name</th>
                          <th>Last Name</th>
                          <th>Graduate Year</th>
                          <th>Address</th>
						  <?php if(in_array('delete',$RoleArray) ||in_array('view',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
                      <tbody>
					    <?php $i = 1; ?>
						<?php foreach ($Data as $row){ ?>
                        <tr>
                          <td> <?php echo $i; ?></td>
                          <td> <?php echo ucwords($row->firstname); ?> </td>
                          <td> <?php echo ucwords($row->middlename); ?> </td>
                          <td> <?php echo ucwords($row->lastname); ?> </td>
                          <td><?php echo $row->graduate_yr; ?></td>
                          <td><?php echo $row->home_address; ?></td>
						  
						  <?php if(in_array('print',$RoleArray) ||in_array('edit',$RoleArray) ||in_array('view',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <td>
						  <?php if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
						  { ?>
						  <a href="{{URL::to('alumni/alumni-pdf-view',array(Crypt::encrypt($row->aid)))}}" class="btn btn-warning btn-xs" title="View PDF"><i class="fa fa-file-pdf-o"></i></a>
						  <?php 
						  }
						  if(in_array('view',$RoleArray) || Auth::user()->added_by== 0)
						  { ?>
						  <a href="{{URL::to('alumni/alumni-view',array(Crypt::encrypt($row->aid)))}}" class="btn btn-success btn-xs" title="View"><i class="fa fa-file"></i></a>
						  <?php 
						  }
						  if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
						  { ?>
						  <a href="{{URL::to('alumni/alumni-edit',array(Crypt::encrypt($row->aid)))}}" class="btn btn-primary btn-xs" title="Edit"><i class="fa fa-edit"></i></a>
						  <?php 
						  }
						  if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0)
						  { ?>
						  <a href="{{URL::to('alumni/alumni-delete',array(Crypt::encrypt($row->aid)))}}" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash"></i></a>
						  <?php } ?>
						  </td>
						  <?php } ?>
                        </tr>
						<?php  $i++; } ?>
                      </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

<script>
// $('.table').dataTable( {
  // "pageLength":50
// });
</script>		
@include('included.footer')