@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','13')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
					<div class="x_title">
						<h2>Gallery <small> gallery images </small></h2>
						<?php if(Auth::user()->added_by== 0 || in_array('add',$ctrl_action)){?>
						<a style="margin-right:5px;" class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#Add_PA_Modal">
						Add 
						</a>
						<?php } ?>
						<?php if(Auth::user()->added_by== 0 || in_array('delete',$ctrl_action) ){?>
						<a style="margin-right:5px;" class="btn btn-sm btn-danger pull-right deleteall">Delete</a>
						<select name="page_id1" id="page_id1" class="form-control input-sm pull-right"  required="required" style="width:120px;">
							<option value="">Select Page</option>
							<?php
							if(count($Pages) >0){
							foreach($Pages as $Page){
							?>
							<option value="<?php echo $Page->page_id;?>"><?php echo $Page->page_title;?></option>
							<?php } } ?>
						</select>
						<?php } ?>
						
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					
						<div class="row image_row">
						<?php foreach($Data as $row) {?>
							<div class="col-md-55">
								<div class="thumbnail">
								  <div class="image view view-first">
									<img style="width: 100%; height:100%; display: block;" src="{!! \Config::get('app.url_base') !!}/images/gallery/<?php echo $row->img; ?>" alt="image" />
									<div class="mask">
									  <p><?php echo $row->img_title; ?></p>
									  <div class="tools tools-bottom">
										<select id="img_status" name="img_status" class="input-xs img_status" style="color:black;font-size:12px;" data-id="<?php echo $row->id; ?>">
										<option value="Active" <?php if($row->img_status=='Active'){echo 'selected';} ?>>Active</option>
										<option value="Inactive" <?php if($row->img_status=='Inactive'){echo 'selected';} ?>>Inactive</option>
										</select>
										<a class="delete_img" data-path="{!! \Config::get('app.url_base') !!}/images/gallery/" data-id="<?php echo $row->id; ?>" style="color:red;"><i class="fa fa-times"></i></a>
									  </div>
									</div>
								  </div>
								  <div class="caption">
									<p class="text-center"><?php echo $row->page_title; ?></p>
								  </div>
								</div>
							</div>
						<?php } ?>
						</div>
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->	
	<!-- Add model start -->
	<div id="Add_PA_Modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Upload Images</h4>
		  </div>
		  <form class="" action="{{URL::route('addgallery')}}"  enctype="multipart/form-data" method="post" >
		  <div class="modal-body">
				<div class="form-group">
				<input type="text" name="img_title" id="img_title" placeholder="Image Title" class="form-control input-sm">
				</div>
				<div class="form-group">
				<select name="page_id" id="page_id" class="form-control input-sm"  required="required">
					<option value="">Select Page</option>
					<?php
					if(count($Pages) >0){
					foreach($Pages as $Page){
					?>
					<option value="<?php echo $Page->page_id;?>"><?php echo $Page->page_title;?></option>
					<?php } } ?>
				</select>
				</div>
				<div class="form-group">
				<input type="file" name="img[]" id="img" multiple class="form-control input-sm"  required="required">
				</div>
				{{ csrf_field() }}
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-success btn-sm">Save</button>
			<button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>			
	<!-- Add model end -->
	

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
	//$(document).ready(function(){
		
		$('.img_status').on('change',function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'gallery/gallerystatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		$('.delete_img').on('click',function(){
			var cid = $(this).attr('data-id');
			var path = $(this).attr('data-path');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'gallery/deletegalleryimg',
				data: 'cid='+ cid+'&path='+ path,
				success: function (data,status) {
					if(status == 'success'){
						//$('.image_row').html(data);
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Image deleted successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						location.reload(1200);
					}
					if(status != 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to delete image! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		$('.deleteall').on('click',function(){
			var cid = $('#page_id1').val();
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'gallery/deleteallgalleryimg',
				data: 'cid='+cid,
				success: function (data,status) {
					if(status == 'success'){
						//$('.image_row').html(data);
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Images deleted successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						location.reload(1200);
					}
					if(status != 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to delete images! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		
	//});
	</script>
  @include('included.footer')