 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','3')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){


foreach($UserData as $user){} 
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Add Company</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit User</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					
                    <div class="col-md-8">
					
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('updateuniversities')}}" enctype="multipart/form-data" method="post">
                        <input id="country_id" name="country_id" type="hidden" value="<?php echo $user->country_id; ?>">

                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input id="country_name" name="country_name" class="form-control col-md-7 col-xs-12"  required="required" type="text" value="<?php echo $user->country_name; ?>" >
						  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Universities <span class="required">*</span>
                        </label>
                       <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
						<textarea name='country_content' id='country_content' class='ckeditor'><?php echo $user->country_content; ?></textarea>
						  <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>				  
                      
					 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_status">Status
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label><input type="radio" name="country_status" required="required" id="country_status" class="flat" value="Active" <?php if($user->country_status =="Active"){echo "checked";} ?> > Active</label>
							<label><input type="radio" name="country_status" required="required" id="country_status" class="flat" value="Inactive" <?php if($user->country_status =="Inactive"){echo "checked";} ?>> Inactive</label>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
                        <div class="col-md-6 col-md-offset-3">
                          <button id="savedata" type="submit" class="btn btn-success">Update</button>
                          <a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/universities">Cancel</a>
                        </div>
                      </div>
                    </form>
					
					</div>
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php }?>
<script>
$(document).ready(function(){
	$('#savedata').click(function(){
		var confirm_password = $('#confirm_password').val();
		var password = $('#password').val();
		if(password !=confirm_password || confirm_password ==""){
				$('.pass_err').show();
				$('.has-confirmpassword').addClass('has-error');
				return false;
		}else{
				$('.pass_err').hide();
				$('.has-confirmpassword').removeClass('has-error');
		}
	});
});
</script>	
		
 @include('included.footer')