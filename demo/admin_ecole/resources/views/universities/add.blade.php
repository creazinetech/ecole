 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','3')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add Unversities</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
                     
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('adduniversities')}}" enctype="multipart/form-data" method="post">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Country Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
						  
                          <input id="country_name" name="country_name" class="form-control col-md-7 col-xs-12" placeholder="Enter user country name" required="required" type="text">
						  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      
					  <div class="item form-group">
					  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Universities <span class="required">*</span>
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                          
						  <textarea name='country_content' id='country_content' class='ckeditor'></textarea>
						 <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
						</div>
					  
					  
					  
					  
					  
					  
					  
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label><input type="radio" name="country_status" required="required" id="country_status" class="flat" value="Active" checked> Active</label>
							<label><input type="radio" name="country_status" required="required" id="country_status" class="flat" value="Inactive"> Inactive</label>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
						<!--<input id="_token" name="_token" type="hidden" value="{{ csrf_field() }}">--->
                        <div class="col-md-6 col-md-offset-3">
						
                          <a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/universities">Cancel</a>
						  <button type="submit" class="btn btn-sm btn-success pull-left">Upload</button>
						
                        </div>
                      </div>
                    </form>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>


  @include('included.footer')