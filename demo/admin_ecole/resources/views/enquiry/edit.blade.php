@include('included.header')
@include('included.super-admin-sidebar')
<?php foreach($Data as $row){} ?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="page-title">
	  <div class="title_left">
		<h2>Edit Enquiry</h2>
	  </div>
	</div>

	<div class="clearfix"></div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel" style="padding:0px;">
		  <div class="x_content">
				<form class="form-horizontal form-label-left" novalidate action="{{URL::route('enquiryupdate')}}" method="post">
					<div class="col-md-2 col-sm-12 col-xs-12" style="overflow-x:auto;"></div>
					<div class="col-md-4 col-sm-6 col-xs-12" style="overflow-x:auto;">
					<input type="hidden" class="form-control input-sm" value="<?php echo ucwords($row->id); ?>" name="id" id="id">
					<br><b>Student's Name:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->name); ?>" name="name" id="name">
					<br><b>Date Of Birth: (DD-MM-YYYY)</b>
					<br>
					<input type="text" class="form-control input-sm" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" name="dob" id="dob">
					<br><b>Residential Address:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->address); ?>" name="address" id="address">
					<br><b>Name Of Present School:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->nopskul); ?>" name="nopskul" id="nopskul">
					<br><b>Present Grade:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->grade); ?>" name="grade" id="grade">
					<br><b>Grade To Join:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->gradetjoin); ?>" name="gradetjoin" id="gradetjoin">
					<br><b>Academic Year For Which Admission Is Sought:</b><br>
					<select class="form-control input-sm" id="year" name="year">
						<option value="">Year?</option>
						<?php 
						$start_year=date('Y')-3;
						$end_year= date('Y')+1;
						$start_year1=$start_year+1;
						$end_year1=$end_year+1;
						for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ ?>
						<option value="<?php echo $i.'-'.$j; ?>" <?php if($row->year == $i.'-'.$j){ echo "selected";} ?>>
						<?php echo $i.'-'.$j;?>
						</option>
					<?php } ?>
					</select>
					<br><b>Name Of The Parents:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->nparents); ?>" name="nparents" id="nparents">
					<br><b>Designation:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->designation); ?>" name="designation" id="designation">
					<br><b>Name Of The Organization:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->organization); ?>" name="organization" id="organization">
					<br><b>Occupation of the Working Parent:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->occupation); ?>" name="occupation" id="occupation">
					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12" style="overflow-x:auto;">
					
					<br><b>Official Address:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->offadd); ?>" name="offadd" id="offadd">
					<br><b>City:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->city); ?>" name="city" id="city">
					<br><b>State:</b><br>
					<select class="form-control" id="state" name="state">
					  <option value=''>Select State</option>
					  <option value='Andhra Pradesh' <?php if($row->state == 'Andhra Pradesh'){ echo 'selected';} ?>>Andhra Pradesh</option>
					  <option value='Arunachal Pradesh' <?php if($row->state == 'Arunachal Pradesh'){ echo 'selected';} ?>>Arunachal Pradesh</option>
					  <option value='Assam' <?php if($row->state == 'Assam'){ echo 'selected';} ?>>Assam </option>
					  <option value='Bihar' <?php if($row->state == 'Bihar'){ echo 'selected';} ?>>Bihar</option>
					  <option value='Chhattisgarh' <?php if($row->state == 'Chhattisgarh'){ echo 'selected';} ?>>Chhattisgarh    </option>
					  <option value='Goa' <?php if($row->state == 'Goa'){ echo 'selected';} ?>>Goa</option>
					  <option value='Gujarat' <?php if($row->state == 'Gujarat'){ echo 'selected';} ?>>Gujarat</option>
					  <option value='Haryana' <?php if($row->state == 'Haryana'){ echo 'selected';} ?>>Haryana </option>
					  <option value='Himachal Pradesh' <?php if($row->state == 'Himachal Pradesh'){ echo 'selected';} ?>>Himachal Pradesh</option>
					  <option value='Jammu & Kashmir' <?php if($row->state == 'Jammu & Kashmir'){ echo 'selected';} ?>>Jammu & Kashmir</option>
					  <option value='Jharkhand' <?php if($row->state == 'Jharkhand'){ echo 'selected';} ?>>Jharkhand </option>
					  <option value='Karnataka' <?php if($row->state == 'Karnataka'){ echo 'selected';} ?>>Karnataka </option>
					  <option value='Kerla' <?php if($row->state == 'Kerla'){ echo 'selected';} ?>>Kerla </option>
					  <option value='Madhya Pradesh' <?php if($row->state == 'Madhya Pradesh'){ echo 'selected';} ?>>Madhya Pradesh</option>
					  <option value='Maharashtra' <?php if($row->state == 'Maharashtra'){ echo 'selected';} ?>>Maharashtra  </option>
					  <option value='Manipur' <?php if($row->state == 'Manipur'){ echo 'selected';} ?>>Manipur </option>
					  <option value='Meghalaya' <?php if($row->state == 'Meghalaya'){ echo 'selected';} ?>>Meghalaya</option>
					  <option value='Mizoram' <?php if($row->state == 'Mizoram'){ echo 'selected';} ?>> Mizoram</option>
					  <option value='Nagaland' <?php if($row->state == 'Nagaland'){ echo 'selected';} ?>>Nagaland</option>
					  <option value='Orissa' <?php if($row->state == 'Orissa'){ echo 'selected';} ?>>Orissa </option>
					  <option value='Punjab' <?php if($row->state == 'Punjab'){ echo 'selected';} ?>>Punjab </option>
					  <option value='Sikkim' <?php if($row->state == 'Sikkim'){ echo 'selected';} ?>>Sikkim </option>
					  <option value='Tamilnadu' <?php if($row->state == 'Tamilnadu'){ echo 'selected';} ?>>Tamilnadu</option>
					  <option value='Tripura' <?php if($row->state == 'Tripura'){ echo 'selected';} ?>>Tripura </option>
					  <option value='Uttar Pradesh' <?php if($row->state == 'Uttar Pradesh'){ echo 'selected';} ?>>Uttar Pradesh </option>
					  <option value='Uttaranchal' <?php if($row->state == 'Uttaranchal'){ echo 'selected';} ?>> Uttaranchal </option>
					  <option value='West Bengal' <?php if($row->state == 'West Bengal'){ echo 'selected';} ?>>West Bengal</option>
					  <option value='Outside India' <?php if($row->state == 'Outside India'){ echo 'selected';} ?>>Outside India</option>
					</select>					
					
					<br><b>Pincode:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->pincode); ?>" name="pincode" id="pincode">
					<br><b>Residence Tel:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->restel); ?>" name="restel" id="restel">
					<br><b>Office Tel:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->offtel); ?>" name="offtel" id="offtel">
					<br><b>Mobile Number:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo ucwords($row->mobile); ?>" name="mobile" id="mobile">
					<br><b>Email Address:</b><br>
					<input type="text" class="form-control input-sm" value="<?php echo $row->email; ?>" name="email" id="email">
					<br><b>What Do You Wish To Know:</b><br>
					<textarea class="form-control input-sm" name="whattoknow" id="whattoknow"><?php echo ucwords($row->whattoknow); ?></textarea>
					<br><b>Tell us how you heard about our School:</b><br>
					<textarea class="form-control input-sm" name="aboutschool" id="aboutschool"><?php echo ucwords($row->aboutschool); ?></textarea>
					</div>
					<div class="col-md-2 col-sm-12 col-xs-12"></div>
					<div class="clearfix"></div>
					<div class="form-group">
					  {{ csrf_field() }}
                        <div class=""><br><br>
						<center>
						<button id="savedata" type="submit" class="btn btn-success btn-sm">Submit</button>
						<a class="btn btn-danger  btn-sm" href="{!! \Config::get('app.url_base') !!}/enquiry">Cancel</a>
						</center>
                        </div>
                    </div>
					  
				</form>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!-- /page content -->
@include('included.footer')