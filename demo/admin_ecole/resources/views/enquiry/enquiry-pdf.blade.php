<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body style="font-size:12px;color:black;font-family:Helvetica, Arial, sans-serif;">
  <?php foreach ($Data as $row){ } ?>
   
		<br><b>Entry Id:</b><br>
		<input type="text" value="<?php echo ucwords($row->id); ?>" style="width:100%;height:20px;">
		<br><b>Student's Name:</b><br>
		<input type="text" value="<?php echo ucwords($row->name); ?>" style="width:100%;height:20px;">
		<br><b>Date Of Birth: (DD-MM-YYYY)</b>
		<br>
		<input type="text" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" style="width:100%;height:20px;">
		<br><b>Residential Address:</b><br>
		<input type="text" value="<?php echo ucwords($row->address); ?>" style="width:100%;height:20px;">
		<br><b>Name Of Present School:</b><br>
		<input type="text" value="<?php echo ucwords($row->nopskul); ?>" style="width:100%;height:20px;">
		<br><b>Present Grade:</b><br>
		<input type="text" value="<?php echo ucwords($row->grade); ?>" style="width:100%;height:20px;">
		<br><b>Grade To Join:</b><br>
		<input type="text" value="<?php echo ucwords($row->gradetjoin); ?>" style="width:100%;height:20px;">
		<br><b>Academic Year For Which Admission Is Sought:</b><br>
		<input type="text" value="<?php echo ucwords($row->year); ?>" style="width:100%;height:20px;">
		<br><b>Name Of The Parents:</b><br>
		<input type="text" value="<?php echo ucwords($row->nparents); ?>" style="width:100%;height:20px;">
		<br><b>Designation:</b><br>
		<input type="text" value="<?php echo ucwords($row->designation); ?>" style="width:100%;height:20px;">
		<br><b>Name Of The Organization:</b><br>
		<input type="text" value="<?php echo ucwords($row->organization); ?>" style="width:100%;height:20px;">
		<br><b>Occupation of the Working Parent:</b><br>
		<input type="text" value="<?php echo ucwords($row->occupation); ?>" style="width:100%;height:20px;">
		<br><b>Official Address:</b><br>
		<input type="text" value="<?php echo ucwords($row->offadd); ?>" style="width:100%;height:20px;">
		<br><b>City:</b><br>
		<input type="text" value="<?php echo ucwords($row->city); ?>" style="width:100%;height:20px;">
		<br><b>State:</b><br>
		<input type="text" value="<?php echo ucwords($row->state); ?>" style="width:100%;height:20px;">
		<br><b>Pincode:</b><br>
		<input type="text" value="<?php echo ucwords($row->pincode); ?>" style="width:100%;height:20px;">
		<br><b>Residence Tel:</b><br>
		<input type="text" value="<?php echo ucwords($row->restel); ?>" style="width:100%;height:20px;">
		<br><b>Office Tel:</b><br>
		<input type="text" value="<?php echo ucwords($row->offtel); ?>" style="width:100%;height:20px;">
		<br><b>Mobile Number:</b><br>
		<input type="text" value="<?php echo ucwords($row->mobile); ?>" style="width:100%;height:20px;">
		<br><b>Email Address:</b><br>
		<input type="text" value="<?php echo $row->email; ?>" style="width:100%;height:20px;">
		<br><b>What Do You Wish To Know:</b><br>
		<input type="text" value="<?php echo ucwords($row->whattoknow); ?>" style="width:100%;height:20px;">
		<br><b>Tell us how you heard about our School:</b><br>
		<input type="text" value="<?php echo ucwords($row->aboutschool); ?>" style="width:100%;height:20px;">

  </body>
</html>