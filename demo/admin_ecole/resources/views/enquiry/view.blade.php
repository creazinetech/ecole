@include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('view',$RoleArray) || Auth::user()->added_by== 0){
foreach ($Data as $row){ }
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Enquiry View</h2>
                    <div class="clearfix"></div>
					<?php if(in_array('print',$RoleArray) || Auth::user()->added_by== 0) { ?>
					<a href="{{URL::to('enquiry/enquiry-pdf',array(Crypt::encrypt($row->id)))}}" class="btn btn-success btn-xs" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Print PDF</a>
					<?php }?>
					<a href="{{URL::to('enquiry')}}" class="btn btn-danger btn-xs" title="Back"><i class="fa fa-close"></i> Back</a>
                  </div>
					<div class="x_content">	
					
    
					<table class="table table-bordered table-condesed" style="color:black;" width="100%" cellspacing="4" cellpadding="5">
					<tr>
					<td><b>Entry Id:</b><br><?php echo ucwords($row->id); ?></td>
					</tr>
					<tr style="background-color:#edf6fe;">
					<td><b>Student's Name:</b><br><?php echo ucwords($row->name); ?></td>
					</tr>
					<tr>
					<td><b>Date Of Birth: (DD-MM-YYYY)</b><br><?php echo date('d-m-Y', strtotime($row->dob)); ?></td>
					</tr>
					<tr style="background-color:#edf6fe;">
					<td><b>Residential Address:</b><br><?php echo ucwords($row->address); ?></td>
					</tr>
					<tr>
					<td><b>Name Of Present School:</b><br><?php echo ucwords($row->nopskul); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Present Grade:</b><br><?php echo ucwords($row->grade); ?></td>
					</tr>
					<tr>
					<td><b>Grade To Join:</b><br><?php echo ucwords($row->gradetjoin); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Academic Year For Which Admission Is Sought:</b><br><?php echo $row->year; ?></td>
					</tr>
					
					<tr>
					<td><b>Name Of The Parents:</b><br><?php echo ucwords($row->nparents); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Designation:</b><br><?php echo ucwords($row->designation); ?></td>
					</tr>
					
					<tr>
					<td><b>Name Of The Organization:</b><br><?php echo ucwords($row->organization); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Occupation of the Working Parent:</b><br><?php echo ucwords($row->occupation); ?></td>
					</tr>
					
					<tr>
					<td><b>Official Address:</b><br><?php echo ucwords($row->offadd); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>City:</b><br><?php echo ucwords($row->city); ?></td>
					</tr>
					<tr>
					<td><b>State:</b><br><?php echo ucwords($row->state); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Pincode:</b><br><?php echo ucwords($row->pincode); ?></td>
					</tr>
					<tr>
					<td><b>Residence Tel:</b><br><?php echo ucwords($row->restel); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Office Tel:</b><br><?php echo ucwords($row->offtel); ?></td>
					</tr>
					<tr>
					<td><b>Mobile Number:</b><br><?php echo ucwords($row->mobile); ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Email Address:</b><br><?php echo ucwords($row->email); ?></td>
					</tr>
					<tr>
					<td><b>What Do You Wish To Know:</b><br><?php echo $row->whattoknow; ?></td>
					</tr>
					<tr style="background-color:#edf6fe">
					<td><b>Tell us how you heard about our School:</b><br><?php echo ucwords($row->aboutschool); ?></td>
					</tr>
					
				</table>
		</div>
		</div>
		</div>
	</div>
	</div>
	</div>
        <!-- /page content -->
<?php } ?>
@include('included.footer')