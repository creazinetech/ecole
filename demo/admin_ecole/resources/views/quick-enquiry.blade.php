@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','4')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('lisitng',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Quick Enquiries</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content" style="overflow-x:auto;">
                     
					<table class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Name</th>
                          <th>Contact</th>
                          <th>Email</th>
                          <th>Date</th>
                          <?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
                          <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						if(count($QuickEnquiries) > 0){ 
						foreach($QuickEnquiries as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $row->qe_name; ?></td>
						<td><?php echo $row->qe_mobile; ?></td>
						<td><?php echo $row->qe_email; ?></td>
						<td><?php echo date('d M, Y H:i A',strtotime($row->created_at)); ?></td>
						<?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<a href="{{URL::to('quick-enquiry/qedelete',array(Crypt::encrypt($row->qe_id)))}}" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm('Are you sure want to delete this?')"><i class="fa fa-trash"></i></a>
						</td>
						<?php } ?>
						</tr>
						<?php $i++; }} ?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
  @include('included.footer')