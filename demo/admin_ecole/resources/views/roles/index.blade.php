 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','8')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('lisitng',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Client Class List</h2>
					<?php if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){ ?>
					<a href="{!! \Config::get('app.url_base') !!}/roles/add" class="btn btn-sm btn-primary pull-right" title="Add Role"> Add Roles</a>
					<?php } ?>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
                     
					<table id="datatable" class="table table-striped table-bordered table-condensed table-hover" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Role</th>
                          <th>Status</th>
						  <?php if(in_array('edit',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
                      <tbody>
					    <?php $i = 1;?>
						@foreach ($Data as $row)
                        <tr>
                          <td> <?php echo $i; ?></td>
                          <td><?php echo $row->role_name; ?></td>
						   <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <td>
						  <select id="role_status" name="role_status" class="form-group role_status" data-id="<?php echo $row->role_id; ?>">
						  <option value="Active" <?php if($row->role_status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->role_status == "Inactive"){echo "selected";} ?>>Inactive</option>
						  </select>	
						  </td>
						   <?php } ?>
						  <?php if(in_array('edit',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <td>
						  <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						  <a href="{{URL::to('roles/edit',array(Crypt::encrypt($row->role_id)))}}" class="btn btn-success btn-xs" title="Edit"><i class="fa fa-edit"></i></a>
						  <?php } 
						  if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){
						  ?>
						  <a href="{{URL::to('roles/delete',array(Crypt::encrypt($row->role_id)))}}" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash"></i></a>
						  <?php } ?>
						  </td>
						  <?php } ?>
                        </tr>
						 <?php $i++; ?>
						@endforeach
                      </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
	$(document).ready(function(){
		$('.role_status').change(function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			var baseurl = $('#baseurl').val();
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: baseurl+'roles/rolestatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
	});
	</script>
		
  @include('included.footer')