 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','8')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Role</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
                     
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('updaterole')}}" enctype="multipart/form-data" method="post">
					  
					  <input id="role_id" name="role_id" type="hidden" value="{{$Data->role_id}}">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role_name">Role Name <span class="required">*</span></label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                          <input id="role_name" name="role_name" class="form-control" placeholder="Role Name" required="required" type="text" value="{{$Data->role_name}}">
                        </div>
					  </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role_status">Status
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label><input type="radio" name="role_status" required="required" id="role_status" class="icheck" value="Active" <?php if($Data->role_status =="Active"){echo "checked";} ?> > Active</label>
							<label><input type="radio" name="role_status" required="required" id="role_status" class="icheck" value="Inactive" <?php if($Data->role_status =="Inactive"){echo "checked";} ?>> Inactive</label>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
                        <div class="col-md-6 col-md-offset-3">
                          <button id="savedata" type="submit" class="btn btn-success">Submit</button>
                          <a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/roles">Cancel</a>
                        </div>
                      </div>
                    </form>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>
  @include('included.footer')