 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','5')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){
foreach ($PageData as $row){ }
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result">
			  </p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
						<h2>Edit Page - <?php echo $row->page_title; ?></h2>
						 <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					
					<form class="form-horizontal form-label-left" novalidate action="{{URL::route('updatepage')}}" enctype="multipart/form-data" method="post">
                    <input id="page_id" name="page_id" type="hidden" value="<?php echo $row->page_id; ?>">

					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Page Heading</span>
						</label>
						<div class="col-md-4 col-sm-6 col-xs-12 has-feedback">
						  <input type="text" id="page_heading" name="page_heading" class="form-control" placeholder="Page Heading" value="<?php echo $row->page_heading; ?>" >
						</div>
					</div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="page_img">Page Image <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-12 col-xs-12 has-feedback">
                          <input type="file" id="page_img" name="page_img" class="form-control">
						  <span class="fa fa-image form-control-feedback right" aria-hidden="true"></span>
                        <br />
						<img src="{!! \Config::get('app.url') !!}/images/pages/<?php echo $row->page_img;?>" style="height:60px;width:60px;">
						</div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="page_banner">Page Banner <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-12 col-xs-12 has-feedback">
                          <input type="file" id="page_banner" name="page_banner" class="form-control">
						  <span class="fa fa-image form-control-feedback right" aria-hidden="true"></span>
                        <br />
						<img src="{!! \Config::get('app.url') !!}/images/pages/<?php echo $row->page_banner;?>" style="height:60px;width:60px;">
						</div>
                    </div>
					  
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Banner Title</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
						  <input type="text" id="banner_title" name="banner_title" class="form-control" placeholder="Banner Title" value="<?php echo $row->banner_title; ?>" >
						</div>
					</div>
					
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Page Meta Title</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
						  <input type="text" id="page_meta_title" name="page_meta_title" class="form-control" placeholder="Page Meta Title" value="<?php echo $row->page_meta_title; ?>" >
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Page Meta Keywords</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
						  <input type="text" id="page_meta_keywords" name="page_meta_keywords" class="form-control" placeholder="Page Meta Description" value="<?php echo $row->page_meta_keywords; ?>" >
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Page Meta Description</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
						  <input type="text" id="page_meta_description" name="page_meta_description" class="form-control" placeholder="Page Meta Description" value="<?php echo $row->page_meta_description; ?>" >
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label" for="short_desc">Short Description</span>
						</label>
						<div class="col-md-12 col-sm-12 col-xs-12">
						  <textarea id="short_desc" name="short_desc" class="form-control col-md-7 col-xs-12"><?php echo $row->short_desc; ?></textarea>
						</div>
					</div>	
					
					<?php if($row->page_id == 25){ ?>
					<div class="item form-group">
						<label class="pull-left" for="brief_desc2">Mission,Vision & Aims</span>
						</label>
						<div class="col-md-12 col-sm-12 col-xs-12">
						<textarea id="brief_desc2" name="brief_desc2" class="ckeditor"><?php echo $row->brief_desc2; ?></textarea>
						</div>
					</div>
					
				<!---	<div class="item form-group">
						<label class="pull-left" for="brief_desc3">Aims</span>
						</label>
						<div class="col-md-12 col-sm-12 col-xs-12">
						<textarea id="brief_desc2" name="brief_desc3" class="ckeditor"><?php echo $row->brief_desc3; ?></textarea>
						</div>
					</div> -->
					
				    <?php } ?>
				    
					<div class="item form-group">
						<label class="pull-left" for="brief_desc">Page Data</span>
						</label>
						<div class="col-md-12 col-sm-12 col-xs-12">
						<textarea id="brief_desc" name="brief_desc" class="form-control col-md-7 col-xs-12"><?php echo $row->brief_desc; ?></textarea>
						</div>
					</div>
					
					<!--<textarea id="brief_desc" name="brief_desc" class="ckeditor" ></textarea>  -->
					
						
					<div class="ln_solid"></div>
					<div class="form-group">
						{{ csrf_field() }}
						<div class="col-md-6 col-md-offset-3">
						<button id="savedata" type="submit" class="btn btn-success">Submit</button>
						<a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/pages">Cancel</a>
						</div>
					</div>
					  
					</form>
					
					
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

<script type="text/javascript">  
 CKEDITOR.replace( 'brief_desc' );  
 CKEDITOR.replace( 'short_desc' );  
</script> 
	  
  <script type="text/javascript">
      $('.summernote').summernote({
        height: 200
      });
  </script>
  
  @include('included.footer')