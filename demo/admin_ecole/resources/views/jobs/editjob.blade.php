@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','14')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
foreach($Data as $row){ }
?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">

	<div class="clearfix"></div>
	<div class="flash-message">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
	  @if(Session::has('alert-' . $msg))

	  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
	  @endif
	@endforeach
	</div>
	
	<div class="flash-message1" style="display:none;">
	  <p class="alert alert-dismissable status_result"></p>
	</div>
	
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_content" style="">
			<div class="clearfix"></div>
			<div class="row">
			<form class="form-horizontal form-label-left"  action="{{URL::route('updatejob')}}" enctype="multipart/form-data" method="post">
			<div class="col-md-4 col-sm-12 col-xs-12">
			<h2>Edit Job</h2>
			<div class="clearfix"></div>
				<div class="item form-group">
					<label class="col-md-12 col-sm-12 col-xs-12" for="name">Title</label>
					<div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
					  <input type="hidden" id="id" name="id" class="form-control input-sm" value="<?php echo $row->id; ?>" >
					  <input type="text" name="title" id="title"  class="form-control input-sm"  required="required" value="<?php echo $row->title; ?>" >
					  <label style="display:none;" class="titleerr red">Please enter title.</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12 col-sm-12 col-xs-12" for="name">Short Description</label>
					<div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
					 <input type="text" name="short_desc" id="short_desc" class="form-control input-sm" value="<?php echo $row->short_desc; ?>">
					 <label style="display:none;" class="short_descerr red">Please short description.</label>
					</div>
				</div>
				<div class="item form-group">
					<label class="col-md-12 col-sm-12 col-xs-12" for="name">Thumbnail
					</label>
					<div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
					 <input type="file" name="thumbnail" id="thumbnail" class="form-control input-sm">
					 <?php echo $row->thumbnail; ?><br>
					 <label style="display:none;" class="thumbnailerr red">Please select .png, .jpg, .gif, .bmp format file.</label>
					</div>
				</div>
				<div class="item form-group">
					<label class="col-md-12 col-sm-12 col-xs-12" for="name">Status
					</label>
					<div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
					 <select id="status" name="status" class="form-group input-sm">
						  <option value="Active" <?php if($row->status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->status == "Inactive"){echo "selected";} ?>>Inactive</option>
					</select>
					<label style="display:none;" class="statuserr red">Please select status.</label>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="item form-group">
					<label class="col-md-12 col-sm-12 col-xs-12" for="name">Brief description
					</label>
					<div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
					 <textarea name="brief_desc" id="brief_desc" class="form-control input-sm"><?php echo $row->brief_desc; ?></textarea>
					</div>
				</div>
				{{ csrf_field() }}
				<div class="item form-group pull-right ">
					<div class="clearfix"></div>
					<button type="submit" class="btn btn-success btn-sm">Save</button>
					<a href="{!! \Config::get('app.url_base') !!}/jobs" class="btn btn-danger btn-sm">Cancel</a>
				</div>
			
			</div>

			</form>
		  </div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!-- /page content -->
<script>
	$(document).ready(function(){
		CKEDITOR.replace( 'brief_desc' ); 
	});
</script>
<script>
	$('#thumbnail').on('change',function(){
		var thumbnail = $('#thumbnail').val();
		var ext = thumbnail.split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				$('.thumbnailerr').show();
				return false;
			}else{
				$('.thumbnailerr').hide();
			}
	});
	
	$('.savenews').on('click',function(){
		var title = $('#title').val();
		var short_desc = $('#short_desc').val();
		var brief_desc = $('#brief_desc').val();
		var thumbnail = $('#thumbnail').val();
		var status = $('#status').val();
		var ext = thumbnail.split('.').pop().toLowerCase();
		if(title ==""){
			$('.titleerr').show();
			return false;
		}else{
			$('.titleerr').hide();
		}
		// if(brief_desc ==""){
			// $('.brief_descerr').show();
			// return false;
		// }else{
			// $('.brief_descerr').hide();
		// }
		if(status ==""){
			$('.statuserr').show();
			return false;
		}else{
			$('.statuserr').hide();
		}
		if(thumbnail !=""){
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				$('.thumbnailerr').show();
				return false;
			}else{
				$('.thumbnailerr').hide();
			}
		}else{
			$('.thumbnailerr').hide();
		}
	});
</script>
	
		
@include('included.footer')