 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','5')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result">
			  </p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
						<h2>Manage Pages</h2>
						 <div class="clearfix"></div>
                  </div>
					<div class="x_content" style="overflow-x:auto;">
					<table class="table table-striped table-bordered dt-responsive datatable-responsive" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th width="3%">Sr No</th>
                          <th>Page</th>
						  <?php if(in_array('edit',$RoleArray) ||in_array('delete',$RoleArray) ||in_array('view',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
                      <tbody>
					    <?php $i = 1; ?>
						<?php foreach ($Pages as $row){ ?>
                        <tr>
                          <td> <?php echo $i; ?></td>
                          <td>
							<b><?php echo $row->page_title; ?></b>
						  </td>
						  <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <td>
						  <a href="{{URL::to('editpage',array(Crypt::encrypt($row->page_id)))}}" class="btn btn-success btn-xs" title="Edit"><i class="fa fa-edit"></i></a>
						  </td>
						  <?php } ?>
                        </tr>
						<?php $i++; } ?>
                      </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
  @include('included.footer')