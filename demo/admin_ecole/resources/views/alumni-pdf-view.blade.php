@include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','6')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('view',$RoleArray) || Auth::user()->added_by== 0){
foreach ($Data as $row){ }
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Alumni PDF</h2>
                    <div class="clearfix"></div>
					<?php if(in_array('print',$RoleArray) || Auth::user()->added_by== 0) { ?>
					<a href="{{URL::to('alumni/alumni-pdf-print',array(Crypt::encrypt($row->aid)))}}" class="btn btn-success btn-xs" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Print PDF</a>
					<?php }?>
					<a href="{{URL::to('alumni')}}" class="btn btn-danger btn-xs" title="Back"><i class="fa fa-close"></i> Back</a>
                  </div>
					<div class="x_content">	
					
    
					<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5">
						<tr style="background-color:#e7e7e7;">
						<td><b>Alumni: Entry Id #<?php echo ucwords($row->aid); ?></b></td>
						</tr>
						<tr style="background-color:#edf6fe;">
						<td><b>First Name:</b><br><?php echo ucwords($row->firstname); ?></td>
						</tr>
						<tr>
						<td><b>Middle Name</b><br><?php echo ucwords($row->middlename); ?></td>
						</tr>
						<tr style="background-color:#edf6fe;">
						<td><b>Last Name:</b><br><?php echo ucwords($row->lastname); ?></td>
						</tr>
						<tr>
						<td><b>Graduate Year:</b><br><?php echo ucwords($row->graduate_yr); ?></td>
						</tr>
						<tr style="background-color:#edf6fe">
						<td><b>Home Address:</b><br><?php echo ucwords($row->home_address); ?></td>
						</tr>
						<tr>
						<td><b>City:</b><br><?php echo ucwords($row->city); ?></td>
						</tr>
						<tr style="background-color:#edf6fe">
						<td><b>Zip:</b><br><?php echo ucwords($row->state); ?></td>
						</tr>
						
						<tr>
						<td><b>Mobile No:</b><br><?php echo ucwords($row->contact1); ?></td>
						</tr>
						<tr style="background-color:#edf6fe">
						<td><b>Personal Email ID:</b><br><?php echo strtolower($row->emailid); ?></td>
						</tr>
						
						<tr>
						<td><b>Are You Studying:</b><br><?php echo ucwords($row->study_type); ?></td>
						</tr>
						<tr style="background-color:#edf6fe">
						<td><b>Occupation:</b><br><?php echo ucwords($row->occupation); ?></td>
						</tr>
						
						<tr>
						<td><b>Name of The Organization/Employer:</b><br><?php echo ucwords($row->organisation); ?></td>
						</tr>
						<tr style="background-color:#edf6fe">
						<td><b>Designation:</b><br><?php echo ucwords($row->designation); ?></td>
						</tr>
						<tr>
						<td><b>Are You Working:</b><br><?php echo ucwords($row->work_type); ?></td>
						</tr>
						<tr style="background-color:#edf6fe">
						<td><b>Gender:</b><br><?php echo ucwords($row->gender); ?></td>
						</tr>
						<tr>
						<td><b>Date Of Birth:</b><br><?php echo date('d-m-Y',strtotime($row->dob)); ?></td>
						</tr>
						<tr style="background-color:#edf6fe">
						<td><b>Are you willing to serve as an alumni ambassador for prospective applicants and current students:</b><br><?php echo ucwords($row->alumni_ambassador); ?></td>
						</tr>		
					</table>
		</div>
		</div>
		</div>
	</div>
	</div>
	</div>
        <!-- /page content -->
<?php } ?>
@include('included.footer')