<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  <?php 
  
  foreach ($Data as $row){ }
  // if($type == 1){
  ?>
   <table class="table table-bordered tabled-stripped table-hover" width="100%" cellpadding="5" style="font-size:12px;border:1px solid black;font-family:Helvetica, Arial, sans-serif;">
		<tr style="background-color:#e7e7e7;">
		<td><b>Alumni: Entry Id #<?php echo ucwords($row->aid); ?></b></td>
		</tr>
		<tr style="background-color:#edf6fe;">
		<td><b>First Name:</b><br><?php echo ucwords($row->firstname); ?></td>
		</tr>
		<tr>
		<td><b>Middle Name</b><br><?php echo ucwords($row->middlename); ?></td>
		</tr>
		<tr style="background-color:#edf6fe;">
		<td><b>Last Name:</b><br><?php echo ucwords($row->lastname); ?></td>
		</tr>
		<tr>
		<td><b>Graduate Year:</b><br><?php echo ucwords($row->graduate_yr); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Home Address:</b><br><?php echo ucwords($row->home_address); ?></td>
		</tr>
		<tr>
		<td><b>City:</b><br><?php echo ucwords($row->city); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Zip:</b><br><?php echo ucwords($row->state); ?></td>
		</tr>
		
		<tr>
		<td><b>Mobile No:</b><br><?php echo ucwords($row->contact1); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Personal Email ID:</b><br><?php echo strtolower($row->emailid); ?></td>
		</tr>
		
		<tr>
		<td><b>Are You Studying:</b><br><?php echo ucwords($row->study_type); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Occupation:</b><br><?php echo ucwords($row->occupation); ?></td>
		</tr>
		
		<tr>
		<td><b>Name of The Organization/Employer:</b><br><?php echo ucwords($row->organisation); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Designation:</b><br><?php echo ucwords($row->designation); ?></td>
		</tr>
		<tr>
		<td><b>Are You Working:</b><br><?php echo ucwords($row->work_type); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Gender:</b><br><?php echo ucwords($row->gender); ?></td>
		</tr>
		<tr>
		<td><b>Date Of Birth:</b><br><?php echo date('d-m-Y',strtotime($row->dob)); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Are you willing to serve as an alumni ambassador for prospective applicants and current students:</b><br><?php echo ucwords($row->alumni_ambassador); ?></td>
		</tr>		
	</table>
  <?php 
  // }
  ?>
  </body>
</html>