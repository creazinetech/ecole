<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  <?php 
  
  foreach ($Data as $row){ }
  ?>
	<b>Apply Now: Entry # <?php echo ucwords($row->id); ?></b><br><br>
	
	<table class="table table-bordered tabled-stripped table-condensed" width="100%" cellspacing="4" cellpadding="5" style="border:1px solid #F1F1F1;border-radius:15px;">
		<tr>
		<td><b>Position applying for:</b>
		<br><?php echo ucwords($row->position); ?></td>
		</tr>
		
		<tr style="background-color:#edf6fe;">
		<td><b>Name:</b>
		<br><?php echo ucwords($row->name); ?></td>
		</tr>
		
		<tr>
		<td><b>Date Of Birth:</b>
		<br><?php echo date('d/m/Y',strtotime($row->dob)); ?></td>
		</tr>
		
		<tr style="background-color:#edf6fe;">
		<td><b>Upload Photograph:</b>
		<br>
		<?php 
		//if (file_exists(public_path().'/images/job-applications/'.$row->photo)) { ?>
		<!--<img style="width:180px; height:200px; display: block;" src="{!! \Config::get('app.url') !!}/images/job-applications/<?php echo $row->photo; ?>" alt="image" />-->
		<?php //} else{?>
		<img style="width:180px; height:200px; display: block;" src="{!! \Config::get('app.pdfimg') !!}/images/job-applications/<?php echo $row->photo; ?>" alt="image" />
		<?php //} ?>
		</td>
		</tr>
		
		<tr><td><b>Gender:</b>
		<br><?php echo $row->gender; ?>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Nationality:</b>
		<br><?php echo ucwords($row->nationality); ?>
		</td></tr>
		
		<tr><td><b>Marital Status:</b>
		<br><?php echo ucwords($row->marital_status); ?>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Children:</b>
		<br><?php echo ucwords($row->children); ?>
		</td></tr>
		
		<tr><td><b>Current Address:</b>
		<br><?php echo ucwords($row->cadd); ?>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Pincode:</b>
		<br><?php echo $row->pincode; ?>
		</td></tr>
		
		<tr><td><b>Landline no.</b>
		<br><?php echo $row->landline; ?>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Mobile no.</b>
		<br><?php echo $row->mob_no; ?>
		</td></tr>
		
		<tr><td><b>Email Id</b>
		<br>
		<a href="mailto:<?php echo $row->email_id; ?>"><?php echo $row->email_id; ?></a>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Permanent Address (Write ‘same’ if the same as current address)</b>
		<br><?php echo ucwords($row->padd); ?>
		</td></tr>
		
		<tr><td><b>Passport no.</b>
		<br><?php echo $row->passport_no; ?>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Date of issue:</b>
		<br><?php echo date('d F Y',strtotime($row->issue_date)); ?>
		</td></tr>
		
		<tr><td><b>Place of Issue:</b>
		<br><?php echo ucwords($row->issue_place); ?>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Expiry Date:</b>
		<br><?php echo date('d F Y',strtotime($row->expiry_date)); ?>
		</td></tr>
		
		<tr>
		<td><b>List your academic history below starting from the one completed last:</b>
		<br>
		<?php if(count($Data1)>0){ ?>
		<table width="100%" class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0">
		<thead>
		<tr style="background-color:#F1F1F1;">
		<th>Name of School/ College/ University</th>
		<th>Board/Degree/Diploma</th>
		<th>Starting Date</th>
		<th>Completion Date</th>
		<th>Grade</th>
		<th>Percentage</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($Data1 as $row1){ ?>
		<tr>
		<td><?php echo ucwords($row1->university); ?></td>
		<td><?php echo ucwords($row1->board); ?></td>
		<td><?php echo ucwords($row1->start_date); ?></td>
		<td><?php echo ucwords($row1->completion_date); ?></td>
		<td><?php echo ucwords($row1->grade); ?></td>
		<td><?php echo ucwords($row1->percentage); ?></td>
		</tr>		
		<?php } ?>
		</tbody>
		</table>	
		<?php } ?>
		
		</td>
		</tr>
		
								
		<tr style="background-color:#edf6fe;">
		<td><b>List the professional development courses attended by you, if any</b>
		<br>
		<?php if(count($Data2)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr style="background-color:#F1F1F1;">
		<th>Attended</th>
		<th>Name of Course</th>
		<th>Starting Date</th>
		<th>Completion Date</th>
		<th>Location</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($Data2 as $row2){ ?>
		<tr>
		<td><?php echo ucwords($row2->pattended); ?></td>
		<td><?php echo ucwords($row2->pcourse); ?></td>
		<td><?php echo ucwords($row2->pstart_date); ?></td>
		<td><?php echo ucwords($row2->pcompletion_date); ?></td>
		<td><?php echo ucwords($row2->plocation); ?></td>
		</tr>		
		<?php } ?>
		</tbody>
		</table>	
		<?php } ?>
		
		</td></tr>
								
		<tr>
		<td><b>List the professional development courses conducted by you, if any</b>
		<br>
		<?php if(count($Data21)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr style="background-color:#F1F1F1;">
		<th>Conducted</th>
		<th>Name of Course</th>
		<th>Starting Date</th>
		<th>Completion Date</th>
		<th>Location</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($Data21 as $row21){ ?>
		<tr>
		<td><?php echo ucwords($row21->pattended1); ?></td>
		<td><?php echo ucwords($row21->pcourse1); ?></td>
		<td><?php echo ucwords($row21->pstart_date1); ?></td>
		<td><?php echo ucwords($row21->pcompletion_date1); ?></td>
		<td><?php echo ucwords($row21->plocation1); ?></td>
		</tr>		
		<?php } ?>
		</tbody>
		</table>	
		<?php } ?>
		
		</td></tr>		
								
		<tr style="background-color:#edf6fe;"><td><b>Publications/Research Papers/ Documents produced (in the last 5 years):</b>
		<br>
		<?php if(count($Data3)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr style="background-color:#F1F1F1;">
		<th>Title</th>
		<th>Published in</th>
		<th>Date of Publishing</th>
		<th>Presented at</th>
		<th>Date of Presentation</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($Data3 as $row3){ ?>
		<tr>
		<td><?php echo ucwords($row3->prtitle); ?></td>
		<td><?php echo ucwords($row3->publish_in); ?></td>
		<td><?php echo ucwords($row3->publish_date); ?></td>
		<td><?php echo ucwords($row3->presented_at); ?></td>
		<td><?php echo ucwords($row3->presentation_date); ?></td>
		</tr>		
		<?php } ?>
		</tbody>
		</table>	
		<?php } ?>
		
		</td></tr>
		
		<tr><td><b>Experience in International Schools:</b>
		<br><?php echo ucwords($row->internation_school_exp); ?>
		</td></tr>						
		
		<tr style="background-color:#edf6fe;"><td><b>Experience in Type of International Education:</b>
		<br>
		<?php 
		if(isset($row->experience_in) && !empty($row->experience_in)){
		$exp_explode = explode(',',$row->experience_in);
		echo '<ul>';
		for($i=0;$i<count($exp_explode)-1;$i++){
			echo '<li>'.$exp_explode[$i].'</li>';
		?>
		<?php 
		} 
		echo '</ul>';
		} ?>
		
		</td>
		</tr>	

		<tr><td><b>Total Experience in years:</b>
		<br><?php echo ucwords($row->experience_year); ?>
		</td></tr>	
		
		<tr style="background-color:#edf6fe;"><td><b>Current Employer:</b>
		<br><?php echo ucwords($row->current_employer); ?>
		</td></tr>	
		
		<tr><td><b>Designation:</b>
		<br><?php echo ucwords($row->designation); ?>
		</td></tr>							
		
		
		
		<tr style="background-color:#edf6fe;"><td><b>Joining Date of Current Job:</b>
		<br><?php echo ucwords($row->joindt_curr_emp); ?>
		</td></tr>	
		
		<tr><td><b>Employment History (Start with the most Recent One First):</b>
		<br>
		<?php if(count($Data4)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr style="background-color:#F1F1F1;">
		<th>Name of Employer</th>
		<th>Designation</th>
		<th>Period of Employment  From - To</th>
		<th>Subject Taught (with Grade Levels)</th>
		<th>Curriculum Taught (IB/IGCSE/ ICSE)</th>
		<th>Additional Responsibility</th>
		<th>Reason for Leaving the Job</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($Data4 as $row4){ ?>
		<tr>
		<td><?php echo ucwords($row4->eh_name); ?></td>
		<td><?php echo ucwords($row4->eh_designation); ?></td>
		<td><?php echo ucwords($row4->period); ?></td>
		<td><?php echo ucwords($row4->subjects); ?></td>
		<td><?php echo ucwords($row4->curriculum); ?></td>
		<td><?php echo ucwords($row4->responsibility); ?></td>
		<td><?php echo ucwords($row4->leaving_reason); ?></td>
		</tr>		
		<?php } ?>
		</tbody>
		</table>	
		<?php } ?>
		
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>I.T. Skills: (Tell us about the IT skills you are good at):</b>
		<br><?php echo ucwords($row->it_skills); ?>
		</td></tr>	
		
		<tr><td><b>Language Skills:</b>
		<br><?php echo ucwords($row->language_speak); ?>
		</td></tr>	

		<tr style="background-color:#edf6fe;"><td><b>Hobbies/ Interests:</b>
		<br><?php echo ucwords($row->hobbies); ?>
		</td></tr>	
		
		<tr><td><b>Current Monthly Salary:</b>
		<br><?php echo ucwords($row->current_sal); ?>
		</td></tr>
		
		<tr style="background-color:#edf6fe;"><td><b>Expected Monthly Salary:</b>
		<br><?php echo ucwords($row->expected_sal); ?>
		</td></tr>	
		
		<tr><td><b>Earliest Joining Date:</b>
		<br><?php echo ucwords($row->newjoin_dt); ?>
		</td></tr>	

		<tr style="background-color:#edf6fe;">
		<td><b>Employment Reference:</b>
		<br>
		<?php if(count($Data5)>0){ ?>
		<table class="table table-bordered table-condesed tabled-stripped" border="1" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr style="background-color:#F1F1F1;">
		<th>Name Of Reference</th>
		<th>Designation</th>
		<th>Phone</th>
		<th>E-mail</th>
		<th>Address</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($Data5 as $row5){ ?>
		<tr>
		<td><?php echo ucwords($row5->ername); ?></td>
		<td><?php echo ucwords($row5->erdesignation); ?></td>
		<td><?php echo ucwords($row5->erphone); ?></td>
		<td><?php echo strtolower($row5->eremail); ?></td>
		<td><?php echo ucwords($row5->eraddress); ?></td>
		</tr>		
		<?php } ?>
		</tbody>
		</table>	
		<?php } ?>
		
		</td></tr>
		
		<tr><td><b>Personal Statement of Education:</b>
		<br><?php echo ucwords($row->edu_statement); ?>
		</td></tr>	
								
		<tr style="background-color:#edf6fe;"><td><b>Any other information, if required:</b>
		<br><?php echo ucwords($row->other_info); ?>
		</td></tr>	
								
		
	</table>
  
  </body>
</html>