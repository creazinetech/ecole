@include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','15')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('view',$RoleArray) || Auth::user()->added_by== 0){
foreach ($Data as $row){ }
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Job Application PDF</h2>
                    <div class="clearfix"></div>
					<?php if(in_array('print',$RoleArray) || Auth::user()->added_by== 0) { ?>
					<a href="{{URL::to('job-applications/pdf-print',array(Crypt::encrypt($row->id)))}}" class="btn btn-success btn-xs" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Print PDF</a>
					<?php }?>
					<a href="{{URL::to('job-applications')}}" class="btn btn-danger btn-xs" title="Back"><i class="fa fa-close"></i> Back</a>
                  </div>
					<div class="x_content">	
					
					<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5">
						<tr>
						<td><b>Apply Now: Entry # <?php echo ucwords($row->id); ?></b></td>
						</tr>
						
						<tr style="background-color:#edf6fe;">
						<td><b>Position applying for:</b></td>
						</tr>
						<tr>
						<td><span style="margin-left:20px;"><?php echo ucwords($row->position); ?></span></td>
						</tr>
						
						<tr style="background-color:#edf6fe;">
						<td><b>Name:</b></td>
						</tr>
						<tr>
						<td><span style="margin-left:20px;"><?php echo ucwords($row->name); ?></span></td>
						</tr>
						
						<tr style="background-color:#edf6fe;">
						<td><b>Date Of Birth:</b></td>
						</tr>
						<tr>
						<td><span style="margin-left:20px;"><?php echo date('d/m/Y',strtotime($row->dob)); ?></span></td>
						</tr>
						
						<tr style="background-color:#edf6fe;">
						<td><b>Upload Photograph:</b></td>
						</tr>
						<tr>
						<td>
						<span style="margin-left:20px;">
						<?php 
						//if (file_exists(public_path().'/images/job-applications/'.$row->photo)) { ?>
                		<!--<img style="width:180px; height:200px; display: block;" src="{!! \Config::get('app.url') !!}/images/job-applications/<?php echo $row->photo; ?>" alt="image" />-->
                		<?php //} else{?>
                		<img style="width:180px; height:200px; display: block;" src="{!! \Config::get('app.pdfimg') !!}/images/job-applications/<?php echo $row->photo; ?>" alt="image" />
                		<?php //} ?>
						</span>
						</td>
						</tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Gender:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo $row->gender; ?></span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Nationality:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->nationality); ?></span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Marital Status:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->marital_status); ?></span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Children:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->children); ?></span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Current Address:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->cadd); ?></span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Pincode:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo $row->pincode; ?></span>
						</td></tr>
						<tr style="background-color:#edf6fe;"><td><b>Landline no.</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo $row->landline; ?></span>
						</td></tr>
						<tr style="background-color:#edf6fe;"><td><b>Mobile no.</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo $row->mob_no; ?></span>
						</td></tr>
						<tr style="background-color:#edf6fe;"><td><b>Email Id</b></td></tr>
						<tr><td><span style="margin-left:20px;">
						<a href="mailto:<?php echo $row->email_id; ?>"><?php echo $row->email_id; ?></a>
						</span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Permanent Address (Write ‘same’ if the same as current address)</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->padd); ?></span>
						</td></tr>
						<tr style="background-color:#edf6fe;"><td><b>Passport no.</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo $row->passport_no; ?></span>
						</td></tr>
						<tr style="background-color:#edf6fe;"><td><b>Date of issue:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo date('d F Y',strtotime($row->issue_date)); ?></span>
						</td></tr>
						<tr style="background-color:#edf6fe;"><td><b>Place of Issue:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->issue_place); ?></span>
						</td></tr>
						<tr style="background-color:#edf6fe;"><td><b>Expiry Date:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo date('d F Y',strtotime($row->expiry_date)); ?></span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>List your academic history below starting from the one completed last:</b></td></tr>
						<tr><td>
						<span style="margin-left:20px;">
						<?php if(count($Data1)>0){ ?>
						<table class="table table-bordered table-condensed">
						<thead>
						<tr>
						<th>Name of School/ College/ University</th>
						<th>Board/Degree/Diploma</th>
						<th>Starting Date</th>
						<th>Completion Date</th>
						<th>Grade</th>
						<th>Percentage</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($Data1 as $row1){ ?>
						<tr>
						<td><?php echo ucwords($row1->university); ?></td>
						<td><?php echo ucwords($row1->board); ?></td>
						<td><?php echo ucwords($row1->start_date); ?></td>
						<td><?php echo ucwords($row1->completion_date); ?></td>
						<td><?php echo ucwords($row1->grade); ?></td>
						<td><?php echo ucwords($row1->percentage); ?></td>
						</tr>		
						<?php } ?>
						</tbody>
						</table>	
						<?php } ?>
						</span>
						</td></tr>
						
												
						<tr style="background-color:#edf6fe;"><td><b>List the professional development courses attended/conducted by you, if any</b></td></tr>
						<tr><td>
						<span style="margin-left:20px;">
						<?php if(count($Data2)>0){ ?>
						<table class="table table-bordered table-condensed">
						<thead>
						<tr>
						<th>Attended/Conducted</th>
						<th>Name of Course</th>
						<th>Starting Date</th>
						<th>Completion Date</th>
						<th>Location</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($Data2 as $row2){ ?>
						<tr>
						<td><?php echo ucwords($row2->pattended); ?></td>
						<td><?php echo ucwords($row2->pcourse); ?></td>
						<td><?php echo ucwords($row2->pstart_date); ?></td>
						<td><?php echo ucwords($row2->pcompletion_date); ?></td>
						<td><?php echo ucwords($row2->plocation); ?></td>
						</tr>		
						<?php } ?>
						</tbody>
						</table>	
						<?php } ?>
						</span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>List the professional development courses conducted by you, if any</b></td></tr>
						<tr><td>
						<span style="margin-left:20px;">
						<?php if(count($Data21)>0){ ?>
						<table class="table table-bordered table-condensed">
						<thead>
						<tr>
						<th>Conducted</th>
						<th>Name of Course</th>
						<th>Starting Date</th>
						<th>Completion Date</th>
						<th>Location</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($Data21 as $row21){ ?>
						<tr>
						<td><?php echo ucwords($row21->pattended1); ?></td>
						<td><?php echo ucwords($row21->pcourse1); ?></td>
						<td><?php echo ucwords($row21->pstart_date1); ?></td>
						<td><?php echo ucwords($row21->pcompletion_date1); ?></td>
						<td><?php echo ucwords($row21->plocation1); ?></td>
						</tr>		
						<?php } ?>
						</tbody>
						</table>	
						<?php } ?>
						</span>
						</td></tr>
						
												
						<tr style="background-color:#edf6fe;"><td><b>Publications/Research Papers/ Documents produced (in the last 5 years):</b></td></tr>
						<tr><td>
						<span style="margin-left:20px;">
						<?php if(count($Data3)>0){ ?>
						<table class="table table-bordered table-condensed">
						<thead>
						<tr>
						<th>Title</th>
						<th>Published in</th>
						<th>Date of Publishing</th>
						<th>Presented at</th>
						<th>Date of Presentation</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($Data3 as $row3){ ?>
						<tr>
						<td><?php echo ucwords($row3->prtitle); ?></td>
						<td><?php echo ucwords($row3->publish_in); ?></td>
						<td><?php echo ucwords($row3->publish_date); ?></td>
						<td><?php echo ucwords($row3->presented_at); ?></td>
						<td><?php echo ucwords($row3->presentation_date); ?></td>
						</tr>		
						<?php } ?>
						</tbody>
						</table>	
						<?php } ?>
						</span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Experience in International Schools:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->internation_school_exp); ?></span>
						</td></tr>						
						
						<tr style="background-color:#edf6fe;"><td><b>Experience in Type of International Education:</b></td></tr>
						<tr><td><span style="margin-left:20px;">
						<?php 
						if(isset($row->experience_in) && !empty($row->experience_in)){
						$exp_explode = explode(',',$row->experience_in);
						echo '<ul>';
						for($i=0;$i<count($exp_explode)-1;$i++){
							echo '<li>'.$exp_explode[$i].'</li>';
						?>
						<?php 
						} 
						echo '</ul>';
						} ?>
						</span>
						</td>
						</tr>	

						<tr style="background-color:#edf6fe;"><td><b>Total Experience in years:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->experience_year); ?></span>
						</td></tr>							
						<tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Current Employer:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->current_employer); ?></span>
						</td></tr>							
						<tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Designation:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->designation); ?></span>
						</td></tr>							
						<tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Joining Date of Current Job:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->joindt_curr_emp); ?></span>
						</td></tr>	
						
						<tr style="background-color:#edf6fe;"><td><b>Employment History (Start with the most Recent One First):</b></td></tr>
						<tr><td>
						<span style="margin-left:20px;">
						<?php if(count($Data4)>0){ ?>
						<table class="table table-bordered table-condensed">
						<thead>
						<tr>
						<th>Name of Employer</th>
						<th>Designation</th>
						<th>Period of Employment  From - To</th>
						<th>Subject Taught (with Grade Levels)</th>
						<th>Curriculum Taught (IB/IGCSE/ ICSE)</th>
						<th>Additional Responsibility</th>
						<th>Reason for Leaving the Job</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($Data4 as $row4){ ?>
						<tr>
						<td><?php echo ucwords($row4->eh_name); ?></td>
						<td><?php echo ucwords($row4->eh_designation); ?></td>
						<td><?php echo ucwords($row4->period); ?></td>
						<td><?php echo ucwords($row4->subjects); ?></td>
						<td><?php echo ucwords($row4->curriculum); ?></td>
						<td><?php echo ucwords($row4->responsibility); ?></td>
						<td><?php echo ucwords($row4->leaving_reason); ?></td>
						</tr>		
						<?php } ?>
						</tbody>
						</table>	
						<?php } ?>
						</span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>I.T. Skills: (Tell us about the IT skills you are good at):</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->it_skills); ?></span>
						</td></tr>	
						
						<tr style="background-color:#edf6fe;"><td><b>Language Skills:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->language_speak); ?></span>
						</td></tr>	

						<tr style="background-color:#edf6fe;"><td><b>Hobbies/ Interests:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->hobbies); ?></span>
						</td></tr>	
						<tr style="background-color:#edf6fe;"><td><b>Current Monthly Salary:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->current_sal); ?></span>
						</td></tr>	
						<tr style="background-color:#edf6fe;"><td><b>Expected Monthly Salary:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->expected_sal); ?></span>
						</td></tr>	
						<tr style="background-color:#edf6fe;"><td><b>Earliest Joining Date:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->newjoin_dt); ?></span>
						</td></tr>	

						<tr style="background-color:#edf6fe;"><td><b>Employment Reference:</b></td></tr>
						<tr><td>
						<span style="margin-left:20px;">
						<?php if(count($Data5)>0){ ?>
						<table class="table table-bordered table-condensed">
						<thead>
						<tr>
						<th>Name Of Reference</th>
						<th>Designation</th>
						<th>Phone</th>
						<th>E-mail</th>
						<th>Address</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($Data5 as $row5){ ?>
						<tr>
						<td><?php echo ucwords($row5->ername); ?></td>
						<td><?php echo ucwords($row5->erdesignation); ?></td>
						<td><?php echo ucwords($row5->erphone); ?></td>
						<td><?php echo strtolower($row5->eremail); ?></td>
						<td><?php echo ucwords($row5->eraddress); ?></td>
						</tr>		
						<?php } ?>
						</tbody>
						</table>	
						<?php } ?>
						</span>
						</td></tr>
						
						<tr style="background-color:#edf6fe;"><td><b>Personal Statement of Education:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->edu_statement); ?></span>
						</td></tr>	
												
						<tr style="background-color:#edf6fe;"><td><b>Any other information, if required:</b></td></tr>
						<tr><td><span style="margin-left:20px;"><?php echo ucwords($row->other_info); ?></span>
						</td></tr>	
												
						
					</table>
		</div>
		</div>
		</div>
	</div>
	</div>
	</div>
        <!-- /page content -->
<?php } ?>
@include('included.footer')