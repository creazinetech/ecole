@include('included.header')
@include('included.super-admin-sidebar')

 <?php foreach($Data as $row){} ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Job Application</h2>
					<a href="{!! \Config::get('app.url_base') !!}/alumni" class="btn btn-danger btn-sm pull-right"><i class="fa  fa-close"></i> Close</a>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					
					<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{!! \Config::get('app.url_base') !!}/images/job-applications/<?php echo $row->photo; ?>" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3><?php echo ucwords($row->name); ?></h3>
                      <strong>Designation</strong>
					  <input  type="text" id="designation" name="designation" value="<?php echo ucwords($row->designation); ?>"></h5>

                      <ul class="list-unstyled user_data">
                        <li><strong>Experience :</strong>
						<input  type="text" id="experience_year" name="experience_year" value="<?php echo $row->experience_year; ?>">
						</li>
                        <li><strong>Experience In :</strong>
						<input  type="text" id="experience_in" name="experience_in" value="<?php echo $row->experience_in; ?>"> 
						</li>
                        <li><strong>Internation School Experience :</strong>
						<input  type="text" id="internation_school_exp" name="internation_school_exp" value="<?php echo $row->internation_school_exp; ?>"> </li>
                        <li><strong>Current Employer :</strong>
						<input  type="text" id="current_employer" name="current_employer" value="<?php echo $row->current_employer; ?>"></li>
						
                        <li><strong>Current Address :</strong>
						<input  type="text" id="cadd" name="cadd" value="<?php echo ucwords($row->cadd); ?>"></li>
						
                        <li>
						<strong>Permanent Address :</strong>
						<input  type="text" id="padd" name="padd" value="<?php  echo ucwords($row->padd); ?>"></li>
						
                        <li> 
						<strong>Mobile No :</strong>
						<input  type="text" id="mob_no" name="mob_no" value="<?php echo $row->mob_no; ?> ">
						</li>
						
						<?php if(!empty($row->fax_no )){ ?>
						<li> <i class="fa fa-fax user-profile-icon"></i><?php echo $row->fax_no; ?> </li>
						<?php } ?>
						
                        <li class="m-top-xs">
						<strong>Email Id :</strong>
						<input  type="text" id="email_id" name="email_id" value="<?php echo $row->email_id; ?> ">
                        </li>
						
                        <li class="m-top-xs">
						<strong>Pincode :</strong>
						<input  type="text" id="pincode" name="pincode" value="<?php  echo $row->pincode; ?>">
						</li>
                        <li class="m-top-xs">
						<strong>Passport No:</strong>
						<input  type="text" id="passport_no" name="passport_no" value="<?php  echo $row->passport_no; ?>"> 
						</li>
						
                        <li class="m-top-xs">
						<strong>Issue Date:</strong>
						<input  type="text" id="issue_date" name="issue_date" value="<?php  echo $row->issue_date; ?>">
						</li>
						
                        <li class="m-top-xs">
						<strong>Expiry Date:</strong>
						<input  type="text" id="expiry_date" name="expiry_date" value="<?php  echo $row->expiry_date; ?>">
						</li>
						
                      </ul>
					  </div>
					
					<div class="col-md-9 col-sm-12 col-xs-12" style="overflow-x:auto;height:auto">
					<?php if(count($EduacationalDetails)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr class="bg-blue"><th colspan="6" class="text-center">Educational Details</th></tr>
							<tr>
							<th>University</th>
							<th>Board</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Grade</th>
							<th>Percentage</th>
							</tr>
							<?php 
							$i=1;
							foreach($EduacationalDetails as $row1){ ?>
							<tr>
							<td>
							<input  type="text" id="university" name="university" value="<?php  echo ucwords($row1->university); ?>">
							</td>
							<td>
							<input  type="text" id="board" name="board" value="<?php  echo ucwords($row1->board); ?>">
							</td>
							<td>
							<input  type="text" id="start_date" name="start_date" value="<?php  echo $row1->start_date; ?>"></td>
							<td>
							<input type="text" id="completion_date" name="completion_date" value="<?php echo $row1->completion_date; ?>">
							</td>
							<td>
							<input  type="text" id="grade" name="grade" value="<?php echo $row1->grade; ?>" >
							</td>
							<td>
							<input  type="text" id="percentage" name="percentage" value="<?php echo $row1->percentage; ?>">
							</td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					
					<?php if(count($professional_courses)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr class="bg-green"><th colspan="6" class="text-center">Professional Courses</th></tr>
							<tr>
							<th>Attended/Conducted</th>
							<th>Course</th>
							<th>Start date</th>
							<th>Completion Date</th>
							<th>Location</th>
							</tr>
							<?php 
							$i=1;
							foreach($professional_courses as $row2){ ?>
							<tr>
							<td>
							<input  type="text" id="pattended" name="pattended" value="<?php  echo ucwords($row2->pattended); ?>"> 
							</td>
							<td>
							<input  type="text" id="pcourse" name="pcourse" value="<?php  echo ucwords($row2->pcourse); ?>"> 
							</td>
							<td>
							<input  type="text" id="pstart_date" name="pstart_date" value="<?php  echo $row2->pstart_date; ?>"> 
							</td>
							<td>
							<input  type="text" id="pcompletion_date" name="pcompletion_date" value="<?php  echo $row2->pcompletion_date; ?>">
							</td>
							<td>
							<input  type="text" id="plocation" name="plocation" value="<?php  echo $row2->plocation; ?>">
							</td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					
					<?php if(count($PaperResearch)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr style="background-color: #8cd417 ;color:#ffffff">
							<th colspan="6" class="text-center">Paper Research</th>
							</tr>
							<tr>
							<th>Paper/Research Title</th>
							<th>Published In</th>
							<th>Publish date</th>
							<th>Publish At</th>
							<th>Presentation Date</th>
							</tr>
							<?php 
							$i=1;
							foreach($PaperResearch as $row3){ ?>
							<tr>
							<td>
							<input  type="text" id="prtitle" name="prtitle" value="<?php  echo ucwords($row3->prtitle); ?>">
							</td>
							<td>
							<input  type="text" id="publish_in" name="publish_in" value="<?php echo ucwords($row3->publish_in); ?>">
							</td>
							<td>
							<input  type="text" id="publish_date" name="publish_date" value="<?php  echo $row3->publish_date; ?>">
							</td>
							<td>
							<input  type="text" id="presented_at" name="presented_at" value="<?php  echo $row3->presented_at; ?>">
							</td>
							<td>
							<input  type="text" id="presentation_date" name="presentation_date" value="<?php  echo $row3->presentation_date; ?>">
							</td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					
					<?php if(count($employment_history)>0){?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
						<table class="table table-responsive table-stripped" width="100%">
							<tr style="background-color:#ffc300;color:#ffffff">
							<th colspan="6" class="text-center">Employment History</th>
							</tr>
							<tr>
							<th>Designation</th>
							<th>Period</th>
							<th>Subjects</th>
							<th>Curriculum</th>
							<th>Resposibility</th>
							<th>Leaving Reason</th>
							</tr>
							<?php 
							$i=1;
							foreach($employment_history as $row4){ ?>
							<tr>
							<td><?php echo $i; ?></td>
							<td>
							<input  type="text" id="eh_designation" name="eh_designation" value="<?php  echo $row4->eh_designation; ?>">
							</td>
							<td>
							<input  type="text" id="period" name="period" value="<?php  echo $row4->period; ?>">
							</td>
							<td>
							<input  type="text" id="subjects" name="subjects" value="<?php  echo $row4->subjects; ?>">
							</td>
							<td>
							<input  type="text" id="curriculum" name="curriculum" value="<?php  echo $row4->curriculum; ?>">
							</td>
							<td>
							<input  type="text" id="responsibility" name="responsibility" value="<?php  echo $row4->responsibility; ?>">
							</td>
							<td>
							<input  type="text" id="leaving_reason" name="leaving_reason" value="<?php  echo $row4->leaving_reason; ?>">
							</td>
							</tr>
							<?php $i++; } ?>
						</table>
					</div>
					<?php } ?>
					<div class="clearfix"></div>
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@include('included.footer')