@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','1')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('lisitng',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Social Media List</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content" style="overflow-x:auto;">
                     
					<table class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Name</th>
                          <th>Link</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
                          <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						if(count($SocialMedia) > 0){ 
						foreach($SocialMedia as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $row->social_title; ?></td>
						<td>
						<p <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ echo 'class="link"'; } ?>  id="<?php echo 'link'.$row->social_id; ?>" data-id="<?php echo $row->social_id; ?>"> 
						<?php echo $row->social_link; ?> 
						</p>
						<input type="text" value="<?php echo $row->social_link;?>" name="social_link" data-id="<?php echo $row->social_id; ?>" id="<?php echo 'social_link'.$row->social_id; ?>" class="social_link" style="display:none;width:100%;">
						</td>
						<td>
						<select id="social_status" name="social_status" class="form-group social_status" data-id="<?php echo $row->social_id; ?>">
						  <option value="Active" <?php if($row->social_status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->social_status == "Inactive"){echo "selected";} ?>>Inactive</option>
						</select>
						</td>
						</tr>
						<?php $i++; }}else{?>
						<tr>
						<td colspan="5" class="text-center">No records found..!</td>
						</tr>
						<?php }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
	$(document).ready(function(){
		
		$('.link').click(function(){
			var Sid = $(this).attr('data-id');
			$('#social_link'+Sid).css('display','block');
			$('#link'+Sid).css('display','none');
		});
		
		$('.link').click(function(){
			var Sid = $(this).attr('data-id');
			$('#social_link'+Sid).css('display','block');
			$('#link'+Sid).css('display','none');
		});
		
		$("social_link").bind("blur", function (e) {
			 var keyCode = e.keyCode || e.which;
			 if(keyCode === 13) {
				 e.preventDefault();
					var Sid = $(this).attr('data-id');
					var Value = $(this).val();
					$('#social_link'+Sid).css('display','none');
					$('#link'+Sid).css('display','block');
					$.ajax({
						type: 'get',
						headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
						url: 'social-media/updatesocial',
						data:'Sid='+Sid+'&Value='+Value,
						success: function (data) {
							if(data == 'success'){
								$('#social_link'+Sid).val(Value);
								$('.flash-message1').show();
								$('.status_result').addClass('alert-success');
								$('.status_result').html('Link updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
							}
							if(data == 'failed'){
								$('.flash-message1').show();
								$('.status_result').addClass('alert-danger');
								$('.status_result').html('Failed to update link! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
							}
						},
						error: function (data, status)
						{
								$('.flash-message1').show();
								$('.status_result').addClass('alert-danger');
								$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						}
					});	
				 
			 }
		 });
		
		
		$('.social_link').blur(function(){
			var Sid = $(this).attr('data-id');
			var Value = $(this).val();
			$('#social_link'+Sid).css('display','none');
			$('#link'+Sid).css('display','block');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'social-media/updatesocial',
				data:'Sid='+Sid+'&Value='+Value,
				success: function (data) {
					if(data == 'success'){
						$('#social_link'+Sid).val(Value);
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Link updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update link! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		
		$('.social_status').change(function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'social-media/socialstatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		
	});
	</script>
		
  @include('included.footer')