@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','11')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Events List</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content" style="overflow-x:auto;">
					<?php if(Auth::user()->added_by== 0 || in_array('add',$ctrl_action)){?>
					<a style="margin-right:5px;" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#Add_PA_Modal">
			         Add </a>
				<?php } ?>
                     
					<table class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Title</th>
                          <th>Thumbnail</th>
                          <th>Short Description</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
                          <?php } ?>
						  <?php if(in_array('delete',$RoleArray) || in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						  <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						foreach($Data as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $row->title; ?></td>
						<td>
						<img src="{!! \Config::get('app.url_base') !!}/images/events/<?php echo $row->thumbnail; ?>" style="height:45px;width:120px;">
						</td>
						<td><?php echo $row->short_desc; ?></td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<select id="status" name="status" class="form-group status" data-id="<?php echo $row->id; ?>">
						  <option value="Active" <?php if($row->status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->status == "Inactive"){echo "selected";} ?>>Inactive</option>
						</select>
						</td>
						<?php } ?>
						<?php if(in_array('edit',$RoleArray) ||in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						
						<a href="{{URL::to('events/editevent',array(Crypt::encrypt($row->id)))}}" class="btn btn-xs btn-success EditBtn" >
						<i class="fa fa-edit"></i></a>
						
						<?php } if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<a href="{{URL::to('events/deleteevent',array(Crypt::encrypt($row->id)))}}" class="btn btn-danger btn-xs">
						<i class="fa fa-trash" title="delete"></i></a>
						<?php } ?>
						</td>
						<?php }?>
						</tr>
						<?php $i++; }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
		
	<!-- Add model start -->
	<div id="Add_PA_Modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Add Event</h4>
		  </div>
		  <form class="" action="{{URL::route('addevent')}}" enctype="multipart/form-data" method="post">
		  <div class="modal-body">
			<div class="form-group">
				<div class="form-group">
					<P>News Title</P>
					<input type="text" name="title" id="title"  placeholder="News Title" class="form-control input-sm"  required="required">
					<label style="display:none;" class="titleerr red">Please enter title.</label>
				</div>
				<div class="form-group">
					<P>Short Description</P>
					<input type="text" name="short_desc" id="short_desc" placeholder="Short Description" class="form-control input-sm">
					<label style="display:none;" class="short_descerr red">Please short description.</label>
				</div>
				<div class="form-group">
					<P>Brief Description</P>
					<label style="display:none;" class="brief_descerr red">Please brief description.</label>
					<textarea name="brief_desc" id="brief_desc" placeholder="Brief Description" required="required"></textarea>
				</div>
				<div class="form-group">
					<P>Thumbnail</P>
					<input type="file" name="thumbnail" id="thumbnail"  placeholder="Thumbnail" class="form-control input-sm"  required="required">
					<label style="display:none;" class="thumbnailerr red">Please select .png, .jpg, .gif, .bmp format file.</label>
				</div>
				<div class="form-group">
					<P>Status</P>
					<select id="status" name="status" class="form-input">
					  <option value="Active">Active</option>
					  <option value="Inactive">Inactive</option>
					</select>
					<label style="display:none;" class="statuserr red">Please select status.</label>
				</div>
				
				{{ csrf_field() }}
		  </div>
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-success btn-sm savenews">Save</button>
			<button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>			
	<!-- Add model end -->	  	

	<script>
		$('#thumbnail').on('change',function(){
			var thumbnail = $('#thumbnail').val();
			var ext = thumbnail.split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
					$('.thumbnailerr').show();
					return false;
				}else{
					$('.thumbnailerr').hide();
				}
		});
		
		$('.savenews').on('click',function(){
			var title = $('#title').val();
			var short_desc = $('#short_desc').val();
			var brief_desc = $('#brief_desc').val();
			var thumbnail = $('#thumbnail').val();
			var status = $('#status').val();
			var ext = thumbnail.split('.').pop().toLowerCase();
			if(title ==""){
				$('.titleerr').show();
				return false;
			}else{
				$('.titleerr').hide();
			}
			// if(brief_desc ==""){
				// $('.brief_descerr').show();
				// return false;
			// }else{
				// $('.brief_descerr').hide();
			// }
			if(status ==""){
				$('.statuserr').show();
				return false;
			}else{
				$('.statuserr').hide();
			}
			if(thumbnail !=""){
				if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
					$('.thumbnailerr').show();
					return false;
				}else{
					$('.thumbnailerr').hide();
				}
			}else{
				$('.thumbnailerr').hide();
			}
		});
	</script>
	
	<script>
	$(document).ready(function(){
		CKEDITOR.replace( 'brief_desc' ); 
		
		$('.status').change(function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'events/eventtatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		
	});
	</script>
  @include('included.footer')