<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="_token" id="_token" content="{{ csrf_token() }}" />

    <title>École Mondiale CMS </title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="{!! \Config::get('app.url') !!}/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
	<script src="{!! \Config::get('app.url') !!}/plugins/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="{!! \Config::get('app.url') !!}/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
	
    <!-- Font Awesome -->
    <link href="{!! \Config::get('app.url') !!}/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
	<!-- include summernote css/js-->
	<link href="{!! \Config::get('app.url') !!}/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" />
	<script src="{!! \Config::get('app.url') !!}/plugins/bootstrap-summernote/summernote.js"></script>
	<!-- include image viewbox css/js-->
	<link href="{!! \Config::get('app.url') !!}/plugins/viewbox/viewbox.css" rel="stylesheet" />
	<script src="{!! \Config::get('app.url') !!}/plugins/viewbox/jquery.viewbox.min.js"></script>

    <!-- NProgress -->
    <link href="{!! \Config::get('app.url') !!}/plugins/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="{!! \Config::get('app.url') !!}/plugins/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="{!! \Config::get('app.url') !!}/plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{!! \Config::get('app.url') !!}/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="{!! \Config::get('app.url') !!}/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{!! \Config::get('app.url') !!}/plugins/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="{!! \Config::get('app.url') !!}/plugins/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="{!! \Config::get('app.url') !!}/plugins/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="{!! \Config::get('app.url') !!}/plugins/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{!! \Config::get('app.url') !!}/plugins/build/css/custom.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    
    <!-- ChartJS 1.0.1 -->
	<script src="{!! \Config::get('app.url') !!}/plugins/parsley.min.js"></script>
	<script src="{!! \Config::get('app.url') !!}/plugins/ckeditor/ckeditor.js"></script>  
		
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

	<script src="{!! \Config::get('app.url') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
$(document).ready(function(){
	$('.datepicker').datepicker({
		autoclose: true
	});
});
</script>

<?php date_default_timezone_set('Asia/Kolkata'); ?>
</head>