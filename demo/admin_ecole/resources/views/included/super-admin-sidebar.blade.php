 <?php
 use Illuminate\Support\Facades\Route;
 $currentPath= Route::getFacadeRoot()->current()->uri();
 ?>
 <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view" style="width:100%;">
            <!--<div class="navbar nav_title" style="border: 0;">
              <a href="/home" class="site_title"><i class="fa fa-industry"></i> <span>Company Panel</span></a>
            </div>-->

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                    <img src="{!! \Config::get('app.url') !!}images/user.png" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo Auth::user()->name; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
				
                <li class=" <?php if($currentPath== 'home'){echo 'active';}?>">
                <a href="{!! \Config::get('app.url_base') !!}/home"><i class="fa fa-dashboard"></i>Dashboard</a>
                </li> 
                
                <?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'admissions' || $currentPath== 'admissions/view/{id}'|| $currentPath== 'admissions/edit/{id}'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/admissions"><i class="fa  fa-ticket"></i>Admissions</a>
                </li>  
				<?php } ?>

				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','2')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'index-sliders'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/index-sliders"><i class="fa fa-image"></i>Index Sliders</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','6')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'alumni' || $currentPath== 'alumni-pdf-view/{id}' || $currentPath== 'alumni-view/{id}'|| $currentPath== 'alumni-edit/{id}'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/alumni"><i class="fa  fa-ticket"></i> Alumni</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','17')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'enquiry' || $currentPath== 'enquiry-view/{id}'|| $currentPath== 'enquiry/edit/{id}'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/enquiry"><i class="fa  fa-phone"></i>Enquiries</a>
                </li>  
				<?php } ?>
				
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','4')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'quick-enquiry'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/quick-enquiry"><i class="fa  fa-ticket"></i>Quick Enquiries</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->orWhere('ctrl_id','8')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
				<li><a><i class="fa fa-youtube-play"></i> Youtube <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
					    
					   <?php 
        				$RoleArray = [];
        				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','7')->get();
        				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
        				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
        				?> 
					    <li class="<?php if($currentPath== 'youtube/category'){echo 'active';} ?>">
					    <a href="{!! \Config::get('app.url_base') !!}/youtube/category">Manage Category</a></li>
					    <?php 
        				}
        				$RoleArray = [];
        				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','8')->get();
        				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
        				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
        				?>
				        <li class="<?php if($currentPath== 'youtube/videos'){echo 'active';} ?>">
					    <a href="{!! \Config::get('app.url_base') !!}/youtube/videos">Manage Videos</a></li>
					    <?php } ?>
					</ul>
				</li>
				<?php } ?>
				
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','5')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'pages'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/pages"><i class="fa fa-file-text"></i>Manage Pages</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','19')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'universities'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/universities"><i class="fa fa-file-text"></i>Manage Universities</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','9')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'newsletter'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/newsletter"><i class="fa fa-file-text"></i>News Letter</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','10')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'news'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/news"><i class="fa fa-newspaper-o"></i>News</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','14')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'jobs'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/jobs"><i class="fa fa-briefcase"></i>Job Vacancies</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','15')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'job-applications'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/job-applications"><i class="fa fa-black-tie"></i>Job Applications</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','11')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'event'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/event"><i class="fa fa-calendar"></i>Events</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','12')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'school-calendar'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/school-calendar"><i class="fa fa-calendar"></i>School Calendar</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','13')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'gallery'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/gallery"><i class="fa fa-image"></i>Gallery Images</a>
                </li>  
				<?php } ?>
				
				<!--
                <?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','1')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'social-media'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/social-media"><i class="fa fa-share-alt"></i>Manage Social Media</a>
                </li>  
				<?php } ?>
				-->
				
				<!-------------SNEHAL----------------->
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','21')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
					<li class="<?php if($currentPath== 'contact-us'){echo 'active';} ?>">
					<a href="{!! \Config::get('app.url_base') !!}/contact-us"><i class="fa  fa-ticket"></i>Contact Us</a>
					</li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','3')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
					<li class="<?php if($currentPath== 'footer-sliders'){echo 'active';} ?>">
					<a href="{!! \Config::get('app.url_base') !!}/footer-sliders"><i class="fa fa-image"></i>Footer Sliders</a>
					</li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','18')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
					<li class="<?php if($currentPath== 'testimonials'){echo 'active';} ?>">
					<a href="{!! \Config::get('app.url_base') !!}/testimonials"><i class="fa fa-image"></i>Testimonials</a>
					</li>  
				<?php } ?>
				<!-------------SNEHAL----------------->
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','20')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'users'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/users"><i class="fa fa-users "></i>Manage Users</a>
                </li>  
				<?php } ?>
				
				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','22')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'roles'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/roles"><i class="fa fa-puzzle-piece "></i> Manage Roles</a>
                </li>  
				<?php } ?>

				<?php 
				$RoleArray = [];
				$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','23')->get();
				foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
				if(in_array('listing',$RoleArray) || Auth::user()->added_by== 0){
				?>
                <li class="<?php if($currentPath== 'permissions'){echo 'active';} ?>">
                <a href="{!! \Config::get('app.url_base') !!}/permissions"><i class="fa fa-gears "></i>Manage Permissions</a>
                </li>  
				<?php } ?>
				
                </ul>
              </div>
 

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{!! \Config::get('app.url') !!}images/user.png">
					<?php echo Auth::user()->name; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
					<?php if(Auth::user()->added_by== 0){ ?>
                    <li><a href="{!! \Config::get('app.url_base') !!}/profile"> Profile</a></li>
					<?php } ?>
                    <li><a href="{!! \Config::get('app.url_base') !!}/resetpassword"> Change Password</a></li>
                    <li><a href="{!! \Config::get('app.url_base') !!}/auth/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
