@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','7')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
?>
<style>
iframe{
	height:120px;
	width:250px;
}
</style>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Youtube Video List</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content" style="overflow-x:auto;">
					<?php if(Auth::user()->added_by== 0 || in_array('add',$ctrl_action)){?>
					<a style="margin-right:5px;" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#Add_PA_Modal">
			         Add Video</a>
				<?php } ?>
                     
					<table class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Video</th>
                          <th>Category</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
                          <?php } 
						  if(in_array('delete',$RoleArray) || in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						  <th>Action</th>
						  <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						foreach($Data as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>
						<td>
						<?php echo $row->yurl; ?>
						</td>
						<td><?php echo $row->catname; ?></td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<select id="ystatus" name="ystatus" class="form-group ystatus" data-id="<?php echo $row->yid; ?>">
						  <option value="Active" <?php if($row->ystatus == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->ystatus == "Inactive"){echo "selected";} ?>>Inactive</option>
						</select>
						</td>
						<?php } ?>
						<?php if( in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<a href="{{URL::to('youtube/deletevideos',array(Crypt::encrypt($row->yid)))}}" class="btn btn-danger btn-xs">
						<i class="fa fa-trash" title="delete"></i></a>
						<?php } ?>
						</td>
						<?php }?>
						</tr>
						<?php $i++; }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
		
	<!-- Add model start -->
	<div id="Add_PA_Modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-md">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Add Video</h4>
		  </div>
		  <form class="" action="{{URL::route('addyoutubevideo')}}" enctype="multipart/form-data" method="post">
		  <div class="modal-body">
				<div class="form-group">
				<select name="ycatid" id="ycatid"  placeholder="Category Name" class="form-control input-sm" required="required">
				<option value="">Select Category</option>
				<?php $YCData = DB::table('youtube_category')->orderBy('catname','asc')->get(); 
				foreach($YCData as $YC){?>
				<option value="<?php echo $YC->ycatid ; ?>"><?php echo $YC->catname ; ?></option>
				<?php } ?>
				</select>
				</div>
				<br>
				<div class="form-group">
				<input type="text" name="yurl" id="yurl"  placeholder="Video URL" class="form-control input-sm" required="required">
				</div>
				<br>
				<div class="form-group">
				<select id="ystatus" name="ystatus" class="form-group"  required="required">
				  <option value="Active">Active</option>
				  <option value="Inactive">Inactive</option>
				</select>
				</div>
				{{ csrf_field() }}
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-success btn-sm">Save</button>
			<button type="reset" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>			
	<!-- Add model end -->

	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
	$(document).ready(function(){
		
		$('.ystatus').change(function(){
			var status = $(this).val();
			var cid = $(this).attr('data-id');
			$('.flash-message1').css('display','none');
			$('.status_result').removeClass('alert-danger');
			$('.status_result').removeClass('alert-success');
			$('.status_result').html('');
			$.ajax({
				type: 'get',
				headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
				url: 'yvideostatus',
				data: 'status='+status+'&cid='+ cid,
				success: function (data) {
					if(data == 'success'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-success');
						$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
					if(data == 'failed'){
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				},
				error: function (data, status)
				{
						$('.flash-message1').show();
						$('.status_result').addClass('alert-danger');
						$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
				}
			});	
		});
		
		
	});
	</script>
		
  @include('included.footer')