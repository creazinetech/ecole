 @include('included.header')
 @include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','3')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){


foreach($UserData as $user){} 
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Add Company</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit User</h2>
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
					
                    <div class="col-md-8">
					
                    <form class="form-horizontal form-label-left" novalidate action="{{URL::route('updateuser')}}" enctype="multipart/form-data" method="post">
                        <input id="id" name="id" type="hidden" value="<?php echo $user->id; ?>">

                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input id="name" name="name" class="form-control col-md-7 col-xs-12" placeholder="Enter user name" required="required" type="text" value="<?php echo $user->name; ?>" >
						  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email ID <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="user@email.com" required="required" type="text" value="<?php echo $user->email; ?>" readonly>
						  <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>				  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-password">
                          <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12" required="required">
						  <span class="fa fa-lock form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>					 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Confirm Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 has-confirmpassword">
                          <input type="password" id="confirm_password" name="confirm_password" class="form-control col-md-7 col-xs-12" required="required">
						  <label class="pass_err red" style="display:none;"><i class="fa fa-alert"></i> Confirm password mismatched..!</label>
						  <span class="fa fa-lock form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>					 
					 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_status">Role
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<select id="role_id" name="role_id" class="form-control" required="required">
							<option value="">Role?</option>
							<?php 
							if(count($Data)>0){
							foreach($Data as $RoleData){
							?>
							<option value="{{$RoleData->role_id}}" <?php if($RoleData->role_id ==$user->role_id){ echo 'selected';} ?>>{{$RoleData->role_name}}</option>
							<?php } } ?>
						  </select>
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_status">Status
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label><input type="radio" name="status" required="required" id="status" class="flat" value="Active" <?php if($user->status =="Active"){echo "checked";} ?> > Active</label>
							<label><input type="radio" name="status" required="required" id="status" class="flat" value="Inactive" <?php if($user->status =="Inactive"){echo "checked";} ?>> Inactive</label>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
					  {{ csrf_field() }}
                        <div class="col-md-6 col-md-offset-3">
                          <button id="savedata" type="submit" class="btn btn-success">Submit</button>
                          <a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/users">Cancel</a>
                        </div>
                      </div>
                    </form>
					
					</div>
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php }?>
<script>
$(document).ready(function(){
	$('#savedata').click(function(){
		var confirm_password = $('#confirm_password').val();
		var password = $('#password').val();
		if(password !=confirm_password || confirm_password ==""){
				$('.pass_err').show();
				$('.has-confirmpassword').addClass('has-error');
				return false;
		}else{
				$('.pass_err').hide();
				$('.has-confirmpassword').removeClass('has-error');
		}
	});
});
</script>	
		
 @include('included.footer')