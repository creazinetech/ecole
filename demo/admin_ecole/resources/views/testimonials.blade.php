@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id','2')->where('ctrl_id','2')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }
if(in_array('lisitng',$RoleArray) || Auth::user()->added_by== 0)
{
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
			
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>
			
			<div class="flash-message1" style="display:none;">
			  <p class="alert alert-dismissable status_result"></p>
			</div>
			
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Testimonials</h2>
					<?php if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){ ?>
					<button class="btn btn-primary btn-sm pull-right add_t">Add Testimonials</button>
					<?php } ?>
                    <div class="clearfix"></div>
					
                  </div>
					<div class="x_content" style="overflow-x:auto;">
					
					<?php if(in_array('add',$RoleArray) || Auth::user()->added_by== 0){ ?>
					<form class="form-horizontal form-label-left" novalidate action="{{URL::route('addtestimonials')}}" enctype="multipart/form-data" method="post" id="slider_form" style="display:none;">
					<div class="col-md-8 col-sm-8 t_content">
					
					<div class="item form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                          <input type="text" name='tby' id='tby' placeholder='Testimonial Person Name?'>
						 
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                          <input type="text" name='tdetails' id='tdetails' placeholder='Details?'>
						 
                        </div>
                    </div>
					<div class="item form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
						  <textarea name='t_content' id='t_content' class='ckeditor'></textarea>
						 
                        </div>
                    </div>
					
					
					
					  
					<div class="col-md-12 col-sm-6 col-xs-12" style="margin-top:5px;">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-sm btn-success pull-left">Upload</button>
						<button type="reset" class="btn btn-sm btn-danger pull-right cancel">Cancel</button>
					</div>
					</div>
					</form>
					<?php } ?>
                     
					<table class="table table-striped table-bordered dt-responsive datatable-responsive  datatable-fixed-header nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr No</th>
                          <th>Content</th>
                          <th>By</th>
                          <th>Details</th>
                          <?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Status</th>
						  <?php } ?>
						  <?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
                          <th>Action</th>
                          <?php } ?>
                        </tr>
                      </thead>
					  <tbody>
						<?php 
						$i=1;
						if(count($Testimonials) > 0){ 
						foreach($Testimonials as $row){ 
						?>
						<tr>
						<td><?php echo $i; ?></td>					
						<td><?php echo $row->t_content; ?></td>
						<td><?php echo $row->tby; ?></td>
						<td><?php echo $row->tdetails; ?></td>
						<?php if(in_array('edit',$RoleArray) || Auth::user()->added_by== 0){ ?>
						
						<td>
						<select id="t_status" name="t_status" class="form-group t_status" data-id="<?php echo $row->t_id; ?>">
						  <option value="Active" <?php if($row->t_status == "Active"){echo "selected";} ?>>Active</option>
						  <option value="Inactive" <?php if($row->t_status == "Inactive"){echo "selected";} ?>>Inactive</option>
						</select>
						</td>
						<?php } ?>
						<?php if(in_array('delete',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<td>
						<a href="{{URL::to('testimonials/tdelete',array(Crypt::encrypt($row->t_id)))}}" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm('Are you sure want to delete this?')"><i class="fa fa-trash"></i></a>
						<?php if($row->t_soindex==0){ ?>
						<a  data-id="<?php echo $row->t_id; ?>" class="btn btn-success btn-xs index" title="Show on index" >Show on index</a>
						<?php } ?>
						<?php if($row->onsidebar==0){ ?>
						<a  data-id="<?php echo $row->t_id; ?>" class="btn btn-warning btn-xs sidebar" title="Show on Sidebar" >Show on Sidebar</a>
						<?php } ?>
						</td>
						<?php } ?>
						</tr>
						<?php $i++; }}else{?>
						<tr>
						<td colspan="5" class="text-center">No records found..!</td>
						</tr>
						<?php }?>
					  </tbody>
                    </table>	
					
					</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php } ?>
	<script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
	<script>
	// $('.table').dataTable( {
	  // "pageLength":50
	// });
	</script>
	<script>
		$(document).ready(function(){
			$('.add_t').click(function(){
				$('#slider_form').slideDown();
			});
			$('.cancel').click(function(){
				$('#slider_form').slideUp();
			});
			
			$('.t_status').change(function(){
				var status = $(this).val();
				var cid = $(this).attr('data-id');
				$('.flash-message1').css('display','none');
				$('.status_result').removeClass('alert-danger');
				$('.status_result').removeClass('alert-success');
				$('.status_result').html('');
				$.ajax({
					type: 'get',
					headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
					url: 'testimonials/tstatus',
					data: 'status='+status+'&cid='+ cid,
					success: function (data) {
						if(data == 'success'){
							$('.flash-message1').show();
							$('.status_result').addClass('alert-success');
							$('.status_result').html('Status updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						}
						if(data == 'failed'){
							$('.flash-message1').show();
							$('.status_result').addClass('alert-danger');
							$('.status_result').html('Failed to update status! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						}
					},
					error: function (data, status)
					{
							$('.flash-message1').show();
							$('.status_result').addClass('alert-danger');
							$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				});	
			});		
			
			$('.index').click(function(){
				var tid = $(this).attr('data-id');
				$('.flash-message1').css('display','none');
				$('.status_result').removeClass('alert-danger');
				$('.status_result').removeClass('alert-success');
				$('.status_result').html('');
				$.ajax({
					type: 'get',
					headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
					url: 'testimonials/showindex',
					data: 'tid='+ tid,
					success: function (data) {
						if(data == 'success'){
							location.reload();
							$('.flash-message1').show();
							$('.status_result').addClass('alert-success');
							$('.status_result').html('index updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						}
						if(data == 'failed'){
							$('.flash-message1').show();
							$('.status_result').addClass('alert-danger');
							$('.status_result').html('Failed to update index! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						}
					},
					error: function (data, status)
					{
							$('.flash-message1').show();
							$('.status_result').addClass('alert-danger');
							$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				});	
			});		
			
			$('.sidebar').click(function(){
				var tid = $(this).attr('data-id');
				$('.flash-message1').css('display','none');
				$('.status_result').removeClass('alert-danger');
				$('.status_result').removeClass('alert-success');
				$('.status_result').html('');
				$.ajax({
					type: 'get',
					headers: {'X-CSRFToken': $('meta[name="token"]').attr('content')},
					url: 'testimonials/showsidebar',
					data: 'tid='+ tid,
					success: function (data) {
						if(data == 'success'){
							location.reload();
							$('.flash-message1').show();
							$('.status_result').addClass('alert-success');
							$('.status_result').html('Updated successfully! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						}
						if(data == 'failed'){
							$('.flash-message1').show();
							$('.status_result').addClass('alert-danger');
							$('.status_result').html('Failed to update! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
						}
					},
					error: function (data, status)
					{
							$('.flash-message1').show();
							$('.status_result').addClass('alert-danger');
							$('.status_result').html('Something went wrong, Please try agian later! <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
					}
				});	
			});				
		});
	</script>

  @include('included.footer')