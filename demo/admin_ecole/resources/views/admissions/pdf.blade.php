<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  <?php 
  
  foreach ($Data as $row){ }
  if($type == 1){
  ?>
    <table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5">
		<tr>
		<td><b>Entry Id:</b><br><?php echo ucwords($row->id); ?></td>
		</tr>
		<tr style="background-color:#edf6fe;">
		<td><b>Student's Name:</b><br><?php echo ucwords($row->name); ?></td>
		</tr>
		<tr>
		<td><b>Date Of Birth: (DD-MM-YYYY)</b><br><?php echo date('d-m-Y', strtotime($row->dob)); ?></td>
		</tr>
		<tr style="background-color:#edf6fe;">
		<td><b>Residential Address:</b><br><?php echo ucwords($row->address); ?></td>
		</tr>
		<tr>
		<td><b>Name Of Present School:</b><br><?php echo ucwords($row->nopskul); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Present Grade:</b><br><?php echo ucwords($row->grade); ?></td>
		</tr>
		<tr>
		<td><b>Grade To Join:</b><br><?php echo ucwords($row->gradetjoin); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Academic Year For Which Admission Is Sought:</b><br><?php echo $row->year; ?></td>
		</tr>
		
		<tr>
		<td><b>Name Of The Parents:</b><br><?php echo ucwords($row->nparents); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Designation:</b><br><?php echo ucwords($row->designation); ?></td>
		</tr>
		
		<tr>
		<td><b>Name Of The Organization:</b><br><?php echo ucwords($row->organization); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Occupation of the Working Parent:</b><br><?php echo ucwords($row->occupation); ?></td>
		</tr>
		
		<tr>
		<td><b>Official Address:</b><br><?php echo ucwords($row->offadd); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>City:</b><br><?php echo ucwords($row->city); ?></td>
		</tr>
		<tr>
		<td><b>State:</b><br><?php echo ucwords($row->state); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Pincode:</b><br><?php echo ucwords($row->pincode); ?></td>
		</tr>
		<tr>
		<td><b>Residence Tel:</b><br><?php echo ucwords($row->restel); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Office Tel:</b><br><?php echo ucwords($row->offtel); ?></td>
		</tr>
		<tr>
		<td><b>Mobile Number Father:</b><br><?php echo ucwords($row->mnfather); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Mobile Number Mother:</b><br><?php echo ucwords($row->mnmother); ?></td>
		</tr>
		<tr>
		<td><b>Email Address Father:</b><br><?php echo $row->emailf; ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Email Address Mother:</b><br><?php echo $row->mnmother; ?></td>
		</tr>
		<tr>
		<td><b>Passport Type:</b><br><?php echo ucwords($row->passport); ?></td>
		</tr>
		<tr style="background-color:#edf6fe">
		<td><b>Tell us how you heard about our School:</b><br><?php echo ucwords($row->ourskul); ?></td>
		</tr>
		
	</table>
  <?php 
  }
  if($type==2){
  ?>
  
	<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
		<tr>
		<td><b>Entry Id:</b><br>
		<input type="text" value="<?php echo ucwords($row->id); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Student's Name:</b><br>
		<input type="text" value="<?php echo ucwords($row->name); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Date Of Birth: (DD-MM-YYYY)</b>
		<br>
		<input type="text" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Residential Address:</b><br>
		<input type="text" value="<?php echo ucwords($row->address); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Name Of Present School:</b><br>
		<input type="text" value="<?php echo ucwords($row->nopskul); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Present Grade:</b><br>
		<input type="text" value="<?php echo ucwords($row->grade); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Grade To Join:</b><br>
		<input type="text" value="<?php echo ucwords($row->gradetjoin); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Academic Year For Which Admission Is Sought:</b><br>
		<input type="text" value="<?php echo ucwords($row->year); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		
		<tr>
		<td><b>Name Of The Parents:</b><br>
		<input type="text" value="<?php echo ucwords($row->nparents); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Designation:</b><br>
		<input type="text" value="<?php echo ucwords($row->designation); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		
		<tr>
		<td><b>Name Of The Organization:</b><br>
		<input type="text" value="<?php echo ucwords($row->organization); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Occupation of the Working Parent:</b><br>
		<input type="text" value="<?php echo ucwords($row->occupation); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Official Address:</b><br>
		<input type="text" value="<?php echo ucwords($row->offadd); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>City:</b><br>
		<input type="text" value="<?php echo ucwords($row->city); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>State:</b><br>
		<input type="text" value="<?php echo ucwords($row->state); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Pincode:</b><br>
		<input type="text" value="<?php echo ucwords($row->pincode); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Residence Tel:</b><br>
		<input type="text" value="<?php echo ucwords($row->restel); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Office Tel:</b><br>
		<input type="text" value="<?php echo ucwords($row->offtel); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Mobile Number Father:</b><br>
		<input type="text" value="<?php echo ucwords($row->mnfather); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Mobile Number Mother:</b><br>
		<input type="text" value="<?php echo ucwords($row->mnmother); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Email Address Father:</b><br>
		<input type="text" value="<?php echo $row->emailf; ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Email Address Mother:</b><br>
		<input type="text" value="<?php echo ucwords($row->emailm); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr>
		<td><b>Passport Type:</b><br>
		<input type="text" value="<?php echo $row->passport; ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		<tr >
		<td><b>Tell us how you heard about our School:</b><br>
		<input type="text" value="<?php echo ucwords($row->ourskul); ?>" style="width:100%;height:30px;">
		</td>
		</tr>
		
	</table>
  <?php } if($type==3){?>
  
	<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5" style="font-style:Verdana, sans-serif;">
		<tr>
		<td><b>Entry Id:</b><br>
		<?php echo ucwords($row->id); ?>
		</td>
		</tr>
		<tr>
		<td><b>Student's Name:</b><br>
		<?php echo ucwords($row->name); ?>
		</td>
		</tr>
		<tr>
		<td><b>Date Of Birth: (DD-MM-YYYY)</b>
		<br>
		<?php echo date('d-m-Y', strtotime($row->dob)); ?>
		</td>
		</tr>
		<tr>
		<td><b>Residential Address:</b><br>
		<?php echo ucwords($row->address); ?>
		</td>
		</tr>
		<tr>
		<td><b>Name Of Present School:</b><br>
		<?php echo ucwords($row->nopskul); ?>
		</td>
		</tr>
		<tr >
		<td><b>Present Grade:</b><br>
		<?php echo ucwords($row->grade); ?>
		</td>
		</tr>
		<tr>
		<td><b>Grade To Join:</b><br>
		<?php echo ucwords($row->gradetjoin); ?>
		</td>
		</tr>
		<tr >
		<td><b>Academic Year For Which Admission Is Sought:</b><br>
		<?php echo ucwords($row->year); ?>
		</td>
		</tr>
		
		<tr>
		<td><b>Name Of The Parents:</b><br>
		<?php echo ucwords($row->nparents); ?>
		</td>
		</tr>
		<tr >
		<td><b>Designation:</b><br>
		<?php echo ucwords($row->designation); ?>
		</td>
		</tr>
		
		<tr>
		<td><b>Name Of The Organization:</b><br>
		<?php echo ucwords($row->organization); ?>
		</td>
		</tr>
		<tr >
		<td><b>Occupation of the Working Parent:</b><br>
		<?php echo ucwords($row->occupation); ?>
		</td>
		</tr>
		<tr>
		<td><b>Official Address:</b><br>
		<?php echo ucwords($row->offadd); ?>
		</td>
		</tr>
		<tr >
		<td><b>City:</b><br>
		<?php echo ucwords($row->city); ?>
		</td>
		</tr>
		<tr>
		<td><b>State:</b><br>
		<?php echo ucwords($row->state); ?>
		</td>
		</tr>
		<tr >
		<td><b>Pincode:</b><br>
		<?php echo ucwords($row->pincode); ?>
		</td>
		</tr>
		<tr>
		<td><b>Residence Tel:</b><br>
		<?php echo ucwords($row->restel); ?>
		</td>
		</tr>
		<tr >
		<td><b>Office Tel:</b><br>
		<?php echo ucwords($row->offtel); ?>
		</td>
		</tr>
		<tr>
		<td><b>Mobile Number Father:</b><br>
		<?php echo ucwords($row->mnfather); ?>
		</td>
		</tr>
		<tr >
		<td><b>Mobile Number Mother:</b><br>
		<?php echo ucwords($row->mnmother); ?>
		</td>
		</tr>
		<tr>
		<td><b>Email Address Father:</b><br>
		<?php echo $row->emailf; ?>
		</td>
		</tr>
		<tr >
		<td><b>Email Address Mother:</b><br>
		<?php echo ucwords($row->emailm); ?>
		</td>
		</tr>
		<tr>
		<td><b>Passport Type:</b><br>
		<?php echo $row->passport; ?>
		</td>
		</tr>
		<tr >
		<td><b>Tell us how you heard about our School:</b><br>
		<?php echo ucwords($row->ourskul); ?>
		</td>
		</tr>
	</table>  
  <?php } ?>
  </body>
</html>