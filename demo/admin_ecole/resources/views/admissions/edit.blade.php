@include('included.header')
@include('included.super-admin-sidebar')
<?php 
foreach($Data as $row){} 
?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">

	<div class="clearfix"></div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
			<h2>Edit Admission</h2>
			<a href="{!! \Config::get('app.url_base') !!}/admissions" class="btn btn-danger btn-sm pull-right"><i class="fa  fa-close"></i> Close</a>
			<div class="clearfix"></div>
		  </div>
		  <div class="x_content">
				  
		  <?php 
		  foreach ($Data as $row){ } 
		  ?>
		  <div class="col-md-2 col-sm-2 col-xs-2"></div>
		  
		  <div class="col-md-8 col-sm-8 col-xs-12">
		  <form class="form-horizontal form-label-left" novalidate action="{{URL::route('updateadmissions')}}" enctype="multipart/form-data" method="post">
			<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
			
				<input type="hidden" value="<?php echo ucwords($row->id); ?>" name="id" id="id" class="form-control input-sm">
				<td><b>Student's Name:</b><br>
				<input type="text" value="<?php echo ucwords($row->name); ?>" name="name" id="name" class="form-control">
				<span id="qnameerr" style="color:red;display:none;"> Please Enter name.</span>
				</td>
				</tr>
				<tr>
				<td><b>Date Of Birth: (DD-MM-YYYY)</b>
				<br>
				<input type="text" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" name="dob" id="dob" class="form-control datepicker">
				<span id="qdateerr" style="color:red;display:none;"> Please Enter Date.</span>
				</td>
				</tr>
				<tr>
				<td><b>Residential Address:</b><br>
				<textarea name="address" id="address" class="form-control"><?php echo ucwords($row->address); ?></textarea>
				<span id="qraddresserr" style="color:red;display:none;"> Please Enter Residential Address.</span>
				</td>
				</tr>
				<tr>
				<td><b>Name Of Present School:</b><br>
				<input type="text" value="<?php echo ucwords($row->nopskul); ?>" name="nopskul" id="nopskul" class="form-control">
				<span id="qnskulerr" style="color:red;display:none;"> Please Enter Name of Present School.</span>
				</td>
				</tr>
				<tr >
				<td><b>Present Grade:</b><br>
				<input type="text" value="<?php echo ucwords($row->grade); ?>" name="grade" id="grade" class="form-control">
				<span id="qgradeerr" style="color:red;display:none;"> Please Enter present grade.</span>
				</td>
				</tr>
				<tr>
				<td><b>Grade To Join:</b><br>
				<input type="text" value="<?php echo ucwords($row->gradetjoin); ?>" name="gradetjoin" id="gradetjoin" class="form-control">
				<span id="qjoinerr" style="color:red;display:none;"> Grade to join</span>
				</td>
				</tr>
				<tr >
				<td><b>Academic Year For Which Admission Is Sought:</b><br>
				<select class='form-control input-sm' id='year' name='year'>
					<option value=''>Admission Year?</option>
					<?php 
					$start_year=date('Y')-1;
					$end_year= date('Y')+2;
					$start_year1=$start_year+1;
					$end_year1=$end_year+1;
					for($i=$start_year,$j=$start_year1;$i<=$end_year,$j<=$end_year1;$i++,$j++){ 
					$Year = $i.'-'.$j;
					?>
					<option value='<?php echo $i.'-'.$j; ?>' <?php if($row->year == $Year){echo 'selected';} ?>><?php echo $Year;?></option>
					<?php } ?>
				</select>
				<span id="qayearerr" style="color:red;display:none;"> Please Enter Academic Year.</span>
				</td>
				</tr>
				
				<tr>
				<td><b>Name Of The Parents:</b><br>
				<input type="text" value="<?php echo ucwords($row->nparents); ?>" name="nparents" id="nparents" class="form-control">
				<span id="qparenterr" style="color:red;display:none;"> Please Enter Parent Name.</span>
				</td>
				</tr>
				<tr >
				<td><b>Designation:</b><br>
				<input type="text" value="<?php echo ucwords($row->designation); ?>" name="designation" id="designation" class="form-control">
				<span id="qdesingnationerr" style="color:red;display:none;"> Please Enter Designation.</span>
				</td>
				</tr>
				
				<tr>
				<td><b>Name Of The Organization:</b><br>
				<input type="text" value="<?php echo ucwords($row->organization); ?>" name="organization" id="organization" class="form-control">
				<span id="qorganizationerr" style="color:red;display:none;"> Please Enter Name of the Organization.</span>
				</td>
				</tr>
				<tr >
				<td><b>Occupation of the Working Parent:</b><br>
				<input type="text" value="<?php echo ucwords($row->occupation); ?>" name="occupation" id="occupation" class="form-control">
				<span id="qoccupationerr" style="color:red;display:none;"> Please Enter Name Occupation of the Working Parent.</span>
				</td>
				</tr>
				<tr>
				<td><b>Official Address:</b><br>
				<textarea name="offadd" id="offadd" class="form-control"><?php echo ucwords($row->offadd); ?></textarea>
				<span id="qoffadderr" style="color:red;display:none;"> Please Enter Official Address.</span>
				</td>
				</tr>
				<tr >
				<td><b>City:</b><br>
				<input type="text" value="<?php echo ucwords($row->city); ?>" name="city" id="city" class="form-control">
				<span id="qcityerr" style="color:red;display:none;"> Please Enter City.</span>
				</td>
				</tr>
				<tr>
				<td><b>State:</b><br>
					<select class="form-control" id="state" name="state">
					  <option value=''>Select State</option>
					  <option value='Andhra Pradesh' <?php if($row->state == 'Andhra Pradesh'){ echo 'selected';} ?>>Andhra Pradesh</option>
					  <option value='Arunachal Pradesh' <?php if($row->state == 'Arunachal Pradesh'){ echo 'selected';} ?>>Arunachal Pradesh</option>
					  <option value='Assam' <?php if($row->state == 'Assam'){ echo 'selected';} ?>>Assam </option>
					  <option value='Bihar' <?php if($row->state == 'Bihar'){ echo 'selected';} ?>>Bihar</option>
					  <option value='Chhattisgarh' <?php if($row->state == 'Chhattisgarh'){ echo 'selected';} ?>>Chhattisgarh    </option>
					  <option value='Goa' <?php if($row->state == 'Goa'){ echo 'selected';} ?>>Goa</option>
					  <option value='Gujarat' <?php if($row->state == 'Gujarat'){ echo 'selected';} ?>>Gujarat</option>
					  <option value='Haryana' <?php if($row->state == 'Haryana'){ echo 'selected';} ?>>Haryana </option>
					  <option value='Himachal Pradesh' <?php if($row->state == 'Himachal Pradesh'){ echo 'selected';} ?>>Himachal Pradesh</option>
					  <option value='Jammu & Kashmir' <?php if($row->state == 'Jammu & Kashmir'){ echo 'selected';} ?>>Jammu & Kashmir</option>
					  <option value='Jharkhand' <?php if($row->state == 'Jharkhand'){ echo 'selected';} ?>>Jharkhand </option>
					  <option value='Karnataka' <?php if($row->state == 'Karnataka'){ echo 'selected';} ?>>Karnataka </option>
					  <option value='Kerla' <?php if($row->state == 'Kerla'){ echo 'selected';} ?>>Kerla </option>
					  <option value='Madhya Pradesh' <?php if($row->state == 'Madhya Pradesh'){ echo 'selected';} ?>>Madhya Pradesh</option>
					  <option value='Maharashtra' <?php if($row->state == 'Maharashtra'){ echo 'selected';} ?>>Maharashtra  </option>
					  <option value='Manipur' <?php if($row->state == 'Manipur'){ echo 'selected';} ?>>Manipur </option>
					  <option value='Meghalaya' <?php if($row->state == 'Meghalaya'){ echo 'selected';} ?>>Meghalaya</option>
					  <option value='Mizoram' <?php if($row->state == 'Mizoram'){ echo 'selected';} ?>> Mizoram</option>
					  <option value='Nagaland' <?php if($row->state == 'Nagaland'){ echo 'selected';} ?>>Nagaland</option>
					  <option value='Orissa' <?php if($row->state == 'Orissa'){ echo 'selected';} ?>>Orissa </option>
					  <option value='Punjab' <?php if($row->state == 'Punjab'){ echo 'selected';} ?>>Punjab </option>
					  <option value='Sikkim' <?php if($row->state == 'Sikkim'){ echo 'selected';} ?>>Sikkim </option>
					  <option value='Tamilnadu' <?php if($row->state == 'Tamilnadu'){ echo 'selected';} ?>>Tamilnadu</option>
					  <option value='Tripura' <?php if($row->state == 'Tripura'){ echo 'selected';} ?>>Tripura </option>
					  <option value='Uttar Pradesh' <?php if($row->state == 'Uttar Pradesh'){ echo 'selected';} ?>>Uttar Pradesh </option>
					  <option value='Uttaranchal' <?php if($row->state == 'Uttaranchal'){ echo 'selected';} ?>> Uttaranchal </option>
					  <option value='West Bengal' <?php if($row->state == 'West Bengal'){ echo 'selected';} ?>>West Bengal</option>
					  <option value='Outside India' <?php if($row->state == 'Outside India'){ echo 'selected';} ?>>Outside India</option>
				  </select>
				  <span id="qstaterr" style="color:red;display:none;"> Please Select State.</span>
				</td>
				</tr>
				<tr >
				<td><b>Country:</b><br>
				<input type="text" value="<?php echo ucwords($row->country); ?>" name="country" id="country" class="form-control">
				<span id="qcountryerr" style="color:red;display:none;"> Please Enter Country.</span>
				</td>
				</tr>
				<tr >
				<td><b>Pincode:</b><br>
				<input type="text" value="<?php echo ucwords($row->pincode); ?>" name="pincode" id="pincode" class="form-control">
				<span id="qpincodeerr" style="color:red;display:none;"> Please Enter 6 digit Pincode.</span>
				</td>
				</tr>
				<tr>
				<td><b>Residence Tel:</b><br>
				<input type="text" value="<?php echo ucwords($row->restel); ?>" name="restel" id="restel" class="form-control">
				<span id="qrtelnerr" style="color:red;display:none;"> Please Enter 10 Digit Residence Tel.</span>
				</td>
				</tr>
				<tr >
				<td><b>Office Tel:</b><br>
				<input type="text" value="<?php echo ucwords($row->offtel); ?>" name="offtel" id="offtel" class="form-control">
				<span id="qotelnerr" style="color:red;display:none;"> Please Enter 10 digit Office Tel.</span>
				</td>
				</tr>
				<tr>
				<td><b>Mobile Number Father:</b><br>
				<input type="text" value="<?php echo ucwords($row->mnfather); ?>" name="mnfather" id="mnfather" class="form-control">
				<span id="qmfathererr" style="color:red;display:none;"> Please Enter 10 Digit Mobile Number Father.</span>
				</td>
				</tr>
				<tr >
				<td><b>Mobile Number Mother:</b><br>
				<input type="text" value="<?php echo ucwords($row->mnmother); ?>" name="mnmother" id="mnmother" class="form-control">
				<span id="qmmothererr" style="color:red;display:none;"> Please Enter 10 digit Mobile Number Mother.</span>
				</td>
				</tr>
				<tr>
				<td><b>Email Address Father:</b><br>
				<input type="text" value="<?php echo $row->emailf; ?>" name="emailf" id="emailf" class="form-control">
				<span id="qfemailerr" style="color:red;display:none;"> Please Enter Email Address Father.</span>
				</td>
				</tr>
				<tr >
				<td><b>Email Address Mother:</b><br>
				<input type="text" value="<?php echo $row->emailm; ?>" name="emailm" id="emailm" class="form-control">
				<span id="qmemailerr" style="color:red;display:none;"> Please Enter Email Address Mother.</span>
				</td>
				</tr>
				<tr>
				<td><b>Passport Type:</b><br>
				<select class="form-control" id="passport" name="passport">
					<option value="Indian Passport" <?php if($row->state == 'Indian Passport'){ echo 'selected';} ?>>Indian Passport</option>
					<option value="Non-Indian Passport" <?php if($row->state == 'Non-Indian Passport'){ echo 'selected';} ?>>Non-Indian Passport</option>
				</select>
				<span id="qpassporterr" style="color:red;display:none;"> Please Enter Passport Type.</span>
				</td>
				</tr>
				<tr >
				<td><b>Tell us how you heard about our School:</b><br>
				<textarea name="ourskul" id="ourskul" class="form-control"><?php echo ucwords($row->ourskul); ?></textarea>
				<span id="qschoolerr" style="color:red;display:none;"> Tell us how you heard about our School.</span>
				</td>
				</tr>
				
			</table>
				<div class="ln_solid"></div>
				<div class="form-group">
					{{ csrf_field() }}
					<div class="col-md-6 col-md-offset-3">
					<button id="admission-btn" type="submit" class="btn btn-success">Submit</button>
					<a class="btn btn-primary" href="{!! \Config::get('app.url_base') !!}/admissions">Cancel</a>
					</div>
				</div>
			</form>
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-2"></div>
  
			</div>
		  </div>
		</div>
	  </div>
	</div>
	</div>
	
<script type="text/javascript">

$(document).ready(function(){
	$('#admission-btn').click(function(){
	var name = $('#name').val();
	var dob = $('#dob').val();
	var address = $('#address').val();
	var nopskul = $('#nopskul').val();
	var grade = $('#grade').val();
	var gradetjoin = $('#gradetjoin').val();
	var year = $('#year').val();
	var nparents = $('#nparents').val();
	var designation = $('#designation').val();
	var organization = $('#organization').val();
	var occupation = $('#occupation').val();
	var offadd = $('#offadd').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	var restel = $('#restel').val();
	var offtel = $('#offtel').val();
	var mnfather = $('#mnfather').val();
	var mnmother = $('#mnmother').val();
	var emailf = $('#emailf').val();
	var emailm = $('#emailm').val();
	var passport = $('#passport').val();
	var ourskul = $('#ourskul').val();
	var captcha_code2 = $('#captcha_code2').val();
	var rannumber = $('#rannumber').val();
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

	    if(name==""){
			$('#qnameerr').css('display','block'); 
			$('#name').focus(); 
			return false;
		}else{
			$('#qnameerr').css('display','none'); 
		}
		if(dob==""){
			$('#qdateerr').css('display','block'); 
			$('#dob').focus(); 
			return false;
		}else{
			$('#qdateerr').css('display','none'); 
		}
		if(address==""){
			$('#qraddresserr').css('display','block'); 
			$('#address').focus(); 
			return false;
		}else{
			$('#qraddresserr').css('display','none'); 
		}
		if(nopskul==""){
			$('#qnskulerr').css('display','block'); 
			$('#nopskul').focus(); 
			return false;
		}else{
			$('#qnskulerr').css('display','none'); 
		}
		if(grade==""){
			$('#qgradeerr').css('display','block'); 
			$('#grade').focus(); 
			return false;
		}else{
			$('#qgradeerr').css('display','none'); 
		}
		if(gradetjoin==""){
			$('#qjoinerr').css('display','block'); 
			$('#gradetjoin').focus(); 
			return false;
		}else{
			$('#qjoinerr').css('display','none'); 
		}
		if(year==""){
			$('#qayearerr').css('display','block'); 
			$('#year').focus(); 
			return false;
		}else{
			$('#qayearerr').css('display','none'); 
		}
		if(nparents==""){
			$('#qparenterr').css('display','block'); 
			$('#nparents').focus(); 
			return false;
		}else{
			$('#qparenterr').css('display','none'); 
		}
		if(designation==""){
			$('#qdesingnationerr').css('display','block'); 
			$('#designation').focus(); 
			return false;
		}else{
			$('#qdesingnationerr').css('display','none'); 
		}
		if(organization==""){
			$('#qorganizationerr').css('display','block'); 
			$('#organization').focus(); 
			return false;
		}else{
			$('#qorganizationerr').css('display','none'); 
		}
		if(occupation==""){
			$('#qoccupationerr').css('display','block'); 
			$('#occupation').focus(); 
			return false;
		}else{
			$('#qoccupationerr').css('display','none'); 
		}
		if(offadd==""){
			$('#qoffadderr').css('display','block'); 
			$('#offadd').focus(); 
			return false;
		}else{
			$('#qoffadderr').css('display','none'); 
		}
		if(city==""){
			$('#qcityerr').css('display','block'); 
			$('#city').focus(); 
			return false;
		}else{
			$('#qcityerr').css('display','none'); 
		}
		if(state==""){
			$('#qstaterr').css('display','block'); 
			$('#state').focus(); 
			return false;
		}else{
			$('#qstaterr').css('display','none'); 
		}
		if(country==""){
			$('#qcountryerr').css('display','block'); 
			$('#country').focus(); 
			return false;
		}else{
			$('#qcountryerr').css('display','none'); 
		}
		if(pincode=="" || pincode.length<6){
			$('#qpincodeerr').css('display','block'); 
			$('#pincode').focus(); 
			return false;
		}else{
			$('#qpincodeerr').css('display','none'); 
		}
		if(restel=="" || restel.length<10 || restel.length>15){
			$('#qrtelnerr').css('display','block'); 
			$('#restel').focus(); 
			return false;
		}else{
			$('#qrtelnerr').css('display','none'); 
		}
		if(offtel=="" || offtel.length<10 || offtel.length>15){
			$('#qotelnerr').css('display','block'); 
			$('#offtel').focus(); 
			return false;
		}else{
			$('#qotelnerr').css('display','none'); 
		}
		if(mnfather=="" || mnfather.length<10 || mnfather.length>15){
			$('#qmfathererr').css('display','block'); 
			$('#mnfather').focus(); 
			return false;
		}else{
			$('#qmfathererr').css('display','none'); 
		}
		if(mnmother=="" || mnmother.length<10 || mnmother.length>15){
			$('#qmmothererr').css('display','block'); 
			$('#mnmother').focus(); 
			return false;
		}else{
			$('#qmmothererr').css('display','none'); 
		}
		
		if(emailf=="" || !email_regex.test(emailf)){
			$('#qfemailerr').css('display','block'); 
			$('#emailf').focus(); 
			return false;
		}else{
			$('#qfemailerr').css('display','none'); 
		}
		if(emailm=="" || !email_regex.test(emailm)){
			$('#qmemailerr').css('display','block'); 
			$('#emailm').focus(); 
			return false;
		}else{
			$('#qmemailerr').css('display','none'); 
		}
		
		if(passport==""){
			$('#qpassporterr').css('display','block'); 
			$('#passport').focus(); 
			return false;
		}else{
			$('#qpassporterr').css('display','none'); 
		}
		if(ourskul==""){
			$('#qschoolerr').css('display','block'); 
			$('#ourskul').focus(); 
			return false;
		}else{
			$('#qschoolerr').css('display','none'); 
		}

	});	
});	
</script> 


<script>
$('.datepicker').datepicker({
	autoclose: true
});
</script>
	<!-- /page content -->
@include('included.footer')