@include('included.header')
@include('included.super-admin-sidebar')
<?php 
$RoleArray = [];
$roles = DB::table('role_ctrl')->select('action')->where('role_id',Auth::user()->role_id)->where('ctrl_id','16')->get();
foreach($roles as $Roles){ array_push($RoleArray,$Roles->action); }

foreach($Data as $row){} 
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           <!-- <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
            </div>-->

            <div class="clearfix"></div>

			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))

			  <p class="alert alert-dismissable alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
			</div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Admission Details</h2>
					<a href="{!! \Config::get('app.url_base') !!}/admissions" class="btn btn-danger btn-sm pull-right"><i class="fa  fa-close"></i> Close</a>
                    <div class="clearfix"></div>
                  </div>
				  <div class="x_content">
					
					<div class="col-md-12 col-sm-12 col-xs-12">
					  
						<div class="col-md-2 col-sm-2 col-xs-2"></div>
						
						<section id="EcolePrint" style="padding:0px;display:block;" >
						<div class="col-md-8 col-sm-8 col-xs-8" style="border:1px solid #ececec;color:black;">
						<br>
							<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5">
								<tr>
								<td><b>Entry Id:</b><br><?php echo ucwords($row->id); ?></td>
								</tr>
								<tr style="background-color:#edf6fe;">
								<td><b>Student's Name:</b><br><?php echo ucwords($row->name); ?></td>
								</tr>
								<tr>
								<td><b>Date Of Birth: (DD-MM-YYYY)</b><br><?php echo date('d-m-Y', strtotime($row->dob)); ?></td>
								</tr>
								<tr style="background-color:#edf6fe;">
								<td><b>Residential Address:</b><br><?php echo ucwords($row->address); ?></td>
								</tr>
								<tr>
								<td><b>Name Of Present School:</b><br><?php echo ucwords($row->nopskul); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Present Grade:</b><br><?php echo ucwords($row->grade); ?></td>
								</tr>
								<tr>
								<td><b>Grade To Join:</b><br><?php echo ucwords($row->gradetjoin); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Academic Year For Which Admission Is Sought:</b><br><?php echo $row->year; ?></td>
								</tr>
								
								<tr>
								<td><b>Name Of The Parents:</b><br><?php echo ucwords($row->nparents); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Designation:</b><br><?php echo ucwords($row->designation); ?></td>
								</tr>
								
								<tr>
								<td><b>Name Of The Organization:</b><br><?php echo ucwords($row->organization); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Occupation of the Working Parent:</b><br><?php echo ucwords($row->occupation); ?></td>
								</tr>
								
								<tr>
								<td><b>Official Address:</b><br><?php echo ucwords($row->offadd); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>City:</b><br><?php echo ucwords($row->city); ?></td>
								</tr>
								<tr>
								<td><b>State:</b><br><?php echo ucwords($row->state); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Pincode:</b><br><?php echo ucwords($row->pincode); ?></td>
								</tr>
								<tr>
								<td><b>Residence Tel:</b><br><?php echo ucwords($row->restel); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Office Tel:</b><br><?php echo ucwords($row->offtel); ?></td>
								</tr>
								<tr>
								<td><b>Mobile Number Father:</b><br><?php echo ucwords($row->mnfather); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Mobile Number Mother:</b><br><?php echo ucwords($row->mnmother); ?></td>
								</tr>
								<tr>
								<td><b>Email Address Father:</b><br><?php echo $row->emailf; ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Email Address Mother:</b><br><?php echo $row->mnmother; ?></td>
								</tr>
								<tr>
								<td><b>Passport Type:</b><br><?php echo ucwords($row->passport); ?></td>
								</tr>
								<tr style="background-color:#edf6fe">
								<td><b>Tell us how you heard about our School:</b><br><?php echo ucwords($row->ourskul); ?></td>
								</tr>
								
							</table>
						</div>
						</section>
						
						<section id="NewPrint" style="padding:0px;display:none;" >
						<div class="col-md-8 col-sm-8 col-xs-8" style="border:1px solid #ececec;color:black;">
						<br>
							<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="2" cellpadding="2" style="font-style:Verdana, sans-serif;">
								<tr>
								<td><b>Entry Id:</b><br>
								<input type="text" value="<?php echo ucwords($row->id); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Student's Name:</b><br>
								<input type="text" value="<?php echo ucwords($row->name); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Date Of Birth: (DD-MM-YYYY)</b>
								<br>
								<input type="text" value="<?php echo date('d-m-Y', strtotime($row->dob)); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Residential Address:</b><br>
								<input type="text" value="<?php echo ucwords($row->address); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Name Of Present School:</b><br>
								<input type="text" value="<?php echo ucwords($row->nopskul); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Present Grade:</b><br>
								<input type="text" value="<?php echo ucwords($row->grade); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Grade To Join:</b><br>
								<input type="text" value="<?php echo ucwords($row->gradetjoin); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Academic Year For Which Admission Is Sought:</b><br>
								<input type="text" value="<?php echo ucwords($row->year); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								
								<tr>
								<td><b>Name Of The Parents:</b><br>
								<input type="text" value="<?php echo ucwords($row->nparents); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Designation:</b><br>
								<input type="text" value="<?php echo ucwords($row->designation); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								
								<tr>
								<td><b>Name Of The Organization:</b><br>
								<input type="text" value="<?php echo ucwords($row->organization); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Occupation of the Working Parent:</b><br>
								<input type="text" value="<?php echo ucwords($row->occupation); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Official Address:</b><br>
								<input type="text" value="<?php echo ucwords($row->offadd); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>City:</b><br>
								<input type="text" value="<?php echo ucwords($row->city); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>State:</b><br>
								<input type="text" value="<?php echo ucwords($row->state); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Pincode:</b><br>
								<input type="text" value="<?php echo ucwords($row->pincode); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Residence Tel:</b><br>
								<input type="text" value="<?php echo ucwords($row->restel); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Office Tel:</b><br>
								<input type="text" value="<?php echo ucwords($row->offtel); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Mobile Number Father:</b><br>
								<input type="text" value="<?php echo ucwords($row->mnfather); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Mobile Number Mother:</b><br>
								<input type="text" value="<?php echo ucwords($row->mnmother); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Email Address Father:</b><br>
								<input type="text" value="<?php echo $row->emailf; ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Email Address Mother:</b><br>
								<input type="text" value="<?php echo ucwords($row->emailm); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr>
								<td><b>Passport Type:</b><br>
								<input type="text" value="<?php echo $row->passport; ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								<tr >
								<td><b>Tell us how you heard about our School:</b><br>
								<input type="text" value="<?php echo ucwords($row->ourskul); ?>" style="width:100%;height:30px;">
								</td>
								</tr>
								
							</table>
						</div>
						</section>
						
						<section id="StudPrint" style="padding:0px;display:none;" >
						<div class="col-md-8 col-sm-8 col-xs-8" style="border:1px solid #ececec;color:black;">
						<br>
							<table class="table table-bordered tabled-stripped table-hover" width="100%" cellspacing="4" cellpadding="5" style="font-style:Verdana, sans-serif;">
								<tr>
								<td><b>Entry Id:</b><br>
								<?php echo ucwords($row->id); ?>
								</td>
								</tr>
								<tr>
								<td><b>Student's Name:</b><br>
								<?php echo ucwords($row->name); ?>
								</td>
								</tr>
								<tr>
								<td><b>Date Of Birth: (DD-MM-YYYY)</b>
								<br>
								<?php echo date('d-m-Y', strtotime($row->dob)); ?>
								</td>
								</tr>
								<tr>
								<td><b>Residential Address:</b><br>
								<?php echo ucwords($row->address); ?>
								</td>
								</tr>
								<tr>
								<td><b>Name Of Present School:</b><br>
								<?php echo ucwords($row->nopskul); ?>
								</td>
								</tr>
								<tr >
								<td><b>Present Grade:</b><br>
								<?php echo ucwords($row->grade); ?>
								</td>
								</tr>
								<tr>
								<td><b>Grade To Join:</b><br>
								<?php echo ucwords($row->gradetjoin); ?>
								</td>
								</tr>
								<tr >
								<td><b>Academic Year For Which Admission Is Sought:</b><br>
								<?php echo ucwords($row->year); ?>
								</td>
								</tr>
								
								<tr>
								<td><b>Name Of The Parents:</b><br>
								<?php echo ucwords($row->nparents); ?>
								</td>
								</tr>
								<tr >
								<td><b>Designation:</b><br>
								<?php echo ucwords($row->designation); ?>
								</td>
								</tr>
								
								<tr>
								<td><b>Name Of The Organization:</b><br>
								<?php echo ucwords($row->organization); ?>
								</td>
								</tr>
								<tr >
								<td><b>Occupation of the Working Parent:</b><br>
								<?php echo ucwords($row->occupation); ?>
								</td>
								</tr>
								<tr>
								<td><b>Official Address:</b><br>
								<?php echo ucwords($row->offadd); ?>
								</td>
								</tr>
								<tr >
								<td><b>City:</b><br>
								<?php echo ucwords($row->city); ?>
								</td>
								</tr>
								<tr>
								<td><b>State:</b><br>
								<?php echo ucwords($row->state); ?>
								</td>
								</tr>
								<tr >
								<td><b>Pincode:</b><br>
								<?php echo ucwords($row->pincode); ?>
								</td>
								</tr>
								<tr>
								<td><b>Residence Tel:</b><br>
								<?php echo ucwords($row->restel); ?>
								</td>
								</tr>
								<tr >
								<td><b>Office Tel:</b><br>
								<?php echo ucwords($row->offtel); ?>
								</td>
								</tr>
								<tr>
								<td><b>Mobile Number Father:</b><br>
								<?php echo ucwords($row->mnfather); ?>
								</td>
								</tr>
								<tr >
								<td><b>Mobile Number Mother:</b><br>
								<?php echo ucwords($row->mnmother); ?>
								</td>
								</tr>
								<tr>
								<td><b>Email Address Father:</b><br>
								<?php echo $row->emailf; ?>
								</td>
								</tr>
								<tr >
								<td><b>Email Address Mother:</b><br>
								<?php echo ucwords($row->emailm); ?>
								</td>
								</tr>
								<tr>
								<td><b>Passport Type:</b><br>
								<?php echo $row->passport; ?>
								</td>
								</tr>
								<tr >
								<td><b>Tell us how you heard about our School:</b><br>
								<?php echo ucwords($row->ourskul); ?>
								</td>
								</tr>
								
							</table>
						</div>
						</section>
						
						<div class="col-md-2 col-sm-2 col-xs-2">
						<?php if(in_array('print',$RoleArray) || Auth::user()->added_by== 0){ ?>
						<p>
						<?php $type=1; ?>
						<a href="{{URL::to('admissions/viewPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs btn-primary" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Ecole Mondiale</a>
						</p>
						<p>
						<?php $type=2; ?>
						<a href="{{URL::to('admissions/viewPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs btn-primary" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> New</a>
						</p>
						<p>
						<?php $type=3; ?>
						<a href="{{URL::to('admissions/viewPDF',array(Crypt::encrypt($row->id),$type))}}" class="btn btn-xs btn-primary" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> <?php echo ucwords($row->name); ?></a>
						</p>
						<?php  } ?>
						</div>
					  
					  
					</div>
					  
                  </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <!-- /page content -->
<script type="text/javascript">
function PrintDoc() {
  var toPrint = document.getElementById('EcolePrint');
  var popupWin = window.open('', '_blank', 'width=800 ,height=500');
  var CSSLINK = '<link rel="stylesheet"media="print" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
  popupWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head><title>Print</title><style>@page { size: auto;  margin: 0mm;}@media print { body{-webkit-print-color-adjust:exact;font-style:"Work Sans", Arial, Helvetica, sans-serif;} }</style></head><body style="padding:20px;" onload="window.print()">'+toPrint.innerHTML+'</body></html>')
  popupWin.document.close();
  //popupWin.print();
}

function PrintDoc1() {
  var toPrint = document.getElementById('NewPrint');
  var popupWin = window.open('', '_blank', 'width=800 ,height=500');
  var CSSLINK = '<link rel="stylesheet"media="print" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
  popupWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head><title>Print</title><style>@page { size: auto;  margin:5mm;}@media print { body{-webkit-print-color-adjust:exact;font-style:Verdana, sans-serif;} }</style></head><body style="padding:20px;" onload="window.print()">'+toPrint.innerHTML+'</body></html>')
  popupWin.document.close();
  //popupWin.print();
}
function PrintDoc2() {
  var toPrint = document.getElementById('StudPrint');
  var popupWin = window.open('', '_blank', 'width=800 ,height=500');
  var CSSLINK = '<link rel="stylesheet"media="print" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">';
  popupWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head><title>Print</title><style>@page { size: auto;  margin:5mm;}@media print { body{-webkit-print-color-adjust:exact;font-style:Verdana, sans-serif;} }</style></head><body style="padding:20px;" onload="window.print()">'+toPrint.innerHTML+'</body></html>')
  popupWin.document.close();
  //popupWin.print();
}
</script>

@include('included.footer')